
CREATE DATABASE upkick
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;


drop table if exists twlogs;
create table twlogs (
	logtm timestamp,  -- timestamp
	msg varchar,      -- 
	usernm varchar,   --
	severity varchar, -- {I, M, E}
	roomnm varchar,   --
	msgsrc varchar    -- {server, client}
);

drop table if exists users;
commit;
create table users (userid integer, usernm varchar);
commit;

drop table if exists exercises;
commit;
create table exercises (exerciseid integer, exercisenm varchar, exercisegrp varchar);
commit;

drop table if exists workouts;
DROP SEQUENCE if exists workouts_nextid;
commit;

create table workouts (workoutid integer, workoutnm varchar, userid integer);
CREATE SEQUENCE workouts_nextid START 2;
commit;

-- drop table workoutexercises;
-- commit;
create table workoutexercises (workoutid integer, exerciseid integer);
commit;

insert into users (userid, usernm) values (1, 'Ryan');
insert into users (userid, usernm) values (2, 'Caitlin');
insert into users (userid, usernm) values (3, 'Amanda');
commit;

insert into exercises (exerciseid, exercisenm, exercisegrp) values (1, 'Push Ups', 'Arms');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (2, 'Crunches', 'Abs');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (3, 'Burpees', 'Body');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (4, 'Push-Up Burpees', 'Arms Body');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (5, 'Jack Squats', 'Legs');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (6, 'Star Jumps', 'Body');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (7, 'Bicycle Abs', 'Abs');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (8, 'Lizard Hops', 'Core Body');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (9, 'Push Ups & Rotation', 'Arms');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (10, 'Plank Punches', 'Core Body');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (11, 'Bear Crawls', 'Body');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (12, 'Squats', 'Legs');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (13, 'Sumo Squats', 'Legs');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (14, 'Butt Kickers', 'Legs');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (15, 'Jumping Lunges', 'Legs');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (16, 'Lunges', 'Legs');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (17, 'Thigh Slaps', 'Legs');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (18, 'Fast Feet', 'Legs');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (19, 'Jump Squats', 'Legs');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (20, 'Reverse Lunges', 'Legs');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (21, 'Clapping Push Ups', 'Arms');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (22, 'Supine Push Ups', 'Back');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (23, 'Push Up Jacks', 'Arms Body');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (24, 'Tricep Dips', 'Arms');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (25, 'Russian Twists', 'Abs Core');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (26, 'Wall Jumps', 'Legs Jumping');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (27, 'Side Plank (Left)', 'Abs');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (28, 'Side Plank (Right)', 'Abs');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (29, 'Double Jump Burpees', 'Legs Jumping');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (30, 'Rocket Squats', 'Legs Jumping');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (31, 'Raised Leg Crunches', 'Abs');
insert into exercises (exerciseid, exercisenm, exercisegrp) values (32, 'Soldier Leg Lifts', 'Abs Legs');

commit;

-- 05/28/18
alter table workoutexercises add column ord integer;
commit;

alter table workoutexercises add column activedur integer;
alter table workoutexercises add column restdur integer;
commit;


alter table users add column salt text;
alter table users add column password text;

CREATE SEQUENCE users_userid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE users_userid_seq OWNED BY users.userid;
ALTER TABLE ONLY users ALTER COLUMN userid SET DEFAULT nextval('users_userid_seq'::regclass);
ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (userid);

commit;

drop table if exists betas;
create table betas (email varchar);

commit;

-- CREATE SEQUENCE users_userid_seq
--     START WITH 1
--     INCREMENT BY 1
--     NO MINVALUE
--     NO MAXVALUE
--     CACHE 1;
