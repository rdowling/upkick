"use strict";

require("dotenv").load();
const https = require('https');

let knackApiKey = process.env.KNACK_GHF_LIVE_API_KEY;
let knackAppId = process.env.KNACK_GHF_LIVE_APP_ID;


// view API ex: https://api.knack.com/v1/pages/scene_key/views/view_key/records

let clientsPageNm = "kn-scene_750";
let clientsViewNm = "view_1483";
let clientIdFieldNm = "field_1872";

let iPageNum = 1;

let knackHeaders = {
  "X-Knack-Application-Id": knackAppId,
  "X-Knack-REST-API-Key": knackApiKey
}

const knackGetOptions = {
  method: 'GET',
  headers: knackHeaders
}

let nPages = 1;

for(let iPage = 1; iPage <= nPages; iPage++) {

  let knackGetViewURL = `https://api.knack.com/v1/pages/${clientsPageNm}/views/${clientsViewNm}/records?page=${iPage}&rows_per_page=1000`;

  const req = https.request(knackGetViewURL, knackGetOptions, (res) => {
    console.log(`statusCode: ${res.statusCode}`);
    res.setEncoding('utf8');

    let sData = '';

    res.on('data', sMoreData => {
      sData += sMoreData;
      //console.log("incoming GET data", sMoreData);
    })

    res.on('end', () => {
      let oData = JSON.parse(sData);

      let nTotalRecords = oData.total_records;

      let aRecords = oData.records;
      let nRecords = aRecords.length;

      for(let iRecord = 0; iRecord <= (nRecords-1); iRecord++) {
        let oRecord = aRecords[iRecord];

        let sRecordId = oRecord.id;
        let sUserNm = oRecord.field_68;
        // PUT ex: PUT https://api.knack.com/v1/objects/object_xx/records/record_ID

        let sCurrClientId = oRecord[clientIdFieldNm];
        
        if (sCurrClientId && sCurrClientId !== "") {
          continue;
        }

        console.log("Updating", sRecordId, sUserNm);

        let oKnackUpdateRec = {}
        oKnackUpdateRec[clientIdFieldNm] = sRecordId;

        let sKnackUpdateRec = JSON.stringify(oKnackUpdateRec);
        
        let sClientObjectNm = "object_7";
        
        issuePutRequest(knackAppId, knackApiKey, sClientObjectNm, sRecordId, sKnackUpdateRec);
      }


    })
  });

  req.on('error', error => {
    console.error(error)
  })

  req.end();

}

function issuePutRequest(sKnackAppId, sKnackApiKey, sClientObjNm, sRecordId, sKnackUpdateRec) {

    console.log(sKnackUpdateRec);

    let knackPutRecordURL = `https://api.knack.com/v1/objects/${sClientObjNm}/records/${sRecordId}`;

    console.log("PUT url: "+knackPutRecordURL);

    let knackPutOptions = {
      "method": "PUT"
      ,"headers": {
        "X-Knack-Application-Id": sKnackAppId
        ,"X-Knack-REST-API-Key": sKnackApiKey
        ,"Content-Type": "application/json"
      }
    }

    const putReq = https.request(knackPutRecordURL, knackPutOptions, (putResp) => {
      console.log(`PUT response statusCode: ${putResp.statusCode}`);

      putResp.setEncoding('utf8');

      putResp.on('data', (sPutData) => {
        console.log("PUT response:\n"+sPutData);
      })

    });

    putReq.on('error', putErr => {
      console.error(`Error PUTting ${knackUpdateRec}`)
    });

    putReq.write(sKnackUpdateRec);
    putReq.end();

}