"use strict";

require("dotenv").load();
const https = require('https');

  

let knackApiKey = process.env.KNACK_GHF_TEST_API_KEY;
let knackAppId = process.env.KNACK_GHF_TEST_APP_ID;

let knackPostURL = "https://api.knack.com/v1/objects/object_5/records/";

let knackHeaders = {
   "X-Knack-Application-Id": knackAppId, 
   "X-Knack-REST-API-Key": knackApiKey
}

// let knackData = {                   // multiple choice: "SINGLE_VALUE" || ["VALUE_ONE", "VALUE_TWO"]

//    //"field_78": "Session Date",   // {Session Date} data format is mm/dd/yyyy:HH:MM:am
//    "field_88":          // data format is first last (this is a connection field to Clients object…it’s field 88 in training sessions, but field 68 in Client obj.
//    //,
//    "field_1523": "Virtual"           // { Session Type, “Virtual” }, // multiple choice field
//   //  "field_1523": "Virtual"           // { Session Type, “Virtual” }, // multiple choice field
//   //  field_1523: "'Virtual'"           // { Session Type, “Virtual” }, // multiple choice field
//   //  "field_1523": "'Virtual'"           // { Session Type, “Virtual” }, // multiple choice field
//    //,"field_89":                  // {Trainer}, data format is first last, connection field.  The field # in Trainer obj is 4

// }

let knackData = {
    
    'field_78': 'Virtual',
    'field_89': '5d2a2a1af2c7e6001533430f'
};

const knackJsonData = JSON.stringify(knackData);

const knackPostOptions = {
  method: 'POST',
  headers: knackHeaders
}

const req = https.request(knackPostURL, knackPostOptions, res => {
  console.log(`statusCode: ${res.statusCode}`)

  res.on('data', d => {
    process.stdout.write(d)
  })
})

req.on('error', error => {
  console.error(error)
})

//req.write(knackData);

req.write(knackData);
req.end();
