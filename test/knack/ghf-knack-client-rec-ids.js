"use strict";

require("dotenv").load();
const https = require('https');

let knackApiKey = process.env.KNACK_GHF_LIVE_API_KEY;
let knackAppId = process.env.KNACK_GHF_LIVE_APP_ID;


// view API ex: https://api.knack.com/v1/pages/scene_key/views/view_key/records

let clientsPageNm = "kn-scene_750";
let clientsViewNm = "view_1484";
let clientIdFieldNm = "field_1871";

let iPageNum = 1;

let knackHeaders = {
  "X-Knack-Application-Id": knackAppId,
  "X-Knack-REST-API-Key": knackApiKey
}

const knackGetOptions = {
  method: 'GET',
  headers: knackHeaders
}

let nPages = 10;

for(let iPage = 1; iPage <= nPages; iPage++) {

  console.log('working on page '+iPage);

  let knackGetViewURL = `https://api.knack.com/v1/pages/${clientsPageNm}/views/${clientsViewNm}/records?page=${iPage}&rows_per_page=1000`;

  const req = https.request(knackGetViewURL, knackGetOptions, (res) => {
    console.log(`statusCode: ${res.statusCode}`);
    res.setEncoding('utf8');

    let sData = '';

    res.on('data', sMoreData => {
      sData += sMoreData;
      //console.log("incoming GET data", sMoreData);
    })

    res.on('end', () => {
      let oData = JSON.parse(sData);

      let aRecords = oData.records;
      let nRecords = aRecords.length;

      for(let iRecord = 0; iRecord <= (nRecords-1); iRecord++) {
        let oRecord = aRecords[iRecord];

        let sCurrClientRecId = (oRecord[clientIdFieldNm]||"").trim();
        
        if (sCurrClientRecId && sCurrClientRecId !== "") {
          continue;
        }

        let sRecordId = oRecord.id;
        let sUserNm = oRecord.field_68;
        // PUT ex: PUT https://api.knack.com/v1/objects/object_xx/records/record_ID
        console.log(`Updating Client ${sUserNm} with Client Rec Id ${sRecordId}`);

        let oKnackUpdateRec = {}
        oKnackUpdateRec[clientIdFieldNm] = sRecordId;

        let sKnackUpdateRec = JSON.stringify(oKnackUpdateRec);
        
        let sClientObjectNm = "object_9";
        
        issuePutRequest(knackAppId, knackApiKey, sClientObjectNm, sRecordId, sKnackUpdateRec);
      }


    })
  });

  req.on('error', error => {
    console.error(error)
  })

  req.end();

}
console.log('done with pages');


function issuePutRequest(sKnackAppId, sKnackApiKey, sClientObjNm, sRecordId, sKnackUpdateRec) {

    // console.log(sKnackUpdateRec);

    let knackPutRecordURL = `https://api.knack.com/v1/objects/${sClientObjNm}/records/${sRecordId}`;

    console.log("PUT url: "+knackPutRecordURL);

    const knackPutOptions = {
         "X-Knack-Application-Id": sKnackAppId, 
         "X-Knack-REST-API-Key": sKnackApiKey,
         "Content-Type": "application/json"
    }

    const putReq = https.request(knackPutRecordURL, knackPutOptions, (putResp) => {
      console.log(`PUT response statusCode: ${putResp.statusCode}`);

      putResp.setEncoding('utf8');

      putResp.on('data', (sPutData) => {
        //console.log("PUT response:\n"+sPutData);
      })

      putResp.on('end', () => {
        console.log(`Client Id ${sRecordId} updated.`)
      })

    });

    putReq.on('error', putErr => {
      console.error(`Error PUTting ${knackUpdateRec}`)
    });

    putReq.write(sKnackUpdateRec);
    putReq.end();

}