
/**
 * Twilio Error Classes
 */
export class TwilioError extends Error {
    code: number;
    toString(): string;
}

export class AccessTokenExpiredError extends TwilioError {
    code: 20104;
    message: 'Access Token expired or expiration date invalid';
}
export class AccessTokenGrantsInvalidError extends TwilioError {
    code: 20106;
    message: 'Invalid Access Token grants';
}
export class AccessTokenHeaderInvalidError extends TwilioError {
    code: 20102;
    message: 'Invalid Access Token header';
}
export class AccessTokenIssuerInvalidError extends TwilioError {
    code: 20103;
    message: 'Invalid Access Token issuer/subject';
}
export class AccessTokenNotYetValidError extends TwilioError {
    code: 20105;
    message: 'Access Token not yet valid';
}
export class AccessTokenSignatureInvalidError extends TwilioError {
    code: 20107;
    message: 'Invalid Access Token signature';
}
export class ConfigurationAcquireFailedError extends TwilioError {
    code: 53500;
    message: 'Unable to acquire configuration';
}
export class ConfigurationAcquireTurnFailedError extends TwilioError {
    code: 53501;
    message: 'Unable to acquire TURN credentials';
}
export class MediaClientLocalDescFailedError extends TwilioError {
    code: 53400;
    message: 'Client is unable to create or apply a local media description';
}
export class MediaClientRemoteDescFailedError extends TwilioError {
    code: 53402;
    message: 'Client is unable to apply a remote media description';
}
export class MediaConnectionError extends TwilioError {
    code: 53405;
    message: 'Media connection failed';
}
export class MediaNoSupportedCodecError extends TwilioError {
    code: 53404;
    message: 'No supported codec';
}
export class MediaServerLocalDescFailedError extends TwilioError {
    code: 53401;
    message: 'Server is unable to create or apply a local media description';
}
export class MediaServerRemoteDescFailedError extends TwilioError {
    code: 53403;
    message: 'Server is unable to apply a remote media description';
}
export class ParticipantDuplicateIdentityError extends TwilioError {
    code: 53205;
    message: 'Participant disconnected because of duplicate identity';
}
export class ParticipantIdentityCharsInvalidError extends TwilioError {
    code: 53202;
    message: 'Participant identity contains invalid characters';
}
export class ParticipantIdentityInvalidError extends TwilioError {
    code: 53200;
    message: 'Participant identity is invalid';
}
export class ParticipantIdentityTooLongError extends TwilioError {
    code: 53201;
    message: 'Participant identity is too long';
}
export class ParticipantMaxTracksExceededError extends TwilioError {
    code: 53203;
    message: 'Participant has too many Tracks';
}
export class ParticipantNotFoundError extends TwilioError {
    code: 53204;
    message: 'Participant not found';
}
export class RoomCompletedError extends TwilioError {
    code: 53118;
    message: 'Room completed';
}
export class RoomConnectFailedError extends TwilioError {
    code: 53104;
    message: 'Unable to connect to Room';
}
export class RoomCreateFailedError extends TwilioError {
    code: 53103;
    message: 'Unable to create Room';
}
export class RoomInvalidParametersError extends TwilioError {
    code: 53114;
    message: 'Room creation parameter(s) incompatible with the Room type';
}
export class RoomMaxParticipantsExceededError extends TwilioError {
    code: 53105;
    message: 'Room contains too many Participants';
}
export class RoomMaxParticipantsOutOfRangeError extends TwilioError {
    code: 53107;
    message: 'MaxParticipants is out of range';
}
export class RoomMediaRegionInvalidError extends TwilioError {
    code: 53115;
    message: 'MediaRegion is invalid';
}
export class RoomMediaRegionUnavailableError extends TwilioError {
    code: 53116;
    message: 'There are no media servers available in the MediaRegion';
}
export class RoomNameCharsInvalidError extends TwilioError {
    code: 53102;
    message: 'Room name contains invalid characters';
}
export class RoomNameInvalidError extends TwilioError {
    code: 53100;
    message: 'Room name is invalid';
}
export class RoomNameTooLongError extends TwilioError {
    code: 53101;
    message: 'Room name is too long';
}
export class RoomNotFoundError extends TwilioError {
    code: 53106;
    message: 'Room not found';
}
export class RoomRoomExistsError extends TwilioError {
    code: 53113;
    message: 'Room exists';
}
export class RoomStatusCallbackInvalidError extends TwilioError {
    code: 53111;
    message: 'StatusCallback is invalid';
}
export class RoomStatusCallbackMethodInvalidError extends TwilioError {
    code: 53110;
    message: 'StatusCallbackMethod is invalid';
}
export class RoomStatusInvalidError extends TwilioError {
    code: 53112;
    message: 'Status is invalid';
}
export class RoomSubscriptionOperationNotSupportedError extends TwilioError {
    code: 53117;
    message: 'The subscription operation requested is not supported for the Room type';
}
export class RoomTimeoutOutOfRangeError extends TwilioError {
    code: 53109;
    message: 'Timeout is out of range';
}
export class RoomTypeInvalidError extends TwilioError {
    code: 53108;
    message: 'RoomType is not valid';
}
export class SignalingConnectionDisconnectedError extends TwilioError {
    code: 53001;
    message: 'Signaling connection disconnected';
}
export class SignalingConnectionError extends TwilioError {
    code: 53000;
    message: 'Signaling connection error';
}
export class SignalingConnectionTimeoutError extends TwilioError {
    code: 53002;
    message: 'Signaling connection timed out';
}
export class SignalingIncomingMessageInvalidError extends TwilioError {
    code: 53003;
    message: 'Client received an invalid signaling message';
}
export class SignalingOutgoingMessageInvalidError extends TwilioError {
    code: 53004;
    message: 'Client sent an invalid signaling message';
}
export class TrackInvalidError extends TwilioError {
    code: 53300;
    message: 'Track is invalid';
}
export class TrackNameCharsInvalidError extends TwilioError {
    code: 53303;
    message: 'Track name contains invalid characters';
}
export class TrackNameInvalidError extends TwilioError {
    code: 53301;
    message: 'Track name is invalid';
}
export class TrackNameIsDuplicatedError extends TwilioError {
    code: 53304;
    message: 'Track name is duplicated';
}
export class TrackNameTooLongError extends TwilioError {
    code: 53302;
    message: 'Track name is too long';
}
export class TrackServerTrackCapacityReachedError extends TwilioError {
    code: 53305;
    message: 'The server has reached capacity and cannot fulfill this request.';
}
