TrainWith Studios <trainwithstudios@gmail.com>
	
Tue, Aug 28, 10:17 PM
	
to info@goldenhomefitness.com

Dear Golden Home Fitness -

I'm building a platform that allows trainers and coaches to offer real-time training online in virtual studios. These dedicated studios support up to 10 concurrent video & audio streams, leaving the class or group size as small or large as the trainer prefers.

I came across your site and I think it's terrific!  Your philosophy on helping people get fit in their own home is exactly why I’m reaching out to you, specifically, today.  I'm interested in working with trainers helping those that want to get in shape, but can't or don't want to go to a gym, and aren't able to afford to have a personal trainer to come to them each time they want to train with their trainer.

Anyway, I'd love to offer you a (very) early-beta invitation to the site, and if possible even jump on a quick phone chat to hear a bit more about your story and the successes and challenges you've had running your business.  Your company  seem to be doing great - I'd love to hear more about it, but I’m sure you’re busy and we'd keep it as brief as you'd like.

For what it's worth as an early-on member any thoughts or suggestions you have would be particularly weighted/valued.  You can check out the new site at https://www.trainwithstudios.com.

If you're interested you drop a quick email back, or feel free to contact me any time at (978) 572-0464.  If not, no sweat.

Either way, I like your story and your attitude - keep being awesome.

Warm Regards,

Ryan Dowling

TrainWith Studios
trainwithstudios@gmail.com
(978) 572-0464