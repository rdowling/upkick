BeFirmFitness@gmail.com

Dear Noina -

I'm building a platform that allows personal trainers and coaches to offer real-time training online in virtual studios. These dedicated studios support up to 10 concurrent video & audio streams, leaving the group size completely up to the trainers.

Your 'own-boostraps' story really resonated with me, and I think your "fitness for everyone" philosophy is awesome. I'm reaching out to you, specifically, because I'm interested in working with trainers helping those that want to get in shape, but can't or don't want to go to a gym, and aren't able to afford to have a personal trainer to come to them.

I'd love to offer you a (very) early-beta invitation to the site, and if possible even jump on a quick phone chat to hear a bit more about your story and the successes and challenges you've had running your own personal training business.  We would keep it as brief as you'd like.

For what it's worth as an early-on member any thoughts or suggestions you have would be particularly weighted/valued.  You can check out the new site at https://www.trainwithstudios.com.

If you're interested you drop a quick email back, or feel free to contact me any time at (978) 572-0464.  If not, no sweat.

Either way, I like your story and your attitude - keep being awesome.

Warm Regards,

Ryan Dowling