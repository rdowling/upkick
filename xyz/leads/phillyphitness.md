Info:

To setup a consultation: 
https://meetme.so/phillyphitness

And in case you haven’t seen our virtual tour:
http://philly-phitness.com/home/free-tour/


Perry OHearn
Owner, Philly-Phitness.com
130 S. 17th Street

---------------------------------------------------------------------
Email: train@philly-phitness.com
---------------------------------------------------------------------

Dear Philly Phitness -

I'm building a platform that allows trainers and coaches to offer
real-time training online in virtual studios. These dedicated studios
support up to 10 concurrent video & audio streams, leaving the class
or group size as small or large as the trainer prefers.

I came across your company in Philadelphia magazine, so I checked out
the site.  It's awesome! I particularly like the stories in your blog
section about helping people that need a little encouragement and/or
guidance finding their fitness.

I'm reaching out to you, specifically, because I'm interested in
working with trainers helping those that want to get in shape, but
can't or don't want to go to a gym, and aren't able to afford to have
a personal trainer to come to them.

(I'm also a little partial because I had the privilege of living in
Philadelphia and saw not only the Phillies record-setting 10,000th
loss, but also their fantastic world series win the following year!)

Anyway, I'd love to offer you a (very) early-beta invitation to the
site, and if possible even jump on a quick phone chat to hear a bit
more about your story and the successes and challenges you've had
running your business.  You guys look like you're doing great - I'd
love to hear about it, but we'd keep it as brief as you'd like.

For what it's worth as an early-on member any thoughts or suggestions
you have would be particularly weighted/valued.  You can check out the
new site at https://www.trainwithstudios.com.

If you're interested you drop a quick email back, or feel free to
contact me any time at (978) 572-0464.  If not, no sweat.

Either way, I like your story and your attitude - keep being awesome.

Warm Regards,

Ryan Dowling