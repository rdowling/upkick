### Trainer types:
(A) Non-professional "peer" trainers/training
(B) Professional trainers/training
​    (B.1) All new online clients (only)
​    (B.2) Existing Clients with "real" & virtual sessions (mixed)
​        (2.a) Augmented with diet/Welness programs
​        (2.b) Offline videos/workouts/follow ups

### Are you a Trainer?

We understand that the connection trainers have with their clients is the key to a trusting and long-lasting relationship.  We're not the trainers, and its them who know best!  We let the trainer decide; how you want to train is completely up to you	

At train/with it's *your* training methodology, name, brand, and clients are all yours to keep.  

If you're already offering one-on-ones or group classes in person, try offering additional and/or discounted virtual studio classes live with you, in addition to the in-persons you do already. Each train/with studio supports up to 10 concurrent video & audio connections!

Existing clients?  We know that real one-on-one training can't be beaten.  Mix one-on-one training with virtual classes to offer more flexibility and accomodationoto your clients.  It's up to you!	
New to training? Use the site to brand yourself and find your "niche" to attract new clients and create your own "regulars" group	
​	
We recognize that no one knows more about your clients and your business than you, so we leave your brand and your classes completely up to you. 	

Use TrainWith the way that fits best with you and your clients! 	

We're getting ready to do a beta - if you're a trainer and you're interested in augmenting your personal sessions with live, virtual ones, sign up now through the Email button, or on the website. All Beta trainers get early access to features and discounts. And by discounts, we mean "free"!

### Looking to train?

Workout with a personal trainer or find and join small group classes with your friends. Get all the benefits of a personal trainer, but without the travel. Your trainer will send you the link or the name of the studio to enter below, and that's it!

