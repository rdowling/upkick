 Hi Seacoast Women's Fitness -

First off, in case you read the subject and thought: "SPAM!", here's some quick background about me:

* I'm not the prince of a country looking to make you wealthy, if only you could cut me a check.
* I'm not offering or looking for any kind of romance (happily married).
* I'm not selling questionable pharmaceuticals.
* I'm not the dark web.

My name is Ryan Dowling - my family and I live in Newburyport, MA.  For the last several years my wife and I have worked out from home, having moved around a bit, being busy with work, raising 3 kids - the usual excuses (er... responsibilities).  Even so, the benefits of a personal trainer or a training class and the social encouragement that comes with it has always appealed to us.

Your philosophy on individually personalized workouts, and working with people to define challenging yet achievable goals with a supportive and comfortable atmosphere struck a chord with us.

As you put it, Life IS Busy!  And our guess is that there's probably many more people that, like ourselves, would like to work out with a trainer, but for whatever reason don't eel they have ready access to one:

Maybe they want to be active, and while they can't physically travel easily, still want to improve their health and agility.
Maybe they don't have the time... or at least don't think they do.
Maybe they've got a great trainer, but then move. Or vice versa.
Maybe they don't feel they can afford a full-priced personal trainer.
Maybe they're new to working out.  Maybe they're just embarrassed.

Who knows. The point is it stops mattering and they run out of reasons to not get moving.

Our idea is to provide a place online that makes it simple for you to schedule and then hold classes or sessions, and allow members anywhere to attend those classes through video on your computer or television, all in real-time. 

Kind of like PayPal crossed with FaceTime, except for both individuals or groups, all geared towards trainers and personal fitness. 

If you're still reading (and I hope you are) you may be surprised to find that I'm actually not trying to sell you anything. We're still in the process of building this platform now, and the reason I'm reaching out to you is because, quite frankly, we only want to build this if folks like yourselves would be interested in something like this, or have tried to add additional clients and/or business online and have had challenges doing so.

If that's the case for you, I'd love to have a quick 10 minute chat - either over the phone, or at your favorite local coffee spot (coffee would of course be on me) - to hear about what sorts of programs and classes you and your members had the most success with, anything you've found difficult in the personal fitness world, and whether you're even interested in adding offerings online (it's OK if you aren't - I'd still like to hear your successes and challenges!).  The lower-touch it would be for you the better I'd feel about it.

I'm very, very excited about this idea but in the end it only matters to me if it matters to people like yourself.  After all, Life IS Busy (getting a lot of mileage out of your line...)! So no pressure, and thanks, sincerely, for the time already.

Kind regards,

Ryan Dowling
company mail: trainercasts@gmail.com
personal mail: ryan.p.dowling@gmail.com
cell phone: 609.954.4137

