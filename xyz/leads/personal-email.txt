Hi Heather -

My name is Ryan Dowling - my family and I live in Newburyport, MA.  For the last several years my wife and I have worked out from home, being busy with work, 3 kids, expenses and the usual responsibilities.  Even so, the value of a trainer and the feeling of accountability that comes with it has always appealed to us.

A thought occurred to us: there's probably many more people that want to work out with a dedicated trainer, in real time, but don't have access to one and can't afford to have one travel to them.

Maybe they want to be active, but can't physically travel easily.
Maybe they don't have the time... or at least don't think they do.
Maybe they've got a great trainer, but then move. Or vice versa. 

Or maybe they're just embarrassed.

Who knows. The point is it stops mattering and they run out of reasons to not get moving.

The idea is to provide a platform to you and students everywhere which would allow you to schedule and hold classes in real time, interacting with participants just like you would in a class or personal setting now, except through video on your computer or television.  So it's face-to-face... sans the hands.

If your guard is up, I apologize. I don't want to sound sale-y, I'm just honestly pretty darned enthusiastic about this idea. I even have a basic version working, which I'd be happy to share with you.

But the point is I'm only interested in building it if people like yourself would see value in it.

I'm not trying to sell you anything and, to the extent I've thought about pricing, the only thing I think I'm really sold on is allowing trainers to set their own pricing models and structures and operate as independently as they do now. They would set their own class schedules, sizes and students. They alone would decide what they would offer, just like they do now.  They'd simply be using this to gain more of their own clients and/or augment their existing programs and offerings, all of which would be theirs and theirs alone.

Kind of like PayPal crossed with YouTube, with real, live video.

If you think this would be something that might be of use to you, were I to build it, and could spare a 10 minute phone call, or let me buy you a quick cup of your favorite coffee near you, I'd love to love to hear about what sorts of programs have worked for you in the past, what challenges you could see that this might help solve, if any, and whether there are any particular "make or break" features or ideas that you might be interested in.  The lower-touch for you that this, though, the better I feel about it.

Beyond that, all I can think to offer is that were this to work out I'd of course not charge you a penny as you use it and, hopefully, as you add more programs and you get more clients.  I'd also look to you for additional features and direction to help you maximize your time and clientele, to the extent you wanted to share your thoughts there. 

I'm excited but this only matters to me if it matters to people like yourself.  So no pressure, and thanks, sincerely, for the time already.

Kind regards,
Ryan Dowling
ryan.p.dowling@gmail.com
609.954.4137
