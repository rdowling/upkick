
---------------------------------------------------------------------
Email: revolutiontrainers@gmail.com
---------------------------------------------------------------------

Dear Revolution Training,

I saw your website today and wanted to first say that the site looks
awesome! It does a great job differentiating you from other trainers –
which was why I landed on it, and why I decided to reach out to you
today.

I like and share in on your take: People are busy. They have a variety
of fitness levels and body types.

Sometimes either they don’t have the time to spend or aren’t
comfortable going to a large busy gym or facility to work out, or want
to join a large workout class. Offering private environments with
holistic fitness goals (i.e. including nutrition) seems to me to be
the smartest and most efficient way to go.

I agree with this so much, in fact, that I recently began working on a
platform to allow trainers to do just that, without the trainer or the
student to have to travel anywhere.

The idea is to build something that gives trainers like you the
ability to offer real-time video to hold one-on-one or group training
in dedicated virtual studios. How many you train and how you train is
completely up to you. It's your coaching and your brand.

The private environment, in this case, could be the student’s home, or
wherever they feel comfortable working with their trainer. Trainers,
therefore, could expand their offerings outside of their physical
travel area. You’re located in Fargo, for instance, and I’m writing
this from Newburyport, MA. These days there’s doesn’t seem to be a
good reason (to me) why someone here shouldn’t be able to work out
with someone at Revolution Training. You'll be able to use the site to
introduce your brand to new, potential clients looking for the exact
experience you specialize in.

The google search that led me to you was “innovative personal
trainers” because I’m looking for trainers and coaches that are
exploring new, independent ways to help those that would like to work
out and get fit, but for whatever reason can’t or don’t want to do so
within the same old confines.  I'd love to offer you a (very)
early-beta invitation to the site, and if possible even jump on a
quick phone chat to hear a bit more about your story and the successes
(and challenges) you've had running your business.  You seem to be
finding a lot of success, and I'd like to hear about it!

For what it's worth as an early-on member any thoughts or suggestions
you have would be particularly weighted/valued.  You can check out the
new site at https://www.trainwithstudios.com.

If you're interested you can drop a quick email back, or feel free to
contact me any time at (978) 572-0464.

If not, no sweat, and I’ll just say thank you for the time.

Either way, I think your philosophy and the direction you’re heading
are spot-on.  Keep going.

Sincerely,

Ryan Dowling

Founder, train/with studios
trainwithstudios@gmail.com
(978) 572-0464