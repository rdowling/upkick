Info:
	
Errick McAdams, CPT
www.empt.us
@errickpt (Instagram)
@errickpt (Twitter)
347-731-2740
"If you keep good food in your fridge, you will eat good food."®
"Hydrate before you caffeinate"®

--------------------------------------------------------------------
Email: errickpt@gmail.com
--------------------------------------------------------------------

Dear Errick

I saw your website today and wanted to reach out. First off, the site
looks awesome, and I'm very glad you have found yourself a successful
training business. Secondly, I strongly resonated with your comment
about saving the chit-chat and getting right down to it.  Life is
busy.

I'm building a platform that allows trainers to offer real-time video
to hold one-on-one or group training in dedicated virtual studios. How
many you train and how you train is completely up to you. It's your
coaching and your brand.

I'd love to offer you a (very) early-beta invitation to the site, and
if possible even jump on a quick phone chat to hear a bit more about
your story and the successes (and challenges) you've had running your
business.  You seem to be finding a lot of success, and I'd like to
hear about it!

For what it's worth as an early-on member any thoughts or suggestions
you have would be particularly weighted/valued.  You can check out the
new site at https://www.trainwithstudios.com.

If you're interested you can drop a quick email back, or feel free to
contact me any time at (978) 572-0464.  If not, no sweat.

Either way, I like the philosophy and the attitude - keep being great.

Warm luck,

Ryan Dowling