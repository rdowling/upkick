// app.js
const video = document.getElementById('video');
const button = document.getElementById('button');
const select = document.getElementById('select');

function gotDevices(mediaDevices) {
    select.innerHTML = '';
    select.appendChild(document.createElement('option'));
    let count = 1;
    mediaDevices.forEach(mediaDevice => {
      if (mediaDevice.kind === 'videoinput') {
        const option = document.createElement('option');
        option.value = mediaDevice.deviceId;
        alert(mediaDevice.deviceId);
        const label = mediaDevice.label || `Camera ${count++}`;
        const textNode = document.createTextNode(label);
        option.appendChild(textNode);
        select.appendChild(option);
      }
    });
  }

button.addEventListener('click', event => {
    // const constraints = {
    //     video: true,
    //     audio: false
    // };
    const videoConstraints = {
        facingMode: 'user'
    };
    const constraints = {
        video: videoConstraints,
        audio: false
    };
    navigator
        .mediaDevices
        .getUserMedia(constraints)
        .then(stream => {
            video.srcObject = stream;
        }).catch(error => {
            console.error(error);
        }); 
});

navigator.mediaDevices.enumerateDevices().then(gotDevices);