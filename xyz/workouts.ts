'use strict';

var __URL__ = "";//window.location.href ;
var __SELECTED_WORKOUTID__ = -1;

let $ = require("jquery");

function createWorkoutListItem(oWorkout) : HTMLLIElement{
    var eWorkout = document.createElement("li");
    eWorkout.innerHTML = oWorkout.workoutnm;
    eWorkout.classList.add("list-group-item", "mt-1", "mb-1", "ml-3", "mr-3");
    eWorkout.setAttribute("workout-id", oWorkout.workoutid);

    eWorkout.onclick = function(e) {
        // highlight only this workout
        selectWorkout(e.target);        
    }
            
    var btnDelete = document.createElement("button");
    btnDelete.innerHTML = "Delete";
    btnDelete.classList.add("btn", "btn-danger", "text-uppercase", "font-weight-bold", "btn-sm", "ml-4", "mr-4");
    btnDelete.onclick = function(e) {

        var delWorkoutId = oWorkout.workoutid;
        
        $.get(__URL__+"/delete-workout", {workoutid: delWorkoutId})
            .then(function(wasDeleted) {
                if (wasDeleted) {
                    eWorkout.parentNode.removeChild(eWorkout);
                }
            }
        );
    }
    eWorkout.appendChild(btnDelete);
    
    return eWorkout;
}


function closeEditWorkoutNm() {
    
    $("#workout-name").modal('hide');
    var workoutnm = $("#workoutnameInput").val().trim();
    $.post(__URL__+"/create-workout", {"workoutnm": workoutnm}, function(newWorkoutRec) {
        var eNewWorkout = createWorkoutListItem(newWorkoutRec);
        var eWorkouts = document.getElementById("workouts") as HTMLDivElement;
        eWorkouts.appendChild(eNewWorkout);
        selectWorkout(eNewWorkout);
    });
}

document.addEventListener('DOMContentLoaded', function () {

    document.getElementById("close-workout-name").onclick = function(e) {
        console.log("CLOSING");
        closeEditWorkoutNm()
    }
    
    var eWorkouts = document.getElementById("workouts") as HTMLDivElement;
    eWorkouts.innerHTML = "";
    
    var eBtnNewWorkout = document.createElement("button");
    eBtnNewWorkout.classList.add("btn", "text-uppercase", "font-weight-bold", "btn-sm", "ml-3", "mr-3", "mb-1", "mt-1");
    eBtnNewWorkout.innerHTML = "Add Workout";
    eBtnNewWorkout.onclick = function(ev) {
        $("#workout-name").modal('show');
    }

    eWorkouts.appendChild(eBtnNewWorkout);

    var eExercises = document.getElementById("all-exercises") as HTMLDivElement;
    eExercises.innerHTML = "";

    var eCurrWorkout = document.getElementById("workout-exercises") as HTMLDivElement;
    eCurrWorkout.innerHTML = `<p class="text-center">Create or Select a Workout!</p>`;

    $.getJSON( __URL__+"/your-workouts", function(aoWorkouts) {
        
        var lFirstWorkout = true;
        for (var oWorkout of aoWorkouts) {
            var eWorkout = createWorkoutListItem(oWorkout);
            eWorkouts.appendChild(eWorkout);
            if (lFirstWorkout) {
                selectWorkout(eWorkout);
                lFirstWorkout = false;
            }
        }
        
    });

    $.getJSON( __URL__+"/all-exercises", function(aoExercises) {

        for (var oExercise of aoExercises) {
            addExerciseToList(eExercises, -1, oExercise.exerciseid, oExercise.exercisenm, "btn-outline-success");
        }

        eExercises.onclick = function(evt) {
            
            if(getSelectedWorkoutId() <= 0) return;

            var eExerciseBtn = evt.target as HTMLButtonElement;
            var nExerciseId = Number(eExerciseBtn.getAttribute("exercise-id"));
            var sExerciseNm = eExerciseBtn.innerHTML;
            var nWorkoutId = getSelectedWorkoutId();
            var oAddedExercise = {exerciseid: nExerciseId, workoutid: nWorkoutId};

            $.post( __URL__+"/add-exercise", oAddedExercise, function(data) {
                if (data) {
                    var eWorkoutExercises = document.getElementById("workout-exercises") as HTMLDivElement;
                    var eAddedExercise = eWorkoutExercises.querySelector(".exercise-id-"+nExerciseId) as HTMLButtonElement;
                    
                    if (!eAddedExercise) {  // if the exercise wasn't already in the workout...
                        eAddedExercise = addExerciseToList(eWorkoutExercises, nWorkoutId, nExerciseId, sExerciseNm, "btn-outline-secondary");

                        eAddedExercise.onclick = function(evt) {
                            $.post(__URL__+"/rem-exercise", oAddedExercise, function(remdata) {
                                if(remdata) {
                                    eWorkoutExercises.removeChild(eAddedExercise);
                                }
                            });
                        }
                    }
                }
            });
        }
        
    });

    
}, false);

function getSelectedWorkoutId() {
    return __SELECTED_WORKOUTID__;
}

function addExerciseToList(list, workoutid, exerciseid, exercisenm, btncolor) {

    let eAddedExercise = document.createElement("button") as HTMLButtonElement;
    
    eAddedExercise.innerHTML = exercisenm;
    eAddedExercise.classList.add("btn", "exercise-id-"+exerciseid, "text-uppercase", "font-weight-bold", "btn-sm", "ml-4", "mr-4", "mb-1", "mt-1", btncolor);
    eAddedExercise.setAttribute("exercise-id", exerciseid);

    list.appendChild(eAddedExercise);

    return eAddedExercise;
}

function selectWorkout(eWorkout) {
    $("#workouts .active").removeClass("active");
    eWorkout.classList.add("active");
    let nWorkoutId = Number(eWorkout.getAttribute("workout-id"));
    __SELECTED_WORKOUTID__ = nWorkoutId;
    
    let eCurrWorkout = document.getElementById("workout-exercises") as HTMLDivElement;
    eCurrWorkout.innerHTML = "";

    $.get( __URL__+"/workout-exercises", {workoutid: nWorkoutId}, function(aoExercises) {
        for (let oExercise of aoExercises) {
            let oAddedExercise = {exerciseid: oExercise.exerciseid, workoutid: nWorkoutId};
            let eAddedExercise = addExerciseToList(eCurrWorkout, nWorkoutId, oExercise.exerciseid, oExercise.exercisenm, "btn-outline-secondary");
            eAddedExercise.onclick = function(evt) {
                $.post(__URL__+"/rem-exercise", oAddedExercise, function(remdata) {
                    if(remdata) {
                        eCurrWorkout.removeChild(eAddedExercise);
                    }
                })
            }

        }
    });
}