
/**
 * todos:
 *  - display timer time at end of pct left
 *  - lock down aTimers
 *  + timers over 60 minutes: how to display?
 *  + make full(er) screen
 *  + clear out completed timers & reuse completed timer colors
 *  * add stop button?
 *  * popup alert when timer is done | audible alert?
 *  . turn into "control"/"view-control" module/component
 *  x name timers
 */


let aTimers = []; 

class CountdownTimer {

    durationSecs = 0;
    startTime: Date;
    timerIndex = 0;

    constructor(timerIndex:number, durSecs: number) {
        this.timerIndex = timerIndex;
        this.durationSecs = durSecs;
    }

    start() {
        this.startTime = new Date();
    }

    secsSince(dt: Date): number{
        return this.diffSecs(this.startTime, dt);
    }

    diffSecs(firstDt: Date, secondDt: Date): number {
        var diffMillis = (secondDt.getTime() - firstDt.getTime()); 
        var diffSecs = Math.round((diffMillis/1000));
        return diffSecs;
    }

    secsRemaining(dUpdate: Date): number {
        let nDiffSecs = this.secsSince(dUpdate);
        let nTimerSecsLeft = Math.max(this.durationSecs - nDiffSecs, 0.0);
        return nTimerSecsLeft;
    }


}

window.onload = function() {

    let canvas = document.getElementById("countdown_canvas") as HTMLCanvasElement;
    let canvasParent = document.getElementById("timer_view") as HTMLDivElement;
    fitToContainer(canvas, canvasParent);

    let addTimerBtn = document.getElementById("btn_add_timer") as HTMLButtonElement;
    addTimerBtn.onclick = addTimer;

    let cvs = canvas; // document.getElementById('countdown_canvas') as HTMLCanvasElement;
    let ctx = cvs.getContext('2d');

    let nArcLineWidth = 18;

    ctx.lineWidth = nArcLineWidth;

    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.font = '15px Trebuchet MS';
    ctx.fillStyle = 'white';

    let eTimers = document.getElementById('current_timers');

    /**
    * @closure
    */
    var draw = (function () {

        /** `arc()`:
            * {00 o'clock} -> -0.5PI
            * {03 o'clock} ->  0.0pi
            * {06 o'clock} ->  0.5pi
            * {09 o'clock} ->  1.0pi
            * {12 o'clock} ->  1.5pi
        */

        var beginCircle  = -0.5 * Math.PI; // "start" (top) circle
        var endCircle =  1.5 * Math.PI; // "end" (top) of circle  
        
        let shiftArcAmtBack = -1 * beginCircle;  // Calculate pct of 2PI and then "shift" it back -0.5PI

        let xPos = 0.5 * canvas.width; // 175;
        let yPos = 0.5 * canvas.height; // 175;

        return function (arcRadius, nSecsLeft, arcColor) {

            let nTimerPctLeft = 1.0;

            if (nSecsLeft > 60) {
                nTimerPctLeft = (nSecsLeft / (60*60))
            } else {
                nTimerPctLeft = (nSecsLeft / 60);
            }

            let nTimerPctDone = (1.0 - nTimerPctLeft);

            let n2PI = 2.0 * Math.PI;

            let nTimerPIDone = n2PI * nTimerPctDone;  // once around full circle, ending back at 3 o'clock

            let beginArc = nTimerPIDone - shiftArcAmtBack;
            
            let finishArc = endCircle;
            if (nSecsLeft > 60) {
                finishArc = beginArc + (n2PI/60);
            } else {
                finishArc = endCircle;
            }

            ctx.strokeStyle = arcColor;
            ctx.beginPath();
            ctx.arc(xPos, yPos, arcRadius, beginArc, finishArc, false);
            ctx.stroke();
        };
    }());

    let aTimerColors = ['palevioletred', 'limegreen', 'steelblue', 'lightcoral', 'lightseagreen', 'lightskyblue'];

    var clock = function () {

        let dUpdate = new Date();

        let sTimersMsg = "";
        let lTimersDone = true;

        let nArcRadiusIncr = nArcLineWidth + 2;
        let nArcRadiusOrig = (canvas.height/2 - nArcLineWidth);// + (nTimers * nArcRadiusIncr);

        ctx.clearRect(0, 0, canvas.width, canvas.height);

        let nTimers = aTimers.length;
        for(let iTimer = 0; iTimer <= nTimers-1; iTimer++) {

            let oTimer = aTimers[iTimer] as CountdownTimer;
            let iTimerIndex = oTimer.timerIndex;

            let nTimerSecsLeft = oTimer.secsRemaining(dUpdate);
            
            if (iTimer > 0) {
                sTimersMsg = sTimersMsg + "<br/>";
            }
            console.log(nTimerSecsLeft);
            sTimersMsg += "t"+(iTimerIndex+1)+": "+nTimerSecsLeft;

            let nArcRadius = nArcRadiusOrig - (iTimerIndex+1)*nArcRadiusIncr;
            let timerColor = aTimerColors[iTimerIndex];

            draw(nArcRadius, nTimerSecsLeft, timerColor);

            lTimersDone = lTimersDone && (nTimerSecsLeft === 0.0);
        }
        
        eTimers.innerHTML = sTimersMsg;

        for(let iTimer = nTimers-1; iTimer >= 0; iTimer--) {
            let oTimer = aTimers[iTimer];
            if (oTimer.secsRemaining(dUpdate) <= 0.0) {
                aTimers.splice(iTimer, 1);
            }
        }

        // ctx.fillText(sTimersMsg, 175, 175);

        if (true || !lTimersDone)  {
            requestAnimationFrame(clock);
        }
    };


  
    function fitToContainer(canvas, canvasParent){

        let smaller = Math.min(canvasParent.offsetWidth, canvasParent.offsetHeight)

        canvas.width = smaller;
        canvas.height = smaller;

        // ...then set the internal size to match
        canvas.width  = smaller; // canvasParent.offsetWidth;
        canvas.height = smaller; // canvasParent.offsetHeight;
        // Make it visually fill the positioned parent
        // canvas.style.width ='100%';
        // canvas.style.height='100%';
        // // ...then set the internal size to match
        // canvas.width  = canvasParent.offsetWidth;
        // canvas.height = canvasParent.offsetHeight;
    }

    clock();

}

function addTimer() {
    let elAddTimerInput = document.getElementById("timer_time") as HTMLInputElement;
    let nTimerMins = parseInt(elAddTimerInput.value);
    let nTimerSecs = nTimerMins * 1;//60;

    let oNewTimer = new CountdownTimer(aTimers.length, nTimerSecs);
    aTimers.push(oNewTimer);
    oNewTimer.start();

}
