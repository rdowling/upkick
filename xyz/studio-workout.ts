var __CURRWORKOUTID__ = -1;
var __CURRACTIVITYNUM__ = -1;

var __STUDIO_SID__ = "";

var ACTIVITY_DURATION = 40;  // 20
var REST_DURATION = 20; // 10
var _activitiesList: ActivitySet;
let __showWorkouts__ = false;

var __STUDIO_SOCKET__ = null;
var __EXERCISE_TIMER__;// = new Timer();
var __WORKOUT_TIMER__// = new Timer();

let Timer = require("easytimer.js");


interface ArrayConstructor {
    from(arrayLike: any, mapFn?, thisArg?): Array<any>;
}


__STUDIO_SOCKET__.on("beginWorkout", function (beginIn, exercises) {
    loadWorkout(exercises);
    countdownToWorkout(beginIn);
})


class ActivitySet {
    activities = new Array();

    getView(): HTMLElement[] {
        var elems = new Array<HTMLElement>();
        for (let activity of this.activities) {
            // var eActivity = document.createElement("div");
            //eActivity.classList.add("activity", "text-dark-current", "list-group-item-warning", "text-center");
            var eActivity = document.createElement("a") as HTMLAnchorElement;
            eActivity.href = "#";
            eActivity.classList.add("list-group-item", "list-group-item-action", "text-center");
            eActivity.innerHTML = activity.exercisenm;
            eActivity["activity"] = activity;
            elems.push(eActivity);
        }
        return elems;
    }

    addActivity(activity: Activity, duration: number) {
        activity.activitySecs = duration;
        this.activities.push(activity);
    }

}


class Activity {
    activityNm: string;
    activitySecs: number;

    constructor(nm: string) {
        this.activityNm = nm;
    }

    getActivityTime(): number {
        return this.activitySecs;
    }
}

function clearCurrentActivitiesList() {
    $("#activities-container").find("a").remove();
}

clearCurrentActivitiesList();

document.addEventListener('DOMContentLoaded', function () {

    //document.getElementById("activities-container").innerHTML = "";
    //clearCurrentActivitiesList();

    if (__showWorkouts__) {

        var ePickWorkout = document.getElementById("workout-picker");
        $.get("/your-workouts", aoWorkouts => {

            for (var oWorkout of aoWorkouts) {

                let eWorkout = createWorkoutPickItem(oWorkout);
                let sWorkoutNm = oWorkout.workoutnm;

                $(eWorkout).click(function (e) {

                    document.getElementById("workout-pick-btn").innerHTML = sWorkoutNm;
                    clearCurrentActivitiesList();

                    var nWorkoutId = Number(e.target.getAttribute("workout-id"));
                    __CURRWORKOUTID__ = nWorkoutId;
                    $.get("/workout-exercises", { workoutid: nWorkoutId }, function (exercises) {
                        loadWorkout(exercises);
                    });

                    e.preventDefault();
                });
                ePickWorkout.appendChild(eWorkout);
            }

        });
    }
}, false);


function createWorkoutPickItem(workout): HTMLAnchorElement {
    var eWorkout = document.createElement("a");
    eWorkout.innerHTML = workout.workoutnm;
    eWorkout.classList.add("dropdown-item");
    eWorkout.setAttribute("href", "#");
    eWorkout.setAttribute("workout-id", workout.workoutid);
    return eWorkout;
}


function loadWorkout(exercises) {

    clearCurrentActivitiesList();
    var totalTimeElem = document.getElementById("total-time-container");

    _activitiesList = new ActivitySet();

    for (var exercise of exercises) {
        _activitiesList.addActivity(exercise, ACTIVITY_DURATION);
    }

    var activitiesContainer = document.getElementById("activities-container");
    var activityNum = 0;
    for (let activityElem of _activitiesList.getView()) {
        activityNum += 1;
        activityElem.id = "activity-" + activityNum;
        activitiesContainer.appendChild(activityElem)
    }

    var liHeight = 60;
    var i = 0;
    $("#activities-container > .list-group-item").each(function () {
        $(this).css("top", i);
        i += liHeight;
    });

    var totalTimeStr = formatSeconds(_activitiesList.activities.length * (ACTIVITY_DURATION + REST_DURATION));
    totalTimeElem.innerHTML = "00:00 / " + totalTimeStr;
}

function pauseWorkout(workoutid, exerciseTimer, totalTimer) {
    // __STUDIO_SOCKET__.emit("pauseWorkout", Studio.sid, __CURRWORKOUTID__);
    exerciseTimer.pause();
    totalTimer.pause();
    let btn = document.getElementById("start-button");
    btn.innerHTML = "Resume";
    btn.classList.remove("btn-outline-success");
    btn.classList.add("btn-outline-warning");
    btn.onclick = function () {
        resumeWorkout(workoutid, exerciseTimer, totalTimer);
    }
}

function resumeWorkout(workoutid, exerciseTimer, totalTimer) {
    // __STUDIO_SOCKET__.emit("resumeWorkout", studio.sid, __CURRWORKOUTID__);
    exerciseTimer.start();
    totalTimer.start();
    let btn = document.getElementById("start-button");
    btn.innerHTML = "Pause";
    btn.classList.remove("btn-outline-warning");
    btn.classList.add("btn-outline-success");
    btn.onclick = function () {
        pauseWorkout(workoutid, exerciseTimer, totalTimer);
    }
}

function countdownToWorkout(nCountdown) {
    var actTimer = document.getElementById("activity-timer");
    //actTimer.style.display = "";    
    actTimer.innerHTML = "Starting...";
    __EXERCISE_TIMER__ = new Timer();
    __EXERCISE_TIMER__.start({ precision: 'secondTenths', startValues: { secondTenths: nCountdown }, countdown: true });

    var countDown = nCountdown;
    var audio = new Audio();
    audio.src = "/lib/button.mp3";
    audio.volume = 0.05;
    audio.load();

    var btnStart = document.getElementById("start-button");
    btnStart.innerHTML = "Starting...";

    __EXERCISE_TIMER__.addEventListener('secondsUpdated', function (e) {
        actTimer.innerHTML = "Starting in " + __EXERCISE_TIMER__.getTimeValues().seconds + "...";
        if (__EXERCISE_TIMER__.getTimeValues().seconds <= 3) {
            audio.play();
        }
    });

    __EXERCISE_TIMER__.addEventListener('targetAchieved', function (e) {
        actTimer.innerHTML = "now: GO!";
        beginWorkout();
    });
}

function formatSeconds(totSecs: number) {
    var mins = Math.floor(totSecs / 60);
    var secs = totSecs - (mins * 60);

    var minsStr = (mins < 10) ? "0" + mins : mins;
    var secsStr = (secs < 10) ? "0" + secs : secs;
    return minsStr + ":" + secsStr;

}

function beginWorkout() {

    var btnStart = document.getElementById("start-button");
    //btnStart.innerHTML = "Starting...";
    btnStart.innerHTML = "Pause";
    btnStart.onclick = function () {
        // $("#play-button").click();
        pauseWorkout(__CURRWORKOUTID__, __EXERCISE_TIMER__, __WORKOUT_TIMER__);
        // countdownToWorkout();
    }

    var actTimer = document.getElementById("activity-timer");

    var activitiesContainer = document.getElementById("activities-container");
    var totalTimeElem = document.getElementById("total-time-container");
    var activityCount = _activitiesList.activities.length;

    var totalTime = activityCount * (ACTIVITY_DURATION + REST_DURATION);
    var totalTimeStr = formatSeconds(totalTime);
    totalTimeElem.innerHTML = "00:00 / " + totalTimeStr;

    var elapsedTotalSecs = 0;
    var elapsedActivitySecs = 0;
    var elapsedRestSecs = 0;

    __CURRACTIVITYNUM__ = 0;
    var activityElem;
    var audio = new Audio();
    audio.src = "/lib/button.mp3";
    audio.volume = 0.08;
    audio.load();
    var workoutComplete = false;

    var i = 0;
    $("#activities-container > a").each(function () {
        $(this).css("top", i);
        i += 60;
    });

    __WORKOUT_TIMER__ = new Timer();
    __WORKOUT_TIMER__.addEventListener('secondsUpdated', function (e) {

        if (__CURRACTIVITYNUM__ > 0) {
            elapsedTotalSecs++;
        }

        if (elapsedActivitySecs == 0 && elapsedRestSecs == 0) {
            __CURRACTIVITYNUM__++;
            elapsedActivitySecs = ACTIVITY_DURATION;
            elapsedRestSecs = REST_DURATION;
        } else if (elapsedActivitySecs > 0) {
            elapsedActivitySecs--;
        } else {
            elapsedRestSecs--;
        }

        activityElem = document.getElementById("activity-" + __CURRACTIVITYNUM__);
        if (elapsedActivitySecs == ACTIVITY_DURATION) {
            activityElem.classList.add("list-group-item-success");
        } else if (elapsedActivitySecs == 0 && elapsedRestSecs == REST_DURATION) {
            activityElem.classList.remove("list-group-item-success");
            activityElem.classList.add("list-group-item-danger");
        } else if (elapsedRestSecs == 0 && elapsedActivitySecs == 0) {
            activityElem.classList.remove("list-group-item-danger");

            if (__CURRACTIVITYNUM__ == activityCount) {
                workoutComplete = true;
            } else {
                __CURRACTIVITYNUM__++;
                elapsedActivitySecs = ACTIVITY_DURATION;
                elapsedRestSecs = REST_DURATION;
                activityElem = document.getElementById("activity-" + __CURRACTIVITYNUM__);
                activityElem.classList.add("list-group-item-success");

            }

            $("#activities-container > a").each(function (nIndex, $elem) {
                moveElem($(this));
            });

        }

        var elapedTotalSecsStr = formatSeconds(elapsedTotalSecs);
        totalTimeElem.innerHTML = elapedTotalSecsStr + " / " + totalTimeStr;

        if (elapsedActivitySecs > 0) {
            actTimer.innerHTML = "Active! " + elapsedActivitySecs;
            if (elapsedActivitySecs <= 3) {
                audio.play();
            }
        } else if (elapsedRestSecs > 0) {
            actTimer.innerHTML = "Rest! " + elapsedRestSecs;
            if (elapsedRestSecs <= 3) {
                audio.play();
            }
        } else if (workoutComplete) {
            actTimer.innerHTML = "Workout Complete!";
            var btnStart = document.getElementById("start-button");
            __WORKOUT_TIMER__.stop();
            btnStart.innerHTML = "Start";
            btnStart.onclick = startWorkout;

            return;
        }

    });
    __WORKOUT_TIMER__.start();

}

function setVideoSizeMaximized(elVideo: HTMLDivElement) {
    if (!elVideo.classList.contains("maximized")) {
        elVideo.classList.add("maximized");
    }
    if (elVideo.classList.contains("minimized")) {
        elVideo.classList.remove("minimized");
    }
}

// var btnStart = document.getElementById("start-button");
// btnStart.onclick = startWorkout;    
function startWorkout() {
    __STUDIO_SOCKET__.emit("beginWorkout", __STUDIO_SID__, __CURRWORKOUTID__);
}

function focusVideo(identity: string) {
    let elVideo = getVideoElementForId(identity);
    setVideoSizeMaximized(elVideo);
}

function getVideoElementForId(id: string) {
    let $elVideo = $("#" + id);
    if ($elVideo && $elVideo.length > 0) {
        return $elVideo[0];
    }

}


function moveElem($elem) {
    var top = parseInt($elem.css("top")) as number;
    var temp = 0;//-1 * $('#activities-container > a').height();

    if (top <= temp) {
        top = 60 * $("#activities-container > a").length as number; // $('.players').height();
        $elem.css("top", top);
    }

    $elem.animate({ top: (top - 60) }, 600, function () {
        //moveElem($(this));
        if (top < temp) {
            $(this).css("top", top);
        }
    });

}
