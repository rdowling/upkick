
console.log(padl("00", null));
console.log(padl("00", 1));
console.log(padl("00", 12));
console.log(padl("00", ''));
console.log(padl("00", '1'));
console.log(padl("00", '12'));


function padl(pad, strToPad) {
  if (!strToPad) {
    return strToPad;

  }
  if (!(strToPad instanceof String)) {
    strToPad = String(strToPad);
  }
  strToPad = String(strToPad);
  return pad.substring(0, pad.length - strToPad.length) + strToPad
}