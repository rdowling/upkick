var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(80); // WARNING: app.listen(80) will NOT work here!

app.use(express.static('.'))

app.get('/socket-ex', function (req, res) {
  res.sendFile(__dirname + '/io-socket-ex.html');
});

io.on('connection', function (socket) {
  socket.on('toggle', function (data) {
    
    console.log("socket:on('toggle'): "+data);
    
    // `broadcast.emit` signals to everyone but youself
    socket.broadcast.emit('toggle', {action: "Someone said toggle"});
        
    // `emit` signals back to yourself
    socket.emit('toggle', {action: "Toggle yourself"});

  });

});