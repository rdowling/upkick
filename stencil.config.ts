import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';
import nodePolyfills from 'rollup-plugin-node-polyfills';

// https://stenciljs.com/docs/config

export const config: Config = {
  plugins: [sass()],

  // globalStyle:  'src/client/stencils/global/app.scss',
  // globalScript: 'src/client/stencils/global/app.ts',
  
  namespace: 'tw-stencils',
  rollupPlugins: {
      after: [
        nodePolyfills(),
      ]
    },
  /* use the two "ionic-" files (and properties) to use tailwindcss components (and styling) */
  globalStyle: 'src/client/stencils/global/style-tw.dist.css', 

  /* use the two "ionic-" files (and properties) to use ionic framework components (and styling) */
  // globalStyle: 'src/client/stencils/global/ionic-app.css',
  // globalScript: 'src/client/stencils/global/ionic-app.ts',
  nodeResolve: {
    browser: true
  },
  srcDir: 'src/client/stencils',
  taskQueue: 'async',
  outputTargets: [
    {
      type: 'dist',
      dir: '.dist/client/tw-stencils'
      //esmLoaderPath: '../loader'
    },

    {
      type: 'www',
      serviceWorker: null // disable service workers
    }
  ]
};
