

export interface TwPoint {
    x: number;
    y: number;
}

export interface TwLine {
    mode: string;
    color: any;
    x1: number;
    y1: number;
    x2: number;
    y2: number;
}
