import { awaitTwilioConnection } from "./util-twilio-video-client";
import { ConnectOptions } from "twilio-video";

async function tests() {
    let tokenJwt = "";
    let roomNm = "test-room";

    const connectionOptions: ConnectOptions = {
        name: roomNm,
        logLevel: "debug",
        audio: false, 
        video: false,
        bandwidthProfile: {
            
            video: {
                mode: 'collaboration',
                dominantSpeakerPriority: 'standard',
                renderDimensions: {
                    high: { height: 1080, width: 1920 },
                    standard: { height: 720, width: 1280 },
                    low: { height: 90, width: 160 },
                },
            },
        },
        dominantSpeaker: true,
        maxAudioBitrate: 12000,
        networkQuality: { local: 1, remote: 1 },
        // preferredVideoCodecs: [{ codec: 'VP8', simulcast: true }],
    };
    await awaitTwilioConnection(connectionOptions, tokenJwt);

} 

tests();

