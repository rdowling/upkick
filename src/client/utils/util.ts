// @ts-ignore

export interface GroupStudioProps {
  identity  : string;
  studioNm  : string;
  companyNm : string;
  clientId  : string;
  clientNm  : string;
  trainerId : string; 
  isTrainer : boolean;
  identityToken: string;

}

export function removeArrayItem(item: any, array: any[]): any[] {
  return array.filter(cand => cand !== item);
}

export function removeClass(el: HTMLElement, classNm: string): boolean {
    if (el.classList.contains(classNm)) {
        el.classList.remove(classNm);
        return true;
    } else {
        return false;
    }
}

export function addClass(el: HTMLElement, classNm: string): boolean {
    if (!el.classList.contains(classNm)) {
        el.classList.add(classNm);
        return true;
    } else {
        return false;
    }
}


export function fetchHtml(cHtmlFile): Promise<void|Document> {
    return fetch(cHtmlFile)
    .then(function(response) {
        // When the page is loaded convert it to text
        return response.text()
    })
    .then(function(html) {
        // Initialize the DOM parser
        var parser = new DOMParser();

        // Parse the text
        var doc = parser.parseFromString(html, "text/html") as Document;

        // You can now even select part of that html as you would in the regular DOM 
        // Example:
        // var docArticle = doc.querySelector('article').innerHTML;

        return doc;
    });
    // .catch(function(err) {  
    //     console.log('Failed to fetch page: ', err);  
    // });
}

/** GHF/NACK **/
export function snapAllVideos(eContainer: HTMLElement) {
  let videos = (eContainer)? eContainer.querySelectorAll('video') : document.querySelectorAll('video');
  for (let i = 0, len = videos.length; i < len; i++) {
      const v = videos[i]

      let eMemberView = v.parentNode as HTMLDivElement;
      let eCanvas = eMemberView.querySelector('canvas') as HTMLCanvasElement; // declare a canvas element in your html
      let ctx = eCanvas.getContext('2d') as CanvasRenderingContext2D;

      // if (!v.src) continue // no video here
      try {
          let nNewHeight = eMemberView.clientHeight-12;
          let nNewWidth  = eMemberView.clientWidth-12;

          eCanvas.width = nNewWidth;
          eCanvas.height = nNewHeight;

          ctx.fillRect(0, 0, nNewWidth, nNewHeight);
          ctx.drawImage(v, 0, 0, nNewWidth, nNewHeight);
          //v.style.backgroundImage = `url(${canvas.toDataURL()})` // here is the magic
          //v.style.backgroundSize = 'contain' 
          //ctx.clearRect(0, 0, w, h); // clean the canvas
      } catch (e) {
          continue
      }
  }
}

/** GHF/NACK **/
export function clearAllSnaps() {
  let canvases = document.querySelectorAll('canvas');
  for (let i = 0, len = canvases.length; i < len; i++) {
      const eCanvas = canvases[i];
      let eMemberView = eCanvas.parentNode as HTMLDivElement;

      let nNewHeight = eMemberView.clientHeight-12;
      let nNewWidth  = eMemberView.clientWidth-12;

      eCanvas.width = nNewWidth;
      eCanvas.height = nNewHeight;

      let ctx = eCanvas.getContext('2d') as CanvasRenderingContext2D;
      ctx.clearRect(0, 0, nNewWidth, nNewHeight); // clean the canvas
  }
}

/** GHF/NACK **/
export function dataURItoBlob(dataURI): Blob {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);
    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], {type:mimeString});
}


/**
 * Get the code snippet from a file.
 * @param {string} relativePath
 * @returns {Promise<string>}
 */
export async function getSnippet(relativePath: string) {
  const response = await fetch(relativePath);
  return response.text();
}

 // Set the canvas size to the video size.
export function setCanvasSizeToVideo(canvas: HTMLCanvasElement, video: HTMLVideoElement) {
  canvas.style.height = video.clientHeight + 'px';
}

import * as queryString from 'query-string';

export {queryString};

export function parseQueryParams(sUrl): queryString.ParsedQuery {
  
  sUrl = sUrl || "";
  let sQueryParams = sUrl

  let lastIndexOfQuestionMark = sUrl.lastIndexOf('?');
  if ( lastIndexOfQuestionMark >= 0 ) {
    sQueryParams = sUrl.substring( lastIndexOfQuestionMark + 1 );
  }
  
  let oParams: queryString.ParsedQuery = queryString.parse(sQueryParams) || {};

  return oParams;
}

export function hasQueryParam(sUrl: string, paramNm: string): boolean {

  const oParams: queryString.ParsedQuery = parseQueryParams(sUrl); 

  //console.debug('hasQueryParam()', 'url:', sUrl, 'parsed-query-params', oParams, 'key', paramNm);

  for(let sCand of Object.keys(oParams)) {
    if (sCand.toLowerCase() === paramNm.toLowerCase()) {
      return true;
    }
  }

  return false;
}

export function arrbuffToStr(arrbuff: ArrayBuffer): string {
  return String.fromCharCode.apply(null, new Uint16Array(arrbuff));
}

export function getQueryParam(sUrl: string, sKey: string): string {

  const oParams: queryString.ParsedQuery = parseQueryParams(sUrl);

  //console.debug('getQueryParam()', 'url:', sUrl, 'parsed-query-params', oParams, 'key', sKey);

  let sValue = null;
  let lKeyFound = false;
  for(let sCand of Object.keys(oParams)) {
    if (!lKeyFound && sCand.toLowerCase() === sKey.toLowerCase()) {
      sValue = oParams[sCand];
      lKeyFound = true;
    }
  }

  return sValue;
}

export function getBooleanQueryParam(sUrl: string, key: string): boolean|null {

  let sValue: string = getQueryParam(sUrl, key);
  
  if (!sValue || sValue === null || sValue === 'undefined') {
    return null;
  } else {
    sValue = sValue.toUpperCase();
    return (sValue === "TRUE");
  }
  
}

/**
 * Take snapshot of the local video from the HTMLVideoElement and render it
 * in the HTMLCanvasElement.
 * @param {HTMLVideoElement} video
 * @param {HTMLCanvasElement} canvas
 */
export function takeVideoSnapshot(video: HTMLVideoElement, canvas: HTMLCanvasElement) {
  var context = canvas.getContext('2d') as CanvasRenderingContext2D;
  context.clearRect(0, 0, canvas.width, canvas.height);
  context.drawImage(video, 0, 0, canvas.width, canvas.height);
}

export function isH264CodecSupported() {
    // const PeerConnection = typeof RTCPeerConnection !== 'undefined'
    //     ? RTCPeerConnection
    //     : typeof webkitRTCPeerConnection !== 'undefined'
    //         ? webkitRTCPeerConnection
    //         : typeof mozRTCPeerConnection !== 'undefined'
    //             ? mozRTCPeerConnection
    //             : null;

    // let isH264Supported;

    // /**
    //  * Test support for H264 codec.
    //  * @returns {Promise<boolean>} true if supported, false if not
    //  */
    // function testH264Support() {
    //     if (typeof isH264Supported === 'boolean') {
    //         return Promise.resolve(isH264Supported);
    //     }
    //     if (!PeerConnection) {
    //         isH264Supported = false;
    //         return Promise.resolve(isH264Supported);
    //     }

    //     let offerOptions = {};
    //     const pc = new PeerConnection();
    //     try {
    //         pc.addTransceiver('video');
    //     } catch (e) {
    //         offerOptions.offerToReceiveVideo = true;
    //     }

    //     return pc.createOffer(offerOptions).then(offer => {
    //         isH264Supported = /^a=rtpmap:.+ H264/m.test(offer.sdp);
    //         return isH264Supported;
    //     });
    // }

    // // Now we can call testH264Support to check if H.264 is supported
    // testH264Support().then(isSupported => {
    //     console.log(`This browser ${isSupported
    //         ? 'supports' : 'does not support'} H264 codec`);
    // });
}


export function envl<T>(xOne: T|null, xOther: T|null) {
  //if ((typeof xOne === 'string') && )
  if (xOne !== undefined && xOne !== null && xOne) {
    return xOne;
  } else {
    return xOther;
  }
}

export function nvl<T>(xOne: T|null, xOther: T|null) {
  if (xOne !== undefined && xOne !== null) {
    return xOne;
  } else {
    return xOther;
  }
}

export const isMobile = (() => {
  if (typeof navigator === 'undefined' || typeof navigator.userAgent !== 'string') {
    return false;
  }
  return /Mobile/.test(navigator.userAgent);
})();

// This function ensures that the user has granted the browser permission to use audio and video
// devices. If permission has not been granted, it will cause the browser to ask for permission
// for audio and video at the same time (as opposed to separate requests).
export function ensureMediaPermissions() {
  return navigator.mediaDevices
    .enumerateDevices()
    .then(devices => devices.every(device => !(device.deviceId && device.label))) // if either is empty we need to request media permissions
    .then(shouldAskForMediaPermissions => {
      if (shouldAskForMediaPermissions) {
        navigator.mediaDevices
          .getUserMedia({ audio: true, video: true })
          .then(mediaStream => mediaStream.getTracks().forEach(track => track.stop()));
      }
    });
}


export function isSafariBrowser() {
  return /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
}

export function padl(pad: string, strToPad: any): string {
  if (!strToPad) {
    return strToPad;
  }

  if (!(strToPad instanceof String)) {
    strToPad = String(strToPad);
  }
  
  return pad.substring(0, pad.length - strToPad.length) + strToPad
}

export function absdiff(num1: number, num2: number) {
  if (num1 > num2) {
    return num1 - num2;
  } else {
    return num2 - num1;
  }
}

export function dist(x1: number, y1: number, x2: number, y2: number) {
  var deltaX = absdiff(x1, x2);
  var deltaY = absdiff(y1, y2);
  var dist = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
  return dist;
}

function randomItem(array:Array<string>) {
  var randomIndex = Math.floor(Math.random() * array.length);
  return array[randomIndex];
}

/**
 * @param {String} HTML string representing one (single) element
 * @return {Node} newly created HTML element / node
 */
export function htmlToElement(html:string) {
  var template = document.createElement("template");
  html = html.trim(); // Never return a text node of whitespace as the result
  template.innerHTML = html;
  return template.content.firstChild;
}

/**
 * @param {String} HTML representing any number of sibling elements
 * @return {NodeList} list of newly created HTML elements/nodes
 */
export function htmlToElements(html:string) {
  var template = document.createElement("template");
  template.innerHTML = html;
  return template.content.childNodes;
}

export function randomName() {
  var ADJECTIVES = [
    "Abrasive",
    "Brash",
    "Callous",
    "Daft",
    "Eccentric",
    "Fiesty",
    "Golden",
    "Holy",
    "Ignominious",
    "Joltin",
    "Killer",
    "Luscious",
    "Mushy",
    "Nasty",
    "OldSchool",
    "Pompous",
    "Quiet",
    "Rowdy",
    "Sneaky",
    "Tawdry",
    "Unique",
    "Vivacious",
    "Wicked",
    "Xenophobic",
    "Yawning",
    "Zesty"
  ];

  var FIRST_NAMES = [
    "Anna",
    "Bobby",
    "Cameron",
    "Danny",
    "Emmett",
    "Frida",
    "Gracie",
    "Hannah",
    "Isaac",
    "Jenova",
    "Kendra",
    "Lando",
    "Mufasa",
    "Nate",
    "Owen",
    "Penny",
    "Quincy",
    "Roddy",
    "Samantha",
    "Tammy",
    "Ulysses",
    "Victoria",
    "Wendy",
    "Xander",
    "Yolanda",
    "Zelda"
  ];

  var LAST_NAMES = [
    "Anchorage",
    "Berlin",
    "Cucamonga",
    "Davenport",
    "Essex",
    "Fresno",
    "Gunsight",
    "Hanover",
    "Indianapolis",
    "Jamestown",
    "Kane",
    "Liberty",
    "Minneapolis",
    "Nevis",
    "Oakland",
    "Portland",
    "Quantico",
    "Raleigh",
    "SaintPaul",
    "Tulsa",
    "Utica",
    "Vail",
    "Warsaw",
    "XiaoJin",
    "Yale",
    "Zimmerman"
  ];
  return (
    randomItem(ADJECTIVES) + randomItem(FIRST_NAMES) + randomItem(LAST_NAMES)
  );
}
