
// TODO: throw these into an "App" or cookie-helper class

export function setCookie(cname: string, cvalue: string, exdays: number | Date) {
    var d = new Date();
    let nDays = 0;
    if (exdays instanceof Date) {
        nDays = exdays.getMilliseconds();
    } else {
        nDays = exdays;
    }
    d.setTime(d.getTime() + (nDays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export function getCookie(cname: string) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return null;
}

export function recallMemberInfo() {
    let sCurrMemberNm = getCookie("memberNm");
    let sCurrIsTrainer = getCookie("memberIsTrainer");
    if (!sCurrIsTrainer) {
        sCurrIsTrainer = "false";
    }
    let bCurrIsTrainer = (sCurrIsTrainer == "true" ? true : false);
    return { memberNm: sCurrMemberNm, memberIsTrainer: bCurrIsTrainer };
}

export function rememberMemberInfo(memberNm: string, memberIsTrainer: string | boolean) {

    var now = new Date();
    now.setTime(now.getTime() + 365 * 24 * 60 * 60 * 1000); // cookie set to expire in one year

    setCookie("memberNm", memberNm, now);

    if (typeof memberIsTrainer == 'boolean') {
        setCookie("memberIsTrainer", memberIsTrainer.toString(), now);
    } else {
        setCookie("memberIsTrainer", memberIsTrainer, now);
    }

}

export function confirmMemberInfo() {
    let memberNm = (document.getElementById("txtMemberNm") as HTMLInputElement).value;
    let memberIsTrainer = (document.getElementById("chkIsTrainer") as HTMLInputElement).checked;

    rememberMemberInfo(memberNm, memberIsTrainer);

    let memberInfo = recallMemberInfo();
    memberNm = memberInfo?.memberNm || "";
    memberIsTrainer = memberInfo?.memberIsTrainer || false;
    if (!memberNm || memberNm == "") {
        // let modalInfoView = document.getElementById('modalUserInfoForm');
        // $(modalInfoView).modal('show');
    } else {
        //letsTrain(memberNm);//, memberIsTrainer);
    }

}

interface StringMap {
  [key: string]: string;
}

export function getUrlVars(url: string): StringMap {
    var vars = {} as StringMap;
    url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(key,value): string {
        vars[key] = value;
        return "";
    });
    return vars;
}
