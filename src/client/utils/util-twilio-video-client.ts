import { ConnectOptions } from "twilio-video";

const TwilioVideo = require("twilio-video");

export async function awaitTwilioConnection(connectionOptions: ConnectOptions, twilioJwtToken: string) {

    // Join the Studio with the token from the server and the LocalParticipant's Tracks.

    let bConnected = false;
    while(!bConnected) {
        try {
            let studio = await TwilioVideo.connect(twilioJwtToken, connectionOptions);
            if (studio) {
                bConnected = true;
                let mySid = studio.localParticipant.sid;
                console.log("You joined studio '" + studio.sid + "' as '" + mySid + "'");
            } else {
                console.log("Failed to connect, no error, but studio object is invalid. hmmmm...", studio);
            }
        } catch(errConnecting) {
            console.error("Failed to connect, got error", errConnecting);
            try {
                await sleep(1000);
            } catch(errSleeping) {
                console.error("Sleeping error", errSleeping);
            }
        }

    }          
}

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}   

