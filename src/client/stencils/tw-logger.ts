
import { DateTime } from '../dt';
import { POST } from './../HTTP';

export type LOG_LEVELS = 'debug'|'info'|'warn'|'error';

export class TwLogger {
    
    private roomName: string;
    private userIdentity: string;

    private logLevel: LOG_LEVELS   = TwLogger.DEFAULT_LOG_LEVEL;

    static DEBUG_LEVEL: LOG_LEVELS = 'debug';
    static INFO_LEVEL:  LOG_LEVELS = 'info';
    static WARN_LEVEL:  LOG_LEVELS = 'warn';
    static ERROR_LEVEL: LOG_LEVELS = 'error';

    static DEFAULT_LOG_LEVEL: LOG_LEVELS = TwLogger.WARN_LEVEL;


    static parseLogLevel(level: string): ('debug'|'info'|'warn'|'error') {

        if (!level || level === null || level === "") {
            level = 'warn';
        }

        level = level.toLowerCase().trim()

        if (level === TwLogger.DEBUG_LEVEL) {
            return 'debug';
        } else if (level === TwLogger.INFO_LEVEL) {
            return 'info';
        } else if (level === TwLogger.WARN_LEVEL) {
            return 'warn';
        } else if (level === TwLogger.ERROR_LEVEL) {
            return 'error';
        } else {
            console.warn(`TwLogger::getLogLevel was a logging level: ${level} which does not match any valid log levels: ('debug'|'info'|'warn'|'error'). Defaulting to 'warn'.`);
            return 'warn';
        }
    }

    getLogLevel(): string {
        return this.logLevel;
    }

    setLogLevel(clientLogLevel: ('debug'|'info'|'warn'|'error')): TwLogger {
        this.logLevel = clientLogLevel;
        return this;
    }
 
    setRoomName(roomNm: string): TwLogger {
        this.roomName = roomNm;
        return this;
    }

    setUserIdentity(userIdentity: string): TwLogger {
        this.userIdentity = userIdentity;
        return this;
    }

    getBrowserInfo(): any {
        let browser = this.determineBrowser();
        let clientInfo = {
            "browserNm": browser.name,
            "browserVer": browser.version,
            "platform": navigator.platform, 
            "userAgent": navigator.userAgent,
            "appVersion": navigator.appVersion,

        }

        return clientInfo;
    }

    determineBrowser(): {"name": string, "version": string} {
        var ua = navigator.userAgent, tem, 
            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if(/trident/i.test(M[1])){
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return {name:'IE',version:(tem[1] || '')};
        }
        if(M[1]=== 'Chrome'){
            tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
            if(tem != null) return {name:tem[1].replace('OPR', 'Opera'),version:tem[2]};
        }
        M = M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
        if((tem = ua.match(/version\/(\d+)/i))!= null)
            M.splice(1, 1, tem[1]);

        return {name:M[0], version:M[1]};
    }


    logDebug(...debugObjs: any[]): void {
    
        if (this.logLevel === TwLogger.DEBUG_LEVEL) {
            console.debug(DateTime.now().getIsoFormat(), ...debugObjs);
            this.submitMsgLogs('debug', ...debugObjs);
        }
    }

    logInfo(...infoObjs: any[]): void {
        if (this.logLevel === TwLogger.INFO_LEVEL || this.logLevel === TwLogger.DEBUG_LEVEL) {
            console.info(DateTime.now().getIsoFormat(), ...infoObjs);
            this.submitMsgLogs('info', ...infoObjs);
        }
    }

    logWarn(...objs: any[]): void {
        console.warn(DateTime.now().getIsoFormat(), ...objs);
        this.submitMsgLogs('warn', ...objs);
    }

    logError(...objs: any[]): void {
        console.error(DateTime.now().getIsoFormat(), ...objs);
        console.trace();
        this.submitMsgLogs('error', ...objs);
    }

    submitMsgLogs(msgSeverity: ('debug'|'info'|'warn'|'error'), ...msgs: any[]) {

        POST("/logmsg", { "msgSeverity": msgSeverity, "roomNm": this.roomName, "userNm": this.userIdentity, "clientInfo": this.getBrowserInfo(), "msgs": msgs})
            .fail((jqXHR, sTextStatus, sErrorThrown) => {
                console.error(`Unable to send ${msgSeverity} message to server `, sTextStatus, sErrorThrown, jqXHR);
            });

    }
    
  
}
