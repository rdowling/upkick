import { Component, h } from '@stencil/core';


@Component({
    tag: 'tw-icons',
    styleUrl: 'tw-icons.css'
})
export class TwIcons {

    
    // https://tablericons.com/
    // https://icons.theforgesmith.com/

    static DEFAULT_STROKE = "#010101";
    static DEFAULT_WIDTH = "36"
    static DEFAULT_HEIGHT = "36";

    static validateStroke(stroke? :string): string {
        if (!stroke || stroke === null || stroke === 'undefined' || stroke === '') {
            return TwIcons.DEFAULT_STROKE;
        } else {
            return stroke;
        }
    }

    static viewIcon(stroke:string): string {
        stroke = TwIcons.validateStroke(stroke);
        return (
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" stroke-width="2.5" stroke={stroke} fill="none" class="duration-300 transform transition-all" width={TwIcons.DEFAULT_WIDTH} height={TwIcons.DEFAULT_HEIGHT}><path d="M53.79 33.1a.51.51 0 000-.4c-.96-1.81-8.5-15.53-21.79-15.86S11 30.61 9.92 32.65a.48.48 0 000 .48C11.1 35.06 19.35 48.05 29.68 49c11.39 1 20.63-7 24.11-15.9z"></path><circle cx="31.7" cy="32.76" r="6.91"></circle></svg>
        );
    }

    static disabledViewIcon(stroke? :string) {
        stroke = TwIcons.validateStroke(stroke);
        return (
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" stroke-width="2.5" stroke={stroke} fill="none" class="duration-300 transform transition-all" width={TwIcons.DEFAULT_WIDTH} height={TwIcons.DEFAULT_HEIGHT}><path d="M20.61 43.86c.62.69 5.28 3.1 5.28 3.1a15.19 15.19 0 004.45 1.14c11.39 1 20.62-7 24.1-15.89a.49.49 0 000-.41C53.49 30 46 16.27 32.66 15.94s-21 13.77-22.08 15.81a.5.5 0 000 .49 49.49 49.49 0 007.78 9.62"></path><circle cx="32.35" cy="31.87" r="6.91"></circle><path d="M54.48 10L10.74 53.74"></path>
            </svg>
        );
    }

    static eyeIcon(stroke? :string) {
        stroke = TwIcons.validateStroke(stroke);
        return (
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-eye" width={TwIcons.DEFAULT_WIDTH} height={TwIcons.DEFAULT_HEIGHT} viewBox="0 0 24 24" stroke-width="1.5" stroke={stroke} fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z"/>
                <circle cx="12" cy="12" r="2" />
                <path d="M2 12l1.5 2a11 11 0 0 0 17 0l1.5 -2" />
                <path d="M2 12l1.5 -2a11 11 0 0 1 17 0l1.5 2" />
            </svg>
        );
    }

    static earIcon(stroke? :string) {
        stroke = TwIcons.validateStroke(stroke);
        return (
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-ear" width={TwIcons.DEFAULT_WIDTH} height={TwIcons.DEFAULT_HEIGHT} viewBox="0 0 24 24" stroke-width="1.5" stroke={stroke} fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z"/>
                <path d="M6 10a7 7 0 1 1 13 3.6a10 10 0 0 1 -2 2a8 8 0 0 0 -2 3  a4.5 4.5 0 0 1 -6.8 1.4" />
                <path d="M10 10a3 3 0 1 1 5 2.2" />
            </svg>
        );
    }

    static earIconCrossedOut(stroke? :string) {
        stroke = TwIcons.validateStroke(stroke);
        return (
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-ear" width={TwIcons.DEFAULT_WIDTH} height={TwIcons.DEFAULT_HEIGHT} viewBox="0 0 24 24" stroke-width="1.5" stroke={stroke} fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z"/>
                <line x1="18" y1="6" x2="6" y2="18" />
                <line x1="6" y1="6" x2="18" y2="18" />
                <path stroke="none" d="M0 0h24v24H0z"/>
                <path d="M6 10a7 7 0 1 1 13 3.6a10 10 0 0 1 -2 2a8 8 0 0 0 -2 3  a4.5 4.5 0 0 1 -6.8 1.4" />
                <path d="M10 10a3 3 0 1 1 5 2.2" />
            </svg>
        );
    }

    static micOff(color:string='white' , size:string='sm') {
        return <box-icon name='microphone-off' color={color} size={size} ></box-icon>
    }

    static micOn(color:string='white' , size:string='sm') {
        return <box-icon name='microphone' type='solid' flip='horizontal' color={color} size={size}  ></box-icon>
    }

    static camera(color:string='white', size:string='sm') {
        return <box-icon name='camera' flip='horizontal' color={color} size={size} ></box-icon>
    }

    static trash(color:string='white', size:string='sm') {
        return <box-icon name='trash' color={color} size={size} flip='horizontal' ></box-icon>
    }

    static arrowsExpand(color:string='white', size:string='sm') {
        return <box-icon name='expand' color={color} size={size} ></box-icon>
    }

    static arrowsCollapse(color:string='white', size:string='sm') {
        return <box-icon name='collapse' color={color} size={size} ></box-icon>;
    }

    static eraserIcon(color:string='white') {
        color = TwIcons.validateStroke(color);
        // <box-icon name='eraser' type='solid' ></box-icon>
        return (
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-eraser" width={TwIcons.DEFAULT_WIDTH} height={TwIcons.DEFAULT_HEIGHT} viewBox="0 0 24 24" stroke-width="1.5" stroke={color} fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z"/>
                <path d="M19 19h-11l-4 -4a1 1 0 0 1 0 -1.41l10 -10a1 1 0 0 1 1.41 0l5 5a1 1 0 0 1 0 1.41l-9 9" />
                <path d="M18 12.3l-6.3 -6.3" />
            </svg>
        );
    }

    static squarexIcon(stroke? :string) {
        stroke = TwIcons.validateStroke(stroke);

        return (
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-square-x" width={TwIcons.DEFAULT_WIDTH} height={TwIcons.DEFAULT_HEIGHT} viewBox="0 0 24 24" stroke-width="1.5" stroke={stroke} fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z"/>
                <rect x="4" y="4" width="16" height="16" rx="2" />
                <path d="M10 10l4 4m0 -4l-4 4" />
            </svg>
        );
    }

    static xIcon(stroke? :string) {
        stroke = TwIcons.validateStroke(stroke);

        return (
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-x" width={TwIcons.DEFAULT_WIDTH} height={TwIcons.DEFAULT_HEIGHT} viewBox="0 0 24 24" stroke-width="1.5" stroke={stroke} fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z"/>
                <line x1="18" y1="6" x2="6" y2="18" />
                <line x1="6" y1="6" x2="18" y2="18" />
            </svg>
        );
    }

    static borderAllIcon(stroke? :string) {
        stroke = TwIcons.validateStroke(stroke);

        return (
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-border-all" width={TwIcons.DEFAULT_WIDTH} height={TwIcons.DEFAULT_HEIGHT} viewBox="0 0 24 24" stroke-width="1.5" stroke={stroke} fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z"/>
                <rect x="4" y="4" width="16" height="16" rx="2" />
                <line x1="4" y1="12" x2="20" y2="12" />
                <line x1="12" y1="4" x2="12" y2="20" />
            </svg>
        );
    }

    static colorSwatchIcon(stroke? :string) {
        stroke = TwIcons.validateStroke(stroke);

        return (
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-color-swatch" width={TwIcons.DEFAULT_WIDTH} height={TwIcons.DEFAULT_HEIGHT} viewBox="0 0 24 24" stroke-width="1.5" stroke={stroke} fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z"/>
                <path d="M19 3h-4a2 2 0 0 0 -2 2v12a4 4 0 0 0 8 0v-12a2 2 0 0 0 -2 -2" />
                <path d="M13 7.35l-2 -2a2 2 0 0 0 -2.828 0l-2.828 2.828a2 2 0 0 0 0 2.828l 9 9" />
                <path d="M7.3 13h-2.3a2 2 0 0 0 -2 2v4a2 2 0 0 0 2 2h12" />
                <line x1="17" y1="17" x2="17" y2="17.01" />
            </svg>
        );
    }

    static brightnessUpIcon(stroke? :string) {
        stroke = TwIcons.validateStroke(stroke);

        return(
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brightness-up" width={TwIcons.DEFAULT_WIDTH} height={TwIcons.DEFAULT_HEIGHT} viewBox="0 0 24 24" stroke-width="1.5" stroke={stroke} fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z"/>
                <circle cx="12" cy="12" r="3" />
                <line x1="12" y1="5" x2="12" y2="3" />
                <line x1="17" y1="7" x2="18.4" y2="5.6" />
                <line x1="19" y1="12" x2="21" y2="12" />
                <line x1="17" y1="17" x2="18.4" y2="18.4" />
                <line x1="12" y1="19" x2="12" y2="21" />
                <line x1="7" y1="17" x2="5.6" y2="18.4" />
                <line x1="6" y1="12" x2="4" y2="12" />
                <line x1="7" y1="7" x2="5.6" y2="5.6" />
            </svg>
        );
    
    }

    static brightenIcon(iconClasses:string = "") {
        return (
            <img class={`${iconClasses}`} src="https://img.icons8.com/material-outlined/24/000000/idea.png"/>
        );
    }

    static darkenIcon(iconClasses:string = "") {
        return (
            <img class={`${iconClasses}`} src="https://img.icons8.com/material-outlined/24/000000/light-on--v1.png"/>
        );
    }


    static squigglyLineIcon(iconClasses: string = '') {
        return (
            <img class={`${iconClasses}`} src="https://img.icons8.com/material-sharp/24/000000/squiggly-line.png"/>
        );
    }

    static lineIcon(iconClasses: string = '') {
        return ( 
            <img class={`${iconClasses}`} src="https://img.icons8.com/material-outlined/24/000000/line.png"/>
        );
    }

    static circleIcon(iconClasses: string = '') {
        return (
            <img class={`${iconClasses}`} src="https://img.icons8.com/material-outlined/24/000000/circled.png"/>
        );
    }

    static arrowUpRightIcon(iconClasses: string = '') {
        return (
            <img class={`${iconClasses}`} src="https://img.icons8.com/material-outlined/24/000000/up-right-arrow.png"/>
        );
    }

    static pencilEraserIcon(iconClasses: string = '') {
        return (
            <img class={`${iconClasses}`} src="https://img.icons8.com/material-outlined/24/000000/eraser.png"/>
        );
    }

    static garbageCanIcon(iconClasses: string = '') {
        return (
            <img class={`${iconClasses}`} src="https://img.icons8.com/material-outlined/24/000000/delete-forever.png"/>
        );
    }

    static broomIcon(iconClasses: string = '') {
        return (
            <img class={`${iconClasses}`} src="https://img.icons8.com/material-outlined/24/000000/broom.png"/>
        );
    }

    static gridToggleIcon(iconClasses:string="") {
        return (
            <img class={`${iconClasses}`} src="https://img.icons8.com/material-outlined/24/000000/view-module.png"/>
        );
    }

    static linesTogetherIcon(iconClasses:string = "") {
        return (
            <img class={`${iconClasses}`} src="https://img.icons8.com/material-outlined/24/000000/merge-horizontal.png"/>
        );
    }

    static linesApartIcon(iconClasses:string = "") {
        return (
            <img class={`${iconClasses}`} src="https://img.icons8.com/material-outlined/24/000000/split-vertical.png"/>
        );
    }

    static configureGridIcon() {
        return (
            <img src="https://img.icons8.com/material-outlined/24/000000/table-properties.png"/>
        );
    }

    render() {
        return (
            <div>
                <p>Hello TwIcons!</p>
            </div>
        );
    }
}
