
import { Component, Prop, State, Event, EventEmitter, h } from '@stencil/core';

@Component({
  tag: 'my-dropdown',
  styleUrls: ['./dropdown.scss'],
  scoped: true
})
export class Dropdown {
  @Prop() dropTitle: string = '';

  // Data/state that can change in the component should use the state decorator
  @State() isToggled: boolean = false;

  // our custom event for other components to listen to
  @Event() toggle: EventEmitter;

  render() {
    return (
      <div>
        <button onClick={() => this.toggleClick()}>
          {this.dropTitle} {this.isToggled ? <span>&#9650;</span> : <span>&#9660;</span>}
        </button>

        <div style={{ display: this.isToggled ? 'block' : 'none' }}>
          <slot></slot>
        </div>
      </div>
    )
  }

  toggleClick() {
    this.isToggled = !this.isToggled;
    // When the user click emit the toggle state value
    // A event can emit any type of value
    this.toggle.emit({ visible: this.isToggled });
  }
}
