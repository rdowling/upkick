import { Component, h, Prop } from "@stencil/core";

@Component({
  tag: 'twl-button',
  styleUrls: ['../style-tw.dist.css', './twl-button.scss'],
  scoped: true
})
export class ButtonComponent {

  @Prop() isBoolean: boolean = false;
  @Prop() aNumber: number = -1;
  @Prop() aString: string = "a"

  componentWillLoad() {
    alert(`componentWillLoad: ${this.isBoolean} | ${this.aNumber} | ${this.aString}`)
  }

  render() {

    /* example from the parent container 
      <twl-button isBoolean={true} aNumber={0} aString={"init-string"}></twl-button> 
    */

    alert(`rendering: ${this.isBoolean} | ${this.aNumber} | ${this.aString}`)
    return (
      <button onClick={() => this.didClick()}class={'w-1/3 self-center flex justify-center mt-1/12 py-2 px-2 border border-transparent font-normal text-lg rounded-md text-white bg-nord9 hover:bg-nord14 focus:outline-none focus:border-nord9 focus:shadow-outline-gray active:bg-nord10 transition duration-150 ease-in-out'}>
        Click to Change
      </button>
    );
  }

  didClick(): void {
    this.isBoolean = !this.isBoolean;
    this.aNumber = this.aNumber+1;
    this.aString = this.aString+"b";
  }
}