import { Component, h } from '@stencil/core';


@Component({
    tag: 'remote-tw-participant',
    styleUrl: 'remote-tw-participant.css'
})
export class RemoteTwParticipant {
    render() {
        return (
            <div>
                <p>Hello RemoteTwParticipant!</p>
            </div>
        );
    }
}
