import { Component, h, Prop, State, Watch, Host } from '@stencil/core';
import { Track, RemoteVideoTrack, LocalVideoTrack, LocalParticipant, RemoteParticipant, LocalAudioTrack, RemoteAudioTrack, AudioTrackPublication, VideoTrackPublication, createLocalVideoTrack, createLocalAudioTrack } from 'twilio-video';
import { TwPoint } from '../../../structs';
import { TwLogger } from '../../tw-logger';
import { TwStudio } from '../tw-studio/tw-studio';
import { Choice } from '../video-chooser/video-choice';
import { getDevicesOfKind } from './media-utils';
import { TwIcons } from '../tw-icons/tw-icons';
import { TwConstants } from '../../tw-constants';
import * as util from '../../../utils/util';
import { DateTime } from '../../../dt';
import { TwToggle } from '../tw-toggle/tw-toggle';

@Component({
    tag: 'tw-participant',
    styleUrl: 'tw-participant.css'
})
export class TwParticipant {

    @Prop() twStudio: TwStudio;
    @Prop() participant: (LocalParticipant|RemoteParticipant) = null;

    @Prop() shortname: string = "";
    @Prop() longname: string = "";

    @Prop() isLocalParticipant: boolean = false;
    @Prop() clientLogLevel: ('debug' | 'info' | 'warn' | 'error') = 'warn';
    @Prop() focusedParticipantSid: string;
    @Prop() isAutoPlayEnabled: boolean = true;
    @Prop({mutable: true}) isRoomMuted: boolean = false;
    @Prop({mutable: true}) imageSnap: string = "";
    @Prop({mutable: true}) isToolbarVisible: boolean = true;
    
    @State() isAudioMuted: boolean = false;
    @State() isMutedBySelf: boolean = false;

    @Prop() _isSharingVideo: boolean = null;
    @Prop() _isSharingAudio: boolean = null;

    participantRef!: HTMLElement;
    videoRef!: HTMLVideoElement;
    audioRef!: HTMLAudioElement;
    overlayCanvasRef!: HTMLCanvasElement;
    snapshotCanvasRef!: HTMLCanvasElement;

    private _logger: TwLogger = null; // new TwLogger();

    private _isDrawing = false;
    private _startX = 0;
    private _startY = 0;
    private _lastX = 0;
    private _lastY = 0;
    private _lines = [];

    private createdDatetime = DateTime.now();

    private videoNeedsAttaching: boolean = false;
    private audioNeedsAttaching: boolean = false;
    @Prop() color: string;
    @Prop() isPIP: boolean = false;
    // private _isVideoShowing: boolean = true;
    

    isShowingSnappedPic(): boolean {
        return this.imageSnap !== "";
    }

    canSynchronizeFocus(): boolean {
        return this.isLocalParticipant && this.isToolbarVisible;
    };


    @Prop({mutable: true}) videoTracks: (LocalVideoTrack|RemoteVideoTrack)[] = [];

    @Watch('videoTracks')
    onVideoTracksChange(newTracks: (LocalVideoTrack|RemoteVideoTrack)[], oldTracks: (LocalVideoTrack|RemoteVideoTrack)[]) {
        
        if (oldTracks && oldTracks.length >= 0) {
            for(let oldTrack of oldTracks) {
                if (!newTracks.includes(oldTrack)) {
                    this.logInfo('tw-participant::onVideoTracksChange', `old video track ${oldTrack.name} detaching from dom`);
                    oldTrack.detach();
                }
            }
        }

        if (newTracks && newTracks.length >= 1) {
            for(let newTrack of newTracks) {
                if (!oldTracks.includes(newTrack)) {
                    this.logInfo('tw-participant::onVideoTracksChange', `new video track found! marking ${newTrack.name} for dom attachment after render`);
                    this.videoNeedsAttaching = true;
                }
            }

        }

    }
     
    @Prop({mutable: true}) audioTracks: (LocalAudioTrack|RemoteAudioTrack)[] = [];
    
    @Watch('audioTracks')
    onAudioTracksChange(newTracks: (LocalAudioTrack|RemoteAudioTrack)[], oldTracks: (LocalAudioTrack|RemoteAudioTrack)[]) {
        
        if (oldTracks && oldTracks.length >= 0) {
            for(let oldTrack of oldTracks) {
                if (!newTracks.includes(oldTrack)) {
                    this.logInfo('tw-participant::onAudioTracksChange', `old audio track ${oldTrack.name} detaching from dom`);
                   oldTrack.detach();
                }
            }
        }

        if (newTracks && newTracks.length >= 1) {
            for(let newTrack of newTracks) {
                if (!oldTracks.includes(newTrack)) {
                    this.logInfo('tw-participant::onAudioTracksChange', `new audio track found! marking ${newTrack.name} for dom attachment after render`);
                    this.audioNeedsAttaching = true;
                }
            }

        }
    }

    attachVideoTracks() {
        
        if (this.videoTracks.length >= 1) {
            let videoTrack = this.videoTracks[0];
            if (videoTrack) {
                this.logInfo(`attaching video track of ${this.participant.identity} to dom`, this.videoRef);
                videoTrack.attach(this.videoRef);
                // this._isVideoShowing = true;
                this.videoNeedsAttaching = false;
            }

        }

    }

    attachAudioTracks() {
        if (this.audioTracks.length >= 1) {
            let audioTrack = this.audioTracks[0];
            if (audioTrack) {
                if (!this.isLocalParticipant) {
                    this.logInfo(`attaching audio track to dom`);
                    audioTrack.attach(this.audioRef);
                    this.audioNeedsAttaching = false;
                }
            }
        }

    }

    componentWillLoad() {
        this.logDebug(`Participant ${this.participant.identity} componentWillLoad current createdDateTime is ${this.createdDatetime.getIsoFormat()}`);
    }

    connectedCallback() {  // TODO: use componentWillLoad() or connectedCallback()
        
        this._logger = new TwLogger();
        this._logger.setLogLevel(this.clientLogLevel)
                    .setUserIdentity(this.participant.identity)
                    .setRoomName(this.twStudio?.room?.name);

        if (!this.participant) {
            this.logWarn("Warning, the participant is not set on tw-participant member");
        } else {

            if (this.isMutedBySelf) {
                this.muteLocalParticipant();
            } else if (this.isRoomMuted && !this.isTrainer()) {
                this.muteByTrainer();
            } else {
                this.enableAudioTracks();
            }

            if (this.twStudio) {
                this.twStudio.registerTwParticipant(this);
            }
             
            this.videoNeedsAttaching = (this.videoTracks.length > 0);
            this.audioNeedsAttaching = (this.audioTracks.length > 0);

            this.logDebug(`TwParticipant::connectedCallback on participant ${this.participant.identity}; video needs attaching? ${this.videoNeedsAttaching}; audio needs attaching? ${this.audioNeedsAttaching}`);

        }

        if (this.isLocalParticipant) {
            this.ensureMediaPermissions()
                .then(() => {
                    return getDevicesOfKind("video");
                })
                .then((videoDevices: MediaDeviceInfo[]) => {
                    this.logInfo(`Local Participant video-devices.  Count: ${videoDevices.length}`, videoDevices);
                    this.videoDevices = videoDevices;
                })
                .then(() => {
                    //TODO: could use the tw-participant here instead
                    let localParticipant = this.getLocalParticipant();
                    let participantView = document.getElementById(this.getSid());

                    if (participantView && localParticipant) {
                        if (localParticipant.videoTracks.size == 0) {
                            this.startLocalVideo(localParticipant);
                        }
                        if (localParticipant.audioTracks.size == 0) {
                            this.startLocalAudio(localParticipant);
                        }
                    }
                })
                .catch((err) => {
                    this.logError("Error getting local media!", err);
                    alert(err);
                });
        }

    }

    disconnectedCallback() {
        this.logDebug(`participant ${this.participant?.identity} disconnectedCallback`);
        
        if (this.twStudio) {
            this.logInfo(`unregistering participant ${this.participant?.identity} with TwStudio members`);
            this.twStudio.unregisterTwParticipant(this);
        }

        if (this.videoRef) {
            this.videoRef.removeEventListener('resize', this.onVideoElementResize);
            this.videoRef = null;
        }

        if (this.audioRef) {
            this.audioRef = null;
        }

        if (this.overlayCanvasRef) {
            this.removeDrawingEventListeners();
            this.overlayCanvasRef = null
        }

        if (this.snapshotCanvasRef) {
            this.snapshotCanvasRef = null;
        }
        
        this.setVideoTracks([]);
        this.setAudioTracks([]);
        // this.participant.removeAllListeners();
    }

    getVideoIcon() {
        if (this.isShowingVideo()) {
            return (<box-icon color="black" name="video" type="solid"></box-icon>);
        } else {
            return (<box-icon color="black" border='circle' name="video-off"></box-icon>);
        }
    }

    getAudioIcon() {
        if (this.isSharingAudio()) {
            return (<box-icon color="black" name="microphone" type="solid"></box-icon>);
        } else {
            return (<box-icon color="black" border='circle' name="microphone-off"></box-icon>);
        }
    }

    isShowingVideo(): boolean {
        if (this._isSharingVideo !== null) {
            return this._isSharingVideo;
        } else {
            return (this.videoTracks && this.videoTracks != null && this.videoTracks.length > 0);
        }
    }

    isSharingAudio(): boolean {
            //if (showAlert) alert(this.audioTracks.length);
        return (this.audioTracks && this.audioTracks != null && this.audioTracks.length > 0 && this.audioTracks[0].isEnabled);
    }

    onVideoElementResize = () => {

        if (this.participant && this.videoRef && this.twStudio) {

            this.logInfo(`Participant: ${this.participant.identity} video element resizing now width: ${this.videoRef.clientWidth}, height: ${this.videoRef.clientHeight}`);
            this.twStudio.resizeParticipants();

            if (this.isShowingSnappedPic()) {
                this.attachImage(this.imageSnap);
            }
        }

    }

    componentDidLoad() {
        this.logInfo(`TwParticipant::componentDidLoad() for participant identity: ${this.participant?.identity}, sid: ${this.participant?.sid}`);

        setTimeout(() => {
            this.participantRef?.classList.add('is-showing');
        }, 1500);

        if (this.participant && this.videoRef) {
            this.videoRef.addEventListener('resize', this.onVideoElementResize);
        }

        this.resizeParticipant();

        if (this.isShowingSnappedPic()) {
            this.attachImage(this.imageSnap);
        }

        if (this.isToolbarVisible) {
            this.setupForDrawing();
        }
    }

    resizeParticipant() {
        
        let videoElement = this.videoRef;
        if (videoElement && videoElement.parentElement) {
            let parentElement = videoElement.parentElement;
            
            let participantElementWidth  = parentElement.clientWidth;
            let participantElementHeight = parentElement.clientHeight;

            let participantAspectRatio = participantElementWidth / participantElementHeight;

            if (participantElementWidth && participantElementHeight) { 

                /* below would set video element equal to intrinsic (unstretched, natural) width & height */
                // videoRef.style.width = participantElementWidth.toString()+'px';
                // videoRef.style.height = participantElementHeight.toString()+'px';

                //sizeElement(parentElement, participantElementWidth, participantElementHeight);

                // if video aspect w/h ratio is less than parent's w/h ratio => less room to expand height so set video height = 100%;
                let videoElementAspectRatio = videoElement.videoWidth / videoElement.videoHeight;

                if (videoElementAspectRatio < participantAspectRatio) {
                    videoElement.style.height = '100%';
                    videoElement.style.width  = 'auto';
                } else {
                    videoElement.style.width = '100%';
                    videoElement.style.height  = 'auto';
                }

                this.resizeCanvases(videoElement);
            }

            if (this.isShowingSnappedPic()) {
                this.attachImage(this.imageSnap);
            }

        }
    }

    resizeCanvases(videoElement: HTMLVideoElement) {
        if (!videoElement) {
            return;
        }

        let parentElement = videoElement.parentElement;

        let participantElementWidth  = parentElement.clientWidth;
        let participantElementHeight = parentElement.clientHeight;

        if (participantElementWidth && participantElementHeight) {
            let canvases = parentElement.querySelectorAll("canvas");
            let aCanvases = Array.from(canvases) as HTMLCanvasElement[];
            for(const canvasElement of aCanvases) {
                this.sizeElement(canvasElement, videoElement.offsetWidth, videoElement.offsetHeight);
            } 
        }
    }

    sizeElement(element: HTMLCanvasElement, setWidth: number, setHeight: number) {
        if (setHeight && setHeight > 0 ) {
            element.height = setHeight;
            element.style.height = setHeight+'px';
        }

        if (setWidth && setWidth > 0) {
            element.width = setWidth;
            element.style.width = setWidth+'px';
        }

        return element;
    }

    getVideoTracks(): Map<Track.SID, VideoTrackPublication> {
        return this.participant?.videoTracks || new Map();
    }

    getAudioTracks():  Map<Track.SID, AudioTrackPublication> {
        return this.participant?.audioTracks || new Map();
    }

    componentDidRender() {
        this.logInfo('componentDidRender');
        if (this.videoNeedsAttaching) {
            this.attachVideoTracks()
        }

        if (this.audioNeedsAttaching && !this.isLocalParticipant) {
            this.attachAudioTracks()
        }

        this.twStudio.resizeParticipants();  // TODO: ?
        if (this.isShowingSnappedPic()) {
            this.attachImage(this.imageSnap);
        }
    }

    setVideoTracks(videoTracks: (LocalVideoTrack|RemoteVideoTrack)[]) {
        this.videoTracks = videoTracks;
    }

    setAudioTracks(audioTracks: (LocalAudioTrack|RemoteAudioTrack)[]) {
        this.audioTracks = audioTracks;
    }

    getAudioTrackPub(trackSid?: string): AudioTrackPublication {
        let trackPub = null as AudioTrackPublication;

        let mTrackPubs = this.getAudioTracks();
        if (!mTrackPubs) {
            mTrackPubs = new Map();
        }

        if (trackSid) {
            trackPub = mTrackPubs.get(trackSid);
        } else {
            let aTrackPubs = Object.values(mTrackPubs);
            if (aTrackPubs && aTrackPubs.length>0) {
                trackPub = aTrackPubs[0];
            }
        }

        return trackPub;
    }

    getVideoTrackPub(trackSid?: string): VideoTrackPublication {
        let trackPub = null as VideoTrackPublication;

        let mTrackPubs = this.getVideoTracks();
        if (!mTrackPubs) {
            mTrackPubs = new Map();
        }

        if (trackSid) {
            trackPub = mTrackPubs.get(trackSid);
        } else {
            let aTrackPubs = Object.values(mTrackPubs);
            if (aTrackPubs && aTrackPubs.length>0) {
                trackPub = aTrackPubs[0];
            }
        }

        return trackPub;
    }

    render() {
        
        if (this.isPIP) {

            // this.isSharingAudio();

            return (
                <li style={{'display': 'flex', 'flex-flow': 'column', 'min-height': '80px'}} class="col-span-1 overflow-hidden bg-white border border-gray-200 rounded-md shadow-sm">
                    
                    <div style={{'display': 'flex', 'flex': '2'}} class={`overflow-hidden items-center justify-center bg-${this.color}-400`}>
                        <span class={(this.isShowingVideo()? 'hidden ': '')+'text-white text-center text-xl leading-5 font-medium'}>{this.shortname}</span>
                        <video class={'pip_participant_video'+' '+(!this.isShowingVideo()? 'hidden'+' ': '')} ref={(elVideo) => this.videoRef = elVideo as HTMLVideoElement}></video>
                        <audio ref={(elAudio) => this.audioRef = elAudio as HTMLAudioElement}></audio>
                    </div>

                    <div style={{'display': 'flex', 'justify-content': 'space-between'}} class="overflow-hidden items-center">
                        <span style={{'display': 'flex'}} class='ml-2 text-sm'>
                            {this.longname}
                        </span>
                        <div style={{}} class='mr-2 sm:flex-col flex' >
                            <div class='m-1'>
                                {this.getAudioIcon()}
                            </div>
                            <div class='m-1'>
                                {this.getVideoIcon()}
                            </div>
                        </div>
                    </div>

                
                </li>
            );

        } else {
            
            let sDisplayNm = this.getDisplayName();
                
            return (
                <Host id={this.getSid()} ref={(participantView)=>this.participantRef = participantView} class={`tw-participant participant ${this.getSid()} rounded-md shadow`} >
                    <div class={`absolute mt-4 inline-flex flex-col`}>
                        <div class={`name participant-name-banner z-50 inline m-0 py-2 pr-2 pl-3`} style={{"background": "rgba(242, 47, 70, 0.8)", "color": "#fff", "font-size": "16px", "line-height": "1"}}> 
                            {sDisplayNm}
                            {this.isTrainer()? <span class=" ml-2 bg-cool-gray-50 text-nord2 font-medium leading-4 px-2 py-1 rounded-full text-sm">Trainer</span> : ""}
                        </div>

                        <div class={`mt-4 ml-2 p-2 w-auto z-50 ${!this.isShowingVideo()?"hidden":""}`} >
                            { this.getSnapshotIcons() }
                            { this.getAudioIcons() }
                            {/* { this.isSelectiveSubscriptionsActive ? this.getSelectiveSubscriptionsIcons() : null } */}
                            { this.getFocusIcons() }
                            { this.getSynchronizeFocusIcons()}
                        </div>
                    </div>

                    <video  class={'studio_video rounded-md' + (this.showBorders()?' --border-blue':'')} ref={(elVideo) => this.videoRef = elVideo as HTMLVideoElement} > </ video >
                    <audio  class={'studio_audio' + (this.showBorders()?' --border-grey':'')} ref={(elAudio) => this.audioRef = elAudio as HTMLAudioElement} autoPlay={this.isAutoPlayEnabled} ></audio>
                    <canvas class={'studio_canvas snap-canvas rounded-md' + (this.showBorders()?' --border-yellow':'')} ref={(elCanvas) => this.snapshotCanvasRef = elCanvas as HTMLCanvasElement }> </ canvas >
                    <canvas class={'studio_canvas overlay-canvas rounded-md' + (this.showBorders()?' --border-green':'')} ref={(elCanvas) => this.overlayCanvasRef = elCanvas as HTMLCanvasElement }> </ canvas >
                </Host>
            );

        }

    }

    isTrainer(): boolean {
        if (this.participant) {
            return this.twStudio.isTrainer(this.participant.sid, this.participant.identity);
        } else {
            return false;
        }
    }

    toggleFocus() {
        if (!this.isFocused()) {
            this.twStudio.focusOnParticipant(this.getSid());
        } else {
            this.twStudio.clearFocusedParticipants();
        }
    }

    isFocused() {
        if (this.twStudio.focusedParticipantSid === "") {
            return false;
        } else if (!this.twStudio) {
            return false;
        } else if (this.getSid() === this.twStudio.focusedParticipantSid) {
            return true;
        }
        return false;
    }

    getFocusIcons() {
        if (!this.isToolbarVisible) {
            return null;
        }

        let icon, label;
        if (this.isFocused() ) {
            icon = TwIcons.arrowsCollapse("white", "md");
            label = 'Collapse View';
        } else {
            icon = TwIcons.arrowsExpand("white", "md");
            label = 'Expand View';
        }

        let lockicon = null;
        let onclick = () => this.toggleFocus();

        if (this.twStudio.isFocusSynched && this.isLocalParticipant) {
            if (this.isTrainer) {
                lockicon = (<box-icon color='white' name='lock-alt'></box-icon>);
                label = 'In-Sync';
            } else {
                lockicon = (<box-icon type='solid' color='white' name='lock-alt'></box-icon>);
                label = 'Locked';
                onclick = () => {};  // pop up temporary sign
            }
        }

        return (
            <div class='relative flex mb-2' onClick={onclick}>
                <div class='ml-2 self-center justify-center'>
                    { icon }
                </div>
                <div class="flex ml-2 self-center justify-center text-xs leading-5 font-medium rounded-md text-white">
                    <div class='self-center justify-center text-xs leading-5 font-semibold rounded-md text-white'>{ label }</div>
                    <div class='self-center justify-center text-xs leading-5 font-semibold rounded-md text-white'>{ lockicon }</div>
                </div>
            </div>
        );

    }

    getSynchronizeFocusIcons() {

        if (!this.canSynchronizeFocus()) {
            return null;

        } else {

            return (
                <div class='relative flex mb-2'>
                    <div class='ml-1 flex justify-center'>
                        <tw-toggle class='mx-auto' is-on={this.twStudio?.isFocusSynched} onToggled={(isOn:boolean, toggle:TwToggle) => this.twStudio.setIsFocusSynched(isOn, toggle)} isOnColor={'bg-indigo-600'} isOffColor={'bg-gray-200'}></tw-toggle>
                    </div>
                    <div class='ml-2 self-center justify-center text-xs leading-5 font-medium rounded-md text-white'>
                        Sync Views With Yours
                    </div>
                </div>
            );
        }
    }

    getSnapshotIcons() {

        if (!this.isToolbarVisible || !(this.isFocused() || this.focusedParticipantSid === "")) {
            return null;
        }

        let icon, label, onclick;
        if (this.isShowingSnappedPic()) {
            icon = TwIcons.trash("white", "md");
            onclick = () => this.clearSnappedPic();
            label = 'Clear Pic';
        } else {
            icon = TwIcons.camera("white", "md");
            onclick = () => this.snapPic();
            label = 'Take Pic';
        }

        if (this.isToolbarVisible && (this.isFocused() || this.focusedParticipantSid === "")) {
            return (
                < div class='relative flex mb-2' onClick={onclick}>
                    <div class="ml-2 self-center justify-center">{ icon }</div>
                    <span class="ml-2 self-center justify-center text-xs leading-5 font-semibold rounded-md text-white">
                        { label }
                    </span>
                </div>
            );
        }
    }


    // getAudioIcons_old() {
    //     if (this.isParticipantAudioMuted()) {
    //         return (
    //             < div class={`flex relative text-white`} onClick={() => this.toggleParticipantAudioMute()}>
    //                 <div>{ TwIcons.unmuteIcon("#FFFFFF") }</div>
    //                 <div class='flex justify-center items-center text-sm'> { this.isRoomMuted? "(trainer)": (this.isMutedBySelf? "(self)" : "")} </div>
    //             </div>
    //         );
            
    //     } else {
    //         return (
    //             < div class='relative inline' onClick={() => this.toggleParticipantAudioMute()}>
    //                 { TwIcons.muteIcon("#FFFFFF") }
    //             </div>
    //         );
    //     }

    // }

    getAudioIcons() {

        let icon, label;
        label = "";
        if (this.isParticipantAudioMuted()) {
            icon =  TwIcons.micOff("white", "md");
            if (this.isRoomMuted) {
                label = "Muted (Trainer)";
            } else if (this.isMutedBySelf) {
                label = "Muted (Self)"
            }
        } else {
            icon = TwIcons.micOn("white", "md");
            if (this.isLocalParticipant) {
                label = "Mute Self";
            }
        }

        return (
            <div class='relative flex mb-2' onClick={() => this.toggleParticipantAudioMute()}>
                <div class='ml-2 self-center justify-center'>{ icon }</div>
                <span class='ml-2 self-center justify-center text-xs leading-5 font-semibold rounded-md text-white'>
                    { label }
                </span>
            </div>
        );

    }

    getLocalParticipant(): LocalParticipant {
        if (this.isLocalParticipant) {
            return this.participant as LocalParticipant;
        } else {
            return null;
        }
    }

    ensureMediaPermissions() {
        return navigator.mediaDevices
        .enumerateDevices()
        .then( (devices:MediaDeviceInfo[])  => {
            return devices.every( (device:MediaDeviceInfo) => {
                return !(device.deviceId && device.label);
            });
        }).then( (shouldAskForMediaPermissions:boolean) => {
            if (shouldAskForMediaPermissions) {
                return navigator
                    .mediaDevices
                    .getUserMedia({ audio: true, video: true})
                    .then( mediaStream => {
                        mediaStream.getTracks().forEach (track => track.stop());
                    });
            }
        });
    }

    async startLocalVideo(localParticipant: LocalParticipant): Promise<LocalVideoTrack> {

        let videoTrack;
        let videoTrackName = TwStudio.genTrackName(this.twStudio?.getRoomNm(), localParticipant.identity, TwConstants.VIDEO_TRACK_KIND)

        this.logInfo(`(Local) Participant ${localParticipant.identity} is starting Local Video (creating new Local Video Track)`);

        try {
            if (this.preferredCamera) {
                videoTrack = await createLocalVideoTrack({
                    deviceId: {exact: this.preferredCamera.deviceId},
                    name: videoTrackName
                }) as LocalVideoTrack;
            } else {
                videoTrack = await createLocalVideoTrack({
                    name: videoTrackName
                });
            }

            this.logInfo(`(Local) Participant ${localParticipant.identity} is now publishing the newly created local video track ${videoTrack.name} `);
            localParticipant.publishTrack(videoTrack);

            this.setVideoTracks([videoTrack]);

        } catch (err) {
            alert("Unable to start video/audio.  Make sure you don't have another open page using video or audio.");
            this.logError("could not create local track", err);
        }
        
        return videoTrack;

    }

    async startLocalAudio(localParticipant: LocalParticipant): Promise<LocalAudioTrack> {
        
        let audioTrack: LocalAudioTrack;
        let audioTrackName = TwStudio.genTrackName(this.twStudio?.getRoomNm(), localParticipant.identity, TwConstants.AUDIO_TRACK_KIND);

        try {

            audioTrack = await createLocalAudioTrack({
                name: audioTrackName
            });

            if (this.isAudioMuted) {
                audioTrack.disable();
            }
            localParticipant.publishTrack(audioTrack);

            this.setAudioTracks([audioTrack]);
        } catch (err) {
            alert("Unable to start video/audio.  Make sure you don't have another open page using video or audio.");
            this.logError("could not create local track", err);
        }

        return audioTrack;
        
    }

    //TODO: twilio video-quickstart snippet
    takeLocalVideoSnapshot(video, canvas) {
        let context = canvas.getContext('2d');
        context.clearRect(0,0,canvas.width, canvas.height);
        context.drawImage(video, 0, 0, canvas.width, canvas.height);
    }

    snapPic(): void {

        this.resizeCanvases(this.videoRef);
            
        let ctx = this.snapshotCanvasRef.getContext('2d');

        ctx.translate(this.snapshotCanvasRef.width, 0);
        ctx.scale(-1, 1);
        ctx.drawImage(this.videoRef, 0, 0, this.videoRef.clientWidth, this.videoRef.clientHeight);  
        // ctx.drawImage(this.videoRef, 0, 0, this.videoRef.videoWidth, this.videoRef.videoHeight);  

        var imageSnapDataURL = this.snapshotCanvasRef.toDataURL("image/png") as string; // .replace(/^data:image\/png;base64,/, "") <- turns it into png
        
        this.attachImage(imageSnapDataURL);

        this.twStudio.postSnapshotAndNotifyParticipants(this.getSid(), imageSnapDataURL);

    }

    clearSnappedPic() {
        this.clearSnapshotCanvas();
        this.twStudio.notifyParticipantsOfSnapshotCleared(this.getSid());
    }


    clearSnapshotCanvas(): boolean {

        if (this.snapshotCanvasRef) {
            const context = this.snapshotCanvasRef.getContext("2d");
            if (context) {
                context.clearRect(0, 0, this.snapshotCanvasRef.width, this.snapshotCanvasRef.height);
            }
        }

        if (this.imageSnap !== "") {
            this.imageSnap = "";
        }

        return !this.isShowingSnappedPic();
    }

    attachImage(imageSnap: string): void {
        
        this.resizeCanvases(this.videoRef);
        
        // let videoWidth  = this.videoRef.clientWidth; // this.videoRef.videoWidth;
        // let videoHeight = this.videoRef.clientHeight; //this.videoRef.videoHeight;

        // this.snapshotCanvasRef.width = videoWidth;
        // this.snapshotCanvasRef.height = videoHeight;
        
        const snapshotCanvas = this.snapshotCanvasRef;
        let img = new window.Image();
        let videoRef = this.videoRef;
        img.addEventListener("load", function () {
            let ctx = snapshotCanvas.getContext('2d');
            ctx.drawImage(img, 0, 0, videoRef.clientWidth, videoRef.clientHeight);
        });
        img.setAttribute("src", imageSnap);

        if (this.imageSnap !== imageSnap) {
            this.imageSnap = imageSnap;
        }
    }

    showBorders(): boolean {
        return this.twStudio?.showBorders();
    }
    
    getSid(): string {
        if (this.participant) {
            return this.participant.sid;
        } else {
            return "";
        }
    }


    detachTracks(tracks) {
        let isSafari = util.isSafariBrowser();
        // TODO: test this backing out change
        tracks.forEach(function (track) {
            if (isSafari) {
                // Avoid detaching the track and only remove the DOM element for Safari.
                // This avoids the issue that happens because of https://github.com/twilio/twilio-video.js/issues/294
                // We are still uncertain about what side effects this problem ultimately has if particants
                // connect and reconnect multiple times.
                if (track && track._attachments) {
                    track._attachments.forEach(function (element) {
                        element.remove();
                    });
                }
            } else {
                if (track && track.kind && track.kind !== 'data') {
                    track.detach().forEach(function (detachedElement) {
                        detachedElement.remove();
                    });
                }
            }
        });
    }
    
    getMemberId() {
        return this.getSid();
    }

    isScreenSmall() {
        return window.innerWidth <= 800 && window.innerHeight <= 600;
    }

    getIdentity(): string {
        if (this.participant) {
            return this.participant.identity;
        } else {
            return "";
        }
    }

    getShortDisplayName(): string {
        return this.getDisplayName();
    }

    getDisplayName(): string {        
        return TwStudio.getDisplayName(this.getIdentity());
    }

    toggleBlur(blurId, blur) {
        this.logInfo('toggleBlur', blurId, blur);
        let eMember = document.getElementById(this.getSid());
        if (eMember) {
            let elVideo = eMember.querySelector("video") as HTMLVideoElement;
            if (blur) {
                if (!elVideo.classList.contains("blurred")) {
                    elVideo.classList.add("blurred");
                }
            } else {
                if (elVideo.classList.contains("blurred")) {
                    elVideo.classList.remove("blurred");
                }

            }
        }

    }

    setGridOverlay(isGridVis: boolean, gridOverlayStyle: string): boolean {
        let eMember = document.getElementById(this.getSid());
        if (eMember) {
            let eMemberCanvas = eMember.querySelector("canvas");
           
            TwStudio.includeClass(isGridVis, eMemberCanvas, "grid-overlay");

            this.overlayCanvasRef.style.backgroundImage = gridOverlayStyle;
        }

        return isGridVis;
    }

    mousedown = (e) => {
        let pos = { x: e.offsetX, y: e.offsetY };
        this.touchDown(pos.x, pos.y);
    }

    mousemove = (e) => {
        let pos = { x: e.offsetX, y: e.offsetY };
        this.touchMove(pos.x, pos.y);
    }

    mouseup = (e) => {
        let pos = { x: e.offsetX, y: e.offsetY };
        this.touchUp(pos.x, pos.y);
    }

    touchstart = (e) => {
         let pos = this.getTouchPos(e);
        // var touch = e.touches[0];
        this.touchDown(pos.x, pos.y);
    }

    touchmove = (e) => {
        let pos = this.getTouchPos(e);
        this.touchMove(pos.x, pos.y);
    }

    touchend = (e) => {
         let pos = this.getTouchPos(e);
        this.touchUp(pos.x, pos.y);
    }

    setupForDrawing() {
        if (this.overlayCanvasRef && this.isToolbarVisible) {
            this.overlayCanvasRef.addEventListener("mousedown", this.mousedown);
            this.overlayCanvasRef.addEventListener("mousemove", this.mousemove);
            this.overlayCanvasRef.addEventListener("mouseup", this.mouseup);
            this.overlayCanvasRef.addEventListener("touchstart", this.touchstart);
            this.overlayCanvasRef.addEventListener("touchmove", this.touchmove);
            this.overlayCanvasRef.addEventListener("touchend", this.touchend);
        }
    }

    removeDrawingEventListeners() {
        if (this.overlayCanvasRef && this.isToolbarVisible) {
            this.overlayCanvasRef.removeEventListener("mousedown", this.mousedown);
            this.overlayCanvasRef.removeEventListener("mousemove", this.mousemove);
            this.overlayCanvasRef.removeEventListener("mouseup", this.mouseup);
            this.overlayCanvasRef.removeEventListener("touchstart", this.touchstart);
            this.overlayCanvasRef.removeEventListener("touchmove", this.touchmove);
            this.overlayCanvasRef.removeEventListener("touchend", this.touchend);
        }
    }

    // Get the position of a touch relative to the canvas
    getTouchPos(touchEvent) {
        if (!touchEvent.touches[0]) {
            return {
                x: this._startX,
                y: this._startY
            };
        } else {
            let touch = touchEvent.touches[0];
            if (touch.offsetX && touch.offsetY) {
                return {
                    x: touch.offsetX,
                    y: touch.offsetY
                };
            } else {
                var rect = this.overlayCanvasRef.getBoundingClientRect();
                return {
                    x: touch.clientX - rect.left,
                    y: touch.clientY - rect.top
                };
            }
        }
    }

    touchDown(posX, posY) {

        let drawMode = this.twStudio.getDrawMode();
        if (drawMode === "clear") {
            this.clearAllDrawn();
            this.sendClearNotif(this.getSid());

        } else if (drawMode === "erase") {
            this.eraseLnsUnderPt({ x: posX, y: posY });
            this.clearOverlayCanvas();
            this.drawLines(this._lines);
            // notify of updated lines

        } else {

            this._isDrawing = true;

            this._startX = posX; // e.offsetX; // e.clientX - rect.left;
            this._startY = posY; // e.offsetY; // e.clientY - rect.top;

            //this._clickWasDrawing = false;
        }
    }

    touchMove(posX, posY) {

        if (this.twStudio.getDrawMode() === "erase") {
            this.eraseLnsUnderPt({ x: posX, y: posY });
            this.clearOverlayCanvas();
            this.drawLines(this._lines);

            let thisMemberId = this.getSid();
            let theseLines = this._lines;

            let eraseData = {
                type: "erase",
                props: {
                    memberId: thisMemberId,
                    lines: theseLines
                }
            };

            this.twStudio.sendMessage(eraseData);

        } else if (this._isDrawing === true) {
            this._lastX = posX; // e.offsetX; // e.clientX - rect.left
            this._lastY = posY; // e.offsetY; // e.clientY - rect.top

            let origWidth = this.overlayCanvasRef.offsetWidth;
            let origHeight = this.overlayCanvasRef.offsetHeight;

            let drawMode = this.twStudio.getDrawMode();
            let thisMemberId = this.getSid();
            let theseLines = this._lines;

            let lineColor =  this.twStudio.getDrawColor();

            let drawData = {
                type: "draw",
                props: {
                    memberId: thisMemberId,
                    lines: theseLines,
                    drawMode: drawMode,
                    drawColor: lineColor,
                    origWidth: origWidth,
                    origHeight: origHeight,
                    x1: this._startX,
                    y1: this._startY,
                    x2: this._lastX,
                    y2: this._lastY
                }
            };

            this._logger.logDebug("sending drawing data", drawData);

            this.twStudio.sendMessage(drawData);

            this.drawLinesAndLine(this._lines, drawMode, lineColor, this._startX, this._startY, this._lastX, this._lastY);

            if (drawMode === "free-draw") {
                this._lines.push({
                    mode: drawMode,
                    color: lineColor,
                    x1: this._startX,
                    y1: this._startY,
                    x2: this._lastX,
                    y2: this._lastY
                });

                this._startX = this._lastX; // e.clientX - rect.left;
                this._startY = this._lastY; // e.clientY - rect.top;
            }

            //this._clickWasDrawing = true;
        }
    }

    touchUp(posX, posY) {
        if (this._isDrawing === true) {
            this._isDrawing = false;

            let lineColor = this.twStudio.getDrawColor();

            this._lastX = posX; // e.offsetX; // e.clientX - rect.left;
            this._lastY = posY; // e.offsetY; // e.clientY - rect.top;

            let drawMode = this.twStudio.getDrawMode();

            this.draw(drawMode, lineColor, this._startX, this._startY, this._lastX, this._lastY);
            this._lines.push({
                mode: drawMode,
                color: lineColor,
                x1: this._startX,
                y1: this._startY,
                x2: this._lastX,
                y2: this._lastY
            });

            this._lastX = 0;
            this._lastY = 0;

            this._startX = 0;
            this._startY = 0;
        }
    }

    drawLinesAndLine(lines, drawMode, lineColor, x1, y1, x2, y2) {

        // clear what's there
        this.clearOverlayCanvas();

        // draw lines so far
        this.drawLines(lines);

        // draw the current line
        this.draw(drawMode, lineColor, x1, y1, x2, y2);
    }

    drawLines(lines) {
        // redraw all previous lines
        for (var i = 0; i < lines.length; i++) {
            let line = lines[i];
            this.draw(line.mode, line.color, line.x1, line.y1, line.x2, line.y2);
        }

    }

    draw(drawMode, lineColor, x1, y1, x2, y2) {
        if (drawMode === "circle-draw") {
            this.drawCircle(lineColor, x1, y1, x2, y2);
        } else if (drawMode === "line-draw") {
            this.drawLine(lineColor, x1, y1, x2, y2);
        } else if (drawMode === "arrow-draw") {
            this.drawArrow(lineColor, x1, y1, x2, y2);
        } else {
            this.drawLine(lineColor, x1, y1, x2, y2);
        }
    }

    clearOverlayCanvas(): boolean {

        if (this.overlayCanvasRef) {
            const context = this.overlayCanvasRef.getContext("2d");
            if (context) {
                context.clearRect(0, 0, this.overlayCanvasRef.width, this.overlayCanvasRef.height);
                return true;
            }
        }

        return false;
    }

    clearAllDrawn() {

        this.clearOverlayCanvas();
        this._lines = [];
    }

    sendClearNotif(memberId) {
        let drawData = {
            type: "clear",
            props: {
                memberId
            }
        };

        this._logger.logDebug("sending draw-clearing", drawData);
        this.twStudio.sendMessage(drawData);
    }

    eraseLnsUnderPt(oP: TwPoint) {

        let nLines = this._lines.length;
        let iLine: number;
        let aLines2 = [];
        for (iLine = 0; iLine < nLines; iLine++) {
            let oLine = this._lines[iLine];
            if (!this.isPointInsideThickLineSegment(oP, { x: oLine.x1, y: oLine.y1 }, { x: oLine.x2, y: oLine.y2 }, this.twStudio.getEraseThickness())) {
                aLines2.push(oLine);
            }
        }
        this._lines = aLines2;

        // redraw all lines; send re-drawn
    }


    //http://stackoverflow.com/a/6868610
    //Tested and Working
    calcNearestPointOnLine(line1, line2, pnt) {
        var L2 = (((line2.x - line1.x) * (line2.x - line1.x)) + ((line2.y - line1.y) * (line2.y - line1.y)));
        if (L2 == 0) return false;
        var r = (((pnt.x - line1.x) * (line2.x - line1.x)) + ((pnt.y - line1.y) * (line2.y - line1.y))) / L2;

        return {
            x: line1.x + (r * (line2.x - line1.x)),
            y: line1.y + (r * (line2.y - line1.y))
        };
    }

    //Tested and Working
    calcDistancePointToLine(pnt: TwPoint, line1: TwPoint, line2: TwPoint): number {
        var L2 = (((line2.x - line1.x) * (line2.x - line1.x)) + ((line2.y - line1.y) * (line2.y - line1.y)));
        if (L2 == 0) return -1;
        var s = (((line1.y - pnt.y) * (line2.x - line1.x)) - ((line1.x - pnt.x) * (line2.y - line1.y))) / L2;
        return Math.abs(s) * Math.sqrt(L2);
    }

    isPointInsideLineSegment(pnt: TwPoint, line1: TwPoint, line2: TwPoint): boolean {
        var L2 = (((line2.x - line1.x) * (line2.x - line1.x)) + ((line2.y - line1.y) * (line2.y - line1.y)));
        if (L2 == 0) return false;
        var r = (((pnt.x - line1.x) * (line2.x - line1.x)) + ((pnt.y - line1.y) * (line2.y - line1.y))) / L2;

        return (0 <= r) && (r <= 1);
    }

    isPointInsideThickLineSegment(pnt: TwPoint, line1: TwPoint, line2: TwPoint, lineThickness: number): boolean {
        var L2 = (((line2.x - line1.x) * (line2.x - line1.x)) + ((line2.y - line1.y) * (line2.y - line1.y)));
        if (L2 == 0) return false;
        var r = (((pnt.x - line1.x) * (line2.x - line1.x)) + ((pnt.y - line1.y) * (line2.y - line1.y))) / L2;

        //Assume line thickness is circular
        if (r < 0) {
            //Outside line1
            return (Math.sqrt(((line1.x - pnt.x) * (line1.x - pnt.x)) + ((line1.y - pnt.y) * (line1.y - pnt.y))) <= lineThickness);
        } else if ((0 <= r) && (r <= 1)) {
            //On the line segment
            var s = (((line1.y - pnt.y) * (line2.x - line1.x)) - ((line1.x - pnt.x) * (line2.y - line1.y))) / L2;
            return (Math.abs(s) * Math.sqrt(L2) <= lineThickness);
        } else {
            //Outside line2
            return (Math.sqrt(((line2.x - pnt.x) * (line2.x - pnt.x)) + ((line2.y - pnt.y) * (line2.y - pnt.y))) <= lineThickness);
        }
    }

    drawCircle(lineColor, x1, y1, x2, y2) {

        var ctx = this.overlayCanvasRef.getContext("2d");
        ctx.beginPath();
        ctx.strokeStyle = lineColor;
        ctx.arc(x1, y1, util.dist(x1, y1, x2, y2), 0, 2 * Math.PI);
        ctx.stroke();
    }

    drawLine(lineColor, x1, y1, x2, y2) {
        let context = this.overlayCanvasRef.getContext("2d");

        context.beginPath();
        context.strokeStyle = lineColor;
        context.lineWidth = this.twStudio.getDrawWidth();
        context.moveTo(x1, y1);
        context.lineTo(x2, y2);
        context.stroke();
        context.strokeStyle = lineColor;
        context.closePath();
    }

    drawArrow(lineColor, x1, y1, x2, y2) {
        //variables to be used when creating the arrow
        var ctx = this.overlayCanvasRef.getContext("2d");
        var headlen = 10;

        var angle = Math.atan2(y2 - y1, x2 - x1);

        //starting path of the arrow from the start square to the end square and drawing the stroke
        ctx.beginPath();
        ctx.strokeStyle = lineColor;
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.lineWidth = this.twStudio.getDrawWidth();
        ctx.stroke();

        //starting a new path from the head of the arrow to one of the sides of the point
        ctx.beginPath();
        ctx.strokeStyle = lineColor;
        ctx.moveTo(x2, y2);
        ctx.lineTo(
            x2 - headlen * Math.cos(angle - Math.PI / 7),
            y2 - headlen * Math.sin(angle - Math.PI / 7)
        );

        //path from the side point of the arrow, to the other side point
        ctx.lineTo(
            x2 - headlen * Math.cos(angle + Math.PI / 7),
            y2 - headlen * Math.sin(angle + Math.PI / 7)
        );

        //path from the side point back to the tip of the arrow, and then again to the opposite side point
        ctx.lineTo(x2, y2);
        ctx.lineTo(
            x2 - headlen * Math.cos(angle - Math.PI / 7),
            y2 - headlen * Math.sin(angle - Math.PI / 7)
        );

        //draws the paths created above
        ctx.strokeStyle = lineColor;
        ctx.lineWidth = this.twStudio.getDrawWidth();
        ctx.stroke();
        ctx.strokeStyle = lineColor;
        ctx.fillStyle = lineColor;
        ctx.fill();
    }

    muteLocalParticipantAudio() {
        let mAudioTracks = this.getAudioTracks() as Map<string, AudioTrackPublication>;

        if (mAudioTracks) {
            if (this.isLocalParticipant) {
                for (let [trackSid, trackPub] of mAudioTracks.entries()) {
                    if (trackPub && trackPub.track) {
                        let localAudioTrackPub = trackPub.track as LocalAudioTrack;
                        this.logInfo(`Muting ${trackSid} through disabling...`);
                        localAudioTrackPub.disable();
                    }
                }
            }
        }
        this.isAudioMuted = true;
    }

    unmuteLocalParticipantAudio() {

        let mAudioTracks = this.twStudio?.room?.localParticipant?.audioTracks;
        if (mAudioTracks) {
            for (let [trackSid, trackPub] of mAudioTracks.entries()) {
                if (trackPub && trackPub.track) {
                    this.logInfo(`Unmuting ${trackSid} through disabling...`);
                    trackPub.track.enable();
                }
            }

        }
        this.isAudioMuted = false;
    }

    muteByTrainer() {
        this.muteLocalParticipantAudio();

        this.isRoomMuted = true;
    }

    unmuteByTrainer() {
        if (!this.isMutedBySelf) {
            this.unmuteLocalParticipantAudio();
        }

        this.isRoomMuted = false;
    }

    muteLocalParticipant() {
        this.muteLocalParticipantAudio()

        this.isMutedBySelf = true;
    }

    unmuteLocalParticipant() {
        if (!this.isRoomMuted || this.isTrainer) {
            this.unmuteLocalParticipantAudio()
        }

        this.isMutedBySelf = false;
    }

    enableAudioTracks() {
        let mAudioTracks = this.getAudioTracks() as Map<string, AudioTrackPublication>;

        if (mAudioTracks) {
            if (this.isLocalParticipant) {
                for (let [_, trackPub] of mAudioTracks.entries()) {
                    if (trackPub && trackPub.track) {
                        let localAudioTrackPub = trackPub.track as LocalAudioTrack;
                        this.logInfo(`Enabling audio track ${trackPub.track.name} ...`);
                        localAudioTrackPub.enable();
                    }
                }
            }
        }
        this.isAudioMuted = false;
    }

    toggleParticipantAudioMute() {
        let isParticipantMuted = this.isParticipantAudioMuted();
        let participantSid = this.participant?.sid || "";
        if (participantSid === "") {
            this.logWarn(`TwParticipant is not connected to a Twilio participant!`);
        } else if (this.isLocalParticipant) {
            if (!this.isRoomMuted || this.isTrainer()) {
                if (isParticipantMuted) {
                    this.unmuteLocalParticipant();
                } else {
                    this.muteLocalParticipant();
                }
            }
        } else {
            //TODO: add privilege 'can-mute/unmute-others (trainers, + configuration)
            if (isParticipantMuted) {
                this.twStudio?.sendUnmuteParticipantMsg(this.participant.sid);
            } else {
                this.twStudio?.sendMuteParticipantMsg(this.participant.sid);
            }
        }
    }

    isParticipantAudioMuted(): boolean {
        let isAudioEnabled = false;

        let mAudioTracks = this.participant?.audioTracks || new Map();
        for (let [_, trackPub] of mAudioTracks.entries()) {

            if (trackPub) {
                isAudioEnabled = isAudioEnabled || trackPub.isTrackEnabled;
            }

            if (trackPub && trackPub.track) {
                let audioTrack = trackPub.track;
                isAudioEnabled = isAudioEnabled || audioTrack.isEnabled;
            }

        }  

        return !isAudioEnabled;
    }

    logDebug(...objs:any[]) {
        if (this._logger) {
            this._logger.logDebug(`Participant (${this.getIdentity()})`,  ...objs);
        } else {
            alert("Logger object is not defined?");
        }
    }

    logInfo(...objs:any[]) {
        if (this._logger) {
            this._logger.logInfo(`Participant (${this.getIdentity()})`,  ...objs);
        } else {
            alert("Logger object is not defined?");
        }
    }

    logWarn(...objs:any[]) {
        if (this._logger) {
            this._logger.logWarn(`Participant (${this.getIdentity()})`,  ...objs);
        } else {
            alert("Logger object is not defined?");
        }
    }

    logError(...objs:any[]) {
        if (this._logger) {
            this._logger.logError(`Participant (${this.getIdentity()})`,  ...objs);
        } else {
            alert("Logger object is not defined?");
        }
    }

    @State() videoDevices: MediaDeviceInfo[] = [];
    preferredCamera: Choice;

    // @Listen('selectionMade')
    // selectionMadeHandler(videoChoice: any) {
    //     this.logInfo("local-tw-participant choice selection handler", videoChoice);

    //     this.preferredCamera = videoChoice.detail;

    //     if (this._isVideoShowing) {
    //         this.switchLocalTracks(this.preferredCamera.deviceId);
    //     }

    // }
    // getDeviceSelectionOptions() {
    //     // before calling enumerateDevices, get media permissions (.getUserMedia)
    //     // w/o medai permissions, browsers do not return device Ids or labels
    //     return this.ensureMediaPermissions().then( () => {
    //         return navigator.mediaDevices.enumerateDevices().then( (deviceInfos) => { 
    //             let kinds = ['audioinput', 'audiooutput', 'videoinput'];
    //             return kinds.reduce( (deviceSelectionOptions, kind) => {
    //                 deviceSelectionOptions[kind] = filterDevicesToKind(deviceInfos, kind);
    //                 return deviceSelectionOptions;
    //             }, {});;
    //         })
    //     });
    // }

    // connectWithSelectedDevices(token: string, audioDeviceId: string, videoDeviceId: string) {
    //     return connect(token, {
    //         audio: {deviceId: {exact: audioDeviceId}},
    //         video: {deviceId: {exact: videoDeviceId}}
    //     });
    // }

    // // TODO: twilio video-quickstart snippet
    // switchLocalTracks(deviceId: string) {
    //     let localParticipant = this.getLocalParticipant();
    //     if (localParticipant) {
    //         createLocalVideoTrack({
    //             name: TwStudio.genTrackName(this.twStudio?.getRoomNm(), this.participant?.identity, TwConstants.VIDEO_TRACK_KIND)

    //             //, deviceId: deviceId 
    //         }).then( newTrack => {
    //                 exact: deviceId
    //             for(let [_, trackPub] of localParticipant.tracks) {
    //                 if (trackPub.kind === newTrack.kind && trackPub.kind !== TwConstants.DATA_TRACK_KIND) {
    //                     let localTrackPub = trackPub as (LocalAudioTrackPublication|LocalVideoTrackPublication);
    //                     // let oldTrack = trackPub.track;
    //                     // if (oldTrack) {
    //                     //     oldTrack.detach().forEach(function (detachedElement) {
    //                     //         detachedElement.remove();
    //                     //     });
    //                     // }
    //                     // ^--above lines should get handled by setVideoTracks(newTrack)
    //                     localTrackPub.track.stop();
    //                     localParticipant.unpublishTrack(localTrackPub.track);
    //                     this.removeTrack(localTrackPub.track)

    //                 }
    //                 localParticipant.publishTrack(newTrack);
    //                 this.setVideoTracks([newTrack])

    //             }
    //         }).catch((err) => {
    //             alert("could not create local track");
    //             this.logError("could not create local track", err);
    //         });
    //     }
    // }

    // TODO: twilio video-quickstart snippet
    // displayLocalVideo(video) {
    //     return createLocalVideoTrack().then(function(localTrack) {
    //         localTrack.attach(video);
    //     })
    // }

    // currently not used
    // async stopLocalMedia(device: MediaDeviceInfo, videoRef: HTMLVideoElement) {
    //     this.logInfo(`stopping local video`, device, videoRef);

    //     for(let [, localTrackPub] of this.getLocalParticipant().audioTracks) {
    //         // if (localTrackPub)   
    //         localTrackPub.track.stop();
    //         // localTrackPub.track.detach();
    //         this.getLocalParticipant().unpublishTrack(localTrackPub.track);
    //         this.removeTrack(localTrackPub.track)
    //     }

    //     for(let [, localTrackPub] of this.getLocalParticipant().videoTracks) {
    //         // if (localTrackPub)   
    //         localTrackPub.track.stop();
    //         // localTrackPub.track.detach();
    //         this.getLocalParticipant().unpublishTrack(localTrackPub.track);
    //         this.removeTrack(localTrackPub.track)

    //     }

    // }


}
