
import { Component, h } from '@stencil/core';
import { testmic } from './testmic';
//import { micLevel } from './miclevel';

@Component({
  tag: 'tw-participant-audio',
  styleUrl: 'tw-participant-audio.css'
})
export class TwParticipantAudio {
    audio: any;

    constructor() {
        console.log('component did load', 'testing mic');
        
    }

    componentDidLoad() {
        console.log('component did load', 'testing mic');
        testmic();
        // const $levelIndicator = $('svg rect', $selectMicModal);
        // const maxLevel = Number($levelIndicator.attr('height'));
        // micLevel(stream, maxLevel, level => $levelIndicator.attr('y', maxLevel - level));
    }


    // async selectMicrophone() {
    //     if (this.audio === null) {
    //         try {
    //         this.audio = await selectMedia('audio', $selectMicModal, stream => {
    //             const $levelIndicator = $('svg rect', $selectMicModal);
    //             const maxLevel = Number($levelIndicator.attr('height'));
    //             micLevel(stream, maxLevel, level => $levelIndicator.attr('y', maxLevel - level));
    //         });
    //         } catch (error) {
    //             showError($showErrorModal, error);
    //             return;
    //         }
    //     }
    //     return selectCamera();
    // }

    render() {
        console.log('rendering');
        return (<div>testing mic</div>);
    }
}
