import { createLocalAudioTrack } from 'twilio-video';
import { pollAudioLevel } from './pollaudiolevel';

export async function testmic() {
    const audioTrack = await createLocalAudioTrack();

    pollAudioLevel(audioTrack, level => {
        console.log('testing mic', level, audioTrack)
    /* Update audio level indicator. */
    });
}

// Display the audio level.
