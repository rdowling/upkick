import { Component, h, State, Prop, Event, EventEmitter, Host } from '@stencil/core';
import { getDevicesOfKind } from './media-utils';
import { Choice } from './video-choice';

@Component({
  tag: 'video-chooser',
  styleUrl: 'video-chooser.scss'
})
export class VideoChooser {

    @Prop() width: string = "";

    @State() isOpen = false;
    @State() selectedChoice: Choice;

    @State() choices = [] as Choice[];
   
    constructor() {
        this.selectedChoice = {label: "Please Choose a Device", deviceId: ""};
    }

    async getVideoDevices(): Promise<MediaDeviceInfo[]> {
        return getDevicesOfKind("video");
    }

    async ensureMediaPermissions() {
        return navigator.mediaDevices
        .enumerateDevices()
        .then( (devices:MediaDeviceInfo[])  => {
            return devices.every( (device:MediaDeviceInfo) => {
                return !(device.deviceId && device.label);
            });
        }).then( (shouldAskForMediaPermissions:boolean) => {
            if (shouldAskForMediaPermissions) {
                return navigator
                    .mediaDevices
                    .getUserMedia({ audio: true, video: true})
                    .then( mediaStream => {
                        mediaStream.getTracks().forEach (track => track.stop());
                    });
            }
        });
    }

    setIsOpen(isOpen: boolean): void {
        this.isOpen = isOpen;
    }
    
    componentWillLoad() {
        this.choices = [];
        this.ensureMediaPermissions().then(() => this.getVideoDevices()).then((videoDevices) => {
            let choices = [];
            videoDevices.forEach(videoDevice => choices.push({label: videoDevice.label, deviceId: videoDevice.deviceId}));

            this.setChoices(choices);
        });
    }

    setChoices(choices: any[]) {
        this.choices = choices;
    }
    
    render() {
        
        return (
            <Host id="tw-dropdown" class={"inline-block text-left"}>
                <div class={this.width}>
                    <span class="rounded-md shadow-sm">
                        <button type="button" onClick={() => this.setIsOpen(!this.isOpen)} class="inline-flex justify-center w-auto rounded-md border border-gray-300 px-4 py-2 bg-white text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition ease-in-out duration-150" id="options-menu" aria-haspopup="true" aria-expanded="true">
                            {this.selectedChoice.label}
                            <svg class="-mr-1 ml-2 h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"/>
                            </svg>
                        </button>
                    </span>
                </div>
                
                <div class={(this.isOpen? 'show transition ease-out duration-100 transform opacity-100 scale-100 ': 'hide transition ease-in duration-75 transform opacity-0 scale-95 ')+'drop-down-content origin-top-right absolute mt-2 w-56 rounded-md shadow-lg'}>
                    <div class="rounded-md bg-white shadow-xs" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                        <div class="py-1">
                            {   this.choices.map( (videoDevice) => {
                                    return (
                                        <a href="#" onClick={() => this.selectionMadeHandler(videoDevice)} class="group flex items-center px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900" role="menuitem">
                                            {videoDevice.label}
                                        </a>
                                    );
                                })  
                            }
                        </div>
                    </div>
                </div>
            </Host>
        );
    }

    // Event called 'todoCompleted' that is "composed", "cancellable" and it will bubble up!
    @Event({
        eventName: 'selectionMade',
        composed: true,
        cancelable: true,
        bubbles: true,
    }) selectionMade: EventEmitter<Choice>;

    selectionMadeHandler(choiceSelected?: Choice): void {
        console.log("video-chooser choice selection handler", choiceSelected);
        this.selectedChoice = choiceSelected;
        this.selectionMade.emit(choiceSelected);
        this.setIsOpen(false);
    }

    getCurrentSelection(): Choice {
        return this.selectedChoice;
    }

}
