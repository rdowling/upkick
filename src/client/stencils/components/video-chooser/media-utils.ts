export async function getDevicesOfKind(kind): Promise<MediaDeviceInfo[]> {
    const devices = await navigator.mediaDevices.enumerateDevices() as MediaDeviceInfo[];
    return filterDevicesToKind(devices, kind);
}

export async function filterDevicesToKind(deviceInfos: MediaDeviceInfo[], kind: string) {
    return deviceInfos.filter( deviceInfo => {
        let deviceKind = deviceInfo.kind.toLowerCase();
        let searchKind = (kind||"").toLowerCase()
        return deviceKind === `${searchKind}input` || deviceKind === searchKind;
    });
}