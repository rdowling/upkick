export interface toggledlistener {
    ontoggled(isOn: boolean): void;
}