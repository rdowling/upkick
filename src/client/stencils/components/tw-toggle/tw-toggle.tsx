import { Component, h, Host, Prop } from '@stencil/core';

export interface TwToggleListener {
    (isOn: boolean, toggle?: TwToggle): void;
}

@Component({
    tag: 'tw-toggle',
    styleUrl: 'tw-toggle.css'
})
export class TwToggle {

    @Prop({attribute: 'is-on', mutable: true, reflect: true}) isOn: boolean = false;

    @Prop() name: string = "default_toggle_nm";
    @Prop() isOnColor: string = "bg-red-600";
    @Prop() isOffColor: string = "bg-red-200";

    // @Prop() toggledlistener: toggledlistener;
    @Prop() onToggled: TwToggleListener;

    render() {
        // <!-- On: "bg-indigo-600", Off: "bg-gray-200" -->
        // this.wastoggled.bind(this.toggleIsOn)
        return (
            <Host>
                <div class="flex items-center">
                    <span role="checkbox" tabindex="0" aria-checked="false" onClick={() => this.toggleIsOn(this.isOn)} class={`${this.isOn? this.isOnColor: this.isOffColor} relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:shadow-outline`}>
                        {/* <!-- On: "translate-x-5", Off: "translate-x-0" --> */}
                        <span aria-hidden="true" class={`${this.isOn?'translate-x-5':'translate-x-0'} inline-block h-5 w-5 rounded-full bg-white shadow transform transition ease-in-out duration-200`}></span>
                    </span>
                </div>        
            </Host>
        );
    }

    getName(): string {
        return this.name;
    }

    // @Event() toggled: EventEmitter<boolean>;

    toggleIsOn(currIsOn: boolean): boolean {
        this.isOn = !currIsOn;
        if (this.onToggled) {
            this.onToggled(this.isOn, this);
        } else {
            alert(this.onToggled);
        }
        // console.log(this.wastoggled);
        // this.wastoggled.call(this.isOn);
        // this.toggledlistener.ontoggled(this.isOn);
        // this.toggled.emit(this.isOn);
        return this.isOn;
    }
}
