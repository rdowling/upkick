import { Component, h } from '@stencil/core';

import '@blaze/atoms';
import '@proyecto26/animatable-component';

// import { createAnimatableComponent } from '@proyecto26/animatable-component';

//#region example-region
//#endregion example-region

@Component({
  tag: 'tw-blaze-toggle',
  styleUrl: 'tw-blaze-toggle.scss'
})
export class Dropdown {

  constructor() { }

  render() {
    return (<blaze-toggle toggled type="error">Error</blaze-toggle>);
  }

}