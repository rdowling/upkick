# my-rating



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute   | Description | Type     | Default |
| ---------- | ----------- | ----------- | -------- | ------- |
| `maxValue` | `max-value` |             | `number` | `5`     |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
