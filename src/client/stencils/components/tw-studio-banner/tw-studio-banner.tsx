import { Component, h } from '@stencil/core';


@Component({
    tag: 'tw-studio-banner',
    styleUrl: 'tw-studio-banner.css'
})
export class TwStudioBanner {

    render() {
        return (

            <div id='brand-bar' class='hide-branding' >
                <div id='brand-logo'></div>
                <div id='brand-heading'></div>
                <div id='memberNmDisplay' style={{'position': 'absolute'}}> </div>
            </div>
        );
    }
}
