
export class Layout {
    numRows: number = 0;
    numCols: number = 0;
    cellSize: number = 0;
}
// taken from: 
// https://math.stackexchange.com/questions/466198/algorithm-to-get-the-maximum-size-of-n-squares-that-fit-into-a-rectangle-with-a/2570649#2570649?newreg=ff9ef0e3152e4d6aa5e7e3d7b1b55a6b
export function calcLayout(x:number, y:number, n:number): Layout {
    // Compute number of rows and columns, and cell size
    var ratio = x / y;
    var ncols_float = Math.sqrt(n * ratio)*1.0;
    var nrows_float = n / ncols_float;

    // Find best option filling the whole height
    var nrows1 = Math.ceil(nrows_float);
    var ncols1 = Math.ceil(n / nrows1);
    while (nrows1 * ratio < ncols1) {
        nrows1++;
        ncols1 = Math.ceil(n / nrows1);
    }
    var cell_size1 = y / nrows1;

    // Find best option filling the whole width
    var ncols2 = Math.ceil(ncols_float);
    var nrows2 = Math.ceil(n / ncols2);
    while (ncols2 < nrows2 * ratio) {
        ncols2++;
        nrows2 = Math.ceil(n / ncols2);
    }
    var cell_size2 = x / ncols2;

    // Find the best values
    var nrows, ncols, cell_size;
    
    if (cell_size1 < cell_size2) {
        nrows = nrows2;
        ncols = ncols2;
        cell_size = cell_size2;
    } else {
        nrows = nrows1;
        ncols = ncols1;
        cell_size = cell_size1;
    }

    let bestLayout = new Layout();
    
    bestLayout.cellSize = cell_size;
    bestLayout.numCols = ncols;
    bestLayout.numRows = nrows;

    return bestLayout;
}