import { Component, h, Prop, State, Element, Listen } from '@stencil/core';
import { Room, LocalParticipant, RemoteParticipant, LocalDataTrack, LocalAudioTrack, LocalVideoTrack, RemoteTrackPublication, Track, RemoteVideoTrack, TwilioError, LocalTrack, LocalAudioTrackPublication, LocalVideoTrackPublication, MediaConnectionError, SignalingConnectionDisconnectedError, RemoteAudioTrack, RemoteTrack, Participant, RemoteDataTrack, AudioTrackPublication, VideoTrackPublication, DataTrackPublication, createLocalVideoTrack, ConnectOptions, CreateLocalTrackOptions } from 'twilio-video';
import { TwLogger } from '../../tw-logger';
import $ from 'jquery';
import { TwParticipant } from '../tw-participant/tw-participant';
import * as HTTP from '../../../HTTP';
import domtoimage from 'dom-to-image';

import { TwConstants } from '../../tw-constants';
import { calcLayout } from './tw-layout';
import { TwIcons } from '../tw-icons/tw-icons';
import { snapAllVideos, clearAllSnaps, dataURItoBlob, arrbuffToStr, isMobile } from '../../../utils/util';
import { parseParicipantProps } from '../room-attributes';
import { TwToggle } from '../tw-toggle/tw-toggle';


/* Testing:
    * When a participant connects, are they automatically added to the room.participants map?
    * Do we only really need a room prop and a 'tap' event to refresh?
    * Can we use room.localParticipant.dataTrack to send/recieve information (rather than hold reference)
*/

@Component({
    tag: 'tw-studio',
    styleUrl: 'tw-studio.css'
})
export class TwStudio {

    @Element() el: HTMLElement;

    @Prop() room: Room;

    @Prop() userIdentity: string;
    
    @Prop({mutable: true}) isRoomMuted: boolean;
    @Prop({mutable: true}) isGridEnabled = TwConstants._defaultIsGridVisible;
    @Prop({mutable: true}) gridSpacing = TwConstants._defaultGridSpacingWidth;
    @Prop({mutable: true}) gridTransparency  = TwConstants._defaultGridTransparency;
    
    @Prop() isToolbarVisible: boolean = true;

    @Prop() isParticipantTrainer: boolean;
    @Prop() isShadowing: boolean = false;
        
    @Prop() clientNm = "";
    @Prop() clientId = "";
    @Prop() companyNm = "";
    @Prop() trainerId = "";
    @Prop() connectOptions: ConnectOptions;
    @Prop() clientLogLevel: ('debug'|'info'|'warn'|'error') = 'warn';

    @State() tapCount = 0;
    
    @State() remoteParticipants: RemoteParticipant[] = []; // TODO: consider watching and updating _members

    @State() localTwParticipant: TwParticipant;

    @State() focusedParticipantSid: string = "";

    //@State() imageSnapDataURL: string;

    _members   = new Map() as Map<string,TwParticipant>
    membersContainerRef: HTMLDivElement;
    viewRef: HTMLElement;
    tobBarRef: HTMLElement;
    toolbarRef: HTMLDivElement;
    participantsList: any;

    localDataTrack: LocalDataTrack; // can only send on LocalDataTrack
        
    leaveStudioBtn: HTMLButtonElement;
    gridMode: any;  // TODO: WIP

    private _logger = new TwLogger();

    private _showBorders: boolean = false;
        
    private _drawColor      = TwConstants._defaultDrawColor as string;
    private _drawMode       = TwConstants._defaultDrawMode as string;  // "clear", "erase", "free-draw", "line-draw", "circle-draw", "arrow-draw";
    private _drawWidth      = TwConstants._defaultDrawWidth as number; //3;
    private _eraseThickness = TwConstants._defaultEraseThickness as number; // 10;

    // TODO: re-enable
    private  isSignaturesEnabled: boolean = true;

    private KNACK_GHF_APP_ID = '5dc1eea311605e0016410fc2' as string;  
 
    @State() snappedImageUrl: string = "";
    @State() snappedParticipantSid: string = "";

    private isMobilePoser: boolean = false;
    @Prop() isFocusSynched: boolean = true;

    setRoom(room: Room) {
        this.room = room;
        //TODO: this.disconnect()?
    }

    isMobile(): boolean {
        return isMobile || this.isMobilePoser;
    }

    componentWillLoad() {
        this.logInfo(`Local participant: {identity: ${this.room.localParticipant.identity}; sid: ${this.room.localParticipant.sid}} joining room: {name: '${this.room.name}'; sid: ${this.room.sid}}`);

        this._logger.setRoomName(this.getRoomNm())
                    .setUserIdentity(this.userIdentity)
                    .setLogLevel(this.clientLogLevel);

        this.logInfo(`client is about to connect to room ${this.getRoomNm()}; logging level is '${this.clientLogLevel}'`);

        return this.init(this.room);
    }

    getRoomNm(): string {
        if (this.room) {
            return this.room.name;
        } else {
            return "";
        }
    }

    async GET_queryRoomState(updState: any = {}): Promise<JQuery.jqXHR> {

        updState["roomSid"] = this.room?.sid || "0";
        updState["roomNm"]  = this.room?.name || "";

        return HTTP.GET(`${TwConstants.ROOMS_API_URL}/${updState["roomSid"]}`);
    }

    async PUT_updateRoomState(updState: any = {}): Promise<JQuery.jqXHR> {

        if (!this.room || this.room === null || !this.room.localParticipant || this.room.localParticipant === null) {
            return this.GET_queryRoomState();
        } else {
            updState["roomSid"] = this.room.sid;
            updState["roomNm"]  = this.room.name;
            
            updState["participantSid"]      = this.room?.localParticipant.sid;
            updState["participantIdentity"] = this.room.localParticipant.identity
            
            updState["isTrainer"]   = this.isTrainer();
            updState["isShadowing"] = this.isShadowing;
            return HTTP.PUT(`${TwConstants.ROOMS_API_URL}/${updState["roomSid"]}`, updState);
        }

    }

    componentDidRender() {
        this.logInfo("TwSimpleRoom::componentDidRender()");

        //NOTE: the only real time to call it? shortcuts?
        this.resizeParticipants();
        this.setGridOverlay(this.isGridEnabled, this.gridSpacing, this.gridTransparency);
    }

    componentDidLoad() {
        if (this.isToolbarVisible && !this.isScreenSmall()) {
            this.bindDrawControls();
            this.bindGridControls();
        }
    }

    registerTwParticipant(twParticipant: TwParticipant) {
        if (this._members.has(twParticipant.getSid())) {
            this.logWarn(`participant ${twParticipant.getDisplayName()} was already in the twStudio._members collection`);
            this._members.delete(twParticipant.getSid());
        }
        this._members.set(twParticipant.getSid(), twParticipant);
        if (twParticipant.isLocalParticipant) {
             // TODO: is localTwParticipant property needed?
             this.localTwParticipant = twParticipant;
        }
    }

    unregisterTwParticipant(twParticipant: TwParticipant) {
        this._members.delete(twParticipant.getSid());
        // this.twParticipants = removeArrayItem(twParticipant, this.twParticipants);
    }

    tap() {
        this.tapCount++;
        this.logInfo(`Tap! count is at ${this.tapCount}`);
    }

    setFocusedParticipant(sid: string) {
        this.logInfo(`TwStudio::setFocusedParticipant() => {focusedOnId: ${sid}}`);
        this.focusedParticipantSid = sid;
    }

    focusOnParticipant(sid: string): void {
        if (this.isFocusSynched) {
            this.sendFocusMember(sid, this.room.localParticipant.sid);
        }
        this.setFocusedParticipant(sid);
    }

    clearFocusedParticipants(): void {
        if (this.isFocusSynched) {
            this.sendUnfocusMembers();
        }
        this.setFocusedParticipant("");
    }

    isParticipantFocused(): boolean {
        if (!this.focusedParticipantSid || this.focusedParticipantSid === "") {
            return false;
        } else if (this.focusedParticipantSid === this.room?.localParticipant?.sid) {
            return true;
        } else {
            let participant = this.getRemoteParticipant(this.focusedParticipantSid);
            if (participant && this.isParticipantVideoSubscribedTo(participant)) {
                return true;
            } else {
                return false;
            }
        }
    }

    //TODO: WIP -- events didn't work here
    resizeParticipants() {
        
        this.setNumRowsAndCols();

        this.logInfo(`Resizing participants (count: ${this._members.size})`);

        for (let [_, twParticipant] of this._members) {
            twParticipant.resizeParticipant();
        }
    }

    static getShortName(identity: string): string {
        return identity.substr(0,1)
    }

    static getDisplayName(identity: string) {
        let participantProps = parseParicipantProps(identity);
        
        return participantProps.displayNm;
    }

    static genTrackName(roomNm: string, participantNm: string, trackKind: string) {
        return `${roomNm}_${participantNm}_${trackKind}`;
    }

    init(twilioRoom: Room) {

        this.publishLocalParticipantTracks(twilioRoom);            
            
        try {
            for(let [_, remoteParticipant] of twilioRoom.participants) {
                this.logInfo(`Room participant ${remoteParticipant.identity} was already connected`);
                this.participantConnected(remoteParticipant);
            };

            twilioRoom.on('participantConnected', (remoteParticipant: RemoteParticipant) => {
                this.logInfo(`Room-event: participantConnected: remote participant ${remoteParticipant.identity} connected (event); num remote participants: ${this.room.participants.size}`);
                this.participantConnected(remoteParticipant);
            });

            twilioRoom.on('trackMessage', (xDataMsg: string | ArrayBuffer, track: RemoteDataTrack , participant: RemoteParticipant) => { 
                this.logInfo(`studio received message on data track ${track.name} from ${participant.identity}:`, xDataMsg);
                if (this.room && this.room.state !== TwStudio.roomIsDisconnected) {

                    let dataMsgStr = "";
                    if (xDataMsg instanceof ArrayBuffer) {
                        dataMsgStr = arrbuffToStr(xDataMsg as ArrayBuffer);
                    } else {
                        dataMsgStr = xDataMsg as string;
                    }
                    
                    this.gotRemoteDataTrackMessage(dataMsgStr);
                }

            });

            twilioRoom.on('participantDisconnected', (remoteParticipant: RemoteParticipant) => {
                this.logInfo(`Room-event: participantDisconnected: remote participant ${remoteParticipant.sid} disconnected (event); num remote participants: ${this.room?.participants.size}`);
                this.participantDisconnected(remoteParticipant);
            });

            twilioRoom.on('participantReconnecting', (remoteParticipant: RemoteParticipant) => {
                this.logInfo(`Room-event: participantReconnecting: remote participant ${remoteParticipant.sid} reconnecting (event)`);
                // self.tap();
            });

            twilioRoom.on('participantReconnected', (remoteParticipant: RemoteParticipant) => {
                this.logInfo(`Room-event: participantReconnected: remote participant ${remoteParticipant.sid} reconnected (event)`);
            });

            // Room-event: recordingStarted ()
            // Room-event: recordingStopped ()
            // Room-event: trackMessage (data: string | ArrayBuffer, track: Remote[Data]Track, participant: RemoteParticipant)

            this.setTrackListeners(twilioRoom);

            twilioRoom.on('reconnecting', (twilioErr: MediaConnectionError | SignalingConnectionDisconnectedError) => {
                this.logWarn(`Local Participant is reconnecting | code: ${twilioErr.code} | name: ${twilioErr.name} | msg: ${twilioErr.message}`, twilioErr.stack);
                // self.logInfo(`studio is attemtping to reconnect (event): ${sErrInfoMsg}`);
            });

            twilioRoom.on('reconnected', () => {
                this.logInfo('Local Participant succesfully reconnected');
            });

            // local participant disconnected (you)
            twilioRoom.once('disconnected', (room: Room, disconnectErr: TwilioError) => {

                // Clear the event handlers on document and window...
                window.onbeforeunload = null;
                if (isMobile) {
                    window.onpagehide = null;
                    document.onvisibilitychange = null;
                }

                if (disconnectErr && disconnectErr.code) {
                    this.logInfo(`Room#disconnected fired by local participant due to error. code: ${disconnectErr.code} | name: ${disconnectErr.name}`);
                    if (disconnectErr.code === 20104) {
                        this.logError('Signaling reconnection failed due to expired AccessToken!');
                    } else if (disconnectErr.code === 53000) {
                        this.logError('Signaling reconnection attempts exhausted!');
                    } else if (disconnectErr.code === 53204) {
                        this.logError('Signaling reconnection took too long!');
                    } else {
                        this.logError(`Local Participant Disconnected (code: ${disconnectErr.code}) | name: ${disconnectErr.name} | msg: ${disconnectErr.message}`);
                    }
                }

                this.stopLocalParticipantTracks(room.localParticipant);
                this.unpublishLocalParticipantTracks(room.localParticipant);
                this.detachLocalParticipantTracks(room.localParticipant);

                for(let [_, participant] of twilioRoom.participants) {
                    //TODO: WIP - .tap() instead?
                    this.participantDisconnected(participant);
                }

                this.setRoom(null);
                //self.setLocalParticipant(null);

                this.setFocusedParticipant("");

                this.localDataTrack = null;
                this.localTwParticipant = null;
                this.remoteParticipants = null;

                this._members.clear();

                this.logInfo("Local participant detached all.");
                
            });

            window.onbeforeunload = () => {
                this.room?.disconnect();
            }

            if (this.isMobile()) {

                // according to the Twilio quickstart guide "pagehide" is not working in iOS Safari
                // but it also says in iOS Safari, "beforeunload" is not fire, so use "pagehide" instead
                window.onpagehide = () => {
                    alert("window.onpagehide")
                    this.room?.disconnect();
                }

                // on mobile browsers, use "visibilityChange" event to determine when
                // the app is backgrounded or foregrounded.
                document.onvisibilitychange = async () => {
                    if (document.visibilityState === 'hidden') {
                        // when the app is backgrounded, your app can no longer capture
                        // video frames. So, stop and unpublish the LocalVideoTrack.

                        for(let [_, locVidTrackPub] of twilioRoom.localParticipant.videoTracks) {
                            if (locVidTrackPub.track) {
                                locVidTrackPub.track.stop();//localVideoTrack.stop();
                                twilioRoom.localParticipant.unpublishTrack(locVidTrackPub.track);
                            }
                        }
                        this.tap();
                    } else {
                        // when the app is foregrounded, your app can now continue to 
                        // capture video frames. So, publish a new LocalVideoTrack.

                        let localVideoTrack;
                        if (typeof this.connectOptions.video === "boolean") {
                            localVideoTrack = await createLocalVideoTrack();
                        } else {
                            let videoConnectOptions = this.connectOptions.video as CreateLocalTrackOptions;
                            localVideoTrack = await createLocalVideoTrack(videoConnectOptions);
                            this.POST_updateParticipantSubscriptions();
                        }

                        if (this.room && this.room.localParticipant && localVideoTrack) {
                            await this.room.localParticipant.publishTrack(localVideoTrack);
                            this.tap();
                        }
                    }
                };
            }

            // On refresh, gets called twice: once for beforeunload!(), then again for pagehide!()
            $(window).on("beforeunload pagehide", (event) => { 
                if (twilioRoom.state !== TwStudio.roomIsDisconnected) {
                    this.logInfo(`Local participant is closing page & disconnecting after event ${event.type} fired`);
                    this.leaveStudio(false);
                    this.logInfo(`Local participant disconnected successfully after event ${event.type} fired`);
                }
            });

            window.addEventListener('resize', (e: Event) => {
                this.logInfo("window:resize event", e);

                this.setNumRowsAndCols();
                this.resizeParticipants();

            }, false);
            
        } catch (e) {
            this.logError("Error initializing or deinitializing studio.", e);
        }
    }
    	
    static roomIsConnected    = "connected";
    static roomIsReconnecting = "reconnecting";
    static roomIsDisconnected = "disconnected";
    
    setTrackListeners(twilioRoom: Room) {

        /* -NOTES- 
              * Most of these are for REMOTE participants only!
                There is, however, a LocalParticipant#trackPublished and 
                a RemoteParticipant#trackPublished event for each 
                participant-type.
              * A lot of these are being handled in tw-participant, but (TODO) should 
                probably be better handled here
        */ 

        // Room-event: dominantSpeakerChanged (dominantSpeaker: RemoteParticipant) 
        twilioRoom.on('dominantSpeakerChanged', (dominantSpeaker: RemoteParticipant) => {
            this.logInfo(`[Room-event: dominantSpeakerChanged] is now Remote Participant Identity: ${dominantSpeaker?.identity}`);
        });

        // Room-event: trackDimensionsChanged (track: RemoteVideoTrack, participant: RemoteParticipant)
        twilioRoom.on("trackDimensionsChanged", (track: RemoteVideoTrack, participant: RemoteParticipant) => {
            this.logInfo(`[Room-event: trackDimensionsChanged] RemoteTrack: ${track} | Remote Participant Identity: ${participant?.identity}`);
            this.resizeParticipants();
        });

        // Room-event: trackPublishPriorityChanged (trackPriority: Track.Priority, publication: RemoteTrackPublication, participant: RemoteParticipant)
        twilioRoom.on("trackPublishPriorityChanged", (trackPriority: Track.Priority, publication: RemoteTrackPublication, participant: RemoteParticipant) => {
            this.logInfo(`[Room-event: trackPublishPriorityChanged] Track.Priority: ${trackPriority} | RemoteTrackPublication: ${publication} | Remote Participant Identity: ${participant?.identity}`)
            this.resizeParticipants();
        });

        // Room-event: trackEnabled (publication: RemoteTrackPublication, participant: RemoteParticipant)
        twilioRoom.on("trackEnabled", (publication: RemoteTrackPublication, participant: RemoteParticipant) => {
            this.logInfo(`[Room-event: trackEnabled] RemoteTrackPublication: ${publication} | Remote Participant Identity: ${participant?.identity}`);
            this.tap();
        });

        // Room-event: trackDisabled (publication: RemoteTrackPublication, participant: RemoteParticipant)
        twilioRoom.on("trackDisabled", (publication: RemoteTrackPublication, participant: RemoteParticipant) => {
            this.logInfo(`[Room-event: trackDisabled] RemoteTrackPublication: ${publication} | Remote Participant Identity: ${participant?.identity}`);
            this.tap();
        });

        // Room-event: trackStarted  (track: RemoteTrack, participant: RemoteParticipant)
        twilioRoom.on("trackStarted", (track: RemoteTrack, participant: RemoteParticipant) => {
            this.logInfo(`[Room-event: trackStarted] RemoteTrack: ${track} | Remote Participant Identity: ${participant.identity}`);
            this.tap();
        });
        
        // Room-event: trackSwitchedOn   (track: RemoteTrack,  publication: RemoteTrackPublication, participant: RemoteParticipant)
        twilioRoom.on("trackSwitchedOn", (track: RemoteTrack, publication: RemoteTrackPublication, participant: RemoteParticipant) => {
            this.logInfo(`[Room-event: trackSwitchedOn] RemoteTrack: ${track} | RemoteTrackPublication: ${publication} | Remote Participant Identity: ${participant.identity}`);
            this.tap();
        });
        
        // Room-event: trackSwitchedOff  (track: RemoteTrack,  publication: RemoteTrackPublication, participant: RemoteParticipant)
        twilioRoom.on("trackSwitchedOff", (track: RemoteTrack, publication: RemoteTrackPublication, participant: RemoteParticipant) => {
            this.logInfo(`[Room-event: trackSwitchedOff] RemoteTrack: ${track} | RemoteTrackPublication: ${publication} | Remote Participant Identity: ${participant.identity}`);
            this.tap();
        });

        // Room-event: trackPublished   (publication: RemoteTrackPublication, participant: RemoteParticipant)
        twilioRoom.on("trackPublished", (publication: RemoteTrackPublication, participant: RemoteParticipant) => {
            this.logInfo(`[Room-event: trackPublished] RemoteTrackPublication: ${publication} | Remote Participant Identity: ${participant.identity}`)
            // TODO: WIP
            this.GET_queryRoomState()
                .then(() => {
                    this.resizeParticipants();
                })
            this.tap();
        });

        // Room-event: trackUnpublished (publication: RemoteTrackPublication, participant: RemoteParticipant)
        twilioRoom.on("trackUnpublished", (publication: RemoteTrackPublication, participant: RemoteParticipant) => {
            this.logInfo(`[Room-event: trackUnpublished] RemoteTrackPublication: ${publication} | Remote Participant Identity: ${participant.identity}`);
            this.tap();
        });

        // Room-event: trackSubscribed   (track: RemoteTrack, publication: RemoteTrackPublication, participant: RemoteParticipant)
        twilioRoom.on("trackSubscribed", (track: RemoteTrack, publication: RemoteTrackPublication, participant: RemoteParticipant) => {
            // NOTE! if the tw-participant hasn't been created by render (because it only uses subscribed participants) it's "subscribed" event won't fire/be-heard.
            let subscriptionMsg = `[Room-event: trackSubscribed] RemoteTrack: ${track} | RemoteTrackPublication: ${publication} | Remote Participant Identity: ${participant.identity}`;
            this.logInfo(subscriptionMsg);
            // alert(subscriptionMsg);
            this.tap();
        });

        // Room-event: trackUnsubscribed (track: RemoteTrack, publication: RemoteTrackPublication, participant: RemoteParticipant)
        twilioRoom.on("trackUnsubscribed", (track: RemoteTrack, publication: RemoteTrackPublication, participant: RemoteParticipant) => {
            this.logInfo(`[Room-event: trackUnsubscribed] RemoteTrack: ${track} | RemoteTrackPublication: ${publication} | Remote Participant Identity: ${participant.identity}`);
            this.tap();
        });

        // Room-event: trackSubscriptionFailed  (error: TwilioError, publication: RemoteTrackPublication, participant: RemoteParticipant)	
        // twilioRoom.on("trackSubscriptionFailed", (error: TwilioError, publication: RemoteTrackPublication, participant: RemoteParticipant) => {
        //     self.logInfo(`Room-event: trackSwitchedOff (${error}: TwilioError, ${publication}: RemoteTrackPublication, ${participant}: RemoteParticipant)`);
        // });
    }

    participantConnected(remoteParticipant: RemoteParticipant) {
        if (!this.remoteParticipants.includes(remoteParticipant)) {
            this.POST_updateParticipantSubscriptions();
            this.setRemoteParticipants([...this.remoteParticipants, remoteParticipant]);
        }
    }

    POST_updateParticipantSubscriptions() {
        HTTP.POST(`${TwConstants.ROOMS_API_URL}/${this.room?.sid}`, {roomSid: this.room?.sid, roomNm: this.room?.name});
    }

    setRemoteParticipants(remoteParticipants: RemoteParticipant[]) {
        this.remoteParticipants = remoteParticipants;
    }

    participantDisconnected(remoteParticipant: RemoteParticipant) {
        if (remoteParticipant.sid === this.focusedParticipantSid) {
            this.setFocusedParticipant("");
        }
        this.setRemoteParticipants(this.remoteParticipants.filter(p => p !== remoteParticipant));
    }

    postSnapshotAndNotifyParticipants(participantSid: string, imageSnapDataURL: string) {

        let self = this;

        this.snappedImageUrl = imageSnapDataURL;
        this.snappedParticipantSid = participantSid;

        HTTP.POST(`${TwConstants.ROOMS_API_URL}/${this.getRoomSid()}/snapshot`, 
            {   roomSid: this.getRoomSid(),
                participantSid: participantSid,
                imageSnap: imageSnapDataURL
            })
            .fail((jqXHR, sErrStatus, sErrMsg) => {
                let errDispMsg = `${sErrStatus} - could not post snapshot to server: ${sErrMsg}`;
                self.logError(errDispMsg, jqXHR, sErrStatus, sErrMsg);
                alert(`${errDispMsg}`);

            })
            .done((data) => {
                self.sendMessage({type: "snapshot"});
                self.logInfo("Snapshot was submitted to the server.", data);
            });
    }

    notifyParticipantsOfSnapshotCleared(clearedSid: string) {
        this.sendMessage({type: "snapshot-cleared", props: { participantSid: clearedSid } });
        this.logInfo("Clear snapshot message sent.", {type: "snapshot-cleared", participantSid: clearedSid});
    }

    bindDrawControls() {
        
        let self = this;

        let colorPicker = document.getElementById("_color_picker") as HTMLInputElement;
        colorPicker.value = this._drawColor;

        colorPicker.addEventListener('change', () => {
            self._drawColor = colorPicker.value;
        });

        var eRadios = document.getElementsByClassName("draw-ctrl-btn") as HTMLCollectionOf<HTMLInputElement>;
        var nRadios = eRadios.length;
        for (var iRadio = 0; iRadio < nRadios; iRadio++) {
            let eRadio: HTMLInputElement = eRadios[iRadio];
            eRadio.addEventListener("click", () => {

                $(".draw-ctrl-btn").removeClass("radio-btn-selected");

                let eRadioBtn = eRadio.querySelector("input");
                self._drawMode = eRadioBtn.value; // "line-draw", "circle-draw", "free-draw", "arrow-draw"

                eRadio.classList.add("radio-btn-selected");

            });
        }

        let eSelectedDrawMode = eRadios[0];
        eSelectedDrawMode.click();
    }

    bindGridControls() {
        
        let self = this;

        document
            .querySelector(".grd-toggle-btn")
            .addEventListener("click", () => {
                self.toggleGridOverlay();
                self.sendGridData(self.isGridEnabled, self.gridSpacing, self.gridTransparency);
                let gridStyle = self.calcGridOverlayStyle();
                Array.from(self._members.values()).forEach(oMemberView => {
                    oMemberView.setGridOverlay(self.isGridEnabled, gridStyle);
                });
            });

        document
            .querySelector(".grd-smaller-btn")
            .addEventListener("click", () =>  {
                self.reduceGridOverlay();
                self.sendGridData(self.isGridEnabled, self.gridSpacing, self.gridTransparency);
                let gridStyle = self.calcGridOverlayStyle();
                Array.from(self._members.values()).forEach(oMemberView => {
                    oMemberView.setGridOverlay(self.isGridEnabled, gridStyle);
                });
            });

        document
            .querySelector(".grd-larger-btn")
            .addEventListener("click", () =>  {
                self.enlargeGridOverlay();
                let gridStyle = self.calcGridOverlayStyle();
                self.sendGridData(self.isGridEnabled, self.gridSpacing, self.gridTransparency);
                Array.from(self._members.values()).forEach(oMemberView => {
                    oMemberView.setGridOverlay(self.isGridEnabled, gridStyle);
                });

            });

        document
            .querySelector(".grd-lighter-btn")
            .addEventListener("click", () =>  {
                self.lightenGridOverlay();
                let gridStyle = self.calcGridOverlayStyle();
                self.sendGridData(self.isGridEnabled, self.gridSpacing, self.gridTransparency);
                Array.from(self._members.values()).forEach(oMemberView => {
                    oMemberView.setGridOverlay(self.isGridEnabled, gridStyle);
                });
            });

        document
            .querySelector(".grd-darker-btn")
            .addEventListener("click", () =>  {
                self.darkenGridOverlay();
                let gridStyle = self.calcGridOverlayStyle();
                self.sendGridData(self.isGridEnabled, self.gridSpacing, self.gridTransparency);
                Array.from(self._members.values()).forEach(oMemberView => {
                    oMemberView.setGridOverlay(self.isGridEnabled, gridStyle);
                });
            });

    }

    sendFocusMember(sFocusedMemberId: string, sFocusingMemberId: string) {

        this.sendMessage({
            type: "focus",
            props: {
                focusedOnId: sFocusedMemberId,
                focusingId: sFocusingMemberId
            }
        });
    }

    sendUnfocusMembers() {
        this.sendMessage({
            type: "unfocus"
        });
    }

    sendGridData(isOverlaid, nSpacingWidth, nTransparencyPct) {

        let drawData = {
            type: "grid",
            props: {
                isOverlaid,
                nSpacingWidth,
                nTransparencyPct
            }
        };

        this.PUT_updateRoomState({isGridEnabled: isOverlaid, gridSpacing: nSpacingWidth, gridTransparencyPct: nTransparencyPct})

        this.logDebug("sending grid data", drawData);
        this.sendMessage(drawData);
    }
    
    static includeClass(includeCls: boolean, el: HTMLCanvasElement, cls: string) {
        if (includeCls) {
            return TwStudio.addClass(el, cls);
        } else {
            return TwStudio.removeClass(el, cls);
        } 
    }

    static addClass(el: HTMLCanvasElement, cls: string): boolean {
        if (!el.classList.contains(cls)) {
            el.classList.add(cls);
            return true;
        } else {
            return false;
        }
    }

    static removeClass(el: HTMLCanvasElement, cls: string): boolean {
        if (el.classList.contains(cls)) {
            el.classList.remove(cls);
            return true;
        } else {
            return false;
        }
    }

    static toggleClass(el: HTMLCanvasElement, cls: string): boolean {
         el.classList.toggle(cls);
         return el.classList.contains(cls);

    }
    
    reduceGridOverlay(): number {
        if (this.isGridEnabled) {
            this.gridSpacing = this.gridSpacing - 10;
        }

        return this.gridSpacing;
    }

    enlargeGridOverlay(): number {
        if (this.isGridEnabled) {
            this.gridSpacing = this.gridSpacing + 10;
        }

        return this.gridSpacing;
    }

    darkenGridOverlay(): number {
        if (this.isGridEnabled) {
            this.gridTransparency = Math.max(this.gridTransparency - 0.1, 0.0);
        }

        return this.gridTransparency;
    }

    lightenGridOverlay(): number {
        if (this.isGridEnabled) {
            this.gridTransparency = Math.min(this.gridTransparency + 0.1, 1.0);

        }

        return this.gridTransparency;
     }

    toggleGridOverlay(): boolean {
        this.isGridEnabled = !this.isGridEnabled;
        return this.isGridEnabled;
    }
    
    publishLocalParticipantTracks(room: Room) {
        if (room && room.localParticipant) {
            let localParticipant: LocalParticipant = room.localParticipant;
            this.logInfo(`Attaching local participant; SID: ${localParticipant.sid} | NAME: ${localParticipant.identity}`);

            /*TODO: perhaps set this beforehand, then loop it like the others?
                    or do this through a setter
            */
            this.localDataTrack = new LocalDataTrack();
            
            localParticipant.publishTrack(this.localDataTrack);

            let localAudioTrackPubs: Map<Track.SID, LocalAudioTrackPublication> = localParticipant.audioTracks;  // local audio tracks published to room
            let localVideoTrackPubs: Map<Track.SID, LocalVideoTrackPublication> = localParticipant.videoTracks;  // local video tracks published to room

            let audioTracks: (LocalAudioTrack|RemoteAudioTrack)[] = TwStudio.audioTrackpubsToTracks(localAudioTrackPubs);
            let videoTracks: (LocalVideoTrack|RemoteVideoTrack)[] = TwStudio.videoTrackpubsToTracks(localVideoTrackPubs);

            if (audioTracks && audioTracks.length > 0) {
                let audioTrack: LocalAudioTrack = audioTracks[0] as LocalAudioTrack;
                localParticipant.publishTrack(audioTrack);
            }

            if (videoTracks && videoTracks.length > 0) { 
                let videoTrack: LocalVideoTrack = videoTracks[0] as LocalVideoTrack;
                localParticipant.publishTrack(videoTrack); 
            }
        }
        
    }

    isScreenSmall(): boolean {
        return window.innerWidth <= 800 && window.innerHeight <= 600;
    }

    logDebug(...debugs: any[]) {
        if (this._logger) {
            this._logger.logDebug(...debugs);
        } else {
            alert("Logger object is not defined?");
        }
    }
        
    logInfo(...infos:any[]) {
        if (this._logger) {
            this._logger.logInfo(...infos);
        } else {
            alert("Logger object is not defined?");
        }
    }

    logWarn(...warns:any[]) {
        if (this._logger) {
            this._logger.logWarn(...warns);
        } else {
            alert("Logger object is not defined?");
        }
    }
    
    logError(...errs:any[]) {
        if (this._logger) {
            this._logger.logError(...errs);
        } else {
            alert("Logger object is not defined?");
        }
    }

    // nGridPctSize: {0.0 ... 1.0}
    // nTransparencyPct: {0.0 ... 1.0}
    calcGridOverlayStyle(): string {
        let nGridSpacingWidth = this.gridSpacing
        let nTransparencyPct  = this.gridTransparency;

        let gridOverlayStyle = "";

        if (this.isGridEnabled) {
            // `repeating-linear-gradient( 0deg, transparent, transparent ${nGridSpacingWidth}px, 
            //                             rgba(200,200,200, ${nTransparencyPct}) ${nGridSpacingWidth}px, 
            //                             rgba(200,200,200, ${nTransparencyPct}) ${(nGridSpacingWidth + 1)}px ), 
            // repeating-linear-gradient( -90deg, transparent, transparent ${nGridSpacingWidth}px, 
            //                             rgba(200,200,200, ${nTransparencyPct}) ${nGridSpacingWidth}px, 
            //                             rgba(200,200,200, ${nTransparencyPct}) ${(nGridSpacingWidth + 1)}px )"`;

            gridOverlayStyle = "repeating-linear-gradient(0deg, transparent, transparent " + nGridSpacingWidth + "px," +
            " rgba(200,200,200," + nTransparencyPct + ") " + nGridSpacingWidth + "px," +
            " rgba(200,200,200," + nTransparencyPct + ") " + (nGridSpacingWidth + 1) + "px " +
            "), " +
            "repeating-linear-gradient(-90deg, transparent, transparent " + nGridSpacingWidth + "px," +
            " rgba(200,200,200," + nTransparencyPct + ") " + nGridSpacingWidth + "px," +
            " rgba(200,200,200," + nTransparencyPct + ") " + (nGridSpacingWidth + 1) + "px " +
            ")";
        }

        return gridOverlayStyle;
    }

    getParticipantsSection(): HTMLElement {
        return document.querySelector("main");
    }
    
    setNumRowsAndCols() {

        let allParticipants = this.getAllVisibleParticipants(this.isParticipantFocused());
        let totParticipantCount = allParticipants.length;

        let participantsSection = this.getParticipantsSection();

        if (allParticipants && allParticipants.length > 0 && participantsSection) {

            let dMembersSectionWidth  = (participantsSection.clientWidth)*1.0;  // eStudioMembersContainer.clientHeight * 1.0; // convert to decimal // eMainArea.clientHeight - eBrandBar.clientHeight; // eStudioMembersContainer.clientHeight;
            let dMembersSectionHeight = (participantsSection.clientHeight)*1.0; // eStudioMembersContainer.clientWidth * 1.0;  // convert to decimal

            let layout = calcLayout(dMembersSectionWidth, dMembersSectionHeight, totParticipantCount);
            
            //this.setCssVariable("--num-pip-participants", numPipParticipants);
            this.setCssVariable("--num-participant-rows", layout.numRows);
            this.setCssVariable("--num-participant-columns", layout.numCols);

        }
    }

    setCssVariable(varNm: string, varVal: any) {
        var root = document.documentElement;
        root.style.setProperty(varNm, varVal);
    }

    getAllParticipants(filterMinimized:boolean=false): (RemoteParticipant|LocalParticipant)[] {

        let allParticipants = [] as (RemoteParticipant|LocalParticipant)[];
        
        if (this.room && this.room.localParticipant) {

            if(!filterMinimized) {
                allParticipants.push(this.room.localParticipant);
                for (let [_, remoteParticipant] of this.room.participants) {
                    allParticipants.push(remoteParticipant);
                }     
            } else {
                if (this.isParticipantFocused()) {
                    let focusedParticipant = this.getParticipant(this.focusedParticipantSid);
                    if (focusedParticipant) {
                        allParticipants.push(focusedParticipant);
                    } else {
                        this.setFocusedParticipant("");
                        filterMinimized = false;
                    }
                }
            }
        }
        return allParticipants;
    }

    getAllVisibleParticipants(filterMinimized:boolean=false): (RemoteParticipant|LocalParticipant)[] {

        let allParticipants = [] as (RemoteParticipant|LocalParticipant)[];
        
        if (this.room && this.room.localParticipant) {
            if(!filterMinimized) {
                allParticipants.push(this.room.localParticipant);
                for (let [_, remoteParticipant] of this.room.participants) {
                    
                    if (this.isParticipantVideoSubscribedTo(remoteParticipant)) {
                        // this.logInfo("getAllVideoSubscribedParticipants - video is subscribed to (in ()):", partKey);
                        allParticipants.push(remoteParticipant);
                    } else {

                        this.logInfo(`Not subscribed to remote participant's (${remoteParticipant.identity}) video (there are ${remoteParticipant.videoTracks.size} video publications & ${TwStudio.videoTrackpubsToTracks(remoteParticipant.videoTracks).length} tracks)`);
                    }
                }     
            } else {
                if (this.isParticipantFocused()) {
                    let focusedParticipant = this.getParticipant(this.focusedParticipantSid);
                    if (focusedParticipant) {
                        allParticipants.push(focusedParticipant);
                    } else {
                        this.setFocusedParticipant("");
                        filterMinimized = false;
                    }
                }
            }
        }

        return allParticipants;
    }

    isParticipantVideoSubscribedTo(remoteParticipant: RemoteParticipant): boolean {
        
        for( let [_, trackPublication] of remoteParticipant.videoTracks) {
            if (trackPublication.isSubscribed) {
                return true;
            } else {
                this.logInfo(`Not currently subscribed to remote participants (${remoteParticipant.identity}) video trackPublication ${trackPublication.trackSid}`);
            }
        }
        return false;
    }

    getParticipant(candSid: string): (LocalParticipant|RemoteParticipant|undefined) {
        return this.getAllParticipants().find(p=>p.sid === candSid);
    }

    getRemoteParticipant(participantSid: string): (RemoteParticipant|undefined) {
        let remoteParticipant: RemoteParticipant;
        for (let [_, candParticipant] of this.room.participants) {
            if (candParticipant.sid === participantSid) {
                remoteParticipant = candParticipant;
                break;
            }
        }

        return remoteParticipant;
    }

    // resizeCanvas(participantSid) {

    //     let videoElement = document.querySelector(`${participantSid} > video`) as HTMLVideoElement;
    //     if (videoElement) {
    //         TwStudio.resizeCanvases(videoElement);
    //     }
    // }
    

    setVideoDimensions(eMemberView: HTMLDivElement, oVideoDimensions: any, nContainerWidth, nContainerHeight) {
        
        this.logInfo("setVideoDimensions()", eMemberView, "video height & width:", oVideoDimensions, "window-WxH", "calculated width:", nContainerWidth, "calculated height:", nContainerHeight);
        
        if (eMemberView) {

            let nVideoHeight = oVideoDimensions.height;
            let nVideoWidth = oVideoDimensions.width;

            let eVideo = eMemberView.querySelector("video") as HTMLVideoElement;

            if (!eVideo || !eVideo.style || (nVideoHeight === 0 && nVideoWidth === 0)) {
                if (eMemberView) {
                    eMemberView.style.width = nContainerWidth+'px';
                    eMemberView.style.height = nContainerHeight+'px';
                }
            } else {
            
                let nVideoRatio = nVideoWidth / nVideoHeight;

                let nContainerRatio = nContainerWidth/nContainerHeight;
                if (nContainerRatio >= nVideoRatio) {
                    eVideo.style.height = "100%";
                    eVideo.style.width  = null;
                    eMemberView.style.height = nContainerHeight+'px';
                    eMemberView.style.width  = nContainerWidth+'px';//((nContainerHeight)*(nVideoWidth/nVideoHeight))+'px';
                } else {
                    eVideo.style.width  = "100%";
                    eVideo.style.height = null;
                    eMemberView.style.width  = nContainerWidth+'px';
                    eMemberView.style.height = nContainerHeight+'px';//((nContainerWidth)*(nVideoHeight/nVideoWidth))+'px';
                }
            }

        }

    }

    getRoomSid(): string|null {
        if (this.room) {
            return this.room.sid;
        } else {
            return null;
        }
    }

    showBorders(): boolean {
        return this._showBorders;
    }

    sendMessage(msgData: any) {
        this.localDataTrack.send(JSON.stringify(msgData));
    }

    async uploadImageToTrainWithServer(participantSid, imgBlobData) {
        HTTP.POST( `${TwConstants.ROOMS_API_URL}/${this.room.sid}/snapshot`, {roomSid: this.room.sid,
            participantSid: participantSid,
            imageSnap: imgBlobData
        })
        .done((data) => {
            this.logInfo("Snapshot was submitted to the server.", data);
            //this.clearAllSnaps();
        });
    }

    async uploadImageToKnack(imgData) {

        var form = new FormData();
        // change #myfileinput to select your <input type="file" /> element
        form.append(`files`, imgData);

        var url = `https://api.knack.com/v1/applications/${this.KNACK_GHF_APP_ID}/assets/file/upload`;
        var headers = {
            'X-Knack-Application-ID': this.KNACK_GHF_APP_ID,
            'X-Knack-REST-API-Key': `knack`,
        };

        // Make the AJAX call
        return $.ajax({
            url: url,
            type: `POST`,
            headers: headers,
            processData: false,
            contentType: false,
            mimeType: `multipart/form-data`,
            data: form,
        });

    }

    // NEW helper
    setVideoPriority(participant, priority) {
        participant.videoTracks.forEach(trackPub => {
            const track = trackPub.track;
            if (track && track.setPriority) {
                track.setPriority(priority);
            }
        });
    }

    dataURItoBlob(dataURI): Blob {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);
        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ia], {type:mimeString});
    }

    render() {

        this.logInfo("TwStudio::render()");
        
        let allParticipants = this.getAllParticipants();
        let visibleParticipants = this.getAllVisibleParticipants();
        let pipTrackParticipants: (RemoteParticipant|LocalParticipant)[] = [];

        let elParticipants;
        if (!this.room || this.room === null) {
            return null;
        }

        this.logInfo(`visibleParticipants count: ${visibleParticipants.length}; allVideoParticipants count: ${allParticipants.length}; pipTrackParticipants count: ${pipTrackParticipants.length}`);

        if (this.isGridMode()) {

            this.setNumRowsAndCols();
            elParticipants = visibleParticipants.map( (p) => (
                <tw-participant twStudio={this} key={p.sid}
                                isPIP={false}
                                imageSnap={(this.snappedParticipantSid === p.sid)? this.snappedImageUrl : ""}
                                focusedParticipantSid={this.focusedParticipantSid} 
                                participant={p} isLocalParticipant={p===this.room.localParticipant} 
                                videoTracks={TwStudio.videoTrackpubsToTracks(p.videoTracks)} 
                                audioTracks={TwStudio.audioTrackpubsToTracks(p.audioTracks)} 
                                isRoomMuted={this.isRoomMuted}
                                clientLogLevel={this.clientLogLevel} 
                                isToolbarVisible={this.isToolbarVisible} 
                >
                </tw-participant>
            ));

            for (let candParticipant of allParticipants) {
                if (!visibleParticipants.includes(candParticipant)) {
                    pipTrackParticipants.push(candParticipant);
                }
            }
            
        } else { // focusedMode
            let focPart = visibleParticipants.find(p=>p.sid === this.focusedParticipantSid);

            if (focPart && focPart !== undefined && focPart !== null) {

                elParticipants = (
                    <tw-participant 
                        twStudio={this} key={focPart.sid}
                        isPIP={false}
                        imageSnap={(this.snappedParticipantSid === focPart.sid)? this.snappedImageUrl : ""}
                        focusedParticipantSid={focPart.sid} 
                        participant={focPart} 
                        isLocalParticipant={focPart===this.room.localParticipant} 
                        videoTracks={TwStudio.videoTrackpubsToTracks(focPart.videoTracks)} 
                        audioTracks={TwStudio.audioTrackpubsToTracks(focPart.audioTracks)} 
                        isRoomMuted={this.isRoomMuted}
                        clientLogLevel={this.clientLogLevel} 
                        isToolbarVisible={this.isToolbarVisible} 
                    >
                    </tw-participant>
                );
                
                pipTrackParticipants = allParticipants.filter(p => p !== focPart);
            }            
        }

        let numPips = pipTrackParticipants.length;
        let isPipTrackVis = numPips > 0; 

        let pipStyle = {};
        if (!isPipTrackVis) {
            pipStyle['display'] = "none";
            this.setCssVariable("--right-side-width", "0fr")
        } else {
            this.setCssVariable("--right-side-width", "1.5fr");
        }

        let pipTrackParticipantRenders = [];
        let colors = ["blue", "green", "purple", "teal", "yellow", "orange"];
        let iPipParticipant = 0;
        for(let pipParticipant of pipTrackParticipants) {
            iPipParticipant = iPipParticipant % colors.length;
            let color = colors[iPipParticipant]
            iPipParticipant++;
            pipTrackParticipantRenders.push(
                (<tw-participant
                    twStudio={this} key={pipParticipant.sid}
                    isPIP={true}
                    longname={TwStudio.getDisplayName(pipParticipant.identity)} 
                    shortname={TwStudio.getShortName(pipParticipant.identity)} 
                    videoTracks={TwStudio.videoTrackpubsToTracks(pipParticipant.videoTracks)} 
                    audioTracks={TwStudio.audioTrackpubsToTracks(pipParticipant.audioTracks)}
                    color={color}
                    imageSnap={(this.snappedParticipantSid === pipParticipant.sid)? this.snappedImageUrl : ""}
                    focusedParticipantSid={this.focusedParticipantSid} 
                    participant={pipParticipant} 
                    isLocalParticipant={pipParticipant===this.room.localParticipant} 
                    isRoomMuted={this.isRoomMuted}
                    clientLogLevel={this.clientLogLevel} 
                    isToolbarVisible={this.isToolbarVisible} 
                >
                </tw-participant>)
            );
        }

        return (
            < div id='page-content' ref={thisView => this.viewRef = thisView} >
                <header class='hidden'>
                    {/* { this.getNavSectionRendering() } */}
                </header>

                <aside id='left-aside' class={`${(this.isMobile || !this.isToolbarVisible) ? 'hidden': ''}`}>
                    { this.isToolbarVisible ? this.getToolBarRendering() : null }
                </aside>
                    
                <main id='participants-section'>
                    { elParticipants }
                </main>


                <aside id='right-aside' style={pipStyle} class='sm:hidden visible'>
                     <div class="z-50">
                        <h2 class="text-gray-500 text-center text-xs font-medium uppercase tracking-wide">Participants</h2>
                        <ul class="mt-3 grid grid-cols-1 gap-5">
                            {pipTrackParticipantRenders}
                        </ul>
                    </div>
                </aside>

                <footer class='hidden'>
                    { this.getFooterSectionRendering() }
                </footer>

            </ div >
        );
    }
    
    calcNumPipParticipants() {
        return 0;
    }
 
    isGridMode() {
        return !this.isFocusedViewMode();
    }

    isFocusedViewMode() {
        let allVideoParticipants = this.getAllVisibleParticipants();

        let focPart = allVideoParticipants.find(p=>p.sid === this.focusedParticipantSid);

        if (focPart && focPart !== undefined && focPart !== null) {
            return true;
        } else {
            return false;
        }
    }

    getFooterSectionRendering() {
        return (<div></div>);
    }

    static audioTrackpubsToTracks(trackMap: Map<Track.SID, AudioTrackPublication>): (LocalAudioTrack|RemoteAudioTrack)[] {
        return Array.from(trackMap.values()).map((pub) => pub.track).filter((track) => track !== null);
    }

    static videoTrackpubsToTracks(trackMap: Map<Track.SID, VideoTrackPublication>): (LocalVideoTrack|RemoteVideoTrack)[] {
        return Array.from(trackMap.values()).map((pub) => pub.track).filter((track) => track !== null);
    }

    static dataTrackpubsToTracks(trackMap: Map<Track.SID, DataTrackPublication>): (LocalDataTrack|RemoteDataTrack)[] {
        return Array.from(trackMap.values()).map((pub) => pub.track).filter((track) => track !== null);
    }

    static getParticipantIdentity(participant: Participant): string {
        return participant.identity;
    }

    static getParticipantShortIdentity(participant: Participant): string {
        let identity = participant.identity;
        let shortIdentity = identity.slice(0,2);
        return shortIdentity;
    }


    @Listen('toggled')
    toggledHandler(event: CustomEvent<boolean>) {
        this.logInfo(`Received the custom 'toggled' event: `, event.detail);
        this.isFocusSynched = event.detail;
    }

    getToolBarRendering() {
        return (
            < div ref={toolbar => this.toolbarRef = toolbar} class="flex-col p-1" data-pg-name="ToolBar" >

                <div class='flex justify-center my-2'>
                    <button id='bnt-leave-studio' ref={(btn) => this.leaveStudioBtn = btn} class="p-1 w-full whitespace-no-wrap border self-center justify-center border-transparent text-xs leading-5 font-medium rounded-md text-white bg-red-500  hover:bg-red-700 focus:outline-none focus:border-red-700 focus:shadow-outline-indigo active:bg-red-700 transition duration-150 ease-in-out"
                        onClick={()=>this.leaveStudio(true)}
                    >
                        {this.isTrainer()? 'Close Studio': 'Leave Studio'}
                    </button>
                </div>

                < h2 class="font-medium m-1 text-center text-gray-500 text-xs tracking-wide uppercase">Tools</h2>

                < div id="draw-controls" class="mt-2 align-top box-border flex-row justify-center items-center relative shadow-sm z-0" data-toggle='buttons'> 
                    <div class='p-1 text-xs text-center text-gray-800 border border-black w-full'> Room Members </div>

                    <div class='flex justify-center my-2'>
                        <button class="p-1 w-full whitespace-no-wrap border self-center justify-center border-transparent text-xs leading-5 font-medium rounded-md text-white bg-cool-gray-500  hover:bg-cool-gray-700 focus:outline-none focus:border-cool-gray-700 focus:shadow-outline-indigo active:bg-cool-gray-700 transition duration-150 ease-in-out"
                            onClick={()=>this.muteAllOthers()} 
                        >
                            Mute
                        </button>
                    </div>

                    <div class='flex justify-center my-2'>
                        <button class="p-1 w-full border self-center justify-center border-transparent text-xs leading-5 font-medium rounded-md text-white bg-teal-500  hover:bg-teal-700 focus:outline-none focus:border-teal-700 focus:shadow-outline-teal active:bg-teal-700 transition duration-150 ease-in-out"
                                onClick={()=>this.unmuteAllOthers()} 
                        >
                            Unmute
                        </button>
                    </div>


                    <div class='p-1 my-2 text-xs text-center text-gray-800 border border-black w-full'> Draw </div>

                    <div class="flex justify-center items-center my-2"> 
                        <input id='_color_picker' class="w-2/3" type="color" value="#be2019" />
                    </div>

                    <label class='py-1 my-1 rounded hover:bg-gray-300 flex justify-center items-center btn-light btn btn-sm radio-btn-item draw-ctrl-btn'> 
                        <input class='radio-btn' type='radio' name='draw-mode' id='free-draw' value='free-draw' autocomplete='off' /> 
                        {TwIcons.squigglyLineIcon("radio-icon studio-btn")}
                    </label>
                    <label class='py-1 my-1 rounded hover:bg-gray-300 flex justify-center items-center btn-light btn btn-sm radio-btn-item draw-ctrl-btn'> 
                        <input class='radio-btn' type='radio' name='draw-mode' id='line-draw' value='line-draw' autocomplete='off' /> 
                        {TwIcons.lineIcon("radio-icon studio-btn")}
                    </label>                     
                    <label class='py-1 my-1 rounded hover:bg-gray-300 flex justify-center items-center btn-light btn btn-sm radio-btn-item draw-ctrl-btn'> 
                        <input class='radio-btn' type='radio' name='draw-mode' id='circle-draw' value='circle-draw' autocomplete='off' /> 
                        {TwIcons.circleIcon("radio-icon studio-btn")}
                    </label>                     
                    <label class='py-1 my-1 rounded hover:bg-gray-300 flex justify-center items-center btn-light btn btn-sm radio-btn-item draw-ctrl-btn'> 
                        <input class='radio-btn' type='radio' name='draw-mode' id='arrow-draw' value='arrow-draw' autocomplete='off' /> 
                        {TwIcons.arrowUpRightIcon("radio-icon studio-btn")}
                    </label>
                    <label class='py-1 my-1 rounded hover:bg-gray-300 flex justify-center items-center btn-light btn btn-sm radio-btn-item draw-ctrl-btn'> 
                        <input class='radio-btn' type='radio' name='draw-mode' id='erase' value='erase' autocomplete='off' /> 
                        {TwIcons.pencilEraserIcon("radio-icon studio-btn")}
                    </label>
                    <label class='py-1 my-1 rounded hover:bg-gray-300 flex justify-center items-center btn-light btn btn-sm radio-btn-item draw-ctrl-btn'> 
                        <input class='radio-btn' type='radio' name='draw-mode' id='clear' value='clear' autocomplete='off' /> 
                        {TwIcons.garbageCanIcon("radio-icon studio-btn")}
                    </label>

                </div>
                
                <div id="grid-controls" class="mt-2 align-top box-border flex-row items-center justify-center relative shadow-sm z-0" > 
                    
                    <div class='p-1 my-2 text-xs text-gray-800 text-center border border-black w-full'> Grid </div>

                     <label class='grd-toggle-btn my-1 py-1 studio-btn rounded hover:bg-gray-300 flex justify-center items-center btn-light btn btn-sm radio-btn-item '> 
                        {TwIcons.gridToggleIcon()}
                    </label>

                     <label class='grd-smaller-btn my-1 py-1 studio-btn rounded hover:bg-gray-300 flex justify-center items-center btn-light btn btn-sm radio-btn-item'> 
                        {TwIcons.linesTogetherIcon()}
                    </label>

                    <label class='grd-larger-btn my-1 py-1 studio-btn rounded hover:bg-gray-300 flex justify-center items-center btn-light btn btn-sm radio-btn-item'> 
                        {TwIcons.linesApartIcon()}
                    </label>

                    <label class='grd-lighter-btn my-1 py-1 studio-btn rounded hover:bg-gray-300 flex justify-center items-center btn-light btn btn-sm radio-btn-item'> 
                        {TwIcons.brightenIcon()}
                    </label>

                    <label class='grd-darker-btn my-1 py-1 studio-btn rounded hover:bg-gray-300 flex justify-center items-center btn-light btn btn-sm radio-btn-item'> 
                        {TwIcons.darkenIcon()}
                    </label>

                </div>
            </div>
        );
    }

    setIsFocusSynched(isOn: boolean, toggle?: TwToggle): void  {
        this.isFocusSynched = isOn;
        this.logInfo('studio synched toggled to: '+isOn+" | name is "+toggle.getName());
        // alert('studio synched toggled to: '+isOn+" | name is "+toggle.getName());
    }

    leaveStudio(btnPressed: boolean): void {
        if (btnPressed && this.companyNm.toLowerCase() === "ghf" && this.isTrainer() && this.isSignaturesEnabled) {
            this.leaveStudioBtn.innerText = "Submitting...";
            this.leaveStudioBtn.disabled = true;
            this.snapAllAndSubmit();
        } else if (this.room) {
            this.room.disconnect();
        }

    }

    snapAllAndSubmit() {

        let elParticipants = document.getElementById("participants-section");
        snapAllVideos(elParticipants);
        let self = this;

        return domtoimage.toPng(elParticipants)
            .then( (imgData) => {
                let dataBlob = dataURItoBlob(imgData);
                self.uploadImageToKnack(dataBlob)
                    .then((sResponseData) => {
                    let oImgData = JSON.parse(sResponseData);
                    //e.g.: {"id":"5ee520cede14d900157546e4","type":"file","filename":"blob","public_url":"https://api.knack.com/v1/applications/5e3c60c311236b0016152a67/download/asset/5ee520cede14d900157546e4/blob","thumb_url":"","size":863857}
                    let sImgFileId = oImgData.id;
                    let sImgUrl = oImgData.public_url;
                    $.ajax({
                        type: 'POST',
                        url: `${TwConstants.ROOMS_API_URL}/${self.getRoomSid()}/signature`,
                        data: {
                            company: self.companyNm,
                            imgId: sImgFileId,
                            imgUrl: sImgUrl,
                            clientId: self.clientId,
                            trainerId: self.trainerId
                            //imgBase64: dataBlob
                        }
                    }).done(() =>  {
                        alert("Snapshot was submitted to the server.");
                        clearAllSnaps();
                        if (self.room) {
                            //TODO: blows up
                            self.room.disconnect();
                        }
                    });
                });
            });
    }

    muteAllOthers(): void { // driven by trainer via button-click
        this.sendTrainerMuteAll();
        this.PUT_updateRoomState({isRoomMuted: true})
    }

    unmuteAllOthers(): void { // driven by trainer via button-click
        this.sendTrainerUnmuteAll();
        this.PUT_updateRoomState({isRoomMuted: false});
    }

    // mute(Local?)Participant(participantSid: string) {
    //     if (this.room && this.localTwParticipant && participantSid === this.room.localParticipant.sid) {
    //         this.localTwParticipant.muteByTrainer();
    //     }
    // }

    // unmute(Local?)Participant(participantSid: string) {
    //     if (this.room && this.localTwParticipant && participantSid === this.room.localParticipant.sid) {
    //         this.localTwParticipant.unmuteByTrainer();
    //     }
    // }

    sendMuteParticipantMsg(participantSid: string) {
        if (this.isTrainer()) {
            let msgMute = { type: "mute",
                            props: {
                                scope: `${participantSid}`,
                                roomNm: this.getRoomNm(),  
                                requesterSid: this.room.localParticipant.sid, 
                                requesterIdentity: this.room.localParticipant.identity,
                                requesterIsTrainer: this.isTrainer()
                            }
                        };
            
            this.logDebug(`Trainer sending message to mute all non-trainers: `, msgMute); 

            this.sendMessage(msgMute);
        }
    }

    sendUnmuteParticipantMsg(participantSid: string) {

        if (this.isTrainer()) {
            let msgUnmute = { type: "unmute",
                            props: {
                                scope: `${participantSid}`,
                                roomNm: this.getRoomNm(),  
                                requesterSid: this.room.localParticipant.sid, 
                                requesterIdentity: this.room.localParticipant.identity,
                                requesterIsTrainer: this.isTrainer()
                            }
                            };
            
            this.logDebug(`Trainer sending message to mute all non-trainers: `, msgUnmute); 

            this.sendMessage(msgUnmute);

        }

    }

    sendTrainerMuteAll() {
        
        if (this.isTrainer()) {

            this.isRoomMuted = true;

            let reqMuteData = { type: "mute",
                                props: {
                                scope: "*",
                                roomNm: this.getRoomNm(),  
                                requesterSid: this.room.localParticipant.sid, 
                                requesterIdentity: this.room.localParticipant.identity,
                                requesterIsTrainer: this.isTrainer()
                                }
                            };
            
            this.logDebug(`Trainer sending message to mute all non-trainers: `, reqMuteData); 

            this.sendMessage(reqMuteData);
        }
    }

    sendTrainerUnmuteAll() {

        if (this.isTrainer()) {
            this.isRoomMuted = false;

            let reqUnmuteData = {   type: "unmute",
                                    props: {
                                    scope: "*",
                                    roomNm: this.getRoomNm(),  
                                    requesterSid: this.room.localParticipant.sid, 
                                    requesterIdentity: this.room.localParticipant.identity,
                                    requesterIsTrainer: this.isTrainer()
                                    }
                            };
                            
            this.logDebug(`Trainer sending message to unmute all non-trainers: `, reqUnmuteData); 

            this.sendMessage(reqUnmuteData);
        }

    }

    isTrainer(candidateSid: string = "", candidateIdentity: string = ""): boolean {
        
        let localParticipantSid = (this.room?.localParticipant?.sid)||"";

        if (candidateSid === "") {
            candidateSid = localParticipantSid;
        }

        if (candidateSid === "") {
            this.logWarn("no sid supplied to isTrainer");
            return false;
        }

        let localParticipantIdentity = (this.room?.localParticipant?.identity)||"";

        if (candidateIdentity === "") {
            candidateIdentity = localParticipantIdentity;
        }

        let participantProps = parseParicipantProps(candidateIdentity);

        if(participantProps.isTrainer ) {
            return true;

        } else if (candidateSid === localParticipantSid && this.isParticipantTrainer) {
            return true;
            
        } else {
            return false;
        }
    }

    getNavSectionRendering() {
        //TODO: currently hidden
        
        return (
            < nav class="bg-white border-b border-gray-200" ref={nav => this.tobBarRef = nav}>
                <div class="max-w-7xl mx-auto px-2">
                    <div class="flex justify-between h-16">
                        <div class="flex">
                            <div class="flex-shrink-0 flex items-center">
                                <img class="block lg:hidden h-8 w-auto" src="https://tailwindui.com/img/logos/workflow-mark-on-white.svg" alt="Workflow logo"/>
                                <img class="hidden lg:block h-8 w-auto" src="https://tailwindui.com/img/logos/workflow-logo-on-white.svg" alt="Workflow logo"/>
                            </div>
                        </div>
                        <div class="hidden sm:ml-6 sm:flex sm:items-center">
                            <button class="p-1 border-2 border-transparent text-gray-400 rounded-full hover:text-gray-500 focus:outline-none focus:text-gray-500 focus:bg-gray-100 transition duration-150 ease-in-out" aria-label="Notifications">
                                <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"/>
                                </svg>
                            </button>

                            <div class="ml-3 relative">
                                <div>
                                    <button class="flex text-sm border-2 border-transparent rounded-full focus:outline-none focus:border-gray-300 transition duration-150 ease-in-out" id="user-menu" aria-label="User menu" aria-haspopup="true">
                                        <img class="h-8 w-8 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt=""/>
                                    </button>
                                </div>
                                <div class="absolute hidden mt-2 origin-top-right right-0 rounded-md shadow-lg w-48">
                                    <div class="py-1 rounded-md bg-white shadow-xs">
                                        <a href="#" class="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 transition duration-150 ease-in-out">Your Profile</a>
                                        <a href="#" class="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 transition duration-150 ease-in-out">Settings</a>
                                        <a href="#" class="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 transition duration-150 ease-in-out">Sign out</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="-mr-2 flex items-center sm:hidden">

                            <button class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">

                                <svg class="block h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"/>
                                </svg>

                                <svg class="hidden h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="hidden sm:hidden">
                    <div class="pt-2 pb-3">
                        <a href="#" class="block pl-3 pr-4 py-2 border-l-4 border-indigo-500 text-base font-medium text-indigo-700 bg-indigo-50 focus:outline-none focus:text-indigo-800 focus:bg-indigo-100 focus:border-indigo-700 transition duration-150 ease-in-out">Dashboard</a>
                        <a href="#" class="mt-1 block pl-3 pr-4 py-2 border-l-4 border-transparent text-base font-medium text-gray-600 hover:text-gray-800 hover:bg-gray-50 hover:border-gray-300 focus:outline-none focus:text-gray-800 focus:bg-gray-50 focus:border-gray-300 transition duration-150 ease-in-out">Team</a>
                        <a href="#" class="mt-1 block pl-3 pr-4 py-2 border-l-4 border-transparent text-base font-medium text-gray-600 hover:text-gray-800 hover:bg-gray-50 hover:border-gray-300 focus:outline-none focus:text-gray-800 focus:bg-gray-50 focus:border-gray-300 transition duration-150 ease-in-out">Projects</a>
                        <a href="#" class="mt-1 block pl-3 pr-4 py-2 border-l-4 border-transparent text-base font-medium text-gray-600 hover:text-gray-800 hover:bg-gray-50 hover:border-gray-300 focus:outline-none focus:text-gray-800 focus:bg-gray-50 focus:border-gray-300 transition duration-150 ease-in-out">Calendar</a>
                    </div>
                    <div class="pt-4 pb-3 border-t border-gray-200">
                        <div class="flex items-center px-4">
                            <div class="flex-shrink-0">
                                <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt=""/>
                            </div>
                            <div class="ml-3">
                                <div class="text-base font-medium leading-6 text-gray-800">Tom Cook</div>
                                <div class="text-sm font-medium leading-5 text-gray-500">tom@example.com</div>
                            </div>
                        </div>
                        <div class="mt-3" role="menu" aria-orientation="vertical" aria-labelledby="user-menu">
                            <a href="#" class="block px-4 py-2 text-base font-medium text-gray-500 hover:text-gray-800 hover:bg-gray-100 focus:outline-none focus:text-gray-800 focus:bg-gray-100 transition duration-150 ease-in-out" role="menuitem">Your Profile</a>
                            <a href="#" class="mt-1 block px-4 py-2 text-base font-medium text-gray-500 hover:text-gray-800 hover:bg-gray-100 focus:outline-none focus:text-gray-800 focus:bg-gray-100 transition duration-150 ease-in-out" role="menuitem">Settings</a>
                            <a href="#" class="mt-1 block px-4 py-2 text-base font-medium text-gray-500 hover:text-gray-800 hover:bg-gray-100 focus:outline-none focus:text-gray-800 focus:bg-gray-100 transition duration-150 ease-in-out" role="menuitem">Sign out</a>
                        </div>
                    </div>
                </div>
            </ nav>
        );
    };
    
    getParticipantCount() {
        let participants = 0;
        if (this.room?.localParticipant) {
            participants++;
        }

        if (this.room?.participants) {
            participants = participants + this.room.participants.size;
        }

        return participants;
    }

    stopLocalParticipantTracks(localParticipant: LocalParticipant) {
         for (let [_, track] of localParticipant.tracks) {
            if (track.kind === TwConstants.AUDIO_TRACK_KIND || track.kind === TwConstants.VIDEO_TRACK_KIND) {
                let avTrack = track as (LocalAudioTrackPublication|LocalVideoTrackPublication);
                this.logInfo(`Stopping local participant track of kind ${track.kind}`);
                avTrack.track.stop();
            }
        }
    }


    unpublishLocalParticipantTracks(localParticipant: LocalParticipant) {
        for (let [_, track] of localParticipant.tracks) {
            track.unpublish();
            this.logInfo(`Unpublished local participant track of kind ${track.kind}`);
        }
    }

    detachLocalParticipantTracks(localParticipant: LocalParticipant) {

        for(let [_, trackPub] of localParticipant.tracks) {
            this.detachLocalParticipantTrack(trackPub.track);
        }
    }

    detachLocalParticipantTrack(track: LocalTrack) {
        if (track.kind === "audio" || track.kind === "video") {
            let detachedElements = track.detach() as HTMLMediaElement[];
            for(let detachedElement of detachedElements) {
                detachedElement.remove();
            }
        }
    }

    gotRemoteDataTrackMessage(dataMsg: string) {
        const self = this;
        const roomSid = this.room?.sid || "_NO_ROOM_SID";

        const { type, props } = JSON.parse(dataMsg);
        if (type === "snapshot") {
            HTTP.GET(`${TwConstants.ROOMS_API_URL}/${this.room.sid}/snapshot`, { roomSid: this.room.sid })
                .then((response) => {
                    
                    let respRoomSid     = response.roomSid;       
                    let participantSid  = response.participantSid;
                    let imageSnap       = response.imageSnap;

                    this.snappedImageUrl = imageSnap;
                    this.snappedParticipantSid = participantSid;

                    if (roomSid === respRoomSid) {
                        let twParticipant = this._members.get(participantSid) as TwParticipant;
                        if (twParticipant) {
                            twParticipant.attachImage(imageSnap);
                        }
                    } else {
                        self.logWarn(`'snapshot' message to room sid ${respRoomSid} does not match this room's sid ${roomSid}`)
                    }

            })
            .then((response) => {
                this.logInfo("Request for room posted, response: ", response);
            })
            .fail((jqXHR, sTextStatus, sErrorThrown) => {
                this.logError(`Error requesting Twilio token from train/with server. Status: ${sTextStatus}; Error: ${sErrorThrown}`);
                console.error(jqXHR, sTextStatus, sErrorThrown);
                alert(sTextStatus+": "+sErrorThrown);
            });

        } else if (type == "snapshot-cleared") {
            const { participantSid } = props;
            this.snappedImageUrl = "";
            this.snappedParticipantSid = "";

            this.logInfo("participant sent clear snapshot to "+participantSid);
            this.localTwParticipant.clearSnapshotCanvas();      

        } else if (type === "mute") {
            const {scope, roomNm, requesterSid, requesterIdentity, requesterIsTrainer} = props;

            if (scope === "*" || scope === this.room.localParticipant.sid) {
                this.logDebug(`(local) participant ${TwStudio.getParticipantIdentity(this.room.localParticipant)} in studio ${roomNm} received message to mute from participant ${requesterIdentity} (sid: ${requesterSid}); is-trainer? ${requesterIsTrainer}`);
                this.localTwParticipant.muteByTrainer();
            }
        
        } else if (type === "unmute") {
            const {scope, roomNm, requesterSid, requesterIdentity, requesterIsTrainer} = props;

            if (scope === "*" || scope === this.room.localParticipant.sid) {
                this.logDebug(`(local) participant ${TwStudio.getParticipantIdentity(this.room.localParticipant)} in studio ${roomNm} received message to unmute from participant ${requesterIdentity} (sid: ${requesterSid}); is-trainer? ${requesterIsTrainer}`);
                this.localTwParticipant.unmuteByTrainer();
            }
    
        } else if (type === "blur") {
            const { blurId, blur } = props;
            let oMemberView = this._members.get(blurId) as TwParticipant;
            oMemberView.toggleBlur(blurId, blur);

        } else if (type === "focus") {
            const { focusedOnId, focusingId } = props;
            if (this.isFocusSynched)  {
                this.setFocusedParticipant(focusedOnId);
                this.logInfo(`focus requested by ${focusingId} & user isFocusSynched`);
                // this.doFocusMember(focusedOnId, focusingId);
            } else {
                this.logInfo(`focus requested by ${focusingId} but was ignored: user !isFocusSynched`);  
            }
        

        } else if (type === "unfocus") {
            if (this.isFocusSynched)  {
                this.setFocusedParticipant("");
                this.logInfo(`clear focus requested: user isFocusSynched`);
            } else {
                this.logInfo(`clear focus requested but was ignored: user !isFocusSynched`);  
            }
        } else if (type === "grid") {
            const { isOverlaid, nSpacingWidth, nTransparencyPct } = props;            
            this.setGridOverlay(isOverlaid, nSpacingWidth, nTransparencyPct);

        } else if (type === "clear") {
            let { memberId } = props;
            let oMemberView: TwParticipant = this._members.get(memberId);
            if (oMemberView) {
                oMemberView.clearAllDrawn();
            }

        } else if (type === "erase") {
            const { memberId, lines } = props;

            let oMemberView: TwParticipant = this._members.get(memberId);

            if (oMemberView) {
                oMemberView.clearAllDrawn();
                oMemberView.drawLines(lines);
            }

        } else if (type === "draw") { // type === "draw"

            const { memberId, lines, drawMode, drawColor, origWidth, origHeight, x1, y1, x2, y2 } = props;

            this.logDebug(
                "received drawing data {member-Id, origin-Width, origin-Height, prev-X, prev-Y, new-X, new-Y}",
                "{" + memberId, origWidth, origHeight, x1, y1, x2, y2 + "}"
            );

            let oMemberView: TwParticipant = this._members.get(memberId);
            let drawOnCanvas = this.getDrawingCanvas(memberId); 

            if (!oMemberView || !drawOnCanvas) {
                this.logInfo(`message to draw meant for another client ${memberId}`);
            } else {
                // let eMemberView = oMemberView?.getMemberView(memberId);

                let tgtCanvasHeight = drawOnCanvas.offsetHeight;
                let tgtCanvasWidth = drawOnCanvas.offsetWidth;

                let widthRatio =
                    origWidth && origWidth !== null && origWidth !== 0
                        ? tgtCanvasWidth / origWidth : 1;

                let heightRatio =
                    origHeight && origHeight !== null && origHeight !== 0
                        ? tgtCanvasHeight / origHeight : 1;

                let adjLines = [];
                let nLines = lines.length;

                for (var iLine = 0; iLine < nLines; iLine++) {
                    let oLine = lines[iLine];
                    adjLines.push({
                        mode: oLine.mode,
                        color: oLine.color,
                        x1: oLine.x1 * widthRatio, y1: oLine.y1 * heightRatio,
                        x2: oLine.x2 * widthRatio, y2: oLine.y2 * heightRatio
                    });
                }

                // oMemberView.clearCanvas(drawOnCanvas);
                oMemberView.drawLinesAndLine(adjLines, drawMode, drawColor, x1 * widthRatio, y1 * heightRatio, x2 * widthRatio, y2 * heightRatio);
            }
        } else {
            this.logError(`received data message of unknown type ${type}`, props);
        }
    
    }

    getEraseThickness(): number {
        return this._eraseThickness
    }

    getDrawColor(): string {
        return this._drawColor;
    }

    getDrawWidth(): number {
        return this._drawWidth;
    }

    getDrawMode(): string {
        return this._drawMode;
    }

    setGridOverlay(isGridOverlaid: boolean, nGridSpacingWidth: number, nGridTransparencyPct: number) {
        this.isGridEnabled = isGridOverlaid;
        this.gridSpacing = nGridSpacingWidth;
        this.gridTransparency = nGridTransparencyPct;
        let gridStyle = this.calcGridOverlayStyle();
        Array.from(this._members.values()).forEach(oMemberView => {
            oMemberView.setGridOverlay(this.isGridEnabled, gridStyle);
        });    

    }

    getDrawingCanvas(participantSid: string): HTMLCanvasElement {
        if (this._members.has(participantSid)) {
            let twParticipant = this._members.get(participantSid);
            return twParticipant.overlayCanvasRef;
        } else {
            let participantContainer = document.getElementById(participantSid);
            if (participantContainer) {
                return participantContainer.querySelector("canvas.overlay-canvas") as HTMLCanvasElement;
            } else {
                return null;
            }
        }
    }
    
    // @State() subscribedSids: string[] = [];
    // @Watch('subscribedSids')
    // onSubscribedSidsChange(newSids: string[], oldSids: string[]) {
    //     for (let oldSid of oldSids) {
    //         if (!newSids.includes(oldSid)) {
    //             this.unsubscribeToParticipantAudio(oldSid);
    //             this.unsubscribeToParticipantVideo(oldSid);
    //         }
    //     }

    //     for (let newSid of newSids) {
    //         if (!oldSids.includes(newSid)) {
    //             this.subscribeToParticipantAudio(newSid);
    //             this.subscribeToParticipantVideo(newSid);
    //         }

    //     }
    // }

    // addSubscribedToSid(newSid: string) {
    //     if (!this.subscribedSids.includes(newSid)) {
    //         this.subscribedSids = [newSid, ...this.subscribedSids];
    //     }
    // }

    // removeSubscribedToSid(sid: string) {
    //     if (!this.subscribedSids.includes(sid)) {
    //         this.subscribedSids = [...this.subscribedSids.filter(currSid => currSid != sid)];
    //     }
    // }

    // subscribeToParticipantAudio(publisherSid: string) {
    //     return this.subscribeToTrackKind("audio", publisherSid);
    // }

    // unsubscribeToParticipantAudio(publisherSid: string) {
    //     return this.unsubscribeToTrackKind("audio", publisherSid);
    // }

    // subscribeToParticipantVideo(publisherSid: string) {
    //     return this.subscribeToTrackKind("video", publisherSid);
    // }

    // unsubscribeToParticipantVideo(publisherSid: string) {
    //     return this.unsubscribeToTrackKind("video", publisherSid);
    // }
    
    /** always send current unsubscription rules patched with the change 
        patch the subscriptions according to this new rule
        on the server: subscribe all (rule#1) then iterate through publishers.... might be too many rules.

        instead: rule #1: sub all
        rule #2..n: ONLY unsubscribed tracks 

    */
    // unsubscribeToTrackKind(trackKind: string, publisherSid: string) {
    //     this.logInfo(`toggling video subscription for participant ${publisherSid}`);

    //     let exclusions: string[][] = [];
        
    //     let remoteParticipants = this.room.participants as Map<Participant.SID, RemoteParticipant>;

    //     for(let [remPartSid, remPart] of remoteParticipants) {

    //         let isDataSub = false, isAudioSub = false, isVideoSub = false;

    //         for(let [, trackPub] of remPart.dataTracks) {
    //             isDataSub = isDataSub || (trackPub.track && trackPub.isSubscribed);
    //         }

    //         for(let [, trackPub] of remPart.audioTracks) {
    //             isAudioSub = isAudioSub || (trackPub.track && trackPub.isSubscribed);
    //         }

    //         for(let [, trackPub] of remPart.videoTracks) {
    //             isVideoSub = isVideoSub || (trackPub.track && trackPub.isSubscribed);
    //         }

    //         if (!isDataSub || (publisherSid === remPartSid && trackKind === 'data')) {
    //             exclusions.push([remPartSid, "data"]);
    //         }

    //         if (!isAudioSub|| (publisherSid === remPartSid && trackKind === 'audio')) {
    //             exclusions.push([remPartSid, "audio"]);
    //         }

    //         if (!isVideoSub|| (publisherSid === remPartSid && trackKind === 'video')) {
    //             exclusions.push([remPartSid, "video"]);
    //         }
    //     }

    //     let unsubscribedInfo = {
    //         type: "update", 
    //         subscriberSid: this.localParticipant.sid, 
    //         exclusions: exclusions,
    //         roomSid: this.room.sid
    //     };

    //     let subscriptionUrl = TwConstants.subscriptionsApi(this.room.sid);

    //     this.logInfo(`changing to ${trackKind} subsciption to unsubscribed through url ${subscriptionUrl}`, unsubscribedInfo);

    //     this.POST(subscriptionUrl, unsubscribedInfo);
    // }
    
    // subscribeToTrackKind(trackKind: string, publisherSid: string) {
    //     this.logInfo(`changing to subscribe to track kind ${trackKind} for participant ${publisherSid}`);

    //     let exclusions: string[][] = [];
        
    //     let remoteParticipants = this.room.participants as Map<Participant.SID, RemoteParticipant>;

    //     for(let [remPartSid, remPart] of remoteParticipants) {

    //         let isDataSub  = (remPartSid === publisherSid && trackKind === 'data'),
    //             isAudioSub = (remPartSid === publisherSid && trackKind === 'audio'),
    //             isVideoSub = (remPartSid === publisherSid && trackKind === 'video');

    //         for(let [, trackPub] of remPart.dataTracks) {
    //             isDataSub = isDataSub || (trackPub.track && trackPub.isSubscribed);
    //         }

    //         for(let [, trackPub] of remPart.audioTracks) {
    //             isAudioSub = isAudioSub || (trackPub.track && trackPub.isSubscribed);
    //         }

    //         for(let [, trackPub] of remPart.videoTracks) {
    //             isVideoSub = isVideoSub || (trackPub.track && trackPub.isSubscribed);
    //         }

    //         if (!isDataSub) {
    //             exclusions.push([remPartSid, "data"]);
    //         }

    //         if (!isAudioSub) {
    //             exclusions.push([remPartSid, "audio"]);
    //         }

    //         if (!isVideoSub) {
    //             exclusions.push([remPartSid, "video"]);
    //         }

    //     }

    //     let unsubscribedInfo = {
    //         type: "update", 
    //         subscriberSid: this.localParticipant.sid, 
    //         exclusions: exclusions,
    //         roomSid: this.room.sid
    //     };

    //     let subscriptionUrl = TwConstants.subscriptionsApi(this.room.sid);

    //     this.logInfo(`submitting unsubscribed through url ${subscriptionUrl}`, unsubscribedInfo);

    //     this.POST(subscriptionUrl, unsubscribedInfo);

    // }

}
