
export class Layout {
    numRows: number = 0;
    numCols: number = 0;
    cellWidth: number = 0;
    cellHeight: number = 0;
}

// taken from: 
// https://math.stackexchange.com/questions/466198/algorithm-to-get-the-maximum-size-of-n-squares-that-fit-into-a-rectangle-with-a/2570649#2570649?newreg=ff9ef0e3152e4d6aa5e7e3d7b1b55a6b
export function calcLayout(x:number, y:number, n:number): Layout {

    let rect_ratio = 1.5;//1.7777; // NEW

    // Compute number of rows and columns, and cell size
    var ratio = ((x / y)*1.0); // / rect_ratio;    // NEW
    var ncols_float = Math.sqrt(n * ratio);
    var nrows_float = n / ncols_float;

    // Find best option filling the whole height
    var nrows1 = Math.ceil(nrows_float);
    // var ncols1 = Math.ceil(n / nrows1);
    while (((nrows1) * ratio) < Math.ceil(n / (nrows1))) {
        nrows1++;
        // ncols1 = Math.ceil(n / nrows1);
    }

    nrows1 = nrows1;
    let y1 = y;;//* (1/rect_ratio);  // NEW
    var cell_size1 = (y1 * nrows1)*1.0;

    // Find best option filling the whole width
    var ncols2 = Math.ceil(ncols_float);
    // var nrows2 = Math.ceil(n / ncols2);
    while ((ncols2) < (Math.ceil(n / (ncols2)) * ratio)) {
        ncols2++;
        // nrows2 = Math.ceil(n / ncols2);
    }

    let x1 = x;// * (rect_ratio);  // NEW
    ncols2 = ncols2;
    var cell_size2 = (x1 / ncols2)*1.0;

    // Find the best values
    var nrows, ncols, cell_size;
    if (cell_size1 > cell_size2) {
        nrows = nrows1;
        ncols = Math.ceil(n / nrows);
        cell_size = cell_size1;
    } else {
        ncols = ncols2;
        nrows = Math.ceil(n / ncols);
        cell_size = cell_size2;
    }

    // console.log('layout calcd', cell_size1, cell_size2, nrows1, ncols1, nrows2, ncols2, nrows, ncols);

    let bestLayout = new Layout();
    
    bestLayout.numCols = ncols;
    bestLayout.numRows = nrows;

    bestLayout.cellWidth = cell_size*(rect_ratio);
    bestLayout.cellHeight = cell_size;

    return bestLayout;
}