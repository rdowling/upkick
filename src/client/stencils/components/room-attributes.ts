
import { getBooleanQueryParam } from '../../utils/util';

export class RoomAttributes {

}

export class RoomType {  // TODO: consider keeping these in the roomNm 
    // group, group-small, peer-to-peer
    static peerToPeerRoom = "peer-to-peer";  // "tiny"
    static smallGroupRoom = "group-small";   // "small"
    static largeGroupRoom = "group";         // "group"
}

export class RoomStatus {
    // can be: {`in-progress`, `failed`, `completed`}
    static in_progress = "in-progress";
    static failed = "failed";
    static completed = "completed";
}


/**
 *  |  company  |  start studio?  |  use toolbar?  |  studio size  |  trainer-vis  |         client-vis        |
 *   ----------- ----------------- ---------------- --------------- --------------- ---------------------------
 *     GHF        trainers only	      trainers           group	      all media        trainers + client-audio
 *     FT         trainers only	       anyone	         small	      all media        trainers + client-audio
 *      ~          anyone	           anyone	         small	      all media        all media
 */

export class Companies {
    static GHF    = "GHF";
    static FT     = "FT";
    static PUBLIC = "*";
}

export class Privileges {

    privileges = {};

    constructor() {
        this.privileges[Companies.GHF]    = {};
        this.privileges[Companies.FT]     = {};
        this.privileges[Companies.PUBLIC] = {};
    }

    getStudioType(roomNm: string): string {
        
        let roomType = RoomType.smallGroupRoom;
        
        if (roomNm.toLowerCase().startsWith("ghf:")) {
            roomType = RoomType.largeGroupRoom;
        } 

        return roomType;

    }

    getHasTrainer(roomNm: string): boolean {
        let isTrainerRequired = false;
        roomNm = roomNm.toUpperCase();

        if (roomNm.startsWith(`${Companies.GHF}:`)) {
            isTrainerRequired = true;
        } 

        return isTrainerRequired;

    }

    canStartStudio(roomNm: string, isTrainer: boolean): boolean {
        roomNm = roomNm.toUpperCase();
        
        if (roomNm.startsWith(`${Companies.GHF}:`) || roomNm.startsWith(`${Companies.FT}:`)) {
            return isTrainer;
        } else {
            return true;
        }

    }

}

export interface RoomProps {  // roomNm max 128 characters ?hasTrainer=false&isRecording=false === 36
    /*  DISPLAY_NAME?hasTrainer=BOOL&isRecording=BOOL */
    displayNm: string;
    hasTrainer: boolean;
    isRecording: boolean;
    roomType: string;  // "tiny", "small", "group"
}

export function parseRequestedRoomSize(requestedRoomSize: string): string {
    requestedRoomSize = requestedRoomSize.toLowerCase().trim();
    if (requestedRoomSize === "tiny") {
        return RoomType.peerToPeerRoom;
    } else if (requestedRoomSize === "small") {
        return RoomType.smallGroupRoom;
    } else if (requestedRoomSize === "group") {
        return RoomType.largeGroupRoom;
    } else {
        return RoomType.peerToPeerRoom;
    }

}

export interface ParticipantProps {
    /*  DISPLAY_NAME?hasTrainer=BOOL&isRecording=BOOL */
    displayNm: string;
    isTrainer: boolean;
    isShadow: boolean;
}

export function parseParicipantProps(participantNm: string): ParticipantProps {
        
    let displayNm = getDisplayNm(participantNm);
    let isTrainer = getBooleanQueryParam(participantNm, "isTrainer");
    let isShadow  = getBooleanQueryParam(participantNm, "isShadow");

    return {displayNm: displayNm, isTrainer: isTrainer, isShadow: isShadow};
}


export function getDisplayNm(unparsedNm: string): string {
    let displayNm = "";
    let firstIndexOfQuestionMark = unparsedNm.indexOf("?");
    if (firstIndexOfQuestionMark >= 0) {
        displayNm = unparsedNm.substring(0, firstIndexOfQuestionMark );
    }

    return displayNm;
}

export class ParticipantType {
    static trainer = "trainer";
    static client  = "client";
}

export class EventStatus {
    eventNm: string = "";
    constructor(eventNm) {
        this.eventNm = eventNm;
    }

    equals(status): boolean {
        return this.eventNm === status;
    }
}

export class StatusEvents {

    static room_created = new EventStatus('room-created');
    static room_ended = new EventStatus('room-ended');
    
    static participant_connected = new EventStatus('participant-connected');
    static participant_disconnected = new EventStatus('participant-disconnected');

    static track_added = new EventStatus('track-added');
    static track_removed = new EventStatus('track-removed');
    static track_enabled = new EventStatus('track-enabled');
    static track_disabled = new EventStatus('track-disabled');

    static recording_stated = new EventStatus('recording-started');
    static recording_completed = new EventStatus('recording-completed');
    static recording_failed = new EventStatus('recording-failed');

}

export class RoomState {

    isRoomMuted: boolean = false;
    isGridEnabled: boolean = false;

    // TODO: last update time, last update msg id?

}