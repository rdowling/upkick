
import { Component, Listen, h } from '@stencil/core';

@Component({
  tag: 'tailwind-ex',
  styleUrl: 'tailwind-ex.scss'
})
export class TailwindEx {
  render() {
    return (
      <div>
        <h1>My Web Component</h1>

        <my-dropdown dropTitle="Toggle">
          Hello World
        </my-dropdown>
      </div>
    );
  }

  @Listen('onToggle')
  log(event) {
    console.log(event);
  }
}