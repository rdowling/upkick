import { Component, h } from '@stencil/core';
import { TwPoint } from '../../../structs';
import * as util from "../../../utils/util";

let _CLIENT_SEP_ = "__";

@Component({
    tag: 'tw-participant-canvas',
    styleUrl: 'tw-participant-canvas.css'
})
export class TwParticipantCanvas {

    _dataTrack: any; // Twilio.LocalDataTrack;

    _view: HTMLDivElement;

    _isMe = false;
    _isTrainer = false;

    // _mutedBySelf = false;
    // _mutedByTrainer = false;

    _clickWasDrawing = false;

    _defaultGridSpacingWidth = 100;
    _defaultGridTransparency = 0.5;

    _isGridVisible = false;

    _isFocused = false;
    _isFocusing = false;

    _nCurrGridSpacingWidth = this._defaultGridSpacingWidth;
    _nCurrTransparencyPct = this._defaultGridTransparency;

    _sCurrDrawColor = "#be2019";
    _nCurrDrawWidth = 3;

    _drawMode = "free-draw";// "clear", "erase", "free-draw", "line-draw", "circle-draw", "arrow-draw";

    _eraseThickness = 10;

    _isDrawing = false;
    _startX = 0;
    _startY = 0;

    _lastX = 0;
    _lastY = 0;

    _lines = [];

    _videoDimensions = {width: 0, height: 0};
    key: string;
    participantIdentity: any;
    participantSid: string;

    render() {
        return (
            <div>
                <p>Hello TwParticipantCanvas!</p>
            </div>
        );
    }

    
    resizeCanvas() {

        console.log("resizing canvas of "+this.participantIdentity);

        let eParentContainer = document.getElementById(this.participantIdentity) as HTMLDivElement;

        if (eParentContainer) {        
            let eCanvas = eParentContainer.querySelector("canvas") as HTMLCanvasElement;
            if (eCanvas) {
                let nNewHeight = eParentContainer.clientHeight-12;
                let nNewWidth  = eParentContainer.clientWidth-12;

                eCanvas.width = nNewWidth;
                eCanvas.height = nNewHeight;
            }
        }

    }

    getDisplayName(): string {
        
        let sMemberNm = "";

        if (this.participantIdentity && this.participantIdentity.includes(_CLIENT_SEP_)) {
            sMemberNm =  this.participantIdentity.substring(this.participantIdentity.indexOf(_CLIENT_SEP_) + _CLIENT_SEP_.length );
        } else if (this.participantIdentity && this.participantIdentity !== "") {
            sMemberNm = this.participantIdentity;
        } else {
            sMemberNm = this.participantSid;
        }
   
        if (this._isMe) {
            sMemberNm = sMemberNm + " (You)";
        } else if (this._isTrainer) {
            sMemberNm = sMemberNm + " (Trainer)";
        }

        return sMemberNm;
    }

    toggleGridOverlay() {
        let eMember = document.getElementById(this.participantSid);
        if (eMember) {
            let eMemberCanvas = eMember.querySelector("canvas");
            eMemberCanvas.classList.toggle("grid-overlay");

            this._isGridVisible = eMemberCanvas.classList.contains("grid-overlay");
            if (!this._isGridVisible) {
                eMemberCanvas.style.backgroundImage = "";
            } else {
                eMemberCanvas.style.backgroundImage = this.calcGridOverlayStyle(this._nCurrGridSpacingWidth, this._nCurrTransparencyPct);
            }
        }
    }

    toggleBlur(blurId, blur) {
        console.log('toggleBlur', blurId, blur);
        let eMember = document.getElementById(this.participantSid);
        if (eMember) {
            let elVideo = eMember.querySelector("video") as HTMLVideoElement;
            if (blur) {
                if (!elVideo.classList.contains("blurred")) {
                    elVideo.classList.add("blurred");
                }
            } else {
                if (elVideo.classList.contains("blurred")) {
                    elVideo.classList.remove("blurred");
                }

            }
        }

    }

    reduceGridOverlay() {
        let eMember = document.getElementById(this.participantSid);
        if (eMember) {
            let eMemberCanvas = eMember.querySelector("canvas");
            let isOverlaid = eMemberCanvas.classList.contains("grid-overlay");

            if (isOverlaid) {
                this._nCurrGridSpacingWidth = this._nCurrGridSpacingWidth - 10;
                eMemberCanvas.style.backgroundImage = this.calcGridOverlayStyle(this._nCurrGridSpacingWidth, this._nCurrTransparencyPct);
            }
        }
    }

    enlargeGridOverlay() {
        let eMember = document.getElementById(this.participantSid);
        if (eMember) {
            let eMemberCanvas = eMember.querySelector("canvas");
            let isOverlaid = eMemberCanvas.classList.contains("grid-overlay");

            if (isOverlaid) {
                this._nCurrGridSpacingWidth = this._nCurrGridSpacingWidth + 10;
                eMemberCanvas.style.backgroundImage = this.calcGridOverlayStyle(this._nCurrGridSpacingWidth, this._nCurrTransparencyPct);
            }
        }
    }

    lightenGridOverlay() {
        let eMember = document.getElementById(this.participantSid);
        if (eMember) {
            let eMemberCanvas = eMember.querySelector("canvas");
            let isOverlaid = eMemberCanvas.classList.contains("grid-overlay");

            if (isOverlaid) {
                this._nCurrTransparencyPct = Math.max(this._nCurrTransparencyPct - 0.1, 0.0);
                eMemberCanvas.style.backgroundImage = this.calcGridOverlayStyle(this._nCurrGridSpacingWidth, this._nCurrTransparencyPct);
            }
        }
    }

    darkenGridOverlay() {
        let eMember = document.getElementById(this.participantSid);
        if (eMember) {
            let eMemberCanvas = eMember.querySelector("canvas");
            let isOverlaid = eMemberCanvas.classList.contains("grid-overlay");

            if (isOverlaid) {
                this._nCurrTransparencyPct = Math.min(this._nCurrTransparencyPct + 0.1, 1.0);
                eMemberCanvas.style.backgroundImage = this.calcGridOverlayStyle(this._nCurrGridSpacingWidth, this._nCurrTransparencyPct);
            }
        }
    }

    // nGridPctSize: {0.0 ... 1.0}
    // nTransparencyPct: {0.0 ... 1.0}
    calcGridOverlayStyle(nGridSpacingWidth: number, nTransparencyPct: number): string {
        let gridOverlayStyle =
            // `repeating-linear-gradient( 0deg, transparent, transparent ${nGridSpacingWidth}px, 
            //                             rgba(200,200,200, ${nTransparencyPct}) ${nGridSpacingWidth}px, 
            //                             rgba(200,200,200, ${nTransparencyPct}) ${(nGridSpacingWidth + 1)}px ), 
            // repeating-linear-gradient( -90deg, transparent, transparent ${nGridSpacingWidth}px, 
            //                             rgba(200,200,200, ${nTransparencyPct}) ${nGridSpacingWidth}px, 
            //                             rgba(200,200,200, ${nTransparencyPct}) ${(nGridSpacingWidth + 1)}px )"`;

            "repeating-linear-gradient(0deg, transparent, transparent " + nGridSpacingWidth + "px," +
            " rgba(200,200,200," + nTransparencyPct + ") " + nGridSpacingWidth + "px," +
            " rgba(200,200,200," + nTransparencyPct + ") " + (nGridSpacingWidth + 1) + "px " +
            "), " +
            "repeating-linear-gradient(-90deg, transparent, transparent " + nGridSpacingWidth + "px," +
            " rgba(200,200,200," + nTransparencyPct + ") " + nGridSpacingWidth + "px," +
            " rgba(200,200,200," + nTransparencyPct + ") " + (nGridSpacingWidth + 1) + "px " +
            ")";

        return gridOverlayStyle;
    }

    setGridOverlay(nMemberId, isOverlaid, nGridSpacingWidth, nTransparencyPct) {
        console.log('setGridOverlay', nMemberId, isOverlaid, nGridSpacingWidth, nTransparencyPct);

        let eMember = document.getElementById(this.participantSid);
        if (eMember) {
            let eMemberCanvas = eMember.querySelector("canvas");

            if (isOverlaid) {
                if (!eMemberCanvas.classList.contains("grid-overlay")) {
                    eMemberCanvas.classList.add("grid-overlay");
                }
            } else {
                if (eMemberCanvas.classList.contains("grid-overlay")) {
                    eMemberCanvas.classList.remove("grid-overlay");
                }
            }

            if (!eMemberCanvas.classList.contains("grid-overlay")) {
                eMemberCanvas.style.backgroundImage = "";
            } else {
                eMemberCanvas.style.backgroundImage = this.calcGridOverlayStyle(
                    nGridSpacingWidth,
                    nTransparencyPct
                );
            }
        }
    }
    getMemberView(participantSid: string): HTMLDivElement {
        let eParticipant = document.getElementById(participantSid) as HTMLDivElement;
        return eParticipant;
    }

    setupForDrawing() {

        let eMemberView: HTMLDivElement = this.getMemberView(this.participantSid);

        if (!eMemberView) {
            return;
        }

        let canvas = eMemberView.querySelector("canvas") as HTMLCanvasElement;

        canvas.addEventListener("mousedown", e => {
            let pos = { x: e.offsetX, y: e.offsetY };
            this.touchDown(canvas, pos.x, pos.y);
        });

        canvas.addEventListener("mousemove", e => {
            let pos = { x: e.offsetX, y: e.offsetY };
            this.touchMove(canvas, pos.x, pos.y);
        });

        canvas.addEventListener("mouseup", e => {
            let pos = { x: e.offsetX, y: e.offsetY };
            this.touchUp(canvas, pos.x, pos.y);
        });

        canvas.addEventListener("touchstart", e => {
            let pos = this.getTouchPos(canvas, e);
            // var touch = e.touches[0];
            this.touchDown(canvas, pos.x, pos.y);
        });

        canvas.addEventListener("touchmove", e => {
            let pos = this.getTouchPos(canvas, e);
            this.touchMove(canvas, pos.x, pos.y);
        });

        canvas.addEventListener("touchend", e => {
            let pos = this.getTouchPos(canvas, e);
            this.touchUp(canvas, pos.x, pos.y);
            // var mouseEvent = new MouseEvent("mouseup", {});
            // canvas.dispatchEvent(mouseEvent);
            // touchUp(e);
        });

    }

    // Get the position of a touch relative to the canvas
    getTouchPos(canvasDom, touchEvent) {
        if (!touchEvent.touches[0]) {
            return {
                x: this._startX,
                y: this._startY
            };
        } else {
            let touch = touchEvent.touches[0];
            if (touch.offsetX && touch.offsetY) {
                return {
                    x: touch.offsetX,
                    y: touch.offsetY
                };
            } else {
                var rect = canvasDom.getBoundingClientRect();
                return {
                    x: touch.clientX - rect.left,
                    y: touch.clientY - rect.top
                };
            }
        }
    }

    touchDown(canvas, posX, posY) {

        if (this._drawMode === "clear") {
            this.clearAllDrawn(canvas);
            this.sendClearNotif(this.participantSid);

        } else if (this._drawMode === "erase") {
            this.eraseLnsUnderPt({ x: posX, y: posY });
            this.clearCanvas(canvas);
            this.drawLines(canvas, this._lines);
            // notify of updated lines

        } else {

            this._isDrawing = true;

            this._startX = posX; // e.offsetX; // e.clientX - rect.left;
            this._startY = posY; // e.offsetY; // e.clientY - rect.top;

            this._clickWasDrawing = false;
        }
    }

    touchMove(canvas, posX, posY) {

        if (this.getDrawMode() === "erase") {
            this.eraseLnsUnderPt({ x: posX, y: posY });
            this.clearCanvas(canvas);
            this.drawLines(canvas, this._lines);

            let thisMemberId = this.participantSid;
            let theseLines = this._lines;

            let eraseData = JSON.stringify({
                type: "erase",
                props: {
                    memberId: thisMemberId,
                    lines: theseLines
                }
            });

            this._dataTrack.send(eraseData);

        } else if (this._isDrawing === true) {
            this._lastX = posX; // e.offsetX; // e.clientX - rect.left
            this._lastY = posY; // e.offsetY; // e.clientY - rect.top

            let origWidth = canvas.offsetWidth;
            let origHeight = canvas.offsetHeight;

            let drawMode = this.getDrawMode();
            let thisMemberId = this.participantSid;
            let theseLines = this._lines;

            let lineColor = this._sCurrDrawColor;

            let drawData = JSON.stringify({
                type: "draw",
                props: {
                    memberId: thisMemberId,
                    lines: theseLines,
                    drawMode: drawMode,
                    drawColor: lineColor,
                    origWidth: origWidth,
                    origHeight: origHeight,
                    x1: this._startX,
                    y1: this._startY,
                    x2: this._lastX,
                    y2: this._lastY
                }
            });

            console.debug("sending drawing data", drawData);

            this._dataTrack.send(drawData);

            this.drawLinesAndLine(canvas, this._lines, drawMode, lineColor, this._startX, this._startY, this._lastX, this._lastY);

            if (drawMode === "free-draw") {
                this._lines.push({
                    mode: drawMode,
                    color: lineColor,
                    x1: this._startX,
                    y1: this._startY,
                    x2: this._lastX,
                    y2: this._lastY
                });

                this._startX = this._lastX; // e.clientX - rect.left;
                this._startY = this._lastY; // e.clientY - rect.top;
            }

            this._clickWasDrawing = true;
        }
    }

    touchUp(canvas, posX, posY) {
        if (this._isDrawing === true) {
            this._isDrawing = false;

            let lineColor = this._sCurrDrawColor;

            this._lastX = posX; // e.offsetX; // e.clientX - rect.left;
            this._lastY = posY; // e.offsetY; // e.clientY - rect.top;

            let drawMode = this.getDrawMode();

            this.draw(canvas, drawMode, lineColor, this._startX, this._startY, this._lastX, this._lastY);
            this._lines.push({
                mode: drawMode,
                color: lineColor,
                x1: this._startX,
                y1: this._startY,
                x2: this._lastX,
                y2: this._lastY
            });

            this._lastX = 0;
            this._lastY = 0;

            this._startX = 0;
            this._startY = 0;
        }
    }


    drawLinesAndLine(canvas, lines, drawMode, lineColor, x1, y1, x2, y2) {

        // clear what's there
        this.clearCanvas(canvas);

        // draw lines so far
        this.drawLines(canvas, lines);

        // draw the current line
        this.draw(canvas, drawMode, lineColor, x1, y1, x2, y2);
    }

    drawLines(canvas, lines) {
        // redraw all previous lines
        for (var i = 0; i < lines.length; i++) {
            let line = lines[i];
            this.draw(canvas, line.mode, line.color, line.x1, line.y1, line.x2, line.y2);
        }

    }

    draw(canvas: HTMLCanvasElement, drawMode, lineColor, x1, y1, x2, y2) {
        if (drawMode === "circle-draw") {
            this.drawCircle(canvas, lineColor, x1, y1, x2, y2);
        } else if (drawMode === "line-draw") {
            this.drawLine(canvas, lineColor, x1, y1, x2, y2);
        } else if (drawMode === "arrow-draw") {
            this.drawArrow(canvas, lineColor, x1, y1, x2, y2);
        } else {
            this.drawLine(canvas, lineColor, x1, y1, x2, y2);
        }
    }

    getDrawMode(): String {
        return this._drawMode;
    }

    clearCanvas(canvas: HTMLCanvasElement): boolean {

        if (canvas) {
            const context = canvas.getContext("2d");
            if (context) {
                context.clearRect(0, 0, canvas.width, canvas.height);
                return true;
            }
        }

        return false;
    }

    clearAllDrawn(canvas: HTMLCanvasElement) {
        this.clearCanvas(canvas);
        this._lines = [];
    }

    sendClearNotif(memberId) {
        let drawData = JSON.stringify({
            type: "clear",
            props: {
                memberId
            }
        });

        console.debug("sending draw-clearing", drawData);
        this._dataTrack.send(drawData);
    }

    eraseLnsUnderPt(oP: TwPoint) {

        let nLines = this._lines.length;
        let iLine: number;
        let aLines2 = [];
        for (iLine = 0; iLine < nLines; iLine++) {
            let oLine = this._lines[iLine];
            if (!this.isPointInsideThickLineSegment(oP, { x: oLine.x1, y: oLine.y1 }, { x: oLine.x2, y: oLine.y2 }, this._eraseThickness)) {
                aLines2.push(oLine);
            }
        }
        this._lines = aLines2;

        // redraw all lines; send re-drawn
    }


    //http://stackoverflow.com/a/6868610
    //Tested and Working
    calcNearestPointOnLine(line1, line2, pnt) {
        var L2 = (((line2.x - line1.x) * (line2.x - line1.x)) + ((line2.y - line1.y) * (line2.y - line1.y)));
        if (L2 == 0) return false;
        var r = (((pnt.x - line1.x) * (line2.x - line1.x)) + ((pnt.y - line1.y) * (line2.y - line1.y))) / L2;

        return {
            x: line1.x + (r * (line2.x - line1.x)),
            y: line1.y + (r * (line2.y - line1.y))
        };
    }

    //Tested and Working
    calcDistancePointToLine(pnt: TwPoint, line1: TwPoint, line2: TwPoint): number {
        var L2 = (((line2.x - line1.x) * (line2.x - line1.x)) + ((line2.y - line1.y) * (line2.y - line1.y)));
        if (L2 == 0) return -1;
        var s = (((line1.y - pnt.y) * (line2.x - line1.x)) - ((line1.x - pnt.x) * (line2.y - line1.y))) / L2;
        return Math.abs(s) * Math.sqrt(L2);
    }

    isPointInsideLineSegment(pnt: TwPoint, line1: TwPoint, line2: TwPoint): boolean {
        var L2 = (((line2.x - line1.x) * (line2.x - line1.x)) + ((line2.y - line1.y) * (line2.y - line1.y)));
        if (L2 == 0) return false;
        var r = (((pnt.x - line1.x) * (line2.x - line1.x)) + ((pnt.y - line1.y) * (line2.y - line1.y))) / L2;

        return (0 <= r) && (r <= 1);
    }

    isPointInsideThickLineSegment(pnt: TwPoint, line1: TwPoint, line2: TwPoint, lineThickness: number): boolean {
        var L2 = (((line2.x - line1.x) * (line2.x - line1.x)) + ((line2.y - line1.y) * (line2.y - line1.y)));
        if (L2 == 0) return false;
        var r = (((pnt.x - line1.x) * (line2.x - line1.x)) + ((pnt.y - line1.y) * (line2.y - line1.y))) / L2;

        //Assume line thickness is circular
        if (r < 0) {
            //Outside line1
            return (Math.sqrt(((line1.x - pnt.x) * (line1.x - pnt.x)) + ((line1.y - pnt.y) * (line1.y - pnt.y))) <= lineThickness);
        } else if ((0 <= r) && (r <= 1)) {
            //On the line segment
            var s = (((line1.y - pnt.y) * (line2.x - line1.x)) - ((line1.x - pnt.x) * (line2.y - line1.y))) / L2;
            return (Math.abs(s) * Math.sqrt(L2) <= lineThickness);
        } else {
            //Outside line2
            return (Math.sqrt(((line2.x - pnt.x) * (line2.x - pnt.x)) + ((line2.y - pnt.y) * (line2.y - pnt.y))) <= lineThickness);
        }
    }

    drawCircle(canvas: HTMLCanvasElement, lineColor, x1, y1, x2, y2) {
        var ctx = canvas.getContext("2d");
        ctx.beginPath();
        ctx.strokeStyle = lineColor;
        ctx.arc(x1, y1, util.dist(x1, y1, x2, y2), 0, 2 * Math.PI);
        ctx.stroke();
    }

    drawLine(canvas: HTMLCanvasElement, lineColor, x1, y1, x2, y2) {
        let context = canvas.getContext("2d");

        context.beginPath();
        context.strokeStyle = lineColor;
        context.lineWidth = this._nCurrDrawWidth;
        context.moveTo(x1, y1);
        context.lineTo(x2, y2);
        context.stroke();
        context.strokeStyle = lineColor;
        context.closePath();
    }

    drawArrow(canvas: HTMLCanvasElement, lineColor, x1, y1, x2, y2) {
        //variables to be used when creating the arrow
        var ctx = canvas.getContext("2d");
        var headlen = 10;

        var angle = Math.atan2(y2 - y1, x2 - x1);

        //starting path of the arrow from the start square to the end square and drawing the stroke
        ctx.beginPath();
        ctx.strokeStyle = lineColor;
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.lineWidth = this._nCurrDrawWidth;
        ctx.stroke();

        //starting a new path from the head of the arrow to one of the sides of the point
        ctx.beginPath();
        ctx.strokeStyle = lineColor;
        ctx.moveTo(x2, y2);
        ctx.lineTo(
            x2 - headlen * Math.cos(angle - Math.PI / 7),
            y2 - headlen * Math.sin(angle - Math.PI / 7)
        );

        //path from the side point of the arrow, to the other side point
        ctx.lineTo(
            x2 - headlen * Math.cos(angle + Math.PI / 7),
            y2 - headlen * Math.sin(angle + Math.PI / 7)
        );

        //path from the side point back to the tip of the arrow, and then again to the opposite side point
        ctx.lineTo(x2, y2);
        ctx.lineTo(
            x2 - headlen * Math.cos(angle - Math.PI / 7),
            y2 - headlen * Math.sin(angle - Math.PI / 7)
        );

        //draws the paths created above
        ctx.strokeStyle = lineColor;
        ctx.lineWidth = this._nCurrDrawWidth;
        ctx.stroke();
        ctx.strokeStyle = lineColor;
        ctx.fillStyle = lineColor;
        ctx.fill();
    }


}
