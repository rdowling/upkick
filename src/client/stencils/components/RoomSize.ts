
export class RoomSize {  // TODO: consider keeping these in the roomNm 
    // group, group-small, peer-to-peer
    static peerToPeerRoom = "peer-to-peer";  // "tiny"
    static smallGroupRoom = "group-small";   // "small"
    static largeGroupRoom = "group";         // "group"
    
}