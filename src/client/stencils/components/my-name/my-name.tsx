
import { Component, Listen, h, Prop } from '@stencil/core';

@Component({
  tag: 'my-name',
  styleUrl: 'my-name.scss'
})
export class MyName {

  @Prop() isBoolean: boolean = true;
  @Prop() aNumber: number = -1;
  @Prop() aString: string = "a-string"

  componentWillLoad() {
    alert(`${this.isBoolean} | ${this.aNumber} | ${this.aString}`)
  }

  render() {



    return (
      <div>
        <h1>My Web Component App</h1>

        <my-dropdown dropTitle="Toggle">
          Hello World
        </my-dropdown>
      </div>
    );
  }

  @Listen('onToggle')
  log(event) {
    console.log(event);
  }
}