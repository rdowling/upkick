import { Component, h, State } from '@stencil/core';
import { TwConstants } from '../../tw-constants';
import { LOG_LEVELS, TwLogger } from '../../tw-logger';

import * as util  from '../../../utils/util';
import { connect, ConnectOptions, Room, TwilioError } from 'twilio-video';
import { POST } from '../../../HTTP';
import { TwStudio } from '../tw-studio/tw-studio';
import { isMobile } from '../../../utils/util';
import 'boxicons';
import { TwToggle } from '../tw-toggle/tw-toggle'
// import { toggledlistener } from '../tw-toggle/toggledlistener';

@Component({
    tag: 'tw-lobby',
    styleUrl: 'tw-lobby.css'
})
export class TwLobby {

    private _logger = new TwLogger();
    
    @State() accessToken: string = "";
    @State() room: Room;
    @State() roomNm: string = 'public-studio';
    @State() userNm: string = '';
    @State() userIdentity: string = '';

    private isRoomMuted: boolean = false;
    private isGridEnabled: boolean = false;
    private gridSpacing = TwConstants._defaultGridSpacingWidth;
    private gridTransparency = TwConstants._defaultGridTransparency;

    private isToolbarVisible: boolean = true;

    @State() isAwaitingRoom: boolean = false;
    @State() isRoomOpen: boolean = false;
    @State() awaitingIncrement = 0;

    // TODO: currently used mostly as State(), but it would be nice to have company-lobbies
    @State() isRecorded: boolean = true;
    
    private isTrainer: boolean = false;  // _IS_TRAINER_
    private isShadowing: boolean = false; //  _SHADOW_TRAINER_

    // private _CLIENT_NM_ = this.userNm
    private _TRAINER_ID_: string = "";
    private _CLIENT_ID_: string = "";
    private _isFocusingSynched: boolean = false;

    private companyNm: string  = ""; // _COMPANY_NM_
    private studioNm: string   = "";  // _STUDIO_NM_
    private locationNm: string = "";

    private clientLogLevel: LOG_LEVELS = 'warn'; // {'DEBUG', 'INFO', 'MESSAGE', 'WARN', 'ERROR'}
    private connectOptions: ConnectOptions;
    
    /**
        TODO: similar to Twilio's quickstart: (github.com/twilio/video-quickstart-js/master/quickstart/src/index.js)

        async function selectAndJoinRoom(error = null) {
            const formData = await selectRoom($joinRoomModal, error);
            // ...
            try {
                // get token, tracks & connectOptions
                // ...
                await joinRoom(token, connectOptions);  

                return selectAndJoinRoom();  // when joinRoom returns, recursively call the same function again which lets them pick a room
            } catch (error) {
                return selectAndJoinRoom(error);  // if it fails, recursively call and give error
            }
        }

        async function joinRoom(token, connectOptions) {
            const room = await Twilio.connect(token, connectOptions);
            // handle participants, local media tracks, etc
            // ...
            return new Promise(resolve, reject) => {
                // set up disconnect/leave/close page handlers (calls to room.disconnect())
                // ...
                room.once('disconnect', (room, error) => {
                    // stop local tracks, disconnect work, etc
                    if (error) {
                        // Reject the Promise with the TwilioError so that the Room selection modal 
                        // (plus the TwilioError message) can be displayed.
                        reject(error);
                    } else {
                        // Resolve the Promise tso that the Room selection modal can be displayed.
                        resolve();
                    }
                });
            } 
        }
     */

    componentWillLoad() {

        //TODO: somehow get the logging level out of the gate before we load (network request?)

        let studioFullUrl  = window.location.href as string;
        let studioPath = window.location.pathname as string;

        this.studioNm  = this.parseStudioNm(studioPath) as string;        
        this.companyNm  = this.parseCompanyNm(studioPath) as string;
        this.locationNm  = this.parseLocationNm(studioPath) as string;
        
        if (util.hasQueryParam(studioFullUrl, "loglevel")) {
            let queryParamLogLevel = util.getQueryParam(studioFullUrl, "loglevel");
            if (queryParamLogLevel) {
                this.clientLogLevel = TwLogger.parseLogLevel(queryParamLogLevel);
                this._logger.setLogLevel(this.clientLogLevel);  

            }
        }

        let clientId  = (util.getQueryParam(studioFullUrl, "clientid")  || "").trim();
        this._CLIENT_ID_ = clientId;

        let trainerId = (util.getQueryParam(studioFullUrl, "trainerid") || "").trim();
        this._TRAINER_ID_ = trainerId;

        let clientNm  = (util.getQueryParam(studioFullUrl, "clientnm")  || "").trim();
        this.userNm = clientNm;  // _CLIENT_NM_

        if (util.hasQueryParam(studioFullUrl, "istrainer")) {
            this.isTrainer = util.getBooleanQueryParam(studioFullUrl, "istrainer");
        } else if (util.hasQueryParam(studioFullUrl, "trainer")) {
            this.isTrainer = util.getBooleanQueryParam(studioFullUrl, "trainer");
        }

        if (util.hasQueryParam(studioFullUrl, "shadow")) {
            this.isShadowing  = util.getBooleanQueryParam(studioFullUrl, "shadow");
        } else if (util.hasQueryParam(studioFullUrl, "isShadow")) {
            this.isShadowing  = util.getBooleanQueryParam(studioFullUrl, "isShadow");
        } else {
            this.isShadowing  = false; // (util.getBooleanQueryParam(studioFullUrl, "shadow"));
        }
        
        if (this.isShadowing) {
            this._CLIENT_ID_ = "";
            this._TRAINER_ID_ = "";
        }

        if (this.studioNm !== "") {
            this.roomNm = this.studioNm;
        }

        if (this.companyNm !== "") {
            this.roomNm = this.companyNm+":"+this.studioNm;
        }

        if (this.locationNm !== "") {
            this.roomNm = this.companyNm+":"+this.locationNm+":"+this.studioNm;
        }

        if (this.companyNm.toLowerCase() === "ghf") {
            this._isFocusingSynched = true;
            let isGhfTrainer = false;
            if (util.hasQueryParam(studioFullUrl, "istrainer")) {
                isGhfTrainer = util.getBooleanQueryParam(studioFullUrl, "istrainer");
            } else if (util.hasQueryParam(studioFullUrl, "trainer")) {
                isGhfTrainer = util.getBooleanQueryParam(studioFullUrl, "trainer");
            }

            isGhfTrainer = isGhfTrainer || (this._CLIENT_ID_ !== "" && this._TRAINER_ID_ !== "" && this._CLIENT_ID_ === this._TRAINER_ID_) || this.isShadowing;  // _IS_TRAINER_

            // this.isToolbarVisible = this.isTrainer;
            this.tryJoiningRoom(null, isGhfTrainer);
        }
        
    }

    tryForDeviceIds():  { audio: string; video: string; } {

        let loadSavedDeviceIds = false

        if (!loadSavedDeviceIds) {
            return null;
        } else {
            // On mobile browsers, there is the possibility of not getting any media even 
            // after the user has given permission, most likely due to some other app reserving 
            // the media device.  So, we make sure users always test their media devices before 
            // joining the Room.
            const deviceIds = {
                audio: isMobile ? null : localStorage.getItem('audioDeviceId'),
                video: isMobile ? null : localStorage.getItem('videoDeviceId') 
            }
            return deviceIds;

        }
    }

    handleUserNmChange(event) {
        this.userNm = event.target.value;
    }

    handleRoomNmChange(event) {
        this.roomNm = event.target.value;
    }

    // TODO: see code above and return a Promise (or something to that effect)
    tryJoiningRoom(event, isTrainer:boolean = true) {
        if (event) { event.preventDefault(); }

        // let self = this;
        this.isTrainer = isTrainer;

        let checkRoomParams = {roomNm: this.roomNm, isRecorded: this.isRecorded, userNm: this.userNm, isTrainer: this.isTrainer, isShadow: this.isShadowing};

        this.POST_checkRoomStatus(checkRoomParams)
            .then((response) => {
                this.isRoomOpen = response.isRoomOpen || false;
                this.isAwaitingRoom = !this.isRoomOpen;
                
                if (response.clientLogLevel) {
                    this.clientLogLevel = TwLogger.parseLogLevel(response.clientLogLevel);
                    
                    if(this._logger.getLogLevel() !== this.clientLogLevel) {
                        this._logger.setLogLevel(this.clientLogLevel)
                    }
                }

                if (this.isAwaitingRoom) {
                    this.awaitingIncrement++;
                    this.accessToken    = "";
                    this.logInfo(`Participant: ${this.userNm} requested studio: ${this.roomNm} -- studio is not yet open, logging with level of '${this.clientLogLevel}'`, checkRoomParams);
                    return null;
                } else {
                    this.userIdentity   = response.userIdentity;
                    this.accessToken    = response.accessToken;
                    this.isRoomMuted    = response.isRoomMuted;
                    this.isGridEnabled  = response.isGridEnabled;
                    this.gridSpacing    = response.gridSpacing
                    this.gridTransparency = response.gridTransparency,
                    this.isToolbarVisible = response.isToolbarVisible;
                    this.logInfo(`Participant: ${this.userNm} requested studio: ${this.roomNm} -- studio is open; participant identity: ${this.userIdentity}, logging with level of '${this.clientLogLevel}'`, checkRoomParams);
                    return this.connectToRoom();
                }
            })
            .fail((jqXHR, sTextStatus, sErrorThrown) => {
                this.logError(`Error checking room status from train/with server. Status: ${sTextStatus}; Error: ${sErrorThrown}`);
                this.logError(jqXHR, sTextStatus, sErrorThrown);
                alert(sTextStatus+": "+sErrorThrown);
            })

    }

    POST_checkRoomStatus(args: { roomNm: string; isRecorded: boolean , userNm: string, isTrainer: boolean, isShadow: boolean}): JQuery.jqXHR {
        return POST(`${TwConstants.ROOMS_API_URL}`, args);
    }

    connectToRoom() {

        
        this.connectOptions = (isMobile ? this.getMobileConnectOptions(this.roomNm, this.userIdentity) : this.getDesktopConnectOptions(this.roomNm, this.userIdentity));

        let deviceIds: { audio: string; video: string; } = this.tryForDeviceIds();

        if (deviceIds && deviceIds !== null) {
            // TODO: need to handle the null case (pick audio)
            if (deviceIds.audio !== null) {
                if (typeof this.connectOptions.audio === 'boolean') {
                    this.connectOptions.audio = {deviceId: {exact: deviceIds.audio}};
                } else {
                    this.connectOptions.audio.deviceId = {exact: deviceIds.audio};
                }
            }

            // TODO: need to handle the null case (pick video)
            if (deviceIds.video !== null) {
                if (typeof this.connectOptions.video === 'boolean') {
                    this.connectOptions.video = {deviceId: {exact: deviceIds.video}};
                } else {
                    this.connectOptions.video.deviceId = {exact: deviceIds.video};
                }
            }
        }

        return connect(this.accessToken, this.connectOptions)
            .then((room: Room) => {
                this.logInfo("You joined room '" + room.sid + "' as '" + room.localParticipant.identity + "'");
                
                this.setRoom(room);

                room.once('disconnected', (room: Room, disconnectErr: TwilioError) => { 
                    if (disconnectErr) {
                        this.logError(`[TwLobby] Participant ${this.userIdentity} errored disconnecting from room ${room.name}`, disconnectErr);
                    }
                    this.setRoom(null);
                    this.accessToken = "";
                });

            })
            .catch((overconstrainedErr: OverconstrainedError) => {
                let errMsg = `There was a problem connecting to your Studio: OverconstrainedError '${overconstrainedErr.constraint}' (error message ${overconstrainedErr.message})`;
                this.logError(errMsg, overconstrainedErr);
                alert(errMsg);
            })
            .catch((connectErr: DOMException) => {
                let errMsg = `There was a problem connecting to your Studio: '${connectErr.message}' (error code ${connectErr.code})`;
                this.logError(errMsg, connectErr);
                alert(errMsg);
            });
    }

    setRoom(room: Room) {
        this.room = room;    
    }

    getMobileConnectOptions(roomNm: string, participantNm: string): ConnectOptions {
        
        // Twilio recommendations: https://www.twilio.com/docs/video/tutorials/developing-high-quality-video-applications#mobile-browser-collaboration-recommended-settings        

        const connectionOptions: ConnectOptions = {

            name: roomNm,

            automaticSubscription: false,
            logLevel: this.clientLogLevel, // 'debug' | 'info' | 'warn' | 'error' | 'off';

            audio: {
                name: TwStudio.genTrackName(roomNm, participantNm, TwConstants.AUDIO_TRACK_KIND)
            },

            video: {
                name: TwStudio.genTrackName(roomNm, participantNm, TwConstants.VIDEO_TRACK_KIND),
                height: 480, frameRate: 24, width: 640
            },

            bandwidthProfile: {
                
                video: {
                    mode: 'collaboration',
                    maxSubscriptionBitrate: 2500000,
                    maxTracks: 5,
                    dominantSpeakerPriority: 'standard'
                },
            },
            dominantSpeaker: true,
            maxAudioBitrate: 16000,
            preferredVideoCodecs: [{ codec: 'VP8', simulcast: true }],
            networkQuality: { local: 1, remote: 1 }            
        };

        return connectionOptions;
    }

    getDesktopConnectOptions(roomNm: string, participantNm: string): ConnectOptions {

        // Twilio recommendations: https://www.twilio.com/docs/video/tutorials/developing-high-quality-video-applications#desktop-browser-collaboration-recommended-settings

        const connectionOptions: ConnectOptions = {

            name: roomNm,

            automaticSubscription: false,
            logLevel: this.clientLogLevel, // 'debug' | 'info' | 'warn' | 'error' | 'off';

            audio: {
                name: TwStudio.genTrackName(roomNm, participantNm, TwConstants.AUDIO_TRACK_KIND)
            },

            video: {
                name: TwStudio.genTrackName(roomNm, participantNm, TwConstants.VIDEO_TRACK_KIND),
                height: 720, frameRate: 24, width: 1280
            },
            bandwidthProfile: {
                video: {
                    mode: 'collaboration',
                    maxTracks: 10,
                    dominantSpeakerPriority: 'standard',
                    renderDimensions: {
                        high: {height:1080, width:1920},
                        standard: {height:720, width:1280},
                        low: {height:176, width:144}
                    }
                }
            },
            dominantSpeaker: true,
            maxAudioBitrate: 16000, //For music remove this line
            preferredVideoCodecs: [{ codec: 'VP8', simulcast: true }],
            networkQuality: {local:1, remote: 1}
        };

        return connectionOptions;
    }

    
    getOriginalConnectOptions(roomNm: string): ConnectOptions {
        const connectionOptions: ConnectOptions = {

            name: roomNm,

            automaticSubscription: false,
            logLevel: 'warn', //"debug",
            audio: true, 
            video: true,
            bandwidthProfile: {
                
                video: {
                    mode: 'collaboration',
                    dominantSpeakerPriority: 'standard',
                    renderDimensions: {
                        high: { height: 1080, width: 1920 },
                        standard: { height: 720, width: 1280 },
                        low: { height: 90, width: 160 },
                    },
                },
            },
            //dominantSpeaker: true,
            //maxAudioBitrate: 12000,
            networkQuality: { local: 1, remote: 1 },
            // preferredVideoCodecs: [{ codec: 'VP8', simulcast: true }],
        };

        return connectionOptions;
    }

    render() {
        
        if (this.accessToken && this.accessToken !== "" && this.accessToken !== null && this.room && this.room !== null) {

            this.logDebug('Lobby:render() -- token gotten. Variables:', 'room', this.room, 'roomNm', this.roomNm, 'clientNm', this.userNm, 'companyNm', 'log-level', this.clientLogLevel, this.companyNm, 'clientId', this._CLIENT_ID_, 'trainerId', this._TRAINER_ID_);
            return (
                <tw-studio 
                    room={this.room}
                    userIdentity={this.userIdentity} 
                    connectOptions={this.connectOptions}
                    isParticipantTrainer={this.isTrainer} 
                    isShadowing={this.isShadowing}
                    
                    isFocusSynched={this._isFocusingSynched}
                    isToolbarVisible={this.isToolbarVisible} 
                    isRoomMuted={this.isRoomMuted}
                    isGridEnabled={this.isGridEnabled}
                    gridSpacing={this.gridSpacing}
                    gridTransparency={this.gridTransparency}
                    
                    clientLogLevel={this.clientLogLevel}
                    clientNm={this.userNm}
                    clientId={this._CLIENT_ID_}
                    companyNm={this.companyNm}
                    trainerId={this._TRAINER_ID_}
                    >
                </tw-studio>
            );

        } else if (this.isAwaitingRoom) {
            
            this.tryJoiningRoom(null, this.isTrainer);
            let numDots = 0;
            if (this.awaitingIncrement <= 3) {
                numDots = 1;
            } else if (this.awaitingIncrement <= 6) {
                numDots = 2;
            } else if (this.awaitingIncrement <= 9) {
                numDots = 3;
            } else {
                this.awaitingIncrement = 0;
            }
            return (
                <div class='my-4 mx-10 pt-8 pb-8 '>
                    <h1 class='leading-4 text-xl '> Waiting for the trainer to start your studio {".".repeat(numDots)} </h1>
                </div>
            );
        
        } else  {
            return this.getRendering();            
        }
    }

    getRendering() {
        let body: HTMLBodyElement = document.querySelector("body");
        util.addClass(body, "bg-gray-100");

        return (
            <div class='mt-2 mx-4 bg-gray-100'>
                <div class='bg-white shadow sm:rounded-lg py-3 flex gap-4'>
                    <div class='px-4 flex items-center'>
                        <h1 class='text-2xl font-medium text-gray-900'> 
                            Studio Entrance 
                        </h1>
                    </div>

                    <div class='ml-4 flex-col'>
                        <div class='flex'>
                            <div class='flex items-center'>
                                <label class="text-base font-medium mr-4 text-gray-700">
                                    Your Name
                                </label>
                            </div>
                            <input 
                                id="participant_name" 
                                class="mt-1 py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                                value={this.userNm} 
                                onChange={(event) => this.handleUserNmChange(event)} required

                            />
                        
                        </div>
                        <div class='mt-2 flex items-center text-left'>
                            <p class='text-left italic text-base font-light leading-5 text-gray-700'>
                                Note: Names are used only for displaying to other participants. Blank names are OK.
                            </p>
                        </div>

                    </div>
                </div>

                <div class='mt-4 flex gap-4 bg-gray-100'>
                    <div class="bg-white shadow sm:rounded-lg flex-1">
                        <div class="px-4 py-5 sm:p-6">
                            <h3 class="text-xl leading-6 font-medium text-gray-900">
                                Clients
                            </h3>

                            <div class="mt-2 flex-col sm:items-start sm:justify-between">
                                
                                <div class="my-5 max-w-xl text-base leading-6 text-gray-500">
                                    Clients, click the button below to join your virtual studio. 
                                    If the studio is not yet started you'll wait in the virtual lobby 
                                    and be automatically let in once the studio beings.
                                </div>
                            

                                <div class="my-5">
                                    <button class="py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-500 shadow-sm hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 transition duration-150 ease-in-out"
                                            onClick={(event)=>this.tryJoiningRoom(event, false)}
                                    >
                                        Click to Join Your Studio!
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="bg-white shadow sm:rounded-lg flex-1">
                        <div class="px-4 py-5 sm:p-6">
                            <h3 class="text-xl leading-6 font-medium text-gray-900">
                                Trainers
                            </h3>

                            <p class="my-5 max-w-xl text-base leading-6 text-gray-500">
                                Trainers, click the button below to open your studio and start your session.  
                                Any waiting clients will automatically be let in, as will clients joining thereafter. 
                            </p>

                            <div class="flex-col">
                                <div class="flex">
                                   {this.getToggle()}
                                    <div class='flex ml-2 items-center flex-no-wrap'>
                                        <label class="text-base flex-no-wrap whitespace-no-wrap w-auto font-medium leading-5 text-gray-700">
                                            {this.isRecorded?'Session Will Be Recorded':'Session Will Not Be Recorded'} (Click to Toggle)
                                        </label>
                                    </div>

                                </div>
                                <p class='mt-2 p-1 text-left italic text-sm font-light leading-5 text-gray-700'>
                                    Note: The recording option is only offered at studio startup. If you change your mind after starting the studio you will need to restart the studio and have your clients refresh their pages. 
                                </p>

                            </div>

                            <div class="my-5">
                                <span class="inline-flex rounded-md shadow-sm">
                                    <button type="submit" class="py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-green-500  hover:bg-green-700 focus:outline-none focus:border-green-700 focus:shadow-outline-indigo active:bg-green-500 transition duration-150 ease-in-out"
                                            onClick={(event)=>this.tryJoiningRoom(event, true)}
                                    >
                                        Click to Start Your Studio!
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    getToggle() {
        return (<tw-toggle is-on={this.isRecorded} onToggled={(isOn: boolean,  toggle: TwToggle) => this.setIsRecorded(isOn, toggle)}></tw-toggle>);
    }

    setIsRecorded(isOn: boolean, toggle: TwToggle): void {
        this.isRecorded = isOn;
        this.logInfo('lobby synched toggled to: '+isOn+" | name is "+toggle.getName());
        //alert('lobby synched toggled to: '+isOn+" | name is "+toggle.getName());
    }

    logDebug(...objs:any[]) {
        if (this._logger) {
            this._logger.logDebug(...objs);
        } else {
            alert("Logger object is not defined?");
        }
    }

    logInfo(...objs:any[]) {
        if (this._logger) {
            this._logger.logInfo(...objs);
        } else {
            alert("Logger object is not defined?");
        }
    }

    logError(...objs:any[]) {
        if (this._logger) {
            this._logger.logError(...objs);
        } else {
            alert("Logger object is not defined?");
        }
    }
    
    parseStudioNm(url: string): string {

        // studios/member|studio || studios/company/member|studio

        let studioNm = "";

        let lastFwdSlashIndex = url.lastIndexOf("/");

        if (lastFwdSlashIndex && lastFwdSlashIndex > 0) {
            studioNm = url.substring(lastFwdSlashIndex + 1);
        }

        studioNm = studioNm.replace(/%20/g, " ");
        studioNm = studioNm.toLowerCase().trim();
        studioNm = studioNm.replace(" ", "_");

        return studioNm;

    }

    parseCompanyNm(url: string): string {

        /* 
        * url examples:         [0]  / [1]     / [2]     / [3]      / [4]
        * - studio+company:          / studios / studio                        # slashes = 2; array-length = 3
        * - studio+company:          / studios / company / studio              # slashes = 3; array-length = 4
        * - studio+company+loc:      / studios / company / location / studio   # slashes = 4; array-length = 5
        */

        let companyNm = "";

        if (url) {
            let urlSegments = url.split("/");

            this.logDebug("getting company name for URL: ", urlSegments);

            if (urlSegments.length <= 3) {
                companyNm = "";
            } else if (urlSegments.length >= 4) {
                companyNm = urlSegments[2];
            }
        }

        return companyNm.trim().toLowerCase();
    }

    
    parseLocationNm(url: string): string {

        /* 
        * url examples:         [0]  / [1]     / [2]     / [3]      / [4]
        * - studio+company:          / studios / studio                        # slashes = 2; array-length = 3
        * - studio+company:          / studios / company / studio              # slashes = 3; array-length = 4
        * - studio+company+loc:      / studios / company / location / studio   # slashes = 4; array-length = 5
        */

        let locationNm = "";

        if (url) {
            let urlSegments = url.split("/");

            this.logDebug("getting company name for URL: ", urlSegments);

            if (urlSegments.length <= 4) {
                locationNm = "";
            } else if (urlSegments.length >= 5) {
                locationNm = urlSegments[3];
            }
        }

        return locationNm.trim().toLowerCase();
    }
}
