// @ts-nocheck
// 'use strict';


const { connect, createLocalAudioTrack, createLocalTracks, createLocalVideoTrack } = require('twilio-video');

function handleMediaError(error) {
  console.error('Failed to acquire media:', error.name, error.message);
}

// Handle media error raised by createLocalAudioTrack.
createLocalAudioTrack().catch(handleMediaError);

// Handle media error raised by createLocalVideoTrack.
createLocalVideoTrack().catch(handleMediaError);

// Handle media error raised by createLocalTracks.
createLocalTracks().catch(handleMediaError);

const mediaErrors = [
  'NotAllowedError',
  'NotFoundError',
  'NotReadableError',
  'OverconstrainedError',
  'TypeError'
];

// Since connect() will acquire media for the application if tracks are not provided in ConnectOptions,
// it can raise media errors.
connect(token, { name: 'my-cool-room' }).catch(error => {
  if (mediaErrors.includes(error.name)) {
    // Handle media error here.
    handleMediaError(error);
  }
});

/**
 * 
 *  Name                Message Cause   Solution
    NotFoundError	    1. Permission denied by system
2. The object cannot be found here
3. Requested device not found	1. User has disabled the input device for the browser in the system settings
2. User's machine does not have any such input device connected to it	1. User should enable the input device for the browser in the system settings
2. User should have at lease one input device connected
NotAllowedError	1. Permission denied
2. Permission dismissed
3. The request is not allowed by the user agent or the platform in the current context
4. The request is not allowed by the user agent or the platform in the current context, possibly because the user denied permission	1. User has denied permission for your app to access the input device, either by clicking the “deny” button on the permission dialog, or by going to the browser settings
2. User has denied permission for your app by dismissing the permission dialog	1. User should allow your app to access the input device in the browser settings and then reload
2. User should reload your app and grant permission to access the input device
TypeError	1. Cannot read property 'getUserMedia' of undefined
2. navigator.mediaDevices is undefined	Your app is being served from a non-localhost non-secure context	Your app should be served from a secure context (localhost or https)
NotReadableError	1. Failed starting capture of a audio track
2. Failed starting capture of a video track
3. Could not start audio source
4. Could not start video source
5. The I/O read operation failed	The browser could not start media capture with the input device even after the user gave permission, probably because another app or tab has reserved the input device	User should close all other apps and tabs that have reserved the input device and reload your app, or worst case, restart the browser
OverconstrainedError	N/A	The input device could not satisfy the requested media constraints	If this exception was raised due to your app requesting a specific device ID, then most likely the input device is no longer connected to the machine, so your app should request the default input device
 */