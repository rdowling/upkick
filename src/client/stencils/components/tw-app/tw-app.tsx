import { Component, h, State } from '@stencil/core';
import * as jQuery from 'jquery';
import { TwConstants } from '../../tw-constants';


@Component({
    tag: 'tw-app',
    styleUrl: 'tw-app.css'
})
export class TwApp {
    @State() roomToken: string = "";
    @State() roomNm: string = 'test-studio';
    @State() userNm: string = 'test-name';

    async handleJoinRoom(evt: Event): Promise<void> {
        evt.preventDefault();
        
        let response = await jQuery.get(`${TwConstants.TOKENS_API_URL}`, 
                { roomNm: this.roomNm, identity: this.userNm}
            ).then(response => {
                jQuery.post(`${TwConstants.ROOMS_API_URL}`);
                return response;
            })
            .fail(function (jqXHR, sTextStatus, sErrorThrown) {
                console.log(jqXHR, sTextStatus, sErrorThrown);
                alert(sTextStatus+": "+sErrorThrown);
                // _logger.logError("Error requesting Twilio token from t/w server: " + sTextStatus + " | " + sErrorThrown);
            });

            this.roomToken = response.token;

    }

    render() {
        return (<h1>tw-app</h1>);
    //     if (this.roomToken) {
    //         return (
    //             <div class='app'>
    //                 <h1>Welcome to Your Room {this.roomNm}</h1>
    //                 <tw-studio user-name={this.userNm} room-name={this.roomNm} room-token={this.roomToken}></tw-studio>
    //             </div>
    //         );
    //     } else {
    //         return (
    //                 <div class='app'>
    //                     {/* <tw-lobby></tw-lobby> */}
    //                     <button onClick={(evt) => this.handleJoinRoom(evt)}>Join Room</button>
    //                 </div>
    //             );

    //     }
    // }
    }
}
