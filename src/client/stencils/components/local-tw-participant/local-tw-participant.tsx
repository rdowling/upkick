import { Component, h, Listen, Prop, Element } from '@stencil/core';

import { Participant, LocalVideoTrack, LocalAudioTrack, LocalDataTrack, createLocalVideoTrack } from 'twilio-video';
import { TwLogger } from '../../tw-logger';

@Component({
  tag: 'local-tw-participant',
  styleUrl: 'local-tw-participant.css'
})
export class LocalTwParticipant {

    @Prop() participant: Participant;
    @Element() el: HTMLElement;
    
    video!: HTMLVideoElement
    audio!: HTMLAudioElement;

    videoTrack: LocalVideoTrack;
    audioTrack: LocalAudioTrack;
    _dataTrack: LocalDataTrack;
    
    _logger = new TwLogger();
    async startLocalVideo() {
        console.log("start video button clicked");
        let videoTrack = await this.createLocalVideoTrack();// .then(track => {
            //  setTimeout(() => {
            //     container.classList.add('live');
            // }, 2000);
        videoTrack.attach(this.video);
        this.el.getElementsByTagName("button")[0].remove();
    }

    async createLocalVideoTrack(): Promise<LocalVideoTrack> {
        let localVideoTrack = await createLocalVideoTrack();
        return localVideoTrack;
    }

    @Listen('selectionMade')
    selectionMadeHandler(event: UIEvent) {
        console.log("local-tw-participant choice selection handler", event.target);
    }

    //updateVideoDevice(event) {
        // let select = event.target;
    //}`

    render() {
        return (
            <div class='participant'>
                <h3>{this.participant? this.participant.identity : 'your name here'} (You) [SID: {this.participant? this.participant.sid : ''}]</h3> 
                <video ref={(el) => this.video = el as HTMLVideoElement}></video>
                <audio ref={(el) => this.audio = el as HTMLAudioElement}></audio>
                <button onClick={() => this.startLocalVideo()}>start video</button>
                <div class="videos">
                    <video-chooser width="w-96"></video-chooser>
                    <div id="video-container"></div>
                </div>
            </div>
        );
    }
}
