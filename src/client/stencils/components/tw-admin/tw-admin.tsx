import { h, Component, State } from '@stencil/core';
import { TwLogger } from '../../tw-logger';
import { TwConstants } from '../../tw-constants';
import $ from 'jquery';

@Component({
    tag: 'tw-admin',
    styleUrl: 'tw-admin.css'
})
export class TwAdmin {

    _logger = new TwLogger();

    @State() rooms = [];
    @State() participants = [];

    // handlePuntParticipant(event) {}

    componentWillLoad() {

        $.get(`${TwConstants.ROOMS_API_URL}/-/Participants`, { }
        ).then((response) => {
            let rooms = response.rooms;
            if (rooms) {
                this.rooms = rooms;
            } 
            console.log('admin-api response:', response.rooms);

        })
        // .then(() => {
        //     return $.get(`${TwConstants.PARTICIPANTS_API_URL}`, {})
        
        // })
        // .then((response) => {
        //     console.log("GOT PARTICIPANTS?", response.participants);
        // })
        .fail(function (jqXHR, sTextStatus, sErrorThrown) {
            this._logger.logError(`Error requesting rooms. Status: ${sTextStatus}; Error: ${sErrorThrown}`);
            console.error(jqXHR, sTextStatus, sErrorThrown);
            alert(sTextStatus+": "+sErrorThrown);
        });
    }
    
    render() {

        const getRoomParticipantHtml = (participant) => (
            <div>
                <h5>{participant.identity}</h5>
                <ul>
                    <li>* Sid: {participant.sid}</li>
                    <li>* Status: {participant.status}</li>
                    <li>* Start-Time: {participant.startTime}</li>
                </ul>
            </div>
        );
        
        const getRoomHtml = (room) => (
            <div>
                <h4>{room.uniqueName}</h4>
                <ul>
                    <li>* Sid:        {room.sid}</li>
                    <li>* Created-At: {room.dateCreated}</li>
                    <li>* Updated-At: {room.dateUpdated}</li>
                    <li>* Recording?: {room.recordParticipantsOnConnect? 'true': 'false'}</li>
                    <li>* Status:     {room.status}</li>
                    <li>* Type:       {room.type}</li>
                </ul>
                <div><h6>Participants</h6>{room.participants.map(participant => getRoomParticipantHtml(participant))}</div>
            </div>
        );


        return (
            <div> Rooms List: 
                {this.rooms.map(room => getRoomHtml(room))}
            </div>
        );
    }

}