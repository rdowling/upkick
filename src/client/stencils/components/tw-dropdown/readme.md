# my-dropdown



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute    | Description | Type     | Default |
| ----------- | ------------ | ----------- | -------- | ------- |
| `dropTitle` | `drop-title` |             | `string` | `''`    |


## Events

| Event      | Description | Type               |
| ---------- | ----------- | ------------------ |
| `onToggle` |             | `CustomEvent<any>` |


## Dependencies

### Used by

 - [my-name](../my-name)

### Graph
```mermaid
graph TD;
  my-name --> my-dropdown
  style my-dropdown fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
