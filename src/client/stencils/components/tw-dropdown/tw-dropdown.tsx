
import { Component, State, h } from '@stencil/core';
import { fetchHtml } from '../../../utils/util';

import '@blaze/atoms';
import '@proyecto26/animatable-component';

// import { createAnimatableComponent } from '@proyecto26/animatable-component';

//#region example-region
//#endregion example-region

@Component({
  tag: 'tw-dropdown',
  styleUrl: 'tw-dropdown.scss'
})
export class Dropdown {

  @State() html: HTMLElement;

  constructor() {
    let self = this;
    fetchHtml('/stencils/components/tw-dropdown/tw-dropdown.html')
    .then(function(doc: Document) {
        // When the page is loaded convert it to text
        console.log("fetched html", doc.body);
        self.html = doc.body;
    })
    .catch(function(err) {  
        console.log('Failed to fetch page: ', err);  
    });
  }

  render() {
    let sHtml = "";
    if (this.html) {
      console.log("Render being called and I have valid html");
      console.log(this.html.innerHTML);
      sHtml = this.html.innerHTML;
    } else {
      sHtml = "<div>Nothing here</div>"
    }

    return (<div innerHTML={sHtml}></div>)
  }

}

