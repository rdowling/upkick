
import * as joda from '@js-joda/core';
import { DateTimeFormatter } from '@js-joda/core';
import { Locale } from '@js-joda/locale_en';


export class DateTime {
 
    private _ldt: joda.LocalDateTime;   

    static now() {
        return new DateTime();
    }

    constructor(dt: joda.LocalDateTime = joda.LocalDateTime.now()) {
        this._ldt = dt;
    }

    getIsoFormat(): string {
        return this._ldt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    getDateFormatted(sDtFormat: string): string {        
        let oFormatter = joda.DateTimeFormatter.ofPattern(sDtFormat);
        return this._ldt.format(oFormatter);
    }

    getHour(): number {
        return this._ldt.hour();
    }

    getMinute(): number {
        return this._ldt.minute();
    }

    getAmOrPm(): string {
        let oAmPmFormatter = joda.DateTimeFormatter.ofPattern('a').withLocale(Locale.ENGLISH);
        return this._ldt.format(oAmPmFormatter);
    }
    
    // static formatTime(dt: Date): string {

    //     // let yearOfTime = dt.getFullYear();
    //     // let monthOfYear = dt.getMonth() + 1;
    //     // let dayOfMonth = dt.getDay();
    //     let hourOfDay = dt.getHours();
    //     let minOfHour = dt.getMinutes();
    //     let secOfMin = dt.getSeconds();
    //     // let millis = dt.getMilliseconds();

    //     return util.padl("00", hourOfDay) + util.padl("00", minOfHour) + util.padl("00", secOfMin);//  + "." + millis;
    // }

    // static formatDateTime(dt: Date): string {

    //     let year = dt.getFullYear();
    //     let monthOfYear = dt.getMonth() + 1;
    //     let dayOfMonth = dt.getDate();
    //     let hourOfDay = dt.getHours();
    //     let minOfHour = dt.getMinutes();
    //     let secOfMin = dt.getSeconds();
    //     // let millis = dt.getMilliseconds();

    //     return year + util.padl("00", monthOfYear) + util.padl("00", dayOfMonth) + "_" + util.padl("00", hourOfDay) + util.padl("00", minOfHour) + util.padl("00", secOfMin);//  + "." + millis;
    // }
    // isAfter(other: DateTime): boolean;
    // isBefore(other: DateTime): boolean;
    // isEqual(other: DateTime): boolean;
    // isSupported(fieldOrUnit: TemporalField | TemporalUnit): boolean;
    // minus(amount: TemporalAmount): DateTime;
    // minus(amountToSubtract: number, unit: TemporalUnit): DateTime;
    // minusDays(days: number): DateTime;
    // minusHours(hours: number): DateTime;
    
    minusMinutes(minutes: number): DateTime {
        return new DateTime(this._ldt.minusMinutes(minutes));
    }

    // minusMonths(months: number): DateTime;
    // minusNanos(nanos: number): DateTime;
    // minusSeconds(seconds: number): DateTime;
    // minusTemporalAmount(amount: TemporalAmount): DateTime;
    // minusWeeks(weeks: number): DateTime;
    // minusYears(years: number): DateTime;
    // minute(): number;
    // month(): Month;
    // monthValue(): number;
    // nano(): number;
    // plus(amount: TemporalAmount): DateTime;
    // plus(amountToAdd: number, unit: TemporalUnit): DateTime;
    // plusDays(days: number): DateTime;
    // plusHours(hours: number): DateTime;
    // plusMinutes(minutes: number): DateTime;
    // plusMonths(months: number): DateTime;
    // plusNanos(nanos: number): DateTime;
    // plusSeconds(seconds: number): DateTime;
    // plusTemporalAmount(amount: TemporalAmount): DateTime;
    // plusWeeks(weeks: number): DateTime;
    // plusYears(years: number): DateTime;
    // query<R>(query: TemporalQuery<R>): R;
    // range(field: TemporalField): ValueRange;
    // second(): number;
    // toJSON(): string;
    // toLocalDate(): LocalDate;
    // toLocalTime(): LocalTime;
    // toString(): string;
    // truncatedTo(unit: TemporalUnit): DateTime;
    // until(endExclusive: Temporal, unit: TemporalUnit): number;
    // with(adjuster: TemporalAdjuster): DateTime;
    // with(field: TemporalField, newValue: number): DateTime;
    // withDayOfMonth(dayOfMonth: number): DateTime;
    // withDayOfYear(dayOfYear: number): DateTime;
    // withHour(hour: number): DateTime;
    // withMinute(minute: number): DateTime;
    // withMonth(month: number | Month): DateTime;
    // withNano(nanoOfSecond: number): DateTime;
    // withSecond(second: number): DateTime;
    // withYear(year: number): DateTime;
    // year(): number;

}
