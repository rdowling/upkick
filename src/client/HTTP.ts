
import $ from 'jquery';

export function GET(url: string, data: any = {}): JQuery.jqXHR {
    return $.get(url, data);
}

export function PUT(url: string, data: any): JQuery.jqXHR {
    return $.ajax({
        type: 'PUT',
        url: url,
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(data)
    });
}

export function POST(url: string, data: any): JQuery.jqXHR     {
    return $.ajax({
        type: 'POST',
        url: url,
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(data)
    });
}