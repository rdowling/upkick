import { Client } from "pg";
import { DateTime } from "./datetime";
import { envl, isEmpty, toStr } from "./util";


export type LOG_LEVELS = 'debug'|'info'|'warn'|'error';

export class TwLogger {
    
    static DEBUG_LEVEL: LOG_LEVELS = 'debug';
    static INFO_LEVEL:  LOG_LEVELS = 'info';
    static WARN_LEVEL:  LOG_LEVELS = 'warn';
    static ERROR_LEVEL: LOG_LEVELS = 'error';

    static LOG_TYPES = [TwLogger.DEBUG_LEVEL, TwLogger.INFO_LEVEL, TwLogger.WARN_LEVEL, TwLogger.ERROR_LEVEL]; 

    static DEFAULT_LOG_LEVEL: LOG_LEVELS = TwLogger.WARN_LEVEL;

    pgDbClient: any = null;
    private logLevel = TwLogger.DEFAULT_LOG_LEVEL;

    static globalLogger: TwLogger = null;

    static getLogger(): TwLogger {
        if (TwLogger.globalLogger === null) {
            TwLogger.globalLogger = new TwLogger();
        }

        return TwLogger.globalLogger;
    }

    static getLogLevel(level: string): ('debug'|'info'|'warn'|'error') {

        if (!level || level === null || level === "") {
            level = 'warn';
        }

        level = level.toLowerCase().trim()

        if (!level || level === null || level === "") {
            return 'warn';
        } else if (level === TwLogger.DEBUG_LEVEL) {
            return 'debug';
        } else if (level === TwLogger.INFO_LEVEL) {
            return 'info';
        } else if (level === TwLogger.WARN_LEVEL) {
            return 'warn';
        } else if (level === TwLogger.ERROR_LEVEL) {
            return 'error';
        } else {
            return 'warn';
        }
    }

    getLogLevel(): string {
        return this.logLevel;
    }

    setLogLevel(logLevel: ('debug'|'info'|'warn'|'error') ) {
        this.logLevel = logLevel;
    }

    enableDbLogging(pgDbClient: Client) {
        this.pgDbClient = pgDbClient;
    }

    logDebug(...debugObjs: any[]): void {
        if (this.logLevel === TwLogger.DEBUG_LEVEL) {
            this.logServerMessage('debug', ...debugObjs);
        }
    }

    logInfo(...infoObjs: any[]): void {
        if (this.logLevel === TwLogger.INFO_LEVEL || this.logLevel === TwLogger.DEBUG_LEVEL) {
            this.logServerMessage('info', ...infoObjs);
        }
    }

    logWarn(...objs: any[]): void {
        // always log the warns for now
        this.logServerMessage('warn', ...objs);
    }

    logError(...objs: any[]): void {
        // always log the errors for now
        this.logServerMessage('error', ...objs);
    }

    static ClientInfo: {browserNm: string, browserVer: string, platform: string, userAgent: string}

    async logServerMessage(msgSeverity: ('debug'|'info'|'warn'|'error'), ...msgs: any[]) {
        return this.logMessage(msgSeverity, null, null, null, 'Server', ...msgs)
                    .catch((err) => console.error("logServerMessage failed with error:", err));
    }

    async logClientMessage(msgSeverity: ('debug'|'info'|'warn'|'error'), roomNm: string, userNm: string, clientInfo: {browserNm: string, browserVer: string, platform: string, userAgent: string}, ...msgs: any[]) {
        return this.logMessage(msgSeverity, roomNm, userNm, clientInfo, 'Client', ...msgs)
                    .catch((err) => console.error("logClientMessage failed with error:", err));
    }

    async logMessage(msgSeverity: ('debug'|'info'|'warn'|'error'), roomNm: string, userNm: string, clientInfo: {browserNm: string, browserVer: string, platform: string, userAgent: string}, msgSource: ('Client'|'Server'), ...msgs: any[]) {

        let userSessionInfo = null;
        
        if (userNm && userNm !== null && roomNm && roomNm !== null) {
            userSessionInfo = `{ User: '${userNm}' @ Room: '${roomNm}' }`;
        }

        let userBrowserInfo = null;
        
        if (clientInfo && clientInfo !== null) {
            const {browserNm, browserVer, platform, userAgent} = clientInfo||{};
            userBrowserInfo = `Browser: ${browserNm} (${browserVer}) | Platform/OS: ${platform} | User-Agent: ${userAgent}`;
        }

        let consoleMsgs: any[] = [DateTime.now().getIsoFormat()];
        if (userSessionInfo !== null) {
            consoleMsgs.push(userSessionInfo);
        }

        consoleMsgs.push(...msgs);

        if(userBrowserInfo !== null) {
            consoleMsgs.push(userBrowserInfo);
        }

        if (msgSeverity === 'error') {
            console.error(...consoleMsgs);
            console.trace();
        } else if (msgSeverity === 'warn') {
            console.warn(...consoleMsgs);
        } else if (msgSeverity === 'info') {
            console.info(...consoleMsgs);
        } else if (msgSeverity === 'debug') {
            console.debug(...consoleMsgs);
        }

        let concatMsgs = ""

        for (let msg of msgs) {
            concatMsgs += (isEmpty(concatMsgs)? "" : " | ");
            concatMsgs += toStr(msg);
        }

        //return new Promise((resolve, reject) => {});

        this.pgDbClient.query(`insert into 
                                twlogs 
                                    (logtm, severity, usernm, roomnm, msg, msgsrc, clientinfo) 
                                values 
                                    (to_timestamp(${Date.now()} / 1000.0), $1, $2, $3, $4, $5, $6) returning *`, 
                                    [msgSeverity, userNm, roomNm, concatMsgs, msgSource, userBrowserInfo]
                            )
                            .catch(e => {
                                console.error(`Failed while logging previous ${msgSeverity} message for ${userSessionInfo}`, e);
                            });

    }

    
}
