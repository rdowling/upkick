"use strict";

/**
 * When testing locally, remember to start (cmd-line):
 * > ngrok start twl 
 * where twl in ~/.ngrok2/ngrok.yml:
  
    authtoken: NGROK-AUTH-TOKEN (go to account to retrieve)

    tunnels:
      twl:
        proto: http
        addr: 3000
        subdomain: twl
 */


require("dotenv").load();

import { TwilioVideoApi } from './twilio-video-api';
import { TwConstants } from './tw-constants';
import { RoomInstance } from 'twilio/lib/rest/video/v1/room';
import { ParticipantInstance } from 'twilio/lib/rest/video/v1/room/roomParticipant';
import { envl } from './util';
import { parseParicipantProps, Privileges, RoomState, StatusEvents } from './room-attributes';

import { Client } from 'pg';
import { postSnapshotToKnack } from './knack-helpers';
import { LOG_LEVELS, TwLogger } from './tw-logger';

let serverLogger = TwLogger.getLogger();
serverLogger.setLogLevel(TwLogger.getLogLevel(process.env.SERVER_LOG_LEVEL));

let VERSION = '2.1.4';

console.log(`Starting Train/With, version '${VERSION}'`);
console.log(`Logging level of SERVER log (SERVER_LOG_LEVEL): {  ${serverLogger.getLogLevel()}  }`);

let twilioVideoApi = new TwilioVideoApi(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN, process.env.TWILIO_API_KEY, process.env.TWILIO_API_SECRET);

let privileges = new Privileges();

let _CLIENT_LOG_LEVEL_: LOG_LEVELS = TwLogger.getLogLevel(process.env.CLIENT_LOG_LEVEL);

console.log(`Logging level of CLIENT log (CLIENT_LOG_LEVEL): [${_CLIENT_LOG_LEVEL_}]`)

let http = require("http");
let path = require("path");

let session = require("express-session");
let express = require("express");

let bodyParser = require("body-parser");
let app = express();

if (app.settings.env == "production") {
    app.use((req, res, next) => {
        if (req.header("x-forwarded-proto") !== "https") {
            res.redirect(`https://${req.header("host")}${req.url}`);
        } else {
            next();
        }
    });

    var enforce = require("express-sslify");
    app.use(enforce.HTTPS({ trustProtoHeader: true }));

    var sslRedirect = require("heroku-ssl-redirect");
    app.use(sslRedirect());
}

app.use(session({ secret: "koffee" }));

let server = http.createServer(app);

let helmet = require("helmet");

app.use(helmet());

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));

//app.use(bodyParser.urlencoded({ extended: true }));
//app.use(bodyParser.json());

var distPath = path.join(__dirname, "..");
var clientPath = path.join(distPath, "client");
var clientHtmlPath = path.join(clientPath, "");

var clientScriptsPath = path.join(distPath, "./node_modules");


// GROUP STUDIO HELPERS
// TODO: how to clear out old studio states?
// let _mStudioStates = new Map<string,StudioState>();

interface StudioState {
    studioNm: string;
    isMuted: boolean;
}

// if this is not first then it will redirect to /index.html
app.get("/", getWelcomePage);

app.use("/", express.static(clientPath));
app.use("/node_modules", express.static(clientScriptsPath));

app.get("/index.html", getWelcomePage);
app.get("/welcome.html", getWelcomePage);

app.get("/studios/:studioNm", redirectToStudio);
app.get("/studios/:companyNm/:studioNm", redirectToCompanyStudio);
app.get("/studios/:companyNm/:locationNm/:studioNm", redirectToCompanyLocationStudio);

app.get(`${TwConstants.API_URL}`, GET_verifyApiUrl);  // NEW (moved in from test-server.ts)

// app.get(`${TwConstants.TOKENS_API_URL}`, GET_accessUserTokenAndIdentity);           // studio.ts: GFH & General calls this

/* API: Rooms */
//app.get(`${TwConstants.ROOMS_API_URL}`, GET_getRoomStatus);       // GET: View Video Rooms List
//app.put(`${TwConstants.ROOMS_API_URL}`, PUT_getOrCreateRoom);          // POST: Create a Video Room

app.post(`${TwConstants.ROOMS_API_URL}`, POST_checkRoomStatus);  
app.post(`${TwConstants.ROOMS_API_URL}/:roomsid`, POST_updateParticipantSubscriptions);
app.put(`${TwConstants.ROOMS_API_URL}/:roomsid/:participantsid`, PUT_participantJoinedRoom);          // PUT: Update room state
app.put(`${TwConstants.ROOMS_API_URL}/:roomsid`, PUT_updateRoomState);          // PUT: Update room state
app.get(`${TwConstants.ROOMS_API_URL}/:roomsid`, GET_queryRoomState);    // GET: Get room state
app.post(`${TwConstants.CALLBACK_API_URL}`, POST_handleTwilioCallback); // PATCH not available; can't use :roomsid (unknown at room creation); studio event callback URL

app.get(`${TwConstants.ADMIN_API_URL}/*`, GET_viewVideoRoomsList);

// POST    /Rooms/[RoomSid]    -> Modify a Video Room
app.post( `${TwConstants.ROOMS_API_URL}/:roomsid`, POST_modifyVideoRoom);

app.get(   `${TwConstants.ROOMS_API_URL}/:roomsid/snapshot`, getSnapshot);
app.post(  `${TwConstants.ROOMS_API_URL}/:roomsid/snapshot`, storeSnapshot);
//app.post(  `${TwConstants.ROOMS_API_URL}/save-snapshot`, saveImageAsPng);   // no-one calls this (?)

app.post(  `${TwConstants.ROOMS_API_URL}/:roomsid/signature`, postSnapshotToKnack);   // studio.ts: GFH calls this

/* Recordings */
// GET     /Rooms/[RoomSid]/Recordings/[RecordingSid]      -> View a Room Recording
app.get(  `${TwConstants.ROOMS_API_URL}/:roomsid/Recordings`, viewRoomRecordingsList); 
// GET     /Rooms/[RoomSid]/Recordings                     -> View Room Recordings List
// DELETE  /Rooms/[RoomSid]/Recordings/[RecordingSid]      -> Delete a Room Recording
// GET     /Rooms/[RoomSid]/Recordings/[MediaSid]/Media    -> View a Room Recording Media


/* Participants */
app.get(  `${TwConstants.PARTICIPANTS_API_URL}`, viewAllParticipantsList);
// GET     /Rooms/[RoomSid]/Participants/[ParticipantSid]                                      -> View a Room Participant
// GET     /Rooms/[RoomSid]/Participants                                                       -> View Room Participants List
app.get(  `${TwConstants.ROOMS_API_URL}/-/Participants`, viewAllRoomsAndParticipantsList); // NEW (moved in from test-server.ts) // PUT: gets or creates a studio & returns Token
app.get(  `${TwConstants.ROOMS_API_URL}/:roomsid/Participants`, viewRoomParticipantsList); // NEW (moved in from test-server.ts) // PUT: gets or creates a studio & returns Token

// POST    /Rooms/[RoomSid]/Participants/[ParticipantSid]                                      -> Modify a Room Participant
// GET     /Rooms/[RoomSid]/Participants/[ParticipantSid]/PublishedTracks/[PublishedTrackSid]  -> View a Room Participant Published Track
// GET     /Rooms/[RoomSid]/Participants/[ParticipantSid]/PublishedTracks                      -> View Room Participant Published Tracks List


/* Participant Subscription */
// GET     /Rooms/[RoomSid]/Participants/[ParticipantSid]/SubscribedTracks/[SubscribedTrackSid]    -> View a Room Participant Subscribed Track
app.get(  TwConstants.subscriptionsApi(), viewParticipantSubscriptions);                    // NEW (moved in from test-server.ts) // PUT: gets or creates a studio & returns Token
// GET     /Rooms/[RoomSid]/Participants/[ParticipantSid]/SubscribedTracks                         -> View Room Participant Subscribed Tracks List
// GET     /Rooms/[RoomSid]/Participants/[ParticipantSid]/SubscribeRules                           -> View a Room Participant Subscribe Rules
// POST    /Rooms/[RoomSid]/Participants/[ParticipantSid]/SubscribeRules                           -> Modify a Room Participant Subscribe Rules
// app.post(  TwConstants.subscriptionsApi(), POST_updateParticipantSubscriptions)

/*  [page: `studio.ts` | clients: {GFH, General}] -- Get Twilio Token & Join Studio  */

function getWelcomePage(req: any, resp: any) {
    //resp.setHeader("isAuthenticated", req.isAuthenticated());
    resp.sendFile(clientHtmlPath + "/welcome.html");

}

// async function GET_apiUrls(req: any, resp: any) {
//     // TODO: respond with the API_URL to use hereafter: e.g. /api/v105/studios
//     resp.send("GET request received; use /api/v105/studios");
// }

async function GET_verifyApiUrl(req: any, resp: any) {
    serverLogger.logInfo(`GET api request 'GET_verifyApiUrl' received at correct URL`);
    resp.send("GET request received at correct api URL");
}

async function POST_checkRoomStatus(req: any, resp: any) {
    // args: { roomNm: string; isRecording: boolean , userNm: string, isTrainer: boolean, isShadow: boolean}): 
    // return:  {roomNm: string, isRoomOpen: boolean, userIdentity: string, accessToken: string, clientLogLevel: string, isRoomMuted: boolean, isGridEnabled: boolean }
        
    let requestParams: any = req.body;

    let roomNm     = envl(requestParams.roomNm, "");
    let isRecorded = envl(requestParams.isRecorded, false);

    let userNm     = envl(requestParams.userNm, "");
    let isTrainer  = envl(requestParams.isTrainer, false);
    let isShadow   = envl(requestParams.isShadow, false);

    let userIdentity = "";
    let accessToken = "";
    let isRoomOpen = false;
    let isRoomMuted = false; 
    let isGridEnabled = TwConstants._defaultIsGridVisible;
    let nGridSpacing = TwConstants._defaultGridSpacingWidth;
    let nGridTransparencyPct = TwConstants._defaultGridTransparency;

    let isToolbarVisible = true;

    let clientLogLevel = _CLIENT_LOG_LEVEL_;

    let roomType = privileges.getStudioType(roomNm);
    let room: RoomInstance = await twilioVideoApi.getOrCreateRoom(roomNm, isRecorded, roomType, isTrainer, isShadow, req);
    
    if (room) {

        isRoomOpen = (room.status === "in-progress"); // type RoomRoomStatus = 'in-progress'|'completed'|'failed';

        let roomState = getRoomState(room.sid);
        if (!roomState) {
            roomState = createRoomState(room.sid);
        }
        isRoomMuted = roomState.isRoomMuted;
        isGridEnabled = roomState.isGridEnabled;
        nGridSpacing = roomState.gridSpacing;
        nGridTransparencyPct = roomState.gridTransparencyPct;
        
        isToolbarVisible = privileges.getIsToolbarVisible(roomNm, isTrainer);
    
        serverLogger.logInfo("POST_checkRoomStatus(): Room returned", room, "room state gotten: ", roomState);
    
        userIdentity = genUserIdentity(userNm, isTrainer, isShadow);

        accessToken = getJtwTokenAndRoomGrant(userIdentity, roomNm);
        updateRoomParticipantsSubscriptions(room.sid, room.uniqueName);
    }

    let responseProps = {roomNm: roomNm, isRoomOpen: isRoomOpen, userIdentity: userIdentity, accessToken: accessToken, clientLogLevel: clientLogLevel, isRoomMuted: isRoomMuted, isGridEnabled: isGridEnabled, gridSpacing: nGridSpacing, gridTransparency: nGridTransparencyPct, isToolbarVisible: isToolbarVisible };
    resp.send( responseProps ); 
}    


function GET_queryRoomState(req: any, resp: any) {
    let roomSid   = req.params.roomSid;
    let roomNm    = req.params.roomNm;
    let roomState: RoomState = null;

    if (roomSid && roomNm) {
        updateRoomParticipantsSubscriptions(roomSid, roomNm);
        roomState = getRoomState(roomSid);
    }

    resp.send(roomState);
}


async function PUT_updateRoomState(req: any, resp: any) {
    
    let roomParams = req.body as any;

    let roomSid = roomParams.roomSid;
    let roomNm  = roomParams.roomNm;

    let participantSid      = roomParams.participantSid;
    let participantIdentity = roomParams.participantIdentity;

    let isRoomMuted          = null; 

    let isGridEnabled        = null;
    let nGridSpacing         = null;
    let nGridTransparencyPct = null;

    if ('isRoomMuted' in roomParams) {
        isRoomMuted = roomParams.isRoomMuted;
    }

    if('isGridEnabled' in roomParams) {
        isGridEnabled = roomParams.isGridEnabled;
        nGridSpacing  = roomParams.gridSpacing;
        nGridTransparencyPct = roomParams.gridTransparencyPct;
    }

    let roomState: RoomState = getRoomState(roomSid);

    if (!roomState) {
        roomState = createRoomState(roomSid);
    }
        
    roomState.isRoomMuted         = envl(isRoomMuted,   roomState.isRoomMuted);
    roomState.isGridEnabled       = envl(isGridEnabled, roomState.isGridEnabled);
    roomState.gridSpacing         = envl(nGridSpacing,  roomState.gridSpacing);
    roomState.gridTransparencyPct = envl(nGridTransparencyPct, roomState.gridTransparencyPct);

    logInfo(`PUT_updateRoomState called by participant ${participantIdentity} (sid: ${participantSid}); updated room-state is: `, roomState);

    updateRoomParticipantsSubscriptions(roomSid, roomNm);
    
    resp.send(roomState)
}

function POST_updateParticipantSubscriptions(req: any, resp: any) {
    let roomSid = req.body.roomSid;
    let roomNm = req.body.roomNm;
    updateRoomParticipantsSubscriptions(roomSid, roomNm);

    resp.send("subscriptions updating");
}


function getJtwTokenAndRoomGrant(identity: string, roomNm: string): string {

    //TODO: check identity uniqueness for room (keep map?)

    let identityToken = twilioVideoApi.generateIdentityToken(identity);// new AccessToken(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_API_KEY, process.env.TWILIO_API_SECRET);
    twilioVideoApi.addRoomVideoGrant(identityToken, roomNm);

    logInfo(`token generated for ${identity}: ${identityToken}`);
    
    return identityToken.toJwt();
}

let _clientNumber_ = 0;

function genUserIdentity(userNm: string, isTrainer: boolean, isShadow: boolean): string {
    if (userNm === "") {
        userNm = "Anonymous";
    }
    _clientNumber_ = _clientNumber_+1;
    return `${userNm}?isTrainer=${isTrainer}&isShadow=${isShadow}&uniq=${_clientNumber_}`;
}

function getFullReqUrl(req: any) {
    /**
     *  prot    subd  hostname    original - url 
     * https://mossy.swamp.com/alabama?filter=very-humid
     * {protocol}://[{subdomains}.]{hostname}[/{originalUrl}]
     * {protocol}://[{subdomains}.]{hostname}[/{path}?filter=very-humid]
     */

    return `${req.protocol}://${req.hostname}${req.originalUrl}`;   
}


function PUT_participantJoinedRoom (req: any, resp: any) {

}

async function GET_viewVideoRoomsList(req: any, resp: any) {
    //let params = req.body as any;

    let rooms = await twilioVideoApi.getRooms();
    serverLogger.logInfo('rooms list returned', rooms);

    resp.send( { rooms: rooms } ); 
}

async function viewAllParticipantsList(req: any, resp: any) {
    //let params = req.body as any;

    let participants = [];
    let rooms = await twilioVideoApi.getRooms();

    for(const room of rooms) {
        let moreParticipants = await twilioVideoApi.getRoomParticipants(room.sid);   
        participants = participants.concat(moreParticipants);
        serverLogger.logDebug('added participants', participants);
    }

    serverLogger.logDebug('returning participants', participants);
    resp.send( { participants: participants } ); 
        
    // let allParticipants = await twilioVideoApi.getParticipants();  //TODO: can't get this to return participants, even if the function does get them.
    // console.log('viewallparts', allParticipants)
    // resp.send( { participants: allParticipants } ); 
}


let roomStates: Map<string,RoomState> = new Map();

function createRoomState(roomSid: string): RoomState {
    let roomState = new RoomState();
    //roomState.roomSid = roomSid;
    return setRoomState(roomSid, roomState);
}

function getRoomState(roomSidOrNm: string): RoomState {
    return roomStates.get(roomSidOrNm);
}

function setRoomState(roomSid: string, roomState: RoomState): RoomState {
    roomStates.set(roomSid, roomState);
    return roomState;
}

function deleteRoomState(roomSid: string) {
    if (roomStates.has(roomSid)) {
        roomStates.delete(roomSid);
    }
}


async function viewAllRoomsAndParticipantsList(req: any, resp: any) {
    //let params = req.body as any;
    let roomsAndParticipants = [];
    
    let allParticipants = [];
    let allRooms: RoomInstance[] = await twilioVideoApi.getRooms();

    for(const room of allRooms) {
        let roomParticipants: ParticipantInstance[] = await twilioVideoApi.getRoomParticipants(room.sid); 
        roomsAndParticipants.push({room: room, participants: roomParticipants});
        allParticipants = allParticipants.concat(roomParticipants);
        serverLogger.logDebug('added participants', allParticipants);
    }

    serverLogger.logDebug('returning participants', allParticipants);
    resp.send( { rooms: roomsAndParticipants } ); 
        
    // let allParticipants = await twilioVideoApi.getParticipants();  //TODO: can't get this to return participants, even if the function does get them.
    // console.log('viewallparts', allParticipants)
    // resp.send( { participants: allParticipants } ); 
}

function viewRoomRecordingsList(req: any, resp: any) {}

function viewParticipantSubscriptions(req: any, resp: any) {}

function viewRoomParticipantsList(req: any, resp: any) {}

// function viewVideoRoom(req: any, resp: any) {
//     //TODO:
// }

function POST_modifyVideoRoom(req: any, resp: any) {
    //TODO:
}

let mSnapshots = {};

function storeSnapshot(req: any, resp: any) {

    let snapParams = req.body as any;

    let roomSid         = snapParams.roomSid;       
    let participantSid  = snapParams.participantSid;
    let imageSnap       = snapParams.imageSnap;

    serverLogger.logInfo("Snapshot being stored", roomSid, participantSid);

    mSnapshots[roomSid] = {roomSid: roomSid, participantSid: participantSid, imageSnap: imageSnap};

    resp.sendStatus(200);// .json({ok: true})
}

function getSnapshot(req: any, resp: any) {

    let roomSid = envl(req.query.roomSid, "");

    let snapshot = mSnapshots[roomSid];

    resp.send(snapshot);

}

function logDebug(...debugs: any[]) {
    serverLogger.logDebug(...debugs);
}

function logInfo(...infos: any[]) {
    serverLogger.logInfo(...infos);
}

function logWarn(...warns: any[]) {
    serverLogger.logWarn(...warns);
}

function logError(...errs: any[]) {
    serverLogger.logError(...errs);
}

async function POST_handleTwilioCallback(req: any, resp: any) {

    let fullUrl = getFullReqUrl(req);
    // let studioNm = extractReqParam("studioNm", req);

    let body = req.body;

    let roomNm = body["RoomName"];
    let roomSid = body["RoomSid"];
    let roomStatus = body["RoomStatus"]; // can be: {`in-progress`, `failed`, `completed`}
    let roomType = body["RoomType"];
    let callbackEvent = body["StatusCallbackEvent"];
    // let sequenceNumber = body["SequenceNumber"];
    let timestamp = body["Timestamp"];

    serverLogger.logInfo(`Twilio event callback: room: ${roomNm} event changed to: ${callbackEvent}; room-sid: ${roomSid}; room-status: ${roomStatus}; room-type: ${roomType}; url: ${fullUrl}; message-timestamp: ${timestamp}`);
     
    let isValidTwilioRequest = twilioVideoApi.validateTwilioRequest(req);
    
    if (!isValidTwilioRequest) {
        serverLogger.logError(`Invalid Request! Room ${roomNm} event changed to: ${callbackEvent}; room-sid: ${roomSid}; room-status: ${roomStatus}; room-type: ${roomType}; url: ${fullUrl}`, roomNm);
    }

    let roomState = getRoomState(roomSid);
    if (!roomState) {
        roomState = createRoomState(roomSid);
    }

    if ( StatusEvents.participant_connected.equals( callbackEvent ) || StatusEvents.participant_disconnected.equals( callbackEvent )) {
        let participantSid = body["ParticipantSid"];
        let participantIdentity = body["ParticipantIdentity"];

        logInfo(`Twilio status event (callback) update: '${callbackEvent}' ( ${genParticipantLogInfo(participantSid, participantIdentity)} )`);

        updateRoomParticipantsSubscriptions(roomSid, roomNm); // , participantSid, participantIdentity); 
    }

    if (['in-progress'].includes(roomStatus)) {
        // update timestamp with last action (log?)
    } else if (['completed', 'failed'].includes(roomStatus)) {
        if (roomState) {
            deleteRoomState(roomSid);
        }
    }

    // return resp;
    // resp.set('Content-Type', 'text/plain')
    // resp.end();
    resp.sendStatus(200);
}

function genParticipantLogInfo(participantSid: string, participantIdentity: string): string {
    return `{identity: ${participantIdentity}} | [sid: ${participantSid}]`;
}

function remArrVal(arr: any[], val: any): any[] {
    let valIndex = arr.indexOf(val);
    if (valIndex>0) {
        arr.splice(valIndex);
    }
    return arr;
}
/*
  Room Status Callback Events:
    room-created	            Room created.
    room-ended	                Room completed. (Note: Rooms created by the REST API will fire room-ended event when room is empty for 5 minutes.)
    participant-connected	    Participant joined the Room.
    participant-disconnected	Participant left the Room.
    track-added	                Participant added a Track.
    track-removed	            Participant removed a Track.
    track-enabled	            Participant unpaused a Track.
    track-disabled	            Participant paused a Track.
    recording-started	        Recording for a Track began
    recording-completed	        Recording for a Track completed
    recording-failed            Failure during a recording operation request
*/


/*  
trainers: [{"include", "all": true}]

clients: [{"exclude", "all": true}, //  <= necessary?
            {"include", "kind": "audio" },
            {"include", "publisher: "trainer[1..n]"} // (audio), video & data
*/


async function updateRoomParticipantsSubscriptions(roomSid: string, roomNm: string) { // , participantSid: string, participantNm: string) {
    
    let participants: ParticipantInstance[] = []; 
    try {
        participants = await twilioVideoApi.getConnectedParticipants(roomSid);
    } catch(err) {
        logError(`Error while obtaining room participants for room {room-name: ${roomNm}} [room-sid: ${roomSid}]`, err);
    }    

    logInfo("UPDATING SUBSCRIPTIONS: participants", participants); 
    
    let hasTrainer = privileges.getHasTrainer(roomNm);

    let trainerSids = [];
    let shadowSids = [];

    for (let participant of participants) {
        if (participant.status === "connected") {
            let participantProps = parseParicipantProps(participant.identity);
            if (participantProps.isTrainer) {
                if (participantProps.isShadow) {
                    shadowSids.push(participant.sid);
                } else {
                    trainerSids.push(participant.sid);
                }
            }
        }
    }

    for (let participant of participants) {
        let rules = [];
        if (participant.status === "connected") {
            let participantProps = parseParicipantProps(participant.identity);
            
            if (!hasTrainer && trainerSids.length === 0 && shadowSids.length === 0) {
                logInfo("SUBSCRIPTIONS: no trainers, everyone subscribes to everyone");
                // no trainers, everyone subscribes to everyone
                rules.push(twilioVideoApi.getSubscribeAllTracksRule());
            } else if (shadowSids.includes(participant.sid)) { 
                // shadow trainers subscribe to all
                logInfo("SUBSCRIPTIONS: shadow trainer, subscribe to everyone");
                rules.push(twilioVideoApi.getSubscribeAllTracksRule());
            } else if (trainerSids.includes(participant.sid)) {
                // trainers subscribe to all
                logInfo("SUBSCRIPTIONS: trainer, subscribe to everyone");
                rules.push(twilioVideoApi.getSubscribeAllTracksRule());
            } else {
                // participants subscribe to all audio + trainers audio and video
                logInfo("SUBSCRIPTIONS: client, include audio only");
                rules.push(twilioVideoApi.getSubscribeRule("include", "audio", "*"));
                for(let trainerSid of trainerSids) {
                    rules.push(twilioVideoApi.getSubscribeRule("include", "*", trainerSid));
                }
                // rules.push(twilioVideoApi.getSubscribeAllTracksRule());
            }

            twilioVideoApi.updateParticipantSubscriptions(roomSid, participant.sid, rules);
        }
    }    
}

app.post("/logmsg", ensureAuthenticated, function logError(req: any, resp: any) {
    let logData = req.body;  // { "msgSeverity": msgSeverity, "roomNm": this.roomName, "userNm": this.userIdentity, "clientInfo": this.getBrowserInfo(), "msgs": msgs}

    let severity: ('debug'|'info'|'warn'|'error') = logData.msgSeverity;
    let roomNm: string   = logData.roomNm;
    let userNm: string   = logData.userNm;
    let clientInfo: any  = logData.clientInfo;
    let messages: any[]  = logData.msgs;

    serverLogger.logClientMessage(severity, roomNm, userNm, clientInfo, messages);
    resp.sendStatus(200);// .json({ok: true})
});


let pgdb;
setupDb(app)
.then(() => {
    console.info("Server established connection to Postgres server");
    serverLogger.logServerMessage('info', `Train/With server started (version '${VERSION}')`);
    if (serverLogger.getLogLevel() === TwLogger.DEBUG_LEVEL) {
        serverLogger.logDebug("Server started, testing logging level 'debug'");
        serverLogger.logInfo("Server started, testing logging level 'info'");
        serverLogger.logWarn("Server started, testing logging level 'warn'");
        serverLogger.logError("Server started, testing logging level 'error'");
    }
})
.catch((err) => {
    console.error("Could not connect to postgres server", err);
});
 

app.get("*", function (req: any, resp: any) {
    resp.sendFile("welcome.html", { root: clientPath });
});


let port = process.env.PORT || 3000;
server.listen(port, function onServerStart() {
    const serverAddress = server.address().address;
    const serverPort = server.address().port;
    console.log(`This express app is listening at address: ${serverAddress} on port: ${serverPort}`);
});

function extractReqParam(paramPropNm, req) {
    
    let paramValue = "";
    if (req && req.params) {
        paramValue = envl(req.params[paramPropNm], "");
        paramValue = paramValue.replace(/%20/g, " ");
        paramValue = paramValue.toLowerCase().trim();
        paramValue = paramValue.replace(/ /g, "_");
    }

    return paramValue;
}

function ensureAuthenticated(req, res, next) {
    let __REQUIRE_AUTH__ = false;

    if (!__REQUIRE_AUTH__ || req.isAuthenticated()) {
        // req.user is available for use here
        return next();
    }

    // denied. redirect to login
    res.redirect("/");
}


async function setupDb(expressApp: any) {
    let dbhost = process.env.PGHOST,
        dbport = process.env.PGPORT,
        database = process.env.PGDATABASE,
        dbuser = process.env.PGUSER,
        dbpassword = process.env.PGPASSWORD,
        dburl;

    if (process.env.PGURL) {
        dburl = process.env.PGURL;
    }
    
    let pgDbClient: Client;

    try {
        if (dburl) {
            pgDbClient = new Client(dburl);
        } else {
            pgDbClient = new Client({
                host: dbhost,
                port: parseInt(dbport),
                database: database,
                user: dbuser,
                password: dbpassword
            });
        }
    } catch (e) {
        console.error(`could not instantiate client`);
    }

    if (pgDbClient) {
        return pgDbClient.connect()
                    .then(() => {
                        console.log("postgres client connect() ran OK");

                        serverLogger.enableDbLogging(pgDbClient);

                        let knex = require("knex")({
                            client: 'pg', version: '7.2',
                            connection: {
                                host: dbhost,
                                port: dbport,
                                database: database,
                                user: dbuser,
                                password: dbpassword
                            }
                        });

                        configureUserAuthRoutes(expressApp, knex, pgDbClient);

                    })
                    .catch((e:Error) => {
                        console.error("Error connecting to database", e);
                    });

    } else {
        return null;
    }
}

function configureUserAuthRoutes(expressApp: any, knex: any, pgDbClient: any) {

    let //path = require("path"),
        passport = require("passport"),
        bcrypt = require("bcrypt-nodejs"),
        LocalStrategy = require("passport-local");

    expressApp.use(passport.initialize());
    expressApp.use(passport.session());

    expressApp.post("/join-beta", function joinBeta(req, res) {
        let email = req.body.email;
        serverLogger.logInfo(`User with email ${email} wants to join the beta!`);

        pgDbClient.query("insert into betas(email) values ($1) returning *", [email])
            .then(insRes => {
                res.send(`Thanks for signing up for the beta, ${email}!  Expect to get an email back from us soon!`);
            })
            .catch(e => {
                serverLogger.logError(`insert into betas with email ${email} failed`, e);
                res.send("It looks like there was a problem signing you up. We're very sorry, and a little embarrassed, frankly... but we'd still love to have you! If it's not too much trouble, email trainwithstudios@gmail.com with subject 'SUBSCRIBE!' and we promise we'll get right back to you!");
            });

        return
    });

    passport.use("local-signup", new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, email, password, done) {

            serverLogger.logInfo("local-signup!", email, password);

            let salt = bcrypt.genSaltSync(8);
            let cryptPwd = bcrypt.hashSync(password, salt);
            let newuser = {
                email: email, password: cryptPwd, salt: salt
            };
            
            knex("users")
                .insert(newuser)
                .then(function () {
                    knex("users")
                        .where({ email: email, password: cryptPwd })
                        .first("userid", "email")
                        .then(user => {
                            if (user) {
                                return done(null, user);  // next: serializeUser();
                            } else {
                                return done(null, false);
                            }
                        });
                }).catch(err => serverLogger.logError(err));

        })
    );

    passport.use("local-login", new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',

            passReqToCallback: true
        },
        function (req, email, password, done) {

            let isTrainer = !!req.body.istrainer;
            //let rememberUser = !!req.body.rememberUser;

            let sIsTrainer = (isTrainer? 'Y': 'N');

            serverLogger.logWarn(email +" is attempting a login...", "");

            knex("users")
                .where({ email: email })
                .first("salt")
                .then(row => {
                    
                    if (!row) {
                        serverLogger.logWarn(email +" not found in users table, creating new login...", "");
                        let salt = bcrypt.genSaltSync(8);
                        let cryptPwd = bcrypt.hashSync(password, salt);
                        let newuser = {
                            email: email, password: cryptPwd, salt: salt, istrainer: sIsTrainer
                        };
                        knex("users")
                            .insert(newuser)
                            .then(function () {
                                knex("users")
                                    .where({ email: email, password: cryptPwd})
                                    .first("userid", "email")
                                    .then(user => {
                                        if (user) {
                                            return done(null, user);  // next: serializeUser();
                                        } else {
                                            return done(null, false);
                                        }
                                    });
                            }).catch(err => serverLogger.logError(err));
                    } else {
                        serverLogger.logInfo(email +" was found in users table; validating password+salt hash against encrypted user password...", "");

                        let hashedPass = bcrypt.hashSync(password, row.salt);
                        knex("users")
                            .where({ email: email, password: hashedPass })
                            .first("userid", "email")
                            .then((userRow) => {
                                if (userRow) {
                                    serverLogger.logInfo(email +" was found and validated; logged in.", "");
                                    userRow.isNewUser = false;
                                    return done(null, userRow);
                                } else {
                                    serverLogger.logWarn(email +" was found  but the password is incorrect; not logged in.", "");
                                    // TODO: post failure back; attempts? redirects
                                    return done(null, false);
                                }
                            });  // next: serializeUser();
                    }
                })
                .catch((err) => serverLogger.logError(err));
        }));

    passport.serializeUser(function (user, cb) {
        return cb(null, user.userid);
    });

    passport.deserializeUser(function (userid, done) { // only called when session is active
        knex("users")
            .where({ userid: userid })
            .first("userid", "email", "istrainer")
            .then(row => !row ? done(null, false) : done(null, row));
    });

    expressApp.get("/", (req, res) => {
        return res.redirect("/welcome.html");
    });

    expressApp.get("/signin", (req, res) => {
        return res.redirect("/login.html");
    });

    expressApp.post("/signup",
        passport.authenticate("local-signup", {
            successRedirect: "/",
            failureRedirect: "/err"
        })
    );

    expressApp.post(
        "/signin",
        passport.authenticate("local-login", { 
            failureRedirect: '/studios' 
        }),
        (req, res) => {
            serverLogger.logInfo("user signing in...", req.user);
            if (req.user) {
                if (req.user.isNewUser) {
                    serverLogger.logInfo("New user succesfully signed up: "+req.user.email+" | is trainer? "+req.user.isTrainer, "");
                } else {
                    serverLogger.logInfo("Existing user logged in: "+req.user.email+" | is trainer? "+req.user.isTrainer, "");
                }
                res.redirect('/');
            } else {
                // ???  

            }
        }
    );

}

function redirectToStudio(req: any, resp: any) {
    let studioNm   = extractReqParam("studioNm",   req).trim();
        
    studioNm   = studioNm.replace(/ /g, "_");

    let originalUrl = req.originalUrl.trim();

    let studioLoc  = `/studios/${studioNm}`;

    serverLogger.logInfo(`Studio URL requested: ${originalUrl} | studio-name: ${studioNm} | studio-location: ${studioLoc}`);
    
    resp.location(studioLoc);

    resp.sendFile(clientHtmlPath + "/stencils/tw-lobby.html"); 
}

function redirectToCompanyStudio(req: any, resp: any) {

    let companyNm  = extractReqParam("companyNm",  req).trim();
    let studioNm   = extractReqParam("studioNm",   req).trim();
    
    companyNm  = companyNm.replace(/ /g, "_");
    studioNm   = studioNm.replace(/ /g, "_");

    let originalUrl = req.originalUrl.trim();

    let studioLoc  = `/studios/${companyNm}/${studioNm}`;

    serverLogger.logInfo(`Studio URL requested: ${originalUrl} | company-name: ${companyNm} | studio-name: ${studioNm} | studio-location: ${studioLoc}`);
    
    resp.location(studioLoc);

    resp.sendFile(clientHtmlPath + "/stencils/tw-lobby.html"); 

}

    /**
     * room types: group, group-small, peer-to-peer
     */

function redirectToCompanyLocationStudio(req: any, resp: any) {

    let companyNm  = extractReqParam("companyNm",  req).trim();
    let locationNm = extractReqParam("locationNm", req).trim();
    let studioNm   = extractReqParam("studioNm",   req).trim();
    
    companyNm  = companyNm.replace(/ /g, "_");
    locationNm = locationNm.replace(/ /g, "_");
    studioNm   = studioNm.replace(/ /g, "_");

    let originalUrl = req.originalUrl.trim();

    let studioLoc  = `/studios/${companyNm}/${locationNm}/${studioNm}`;

    serverLogger.logInfo(`Studio URL requested: ${originalUrl} | company-name: ${companyNm} | locationNm: ${locationNm} | studio-name: ${studioNm} | studio-location: ${studioLoc}`);
    
    resp.location(studioLoc);

    resp.sendFile(clientHtmlPath + "/stencils/tw-lobby.html"); 

}