import { DateTime } from "./datetime";

export function postImageToKnack(sKnackAppId, sKnackApiKey, dtStart: DateTime, sClientId, sTrainerId, sImgFileUrl, sImgFileId) {
    let sKnackUrl = `https://api.knack.com/v1/objects/object_72/records`;

    let knackHeaders = {
        'X-Knack-Application-Id': sKnackAppId,
        'X-Knack-REST-API-Key': sKnackApiKey,
        'Content-Type': 'application/json'
    }

    const knackPostOptions = {
        method: 'POST',
        headers: knackHeaders
    }

    let dtFrom = dtStart;
    let sFromDt = dtFrom.getDateFormatted("MM/dd/yyyy");
    let sFromHrs = dtFrom.getHour();
    let sFromMins = dtFrom.getMinute();
    let sFromAmOrPm = dtFrom.getAmOrPm();

    let dtTo = DateTime.now();
    let sToDt = dtTo.getDateFormatted("MM/dd/yyyy");
    let sToHrs = dtTo.getHour();
    let sToMins = dtTo.getMinute();
    let sToAmOrPm = dtTo.getAmOrPm();


    /**
    *  // From-to date, specific times, as an object:
    {
        "date":"02/08/2013",
        "hours":5,
        "minutes":0,
        "am_pm":"pm",
        "to":{
            "date":"02/11/2013",
            "hours":3,
            "minutes":0,
            "am_pm":"pm"
        }
    }
    */

    let oDateTime = {
        "date": sFromDt,
        "hours": sFromHrs,
        "minutes": sFromMins,
        "am_pm": sFromAmOrPm, 
        
        "to": {
            "date": sToDt,
            "hours": sToHrs,
            "minutes": sToMins,
            "am_pm": sToAmOrPm
        }
    }

    //KNACK_IDs for posting virtual signatures.
    let sSessDtField      = "field_1881";   // name: "Session Date"                         type: 'Date/Time' 
    let sAddedDtField     = "field_1870";   // name: "Date/Time Activity Added"             type: 'Date/Time' 
    let sPersonConnField  = "field_2077";   // name: "Person"                               type: 'Connection'
    let sTrainerConnField = "field_1878";   // name: "Trainer"                              type: 'Connection'
    let sImageIdField     = "field_1932";   // name: "Virtual Training Session Signature"   type: 'Image'
    let sSessTypeField    = "field_1920";   // name: "Session Types"                        type: 'Multiple Choice'

    const oData = {
        [sSessDtField]: oDateTime,
        [sAddedDtField]: oDateTime,
        [sPersonConnField]: sClientId,
        [sTrainerConnField]: sTrainerId,
        [sImageIdField]: sImgFileUrl, 
        [sSessTypeField]: "Virtual"
    };

    const sData = JSON.stringify(oData);
    const https = require('https');

    const sKnackPostOptions = JSON.stringify(knackPostOptions);

    console.info(`Posting Knack signature:\nURL: ${sKnackUrl}\nOptions: ${sKnackPostOptions}\Data: ${sData}`);

    const postReq = https.request(sKnackUrl, knackPostOptions, (postResp) => {  
        console.log(`POST response statusCode: ${postResp.statusCode}`);

        postResp.on('data', (sPostData) => {
            // console.log("POST data response:\n"+sPostData);
        })

        postResp.on('end', () => {
            console.log(`POST snapshot/signature ended (sent succesfully).`)
        })

    });

    postReq.on('error', (putErr) => {
        console.error(`Error POSTing ${sKnackUrl}`)
    });

    postReq.write(sData);
    postReq.end();

}

export function postSnapshotToKnack(req, resp) {

    let sImgFileId  = req.body.imgId;
    let sImgFileUrl = req.body.imgUrl;
    let sClientId   = req.body.clientId;
    let sTrainerId  = req.body.trainerId;

    console.log(`postSnapshotToKnack: received file id ${sImgFileId} and file url ${sImgFileUrl}; clientId: ${sClientId}; trainerId: ${sTrainerId}`);
    // var base64Data = req.body.imgBase64;//.replace(/^data:image\/png;base64,/, "");

    let dtStart = DateTime.now().minusMinutes(45);

    postImageToKnack(process.env.KNACK_GHF_APP_ID, process.env.KNACK_GHF_API_KEY, dtStart, sClientId, sTrainerId, sImgFileUrl, sImgFileId);

    resp.send("OK");
}

export function saveImageAsPng(req, resp) {

    var base64Data = req.body.imgBase64.replace(/^data:image\/png;base64,/, "");
    require('fs').writeFile("./images/img.png", base64Data, 'base64', function(err) {
        if(err){
            console.error(err);
        } else {
            resp.sendStatus(200);
        }
    });

}
