export interface GroupStudioProps {
  identity  : string;
  studioNm  : string;
  companyNm : string;
  clientId  : string;
  clientNm  : string;
  trainerId : string; 
  isTrainer : boolean;
  isRecorded : boolean;
  identityToken: string;
}

export interface StudioInfo {

  identity  : string;
  studioNm  : string;
  companyNm : string;
  clientId  : string;
  clientNm  : string;
  trainerId : string; 
  isTrainer : boolean;
  isRecorded : boolean;
  identityToken: string;

}

export function isStr(xVal: any): boolean {
  return typeof xVal === "string";
}

export function isEmpty(xVal: any): boolean {
  if (isStr(xVal)) {
    return xVal === undefined || xVal === null || xVal === "";
  } else {
    return xVal === undefined || xVal === null;
  }
}

export function toStr(xVal: any): string {
  if (typeof xVal === 'string') {
    return xVal;
  } else {
    return JSON.stringify(xVal);
  }
}

export function writeToFile(buf, filepath) {
  require("fs").writeFile(filepath, buf, function(err) {
      if (err) {
        console.error(`There was an error saving the file to '${filepath}'.`, err);
      } else {
        console.log(`The file was saved to '${filepath}'.`);
      }
  });
}

import * as queryString from 'query-string';

export {queryString};

export function parseQueryParams(sUrl): queryString.ParsedQuery {
  
  sUrl = sUrl || "";
  let sQueryParams = sUrl

  let lastIndexOfQuestionMark = sUrl.lastIndexOf('?');
  if ( lastIndexOfQuestionMark >= 0 ) {
    sQueryParams = sUrl.substring( lastIndexOfQuestionMark + 1 );
  }
  
  let oParams: queryString.ParsedQuery = queryString.parse(sQueryParams) || {};

  return oParams;
}

export function hasQueryParam(sUrl: string, paramNm: string): boolean {

  const oParams: queryString.ParsedQuery = parseQueryParams(sUrl); 

  // console.log('hasQueryParam()', 'url:', sUrl, 'parsed-query-params', oParams, 'key', paramNm);

  for(let sCand of Object.keys(oParams)) {
    if (sCand.toLowerCase() === paramNm.toLowerCase()) {
      return true;
    }
  }

  return false;
}

export function getQueryParam(sUrl: string, sKey: string): string {

  const oParams: queryString.ParsedQuery = parseQueryParams(sUrl);

  // console.debug('getQueryParam()', 'url:', sUrl, 'parsed-query-params', oParams, 'key', sKey);

  let sValue = null;
  let lKeyFound = false;
  for(let sCand of Object.keys(oParams)) {
    if (!lKeyFound && sCand.toLowerCase() === sKey.toLowerCase()) {
      sValue = oParams[sCand];
      lKeyFound = true;
    }
  }

  return sValue;
}

export function getBooleanQueryParam(sUrl: string, key: string): boolean|null {

  let sValue: string = getQueryParam(sUrl, key);
  
  if (!sValue || sValue === null || sValue === 'undefined') {
    return null;
  } else {
    sValue = sValue.toUpperCase();
    return (sValue === "TRUE");
  }
  
}

export function extractReqParam(paramPropNm, req) {
    
    let paramValue = "";
    if (req && req.params) {
        paramValue = envl(req.params[paramPropNm], "");
        paramValue = paramValue.replace(/%20/g, " ");
        paramValue = paramValue.toLowerCase().trim();
        paramValue = paramValue.replace(/ /g, "_");
    }

    return paramValue;
}

export function isBool(xVar): boolean {
  return typeof xVar === "boolean";
}

export function envl(xOne, xOther) {
  if (xOne !== undefined && xOne !== 'undefined' && xOne !== null && (xOne || isBool(xOne))) {
    return xOne;
  } else {
    return xOther;
  }
}

export function nvl(xOne, xOther) {
  if (xOne !== undefined && xOne !== 'undefined' && xOne !== null) {
    return xOne;
  } else {
    return xOther;
  }
}


export function padl(pad: string, strToPad: any): string {
  if (!strToPad) {
    return strToPad;
  }

  if (!(strToPad instanceof String)) {
    strToPad = String(strToPad);
  }
  
  return pad.substring(0, pad.length - strToPad.length) + strToPad
}

export function absdiff(num1, num2) {
  if (num1 > num2) {
    return num1 - num2;
  } else {
    return num2 - num1;
  }
}

export function dist(x1, y1, x2, y2) {
  var deltaX = absdiff(x1, x2);
  var deltaY = absdiff(y1, y2);
  var dist = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
  return dist;
}

function randomItem(array) {
  var randomIndex = Math.floor(Math.random() * array.length);
  return array[randomIndex];
}

export function randomName() {
  var ADJECTIVES = [
    "Abrasive",
    "Brash",
    "Callous",
    "Daft",
    "Eccentric",
    "Fiesty",
    "Golden",
    "Holy",
    "Ignominious",
    "Joltin",
    "Killer",
    "Luscious",
    "Mushy",
    "Nasty",
    "OldSchool",
    "Pompous",
    "Quiet",
    "Rowdy",
    "Sneaky",
    "Tawdry",
    "Unique",
    "Vivacious",
    "Wicked",
    "Xenophobic",
    "Yawning",
    "Zesty"
  ];

  var FIRST_NAMES = [
    "Anna",
    "Bobby",
    "Cameron",
    "Danny",
    "Emmett",
    "Frida",
    "Gracie",
    "Hannah",
    "Isaac",
    "Jenova",
    "Kendra",
    "Lando",
    "Mufasa",
    "Nate",
    "Owen",
    "Penny",
    "Quincy",
    "Roddy",
    "Samantha",
    "Tammy",
    "Ulysses",
    "Victoria",
    "Wendy",
    "Xander",
    "Yolanda",
    "Zelda"
  ];

  var LAST_NAMES = [
    "Anchorage",
    "Berlin",
    "Cucamonga",
    "Davenport",
    "Essex",
    "Fresno",
    "Gunsight",
    "Hanover",
    "Indianapolis",
    "Jamestown",
    "Kane",
    "Liberty",
    "Minneapolis",
    "Nevis",
    "Oakland",
    "Portland",
    "Quantico",
    "Raleigh",
    "SaintPaul",
    "Tulsa",
    "Utica",
    "Vail",
    "Warsaw",
    "XiaoJin",
    "Yale",
    "Zimmerman"
  ];
  return (
    randomItem(ADJECTIVES) + randomItem(FIRST_NAMES) + randomItem(LAST_NAMES)
  );
}
