
// import * as util from './util';

require('dotenv').load();

let __USERID__: any;
let dbhost = process.env.PGHOST,
dbport = process.env.PGPORT,
database = process.env.PGDATABASE,
dbuser = process.env.PGUSER,
dbpassword = process.env.PGPASSWORD,
dburl;

if(process.env.PGURL) {
    dburl = process.env.PGURL;
}

import * as util from './util';

const { Pool, Client } = require('pg')
let pgdb;

if (dburl) {
pgdb = new Client({url: dburl});
} else {
pgdb = new Client({
    host: dbhost,
    port: dbport,
    database: database,
    user: dbuser,
    password: dbpassword
});
}

pgdb.connect();

let knex = require("knex")({
client: 'pg',    
version: '7.2',
connection: {
    host: dbhost,
    port: dbport,
    database: database,
    user: dbuser,
    password: dbpassword
}  
});

let app: any;

connectApp(app, knex);
app.post("/join-beta", function joinBeta(req, res) {
    let email = req.body.email;
    console.log("Hey "+email+" wants to join!");

    pgdb.query("insert into betas(email) values ($1) returning *", [email])
        .then(insRes => {
            res.send("Thanks for signing up for the beta, "+email+"!  Expect to get an email back from us soon!"); 
        })
        .catch(e => {
            console.error("insert into betas with email " + email+ " failed "+e);
            res.send("It looks like there was a problem signing you up. We're very sorry, and a little embarrassed, frankly... but we'd still love to have you! If it's not too much trouble, email trainwithstudios@gmail.com with subject 'SUBSCRIBE!' and we promise we'll get right back to you!"); 
        });
    ;
    return 
});

require('dotenv').load();

app.get('/all-exercises', ensureAuthenticated, function getAllExercises(request, response) {
    queryExercises(pgdb).then(function(exercises) {
    response.send(exercises);
    })
});

app.get("/workouts", ensureAuthenticated, function(req, resp) {
    resp.sendFile("/workouts.html");
});

app.get("/delete-workout", ensureAuthenticated, function getDeleteWorkout(req, res) {
    var workoutid = Number(req.query.workoutid);
    pgdb.query("delete from workouts where workoutid = $1", [workoutid])
        .then(delRes => {
            console.log("Deleted "+workoutid+"!");
            res.send(true);
        })
        .catch(e => {
            console.error("delete failed "+e);
        });
;
});


app.post("/create-workout", ensureAuthenticated, function postCreateWorkout(req,res) {
    let sWorkoutNm = req.body.workoutnm; 
    pgdb.query("select nextval('workouts_nextid') as nextid")  // be careful about auto-lowercasing: alias "nextId" on object is "nextid" 
        .then((queryRes) => {
            let rows = [];
            rows.push(...queryRes.rows);
            
            let nNextId = Number(rows[0].nextid);  // start it at 2 in case there is a workoutid of 1 already

            console.log("Next Row ID found for new workout:", nNextId, "typeof:", typeof nNextId);
        
            console.log("insert into workouts(workoutid, workoutnm, userid) values ($1, $2, $3) returning *", [nNextId, sWorkoutNm, __USERID__]);
            pgdb.query("insert into workouts(workoutid, workoutnm, userid) values ($1, $2, $3) returning *", [nNextId, sWorkoutNm, __USERID__])
                .then(insRes => {
                    let newWorkoutRecord = insRes.rows[0];
                    res.send(newWorkoutRecord);
                })
                .catch(e => {
                    console.error("insert failed "+e);
                });
        })
        .catch(e => {
            console.error("select nextval failed "+e);
        });
;

});

app.get('/your-workouts', ensureAuthenticated, function(request, response) {

    pgdb.query("select * from workouts")  // be careful about auto-lowercasing: alias "nextId" on object is "nextid" 
    .then((queryRes) => { 
        response.send(queryRes.rows);
    })
    .catch(e => console.error(e));
});

app.post("/add-exercise", ensureAuthenticated, function addExerciseRequest(request, response) {

    let workoutid = Number(request.body.workoutid);
    let exerciseid = Number(request.body.exerciseid);
    
    (async function getExercise() {
        let rows = [];
        
        try{
            const res = await pgdb.query('select * from workoutexercises where workoutid = $1 and exerciseid = $2', [workoutid, exerciseid]);
            rows.push(...res.rows);

            if(rows.length == 0) {
                console.log("Inserting exerciseid "+exerciseid+" for workoutid "+workoutid);
                const res = await pgdb.query('insert into workoutexercises (workoutid, exerciseid) values ($1, $2) returning *', [workoutid, exerciseid]);
                rows.push(...res.rows);
            }
        } catch(e) {
            console.error("add-exercise failed " + e);
        }
        response.send( rows.length>0 );
    })();

});


app.post("/rem-exercise", ensureAuthenticated, function postRemoveExercise(request, response) {
    
    var workoutid = Number(request.body.workoutid);
    var exerciseid = Number(request.body.exerciseid);

    console.log("Removing exerciseid:", exerciseid, typeof exerciseid, "from workoutid:", workoutid, typeof workoutid);

    (async function remExercise() {
        const rows = [];
        try {
            const res = await pgdb.query('delete from workoutexercises where workoutid = $1 and exerciseid = $2', [workoutid, exerciseid]);
            console.log("Deleted "+res.rows);
            //rows.push(...res.rows);
            //response.send(rows.length>0 );
            response.send(true);
        } catch(e) {
            console.error("postRemoveExercise failed "+e);
        }

    })();

}); 

app.get("/workout-exercises", function getWorkoutExercises(request, response) {    
    var workoutid = Number(request.query.workoutid);
    try {
        (async function getWorkoutExcercisesAsync() {
            try {
                var exercises = await getWorkoutExercises(pgdb, workoutid);
                response.send( exercises );
            } catch(e) {
                console.error("getworkoutexercises failed "+e);
            }

        })();
    } catch(e) {
        console.error("/workout-exercises blew up");
    }
}); 

let io:any;
let studios = {};
io.use(function configureSocket(socket, next){
    // return the result of next() to accept the connection.
    
    socket.on("beginWorkout", function onBeginWorkout(studioNm, workoutid) {
        try{
            (async function() {
                console.log("Fetching exercises for workoutid "+workoutid);
                try {
                    var exercises = await getWorkoutExercises(pgdb, workoutid);
                } catch(ge) {
                    console.log("getWorkoutExcercises failed" + ge);
                }
                studios[studioNm].start(exercises);
            })();
        } catch(e) {
            console.log(e);    
        }
    });
});

function ensureAuthenticated(req, res, next) {
    if (true || req.isAuthenticated()) {
        // req.user is available for use here
        return next(); 
    }

    // denied. redirect to login
    res.redirect('/')
}

export async function printExercises(db) {
    try {
        var allExercises = await queryExercises(db) as Exercise[];
        console.log("trying");
        for (var exercise of allExercises) {
            console.log(exercise.toS());
        }
    } catch(e) {
        console.error("printExercises blew up");
    }
}

export async function queryExercises(db): Promise<Exercise[]> {
    var exercises = [] as Exercise[];
    try {
        const res = await db.query('SELECT * from exercises');
        for(var row of res.rows) {
            var exercise = new Exercise();
            exercise.exerciseid = row.exerciseid;
            exercise.exercisenm = row.exercisenm;
            exercise.exercisegrp = row.exercisegrp;
            exercises.push(exercise);
        }
    }catch(e) {
        console.error("queryExercises blew up");
    }
    return new Promise<Exercise[]>(resolve => {
        resolve(exercises);
    });
}


export async function getWorkoutExercises(db, workoutid) {
    let exercises = [];
    try{
        const res = await db.query(`
            select 
                e.exerciseid, e.exercisenm, e.exercisegrp 
            from exercises e 
            inner join workoutexercises we 
                on e.exerciseid = we.exerciseid 
            where we.workoutid = $1`,
            [workoutid]
        );
        

        for(var row of res.rows) {
            var exercise = new Exercise();
            exercise.exerciseid = row.exerciseid;
            exercise.exercisenm = row.exercisenm;
            exercise.exercisegrp = row.exercisegrp;
            exercises.push(exercise);
        }

        console.log("Workout: ",workoutid);
        console.log("returning exercises count: ", exercises.length);
    } catch(e) {
        console.error("getWorkoutExcercises up");
    }

    return exercises;

}

let path = require("path"),
    passport = require("passport"),
    bcrypt   = require("bcrypt-nodejs"),
    LocalStrategy = require("passport-local");
    
function connectApp(app, knex) {

    app.use(passport.initialize());
    app.use(passport.session());

    passport.use("local-signup", new LocalStrategy( 
        {   usernameField: 'usernm',
            passwordField: 'password',
            passReqToCallback: true
        }, 
        function( req, usernm, password, done) {

            let salt     = bcrypt.genSaltSync(8);
            let cryptPwd = bcrypt.hashSync(password, salt);
            let newuser = {
                usernm: usernm, password: cryptPwd, salt: salt
            };

            knex("users")
            .insert(newuser)
            .then(function() {
                knex("users")
                .where({usernm: usernm, password: cryptPwd})
                .first("userid", "usernm")
                .then(user => {
                    if (user) {
                        return done(null, user);  // next: serializeUser();
                    } else {
                        return done(null, false);
                    }
                });
            }).catch(err => console.error(err));
        })
    );

    passport.use("local-login", new LocalStrategy(
        {   usernameField: 'usernm',
            passwordField: 'password',
            passReqToCallback: true
        }, 
        function( req, usernm, password, done) {
            console.log("local-login!", usernm, password);
        knex("users")
        .where({usernm: usernm})
        .first("salt")
        .then(row => {
            console.log("row? ", row);
            if (!row) {
                return done(null, false);
            } else {
                let hashedPass = bcrypt.hashSync(password, row.salt);
                knex("users")
                .where({usernm: usernm, password: hashedPass})
                .first("userid", "usernm")
                .then(row => {
                    console.log("Row found?", row);
                    return !row ? done(null, false) : done(null, row);
                });  // next: serializeUser();
            }
        })
        .catch(err => console.error(err));
    }));

    passport.serializeUser(function(user, cb) {
        return cb(null, user.userid);
    });

    passport.deserializeUser(function(userid, done) { // only called when session is active
        knex("users")
        .where({userid: userid})
        .first("userid", "usernm")
        .then(row => !row ? done(null, false) : done(null, row));
    });

    app.get("/", (req, res) => {
        return res.redirect("/welcome.html");
    });

    app.get("/signin", (req, res) => {
        return res.redirect("/signin.html");
    });

    app.post(
        "/signup",
        passport.authenticate("local-signup", {
            successRedirect: "/",
            failureRedirect: "/signin"
        })
    );

    app.post(
        "/login",
        passport.authenticate(
            "local-login", 
            { failureRedirect: '/signin' }
        ), 
        (req, res) => {
            console.log("signin hit");
            if (req.user) {
                console.log("redictecting");
                res.redirect('/index.html');
            }
        }
    );

}

export class Exercise {

    exerciseid: number;
    exercisenm: string;
    exercisegrp: string;

    toS() {
        return this.exerciseid + " | "+this.exercisenm+ " | "+this.exercisegrp;
    }
}

export class Tasks {

    knex: any;

    constructor(knex: any) {
        this.knex = knex;
    }

    // -todo- move this to wickets.ts !!
    getWickets(userid: number, user: {}, call: (rows) => void): void {
        this.knex
            .select()
            .from("wickets")
            .where("wickets.userid" , "=", userid)
            .orderBy("wickets.wicketid")
            .map(row => {
                console.log(row);
                return {wicketid: row.wicketid, wicketname: row.wicketname};
            })
            .then(rows => call(rows))
            .catch(err => console.error(err));
    }

    tasksToText(tasks) {
        // task: {taskid: row.taskid, taskname: row.taskname, status: row.status};

        let text = "";
        for (let task of tasks) {
            text += "- " + task.taskname + "\n";
        }
        return text;
    }

    tasksToHtml(tasks) {
        // task: {taskid: row.taskid, taskname: row.taskname, status: row.status};
        let html = "";

        for (let task of tasks) {
            html += "<li>" + task.taskname + "</li>";
        }
        return html;
    }

    getAllTasks(userid: number, wicket: {wicketid: number}, call: (rows) => void): void {
        this.getDayTasks(userid, wicket, (daytasks) => {
            let tasks = {"day": daytasks};
            this.getWeekTasks(userid, wicket, (weektasks) => {
                tasks["week"] = weektasks;
                call(tasks);
            });
        } );
    }

    getDayTasks(userid: number, wicket: {wicketid: number}, call: (rows) => void): void {
        this.knex("tasks")
            .join("daytasks", "tasks.taskid", "=", "daytasks.taskid")
            .where("daytasks.wicketid", "=", wicket.wicketid)
            .select()
            .map(row => {
                return {taskid: row.taskid, wicketid: row.wicketid, taskname: row.taskname, status: row.status};
            })
            .then(rows => call(rows))
            .catch(err => console.error(err));
    }

    getWeekTasks(userid: number, wicket: {wicketid: number}, call: (rows) => void): void {
        this.knex("tasks")
            .join("weektasks", "tasks.taskid", "=", "weektasks.taskid")
            .where("weektasks.wicketid", "=", wicket.wicketid)
            .select()
            .map(row => {
                return {taskid: row.taskid, wicketid: row.wicketid, taskname: row.taskname, status: row.status};
            })
            .then(rows => call(rows))
            .catch(err => console.error(err));
    }

    createDayTask(userid: number, task: {taskname: string, wicketid: number}, call: (rows) => void): void {
        this.knex("tasks")
            .returning("taskid")
            .insert({taskname: task.taskname, status: "*", userid: userid})
            .then(rows => {
                console.log(rows);
                let newtaskid = rows[0];  // ?PG: rows[0].taskid
                console.log("NEW TASK ID" + newtaskid);
                return this.knex("daytasks")
                    .returning("*")
                    .insert({taskid: newtaskid, wicketid: task.wicketid})
                    .then(rows => this.knex("weektasks")
                                        .returning("*")
                                        .insert({taskid: newtaskid, wicketid: task.wicketid}))
                    .then(rows => this.knex("tasks")
                                        .select()
                                        .where("tasks.taskid", "=", newtaskid)
                                        .andWhere("tasks.userid", "=", userid)
                    );
            })
            .map(row => {
                return {taskid: row.taskid, wicketid: row.wicketid, taskname: row.taskname, status: row.status};
            })
            .then(rows => {
                call(rows[0]);
                return rows;
            })
            .catch(err => console.error(err));
    }

    createWeekTask(userid: number, task: {taskname: string, wicketid: number}, call: (rows) => void): void {
        this.knex("tasks")
            .returning("taskid")
            .insert({taskname: task.taskname, status: "*", userid: userid})
            .then(rows => {
                let newtaskid = rows[0]; // rows[0].taskid for postgres?
                return this.knex("weektasks")
                    .returning("*")
                    .insert({taskid: newtaskid, wicketid: task.wicketid})
                    .then(rows => this.knex("tasks")
                                        .select()
                                        .where("tasks.taskid", "=", newtaskid)
                                        .andWhere("tasks.userid", "=", userid)
                    );
            })
            .map(row => {
                return {taskid: row.taskid, wicketid: row.wicketid, taskname: row.taskname, status: row.status};
            })
            .then(rows => {
                call(rows[0]);
                return rows;
            })
            .catch(err => console.error(err));
    }

    removeTaskFromWeekList(userid: number, task: {taskid: number, wicketid: number}, call: (rows) => void): void {
        return this.knex("weektasks")
            .where("taskid", task.taskid)
            .andWhere("wicketid", "=", task.wicketid)
            .del()
            .then(rows => this.removeTaskFromDayList(userid, task, call))
            .catch(err => console.error(err));
    }

    removeTaskFromDayList(userid: number, task: {taskid: number, wicketid: number}, call: (rows) => void): void {
        return this.knex("daytasks")
            .where("taskid", task.taskid)
            .andWhere("wicketid", "=", task.wicketid)
            .del()
            .then(rows => call(rows))
            .catch(err => console.error(err));
    }

    addTaskToDayList(userid: number, task: {taskid: number, wicketid: number}, call: (rows) => void): void {
        return this.knex("daytasks")
            .returning("*")
            .insert({taskid: task.taskid, wicketid: task.wicketid})
            .then(rows => call(rows))
            .catch(err => console.error(err));
    }

    updateTaskStatus(userid: number, task: {taskid: number, status: string}, call: (rows) => void): void {
        return this.knex("tasks")
            .where("taskid", task.taskid)
            .andWhere("userid", "=", userid)
            .update("status", task.status)
        .then(rows => call(rows))
        .catch(err => console.error(err));
    }
}

// app.get("/get-recorded-rooms", ensureAuthenticated, function (req, resp) {
//     let client = vidRecs.getTwilioClient();

//     client.video.recordings
//         .list({ limit: 20 })
//         .then(recs => {
//             let rooms = [];
//             recs.forEach(rec => {
//                 let type = rec.type;
//                 if (type === 'video') {
//                     let roomSid = rec.groupingSids.room_sid;
//                     if (!rooms.includes(roomSid)) {
//                         rooms.push(roomSid);
//                     }
//                 }
//             });
//             return rooms;
//         }).then(rooms => {

//             let roomsList = [];
//             let iRoom = 0;
//             let nRooms = rooms.length;

//             for (iRoom = 0; iRoom < nRooms; iRoom++) {
//                 let roomSid = rooms[iRoom];
//                 client.video.rooms(roomSid)
//                     .fetch()
//                     .then(room => {
//                         let roomMap = {
//                             roomUrl: room.url,
//                             participantsUrl: room.links.participants,
//                             recordingsUrl: room.links.recordings,
//                             roomSid: room.sid,
//                             uniqueName: room.uniqueName,
//                             dateCreated: room.dateCreated,
//                             endTime: room.endTime,
//                             duration: room.duration,
//                             status: room.status,
//                             participants: []
//                         };
//                         console.log(roomMap);
//                         client.video.rooms(roomSid)
//                             .participants
//                             .list({ limit: 20 })
//                             .then(parts => {
//                                 parts.forEach(part => {
//                                     roomMap.participants.push(part.identity);
//                                     console.log(part.identity)
//                                 });

//                                 roomsList.push(roomMap);
//                             });


//                     })
//                     .catch(err => console.error("getRooms failed", err));
//             }

//         })
//         .catch(err => console.error("getRooms failed", err));

//     // resp.send(identityToken.toJwt());
// });


// get("/studios1/:studioNm", (ctx) -> {
//         if (!needsRedirect(ctx)) {

//         String studioNm = Util.envl(ctx.pathParam("studioNm"), "").toLowerCase();
//         println("Going to studio " + studioNm);

//         String studioHtml = Util.fileToString("target/classes/client/studio.v1.html");

//         ctx.html(studioHtml);
//     }
// });