

export function addRoutes(expressApp: any, ensureAuthenticated: any, pgDbClient: any) {
    expressApp.get('/all-exercises', ensureAuthenticated, function getAllExercises(request, response) {
        queryExercises(pgDbClient).then(function (exercises) {
            response.send(exercises);
        })
    });

    return expressApp;
}

export async function queryExercises(db) {
    var exercises = []; //as Exercise[];
    try {
        const res = await db.query('SELECT * from exercises');
        for (var row of res.rows) {
            var exercise = {};
            exercise["exerciseid"] = row.exerciseid;
            exercise["exercisenm"] = row.exercisenm;
            exercise["exercisegrp"] = row.exercisegrp;
            exercises.push(exercise);
        }
    } catch (e) {
        console.error("queryExercises blew up");
    }
    return new Promise<Exercise[]>(resolve => {
        resolve(exercises);
    });
}

export class Exercise {

    exerciseid: number;
    exercisenm: string;
    exercisegrp: string;

    toS() {
        return this.exerciseid + " | " + this.exercisenm + " | " + this.exercisegrp;
    }
}
