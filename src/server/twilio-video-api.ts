

/**
 * room types: { "group", "group-small", "peer-to-peer" }  [peer-to-peer not recordable]
 * room statuses: { "in-progress", "failed", "completed" }
 * recordable room types: { "group", "group-small" }
 */

/** twilio-node (Server) reference:
 Rooms:
// .rooms => RoomListInstance
// .rooms.list() => Promise<RoomInstance[]>
// .rooms(sid) => RoomContext
// alt^: .rooms.get(sid) => RoomContext

// Participants:
// .rooms(sid).participants => ParticipantListInstance
// .rooms(sid).participants.list() => Promise<ParticipantInstance[]>
// .rooms(sid).participants(sid) => ParticipantContext
// alt^: .rooms(sid).participants.get(sid)  => ParticipantContext
 */

import { Twilio, jwt, validateRequest } from 'twilio';

import AccessToken, { AccessTokenOptions } from 'twilio/lib/jwt/AccessToken';
import { ParticipantContext, ParticipantInstance } from 'twilio/lib/rest/video/v1/room/roomParticipant';
import { RoomInstance } from 'twilio/lib/rest/video/v1/room';
import { TwConstants } from './tw-constants';
import { SubscribeRulesInstance } from 'twilio/lib/rest/video/v1/room/roomParticipant/roomParticipantSubscribeRule';
import { nvl } from './util';
import { Privileges } from './room-attributes';
import { TwLogger } from './tw-logger';

let VideoGrant = jwt.AccessToken.VideoGrant;

export class TwilioVideoApi {

    _acctSid: string;
    _authToken: string;
    _apiKey: string;
    _apiSecret: string;
    _twilio: typeof Twilio;
    _client: Twilio;
    private _logger: TwLogger; 

    private privileges = new Privileges();


    constructor(twilioAcctSid: string, twilioAuthToken: string, twilioApiKey: string, twilioApiSecret: string) {
        this._logger    = TwLogger.getLogger();
        this._acctSid   = twilioAcctSid;
        this._authToken = twilioAuthToken;
        this._apiKey    = twilioApiKey;
        this._apiSecret = twilioApiSecret;
        this._twilio    = Twilio;
        this._client    = new Twilio(this._acctSid, this._authToken);
        //this._client = require('twilio')(this._acctSid, this._authToken);
    }

    _defaultTtlSecs = (60* (60*4));

    generateIdentityToken(identity: string, tokenTtlSecs: number = this._defaultTtlSecs): AccessToken {

        let accessTokenOptions: AccessTokenOptions = {ttl: tokenTtlSecs, identity: identity};

        let identityToken: AccessToken = new AccessToken(this._acctSid, this._apiKey, this._apiSecret, accessTokenOptions);
        // identityToken.identity = identity;
        
        return identityToken;
    }

    addRoomVideoGrant(accessToken: AccessToken, roomname: string): AccessToken {
        let videoGrant = new VideoGrant( {room: roomname});
        accessToken.addGrant(videoGrant);
        return accessToken
    }

    async getRoom(roomNmOrSid: string): Promise<RoomInstance> {
            
        try {
            if (roomNmOrSid && roomNmOrSid !== "") {

                let candRooms = await this._client.video.rooms.list({status: 'in-progress', limit: 20}); //TODO: include {uniqueName: roomNmOrSid}

                for (let candRoom of candRooms) {
                    if (candRoom.sid === roomNmOrSid || candRoom.uniqueName === roomNmOrSid) {
                        this.logInfo(`Room ${roomNmOrSid} is open and ready.`);
                        return candRoom;
                    }
                }
            }
        } catch(error) {
            this.logError(`Could not find room ${roomNmOrSid}: ${error.message}; code: ${error.code}; status: ${error.status}`, roomNmOrSid);
        }

        return null;

    }

    async getOrCreateRoom(roomNm: string, isRecorded: boolean, roomType: string, isTrainer: boolean, isShadow: boolean, req:any): Promise<RoomInstance> {
        let room: RoomInstance;

        room = await this.getRoom(roomNm);
        if (room) {
            // let roomSid = room.sid;
            let isRoomRecording = room.recordParticipantsOnConnect || false;
            
            if (isRecorded != isRoomRecording && isTrainer) {
                await this.closeRoom(roomNm);
                room = null;
            }
        }

        //TODO: use CALLBACK API location to pass instead of boolean

        if (!room && this.privileges.canStartStudio(roomNm, isTrainer)) {
            room = await this.createRoom(roomNm, isRecorded, roomType, req);

            if (!room || room === null) {
                this.logError(`A new studio ${roomNm} could not be created.`, roomNm);            
            }
        }

        return room;
    }

    getSubscribeRule(type: "include"|"exclude", kind: "video"|"audio"|"data"|"*", publisherSidOrNm: string|"*") {
        if (kind === "*") {
            return {"type": type, "publisher": publisherSidOrNm};
        } else if (publisherSidOrNm === "*") {
            return {"type": type, "kind": kind };
        } else {
            return {"type": type, "kind": kind, "publisher": publisherSidOrNm};
        }
    }

    getSubscribeNoTracksRule(): any {
        return {"type": "exclude", "all": true};
    }

    getSubscribeAllTracksRule(): any {
        return {"type": "include", "all": true};
    }

    async createRoom(roomNm: string, bIsRecorded: boolean, roomType: string, req: any): Promise<RoomInstance> {

        let callbackProtocol = "https";

        let reqHostname: string = nvl(req.hostname, "").toLowerCase();
        if (req.protocol === "http" && !reqHostname.includes("trainwith")) {
            if (reqHostname === 'localhost' || reqHostname.includes("ngrok.io")) {
                callbackProtocol = "http";
            }

        }

        let callbackUrl = `${callbackProtocol}://${req.hostname}${TwConstants.CALLBACK_API_URL}`;

        //TODO: use CALLBACK API location
        this.logInfo(`creating studio (API): ${roomNm}; callback-base-url: ${callbackUrl}`);

        let roomParams = {
            uniqueName: roomNm
        };

        roomParams["statusCallback"] = `${callbackUrl}`;
        roomParams["statusCallbackMethod"] = 'POST';

        roomParams["recordParticipantsOnConnect"] = bIsRecorded;
        //if (bIsRecorded)  {
        roomParams["type"] = roomType;
        roomParams["enableTurn"] = false;
        // } else {
        //     roomParams["type"] = 'peer-to-peer';
        //     roomParams["enableTurn"] = true;
        //}

        let room = null as any;

        try {

            room = await this._client.video.rooms.create(roomParams);
            this.logInfo(`Studio ${roomNm} successfully CREATED`, roomNm);
        
        } catch(error) {

            if (error.status === 400 && error.code === 53113) {  // Room exists

                this.logWarn(`Studio ${roomNm} already exists in-progress; message: ${error.message}; code: ${error.code}; status: ${error.status}`, roomNm);
                
                try {
                    room = await this.getRoom(roomNm);
                    this.logInfo(`Studio ${roomNm} successfully GOTTEN`, roomNm);
                } catch(getRoomErr) {
                    this.logError(`Failed to GET-ROOM ${roomNm} for unknown reasons; message: ${getRoomErr.message}; code: ${getRoomErr.code}; status: ${getRoomErr.status}`, roomNm);
                }
            } else {
                this.logError(`Failed to CREATE-ROOM ${roomNm}; message: ${error.message}; code: ${error.code}; status: ${error.status}`, roomNm);
            
            }
        }

        return room;

    }
    

    async updateParticipantSubscriptions(roomSid: string, subscriberSid: string, rules: any[]): Promise<SubscribeRulesInstance> {

        let participant: ParticipantContext = null;
        
        try {
            participant = await this._client.video
                .rooms(roomSid)
                .participants.get(subscriberSid);
        } catch (err) {
            this.logError(`Error while querying twilio participant {sid: ${subscriberSid}}`, err);
        }

        let subscribeRulesInstance: SubscribeRulesInstance = null;

        if(participant && participant !== null) {
            try {
                subscribeRulesInstance = await participant
                    .subscribeRules
                    .update({
                        rules: rules
                    })
                    .catch((err) => {
                        this.logError(`Error (in '.catch((err) => {'] updating subscribeRules for subscriber sid: ${subscriberSid}`, err);
                        return null;
                    });
            } catch (err) {
                this.logError(`Error [in '} catch(err) {']updating subscribeRules for subscriber sid: ${subscriberSid}`, err);
            }

        }


        return subscribeRulesInstance;
    }

    async getRooms(): Promise<RoomInstance[]> {
        return this._client.video.rooms.list();
    }
    
    async closeRoom(roomNm) {

        let room = await this.getRoom(roomNm);
        if (room) {
            this.logWarn(`room ${roomNm} is in progress; trainer is restarting it`);
            await this.completeRoom(room.sid);
        }
        
    }

    async getAllParticipants(): Promise<ParticipantInstance[]> {

        let rooms: RoomInstance[] = [];
        let allParticipants: ParticipantInstance[] = [];
        
        rooms = await this.getRooms();
        for (let room of rooms ) {
            let roomParticipants: ParticipantInstance[] = await this._client.video.rooms(room.sid).participants.list();
            for (let roomParticipant of roomParticipants) {
                allParticipants.push(roomParticipant);
            }
        }

        return allParticipants;
    }

    
    async getConnectedParticipants(roomSid: string): Promise<ParticipantInstance[]> {

        let connectedParticipants: ParticipantInstance[] = [];
        
        let roomParticipants: ParticipantInstance[] = await this._client.video.rooms(roomSid).participants.list();
        for (let roomParticipant of roomParticipants) {
            if (roomParticipant.status === 'connected') {
                connectedParticipants.push(roomParticipant);
            }
        }

        return connectedParticipants;
    }

    async getRoomParticipants(roomSid: string): Promise<ParticipantInstance[]> {
        return await this._client.video
            .rooms(roomSid)
            .participants.list(); 
    }

    async completeRoom(roomSid: string) {
        return await this._client.video
            .rooms(roomSid)
            .update({status: 'completed'});
    }

    async getCompletedRooms(roomNm: string): Promise<RoomInstance[]> {
        return this._client.video.rooms.list({status: 'completed', uniqueName: roomNm, limit: 20});
    }

    listCompletedRooms(roomNm: string): void { 
        this.getCompletedRooms(roomNm)
            .then(rooms => rooms.forEach(r => this.logInfo(r.sid)));
    }

        
    getParticipantsSubscriptionList(roomSid: string, participantSid: string) {

        return this._client.video
            .rooms(roomSid)
            .participants.get(participantSid)
            .subscribedTracks
            .list();
    }

    logDebug(...debugs:any[]) {
        this._logger.logDebug(...debugs);
    }

    logInfo(...infos:any[]) {
        this._logger.logInfo(...infos);
    }

    logWarn(...warns: any[]) {
        this._logger.logWarn(...warns);
    }

    logError(...errs:any[]) {
        this._logger.logError(...errs);
    }

    async validateTwilioRequest( req ): Promise<boolean> {
        
        let fullUrl = "";
        
        let reqHostname: string = nvl(req.hostname, "").toLowerCase();
        if (req.protocol === "http" && reqHostname.includes("trainwith")) {
            fullUrl = `https://${req.hostname}${req.originalUrl}`;   
        } else {
            fullUrl = this.getFullReqUrl(req);
        }

        if (!req.body) {
            this.logError("invalid twilio request: no body found");
            return false;
        }

        let twilioReqSignature = req.header('X-Twilio-Signature');

        let twilioReqBody = req.body;

        /** NOTE: these parameters must be in unix-style alphabetical order to validate correctly */
        const params = this.getJustProps(twilioReqBody); 
        
        let isValidReq = validateRequest(this._authToken, twilioReqSignature, fullUrl, params);

        if (!isValidReq) {
            this.logWarn(`twilio::validateRequest reported invalid request | url: ${fullUrl} | request signature:, params:`, twilioReqSignature, fullUrl, params);
        }

        return isValidReq;
    }


    getJustProps(oObj: any) {

        let oProps = {};

        let aUnsortedPropNms = Object.getOwnPropertyNames(oObj) || [];
        let aPropNms = aUnsortedPropNms.sort();

        for (let sPropNm of aPropNms) {
            let xPropVal = oObj[sPropNm];
            oProps[sPropNm] = xPropVal;
        }

        return oProps;
    }

    getFullReqUrl(req): string {
        /**
         *  prot    subd  hostname    original - url 
         * https://mossy.swamp.com/alabama?filter=very-humid
         * {protocol}://[{subdomains}.]{hostname}[/{originalUrl}]
         * {protocol}://[{subdomains}.]{hostname}[/{path}?filter=very-humid]
         */

        return `${req.protocol}://${req.hostname}${req.originalUrl}`;   
    }

}


/* Subscription functions */

// unsubscribeToTrackKind(trackKind: string, publisherSid: string) {
//     this.logInfo(`toggling video subscription for participant ${publisherSid}`);

//     let exclusions: string[][] = [];
    
//     let remoteParticipants = this.room.participants as Map<string, any>;

//     for(let [remPartSid, remPart] of remoteParticipants) {

//         let isDataSub = false, isAudioSub = false, isVideoSub = false;

//         for(let [, trackPub] of remPart.dataTracks) {
//             isDataSub = isDataSub || (trackPub.track && trackPub.isSubscribed);
//         }

//         for(let [, trackPub] of remPart.audioTracks) {
//             isAudioSub = isAudioSub || (trackPub.track && trackPub.isSubscribed);
//         }

//         for(let [, trackPub] of remPart.videoTracks) {
//             isVideoSub = isVideoSub || (trackPub.track && trackPub.isSubscribed);
//         }

//         if (!isDataSub || (publisherSid === remPartSid && trackKind === 'data')) {
//             exclusions.push([remPartSid, "data"]);
//         }

//         if (!isAudioSub|| (publisherSid === remPartSid && trackKind === 'audio')) {
//             exclusions.push([remPartSid, "audio"]);
//         }

//         if (!isVideoSub|| (publisherSid === remPartSid && trackKind === 'video')) {
//             exclusions.push([remPartSid, "video"]);
//         }
//     }

//     let unsubscribedInfo = {
//         type: "update", 
//         subscriberSid: this.localParticipant.sid, 
//         exclusions: exclusions,
//         roomSid: this.room.sid
//     };

//     let subscriptionUrl = TwConstants.subscriptionsApi(this.room.sid);

//     this.logInfo(`changing to ${trackKind} subsciption to unsubscribed through url ${subscriptionUrl}`, unsubscribedInfo);

//     this.POST(subscriptionUrl, unsubscribedInfo);
// }

// subscribeToTrackKind(trackKind: string, publisherSid: string) {
//     this.logInfo(`changing to subscribe to track kind ${trackKind} for participant ${publisherSid}`);

//     let exclusions: string[][] = [];
    
//     let remoteParticipants = this.room.participants as Map<string, any>;

//     for(let [remPartSid, remPart] of remoteParticipants) {

//         let isDataSub  = (remPartSid === publisherSid && trackKind === 'data'),
//             isAudioSub = (remPartSid === publisherSid && trackKind === 'audio'),
//             isVideoSub = (remPartSid === publisherSid && trackKind === 'video');

//         for(let [, trackPub] of remPart.dataTracks) {
//             isDataSub = isDataSub || (trackPub.track && trackPub.isSubscribed);
//         }

//         for(let [, trackPub] of remPart.audioTracks) {
//             isAudioSub = isAudioSub || (trackPub.track && trackPub.isSubscribed);
//         }

//         for(let [, trackPub] of remPart.videoTracks) {
//             isVideoSub = isVideoSub || (trackPub.track && trackPub.isSubscribed);
//         }

//         if (!isDataSub) {
//             exclusions.push([remPartSid, "data"]);
//         }

//         if (!isAudioSub) {
//             exclusions.push([remPartSid, "audio"]);
//         }

//         if (!isVideoSub) {
//             exclusions.push([remPartSid, "video"]);
//         }

//     }

//     let subscribeRule = {
//         type: "update", 
//         subscriberSid: this.localParticipant.sid, 
//         exclusions: exclusions,
//         roomSid: this.room.sid
//     };

//     let subscriptionUrl = TwConstants.subscriptionsApi(this.room.sid);

//     this.logInfo(`submitting unsubscribed through url ${subscriptionUrl}`, subscribeRule);

//     this.POST(subscriptionUrl, subscribeRule);

// }


// updateParticipantsSubscriptions(roomSid: string, subscriberSid: string, exclusions: any[]) {
//     this.getParticipantsSubscriptionList(roomSid, subscriberSid)
//     .then((subscribedTracks:any[]) => { // SubscribedTrackInstance[]
//         this.logInfo("Prior to changing subscriptions, count: "+subscribedTracks.length);
//         for (let subscribedTrack of subscribedTracks) {
//             this.logInfo('Read subscribed track with sid = ' + subscribedTrack.sid, subscribedTrack);
//         }
//     })
//     .then(async () => {
//         let rules = [];
//         rules.push(twilioVideoApi.getSubscribeAllTracksRule());

//         for (let exclusion of exclusions) {
//             rules.push(twilioVideoApi.getSubscribeRule("exclude", exclusion[1], exclusion[2]));
//         }
//         return await twilioVideoApi.updateParticipantSubscriptions(roomSid, subscriberSid, rules);
//     })
//     .then((result) => {
//         return getParticipantsSubscriptionList(roomSid, subscriberSid);
//     })
//     .then((subscribedTracks:any[]) => { // SubscribedTrackInstance[]
//         this.logInfo("After to changing subscriptions, count: "+subscribedTracks.length);
//         for (let subscribedTrack of subscribedTracks) {
//             this.logInfo('Read subscribed track with sid = ' + subscribedTrack.sid, subscribedTrack);
//         }
//     })
//     .catch(error => {
//         this.logError('Error fetching subscribed tracks' + error)
//     });
// }
