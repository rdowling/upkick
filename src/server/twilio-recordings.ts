98// NOTE: This example uses the next generation Twilio helper library - for more
// information on how to download and install this version, visit
// https://www.twilio.com/docs/libraries/node

"use strict";

require("dotenv").load();

const request = require('request');
const fs = require('fs');

export function getTwilioClient() {
    const API_KEY_SID = process.env.TWILIO_API_KEY;

    const API_KEY_SECRET = process.env.TWILIO_API_SECRET;
    const ACCOUNT_SID = process.env.TWILIO_ACCOUNT_SID;

    const Twilio = require("twilio");

    const client = new Twilio(API_KEY_SID, API_KEY_SECRET, {
        accountSid: ACCOUNT_SID
    });
    return client;
}
let client = getTwilioClient();


export function getLogs(client) {
    console.log('getLogs')
    var fromDate = new Date(2019, 8, 3);
    console.log(fromDate.toISOString()); // dateCreatedAfter: fromDate.toISOString(), 
    client.video.logs.list({ accountSid: process.env.TWILIO_ACCOUNT_SID, limit: 20 })
        .then(rooms => {
            console.log(rooms);
            rooms.forEach(room => {
                console.log(room);
            })
        })
        .catch(err => console.error("getLogs failed", err));
}

export function getRooms(client) {
    console.log('getRooms')
    var fromDate = new Date(2019, 8, 3);
    console.log(fromDate.toISOString()); // dateCreatedAfter: fromDate.toISOString(), 
    client.video.rooms.list({ accountSid: process.env.TWILIO_ACCOUNT_SID, limit: 20 })
        .then(rooms => {
            console.log(rooms);
            rooms.forEach(room => {
                console.log(room);
            })
        })
        .catch(err => console.error("getRooms failed", err));
}

export function listCompositions(client) {
    client.video.compositions.list({ limit: 20 })
        .then(comps => {
            comps.forEach(comp => {
                let status = comp.status;
                console.log(comp);
            });
        })
}

export function getCompositions(client) {
    client.video.compositions.list({ limit: 20 })
        .then(comps => {
            comps.forEach(comp => {
                let status = comp.status;
                if (status === 'completed') {
                    downloadComposition(client, comp.url, "c:\\dev\\upkick\\.recordings\\" + comp.sid + ".mp4");
                }
                console.log(comp);
            });
        })
}

export function getRecordedRooms(roomsListProcessor) {
    let client = getTwilioClient();

    console.log('getRecordedRooms')

    var fromDate = new Date(2019, 8, 3);

    client.video.recordings
        .list({ limit: 20 })
        .then(recs => {
            let rooms = [];
            recs.forEach(rec => {
                let type = rec.type;
                if (type === 'video') {
                    let roomSid = rec.groupingSids.room_sid;
                    if (!rooms.includes(roomSid)) {
                        rooms.push(roomSid);
                    }
                }
            });

            let roomsList = [];
            rooms.forEach(roomSid => {
                client.video.rooms(roomSid)
                    .fetch()
                    .then(room => {
                        let roomMap = { uniqueName: room.uniqueName, dateCreated: room.dateCreated, endTime: room.endTime, duration: room.duration, status: room.status, participants: [] }
                        //console.log(room.uniqueName, room.dateCreated, room.endTime, room.duration, room.status);
                        client.video.rooms(roomSid)
                            .participants
                            .list({ limit: 20 })
                            .then(parts => {
                                parts.forEach(part => {
                                    roomMap.participants.push(part.identity);
                                    // console.log(part.identity)
                                });
                            });

                        roomsList.push(roomMap);
                    })
                    .catch(err => console.error("getRooms failed", err));
            });

            roomsListProcessor(roomsList);

        })
        .catch(err => console.error("getRooms failed", err));
}

export function processRoomRecordings(roomSid, processBlk) {
    let client = getTwilioClient();

    client.video.rooms(roomSid)
        .list({})
}

export function getRoomRecordings(client, roomSid) {
    client.video.rooms(roomSid)
        .recordings
        .list({ limit: 20 })
        .then(recordings => {
            recordings.forEach(rec => {
                generateRoomComposition(client, roomSid, function (composition) {
                    console.log(composition);
                    var compUrl = composition.url;
                    var fileNm = roomSid + "_" + roomSid + "_" + "X" + ".mp4";
                    console.log("downloading", compUrl, fileNm);
                    if (!compUrl) {
                        console.error("problem...");
                    }
                    downloadComposition(client, compUrl, "c:\\dev\\upkick\\.recordings\\" + fileNm);
                    console.log("Downloaded Composition " + compUrl);
                });
            })
        })
        .catch(err => console.error(err));
}

function generateRoomComposition(client, roomSid, fnApply) {
    console.log("generating room comp", roomSid);
    try {
        client.video.compositions
            .create({
                roomSid: roomSid,
                audioSources: "*",
                videoLayout: {
                    single: {
                        video_sources: ["*"]
                    }
                },
                // statusCallback: "http://my.server.org/callbacks",
                format: "mp4"
            })
            .then(composition => {
                console.log("generateRoomComposition - about to apply function")
                fnApply(composition);
            })
            .catch(err => console.error("generating room composition", err));
    } catch (exc) {
        console.log(exc);
    }
}

function downloadComposition(client, compUrl, compFile) {
    // NOTE: This example uses the next generation Twilio helper library - for more
    // information on how to download and install this version, visit
    // https://www.twilio.com/docs/libraries/node

    const uri = compUrl + '/Media?Ttl=3600';
    //const uri = compUrl;

    client
        .request({
            method: 'GET',
            uri: uri
        })
        .then(response => {
            console.log(response);
            // Extract the media location of the file first
            const mediaLocation = JSON.parse(response.body).redirect_to;

            // Get the file and (ex:) download the media to a local file
            var file = fs.createWriteStream(compFile);
            var r = request(mediaLocation);
            r.on('response', (res) => {
                console.log("building file " + compFile);
                res.pipe(file);
                console.log("Composition downloaded to ", compFile);

            });

        })
        .catch(error => {
            console.log("Error downloading composition ", error);
        });
}

/**
 * 
 * @param client 
 * @param roomSid: "RMc1455e6010f3d96035eaf7a883f6c39e" 
 * @param roomNm: "ghf"
 * @param memberNm: "Kevin A Trainer"
 */

export function getRoomParticipantRecording(client, roomSid, memberNm) {
    client.video.rooms(roomSid)
        .participants
        .list({ limit: 20 })
        .then(parts => {
            parts.forEach(part => {

                if (part.identity === memberNm) {
                    let partSid = part.sid;
                    generateSingleComposition(client, roomSid, partSid, function (composition) {
                        console.log("Created Composition ", composition.url, composition);
                        var compUrl = composition.url;
                        var fileNm = memberNm + "_" + roomSid + "_" + ".mp4";
                        downloadComposition(client, compUrl, "c:\\dev\\upkick\\.recordings\\" + fileNm);
                    });

                }
            });
        })
        .catch(err => console.error(err));
}

function generateSingleComposition(client, roomSid, partSid, fnApply) {
    try {
        client.video.compositions.create({
            roomSid: roomSid,
            audioSources: partSid,
            videoLayout: {
                single: {
                    video_sources: [partSid]
                }
            },
            // statusCallback: "http://my.server.org/callbacks",
            format: "mp4"
        }).then(composition => {
            console.log("applying composition function")
            fnApply(composition);
        }).catch(err => {
            console.error(err);
        });
    } catch (error) {
        console.error(error);
    }
}

function getSingleComposition(client, roomSid, roomNm, memberNm) {
    // var compSid = composition.sid
    let partSid = "PA39b6d2f481e5a42f9d05ccb8258be1f7";
    generateSingleComposition(client, roomSid, partSid, function (composition) {
        var compUrl = composition.url;
        console.log(composition);
        var fileNm = memberNm + "_" + roomNm + "_" + ".mp4";
        downloadComposition(client, compUrl, "c:\\dev\\upkick\\.recordings\\" + fileNm);
        console.log("Created Composition with SID=" + composition.sid);
    });

}

// generateRoomComposition(client, roomSid, function (composition) {
//     console.log("generated comp for room " + roomSid);
//     var compUrl = composition.url;
//     var fileNm = roomSid + (Date.now().toString()) + ".mp4";
//     console.log("downloading", compUrl, fileNm);
//     if (!compUrl) {
//         console.error("problem...");
//     }
//     downloadComposition(client, compUrl, "c:\\dev\\upkick\\.recordings\\" + fileNm);
//     console.log("Downloaded Composition " + compUrl);
// });
