import { TwilioVideoApi } from "./twilio-video-api";

require("dotenv").load();

let twilioVideoApi = new TwilioVideoApi(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN, process.env.TWILIO_API_KEY, process.env.TWILIO_API_SECRET);

let accessToken = twilioVideoApi.generateIdentityToken("test", 100);
// grants == 0
twilioVideoApi.addRoomVideoGrant(accessToken, "test-room");
let tokenJwt = accessToken.toJwt();
console.log(accessToken, tokenJwt);
