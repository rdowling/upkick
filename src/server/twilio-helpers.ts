

import { LocalVideoTrack } from "twilio-video";

var Video = require('twilio-video');

/**
 * Display local video in the given HTMLVideoElement.
 * @param {HTMLVideoElement} video
 * @returns {Promise<void>}
 */
export function displayLocalVideo(video: HTMLVideoElement) {
  return Video.createLocalVideoTrack().then(function(localTrack: LocalVideoTrack) {
    localTrack.attach(video);
  });
}


export function getTwilioStudioStats(studio: { getStats: () => Promise<any>; }) {
    studio.getStats().then((statsReports) => {
        statsReports.forEach((statsReport: { remoteVideoTrackStats: any[]; }) => {
            statsReport.remoteVideoTrackStats.forEach((videoTrackStats) => {
                console.log("************", videoTrackStats);
                // console.log(videoTrackStats.frameRate);
            })
        })
    });
}
