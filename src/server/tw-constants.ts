
export class TwConstants {

    static API_URL = '/api/v2';

    static TOKENS_API_URL           = `${TwConstants.API_URL}/tokens`
    static ROOMS_API_URL            = `${TwConstants.API_URL}/rooms`;
    static PARTICIPANTS_API_URL     = `${TwConstants.API_URL}/participants`;
    static RECORDINGS_API_URL       = `${TwConstants.API_URL}/recordings`;
    static SUBSCRIPTIONS_API_URL    = `${TwConstants.API_URL}/recordings`;
    
    static CALLBACK_API_URL         = `${TwConstants.API_URL}/callbacks`;

    static ADMIN_API_URL            = `${TwConstants.API_URL}/admin`;

    // default grid settings
    static _defaultIsGridVisible: boolean   = false;
    static _defaultGridSpacingWidth: number = 100;
    static _defaultGridTransparency: number = 0.5;

    static subscriptionsApi(roomSid:string = ":roomsid") {
        return `${TwConstants.ROOMS_API_URL}/${roomSid}/subscriptions`;
    }

}