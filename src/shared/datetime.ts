

// import {DateTime as LuxonDateTime} from 'luxon';

//var LocalDate = require("@js-joda/core").LocalDate;
import * as joda from '@js-joda/core';
import { Locale } from '@js-joda/locale_en';

let ldt = joda.LocalDate.now();

export class DateTime {
 
    private _ldt: joda.LocalDateTime;   

    static now() {
        return new DateTime();
    }

    constructor(dt: joda.LocalDateTime = joda.LocalDateTime.now()) {
        this._ldt = dt;
    }

    getDateFormatted(sDtFormat: string): string {        
        let oFormatter = joda.DateTimeFormatter.ofPattern(sDtFormat);
        return this._ldt.format(oFormatter);
    }

    getHour(): number {
        return this._ldt.hour();
    }

    getMinute(): number {
        return this._ldt.minute();
    }

    getAmOrPm(): string {
        let oAmPmFormatter = joda.DateTimeFormatter.ofPattern('a').withLocale(Locale.ENGLISH);
        return this._ldt.format(oAmPmFormatter);
    }
    
    // isAfter(other: DateTime): boolean;
    // isBefore(other: DateTime): boolean;
    // isEqual(other: DateTime): boolean;
    // isSupported(fieldOrUnit: TemporalField | TemporalUnit): boolean;
    // minus(amount: TemporalAmount): DateTime;
    // minus(amountToSubtract: number, unit: TemporalUnit): DateTime;
    // minusDays(days: number): DateTime;
    // minusHours(hours: number): DateTime;
    
    minusMinutes(minutes: number): DateTime {
        return new DateTime(this._ldt.minusMinutes(minutes));
    }

    // minusMonths(months: number): DateTime;
    // minusNanos(nanos: number): DateTime;
    // minusSeconds(seconds: number): DateTime;
    // minusTemporalAmount(amount: TemporalAmount): DateTime;
    // minusWeeks(weeks: number): DateTime;
    // minusYears(years: number): DateTime;
    // minute(): number;
    // month(): Month;
    // monthValue(): number;
    // nano(): number;
    // plus(amount: TemporalAmount): DateTime;
    // plus(amountToAdd: number, unit: TemporalUnit): DateTime;
    // plusDays(days: number): DateTime;
    // plusHours(hours: number): DateTime;
    // plusMinutes(minutes: number): DateTime;
    // plusMonths(months: number): DateTime;
    // plusNanos(nanos: number): DateTime;
    // plusSeconds(seconds: number): DateTime;
    // plusTemporalAmount(amount: TemporalAmount): DateTime;
    // plusWeeks(weeks: number): DateTime;
    // plusYears(years: number): DateTime;
    // query<R>(query: TemporalQuery<R>): R;
    // range(field: TemporalField): ValueRange;
    // second(): number;
    // toJSON(): string;
    // toLocalDate(): LocalDate;
    // toLocalTime(): LocalTime;
    // toString(): string;
    // truncatedTo(unit: TemporalUnit): DateTime;
    // until(endExclusive: Temporal, unit: TemporalUnit): number;
    // with(adjuster: TemporalAdjuster): DateTime;
    // with(field: TemporalField, newValue: number): DateTime;
    // withDayOfMonth(dayOfMonth: number): DateTime;
    // withDayOfYear(dayOfYear: number): DateTime;
    // withHour(hour: number): DateTime;
    // withMinute(minute: number): DateTime;
    // withMonth(month: number | Month): DateTime;
    // withNano(nanoOfSecond: number): DateTime;
    // withSecond(second: number): DateTime;
    // withYear(year: number): DateTime;
    // year(): number;

}


let dtFrom = DateTime.now();

let sFromDt = dtFrom.getDateFormatted("MM/dd/yyyy");
let sFromHrs = dtFrom.getHour();
let sFromMins = dtFrom.getMinute();
let sFromAmOrPm = dtFrom.getAmOrPm();

let sToDt = dtFrom.getDateFormatted("MM/dd/yyyy");
let sToHrs = dtFrom.getHour();
let sToMins = dtFrom.getMinute();
let sToAmOrPm = dtFrom.getAmOrPm();


console.log(dtFrom.getDateFormatted("MM/dd/yyyy"));
console.log(dtFrom.getHour());
console.log(dtFrom.getMinute());
console.log(dtFrom.getAmOrPm());

// let pmDt = joda.LocalDateTime.of(2020, 3, 1, 23, 55, 1);

// let sFromPmHrs = pmDt.hour();
// let sFromMins = 

// console.log(pmDt.format(joda.DateTimeFormatter.ofPattern("hh")))

// console.log(sFromPmHrs);

/**
 *  // From-to date, specific times, as an object:
    {
        "date":"02/08/2013",
        "hours":5,
        "minutes":0,
        "am_pm":"pm",
        "to":{
            "date":"02/11/2013",
            "hours":3,
            "minutes":0,
            "am_pm":"pm"
        }
    }
    */




