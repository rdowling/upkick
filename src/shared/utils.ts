
//todo: put a lot of the utils that are shared, here.  
// At build time include into client & server build folders (and dist) 


export interface StudioInfo {
  identity  : string;
  studioNm  : string;
  companyNm : string;
  clientId  : string;
  clientNm  : string;
  trainerId : string; 
  isTrainer : boolean;
  isRecorded : boolean;
  identityToken: string;

}


function toProperCase(str: string): string {
    let aWords = str.toLowerCase().split(" ");
    let nWords = aWords.length;
    for (let iWord = 0; iWord < nWords; iWord++) {
        let sWord = aWords[iWord];
        if (sWord && sWord.length > 1) {
            let sFirstLetter = sWord.substr(0, 1) || "";
            let sRemaining = sWord.slice(1);
            sWord = sFirstLetter.toUpperCase() + sRemaining;
        }
        aWords[iWord] = sWord;
    }
    return aWords.join(" ");
}
